# v2023.04 (2023-04-13)

## Changes

* serial: remove unused header and add missing ones
* emac test: Fix MTU overhead issue.
* hal: Remove device.h include directives, and replace with more specific header files where necessary.
* license: Fix greentea-client category, it should be Cat2 instead of Cat1
* mbed_wait: Refactor of delay function

  Refactor of the delay function that was previously written using a macro and type casting. This is now implemented using a noinline naked function.
* cmake: Make mcu-driver-hal to be an independent, API-only CMake target.
* cmake: Rename main target to mcu-driver-hal-api (add a `-api` suffix) to help communicate the purpose of this INTERFACE-type lib.
* i2c: Coding style improvements to I2C system test

  Documentation and style improvements added to improve system
  test interpretability and quality.
* ci: Enable possible tpip violation warning messages in Merge Requests.
* cmake: Add cmsis-core-generic library providing just the most basic platform-agnostic CMSIS-Core features.
* cmake: Rename HAL Toolbox lib to `mcu-driver-hal-toolbox`, and remove external, target-specific dependencies.
* bootstrap: Extract mbed-atomic from bootstrap and make it a standalone lib.
* bootstrap: Extract mbed-trace from bootstrap and make it a standalone lib.
* bootstrap: Extract mbed-critical from bootstrap lib into a standalone target.
* fpga-ci-test-shield: Extract platform-specific calls to remove dependency on the bootstrap lib.
* bootstrap: Remove deprecated mbed_wait_api, and the mcu-driver-bootstrap CMake target.
* cmake: Remove CMake utilities hosted in tools/cmake directory.


# v2023.01 (2023-01-19)

## Changes

* changelog: Add towncrier news fragments and configuration
* cppcheck: Remove unused files and fixed errors found with v2.9
* ci: Add pull pipeline from the public Gitlab repository
* i2c: Fix syntax error
* mbed-tester: Fix datatype mismatch of the loop iterators
* cmake: Exclude all targets from all
* clang-format: Enable sorting includes
* license: Add components information

  LICENSE.md does not only list name of components and their
  licenses but also more detailed information like version,
  url-origin.
* mbed-tester: Fix a BusFault introduced by the most recent updates.
* i2c: Fix parameter name mismatch
* Remove use of custom Mbed target configuration variables. Replace the
  MBED_TARGET_DEFINITIONS and MBED_CONFIG_DEFINITIONS custom config variables
  with CMake target compile definitions. It's no longer required to specify
  MBED_TARGET_DEFINITIONS or MBED_CONFIG_DEFINITIONS.
* spi: Fix build errors that occur when building asynchronous SPI FPGA test sheild tests
* emac: Update Doxygen to be aligned with current state of the API, where callbacks are set via mdh_emac_power_up method.
* tests: Fix undefined ticker type.
* tests: Fix the EMAC test; change the global RX state back to `RX_STATE_IDLE` only in the main
  test flow, not in the RX callback to allow processing already received data to continue.
* i2c: Add bootstrap library required by example

  I2C example included mbed_trace.h but the
  bootstrap library containing it was not part of the build.


This changelog should be read in conjunction with release notes provided
for a specific release version.
