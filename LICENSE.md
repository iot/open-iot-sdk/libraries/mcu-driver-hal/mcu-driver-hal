Unless specifically indicated otherwise in a file, files are licensed under the Apache 2.0 license,
as can be found in: LICENSE-apache-2.0.txt

## Components

Folders containing external components are listed below. Each component should contain its own README file with license specified for its files. The original license text is included in those source files.

```json:table
{
    "fields":[
        "Component",
        "Path",
        "License",
        "Origin",
        "Category",
        "Version",
        "Security risk"
    ],
    "items" :[
        {
            "Component": "Unity",
            "Path": "tools/unity",
            "License": "MIT",
            "Origin": "https://github.com/ARMmbed/unity",
            "Category": "1",
            "Version": "v3.1.1",
            "Security risk": "low"
        },
        {
            "Component": "greentea-client",
            "Path": "tools/greentea-custom_io",
            "License": "Apache 2.0",
            "Origin": "https://github.com/ARMmbed/greentea-client",
            "Category": "2",
            "Version": "08fdb8b68f99c523ee298dea6c93eedb60aef9a8",
            "Security risk": "low"
        },
        {
            "Component": "utest",
            "Path": "tools/utest",
            "License": "Apache 2.0",
            "Origin": "https://github.com/ARMmbed/utest",
            "Category": "1",
            "Version": "f70058b16540482b5fe60217ada16c0399a8a266",
            "Security risk": "low"
        }
    ]
}
```
