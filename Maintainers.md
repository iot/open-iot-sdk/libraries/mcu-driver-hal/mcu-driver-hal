All contributions are ultimately merged by the maintainers listed below.
Technical ownership of most parts of the codebase falls on the code owners
listed below. An acknowledgement from these code owners is required before
the maintainers merge a contribution.

Maintainers
===========

Martin Kojtal
    :email: `Martin Kojtal <Martin.Kojtal@arm.com>`

Marcelo Salazar
    :email: `Marcelo Salazar <Marcelo.Salazar@arm.com>`

Evelyne Donnaes
    :email: `Evelyne Donnaes <evelyne.donnaes@arm.com>`

Code owners
===========

Marcelo Salazar
    :email: `Marcelo Salazar <Marcelo.Salazar@arm.com>`

Wilfried Chauveau
    :email: `Wilfried Chauveau <Wilfried.Chauveau@arm.com>`
