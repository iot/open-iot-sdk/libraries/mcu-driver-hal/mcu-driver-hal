# MCU-Driver-HAL

This repository contains the MCU-Driver HAL API, tests, documentation and example applications to demonstrate the usage of the HAL features.

This repository is referenced from Silicon Partners Drivers.

## Getting started

Check out the [getting started guide](./docs/user/README.md) or [the docs](./docs/README.md).

## License and contributions

The software is provided under the [Apache-2.0 license](LICENSE-apache-2.0.txt). All contributions to software and documents are licensed by contributors under the same license model as the software/document itself (ie. inbound == outbound licensing). MCU-Driver-HAL may reuse software already licensed under another license, provided the license is permissive in nature and compatible with Apache v2.0.

Folders containing files under different permissive license than Apache 2.0 are listed in the [LICENSE](LICENSE.md) file.

Specifications are licensed under the Creative Commons [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license (Creative Commons Attribution-ShareAlike 4.0 International Public License).

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## Security issues reporting

If you find any security vulnerabilities, please do not report it in the GitLab issue tracker. Instead, send an email to the security team at arm-security@arm.com stating that you may have found a security vulnerability in the MCU Driver HAL.

More details can be found at [Arm Developer website](https://developer.arm.com/support/arm-security-updates/report-security-vulnerabilities).
