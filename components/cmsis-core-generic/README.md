# CMSIS-Core Generic API

This INTERFACE library provides only the generic part of CMSIS-Core API, and
exposes it via `cmsis_core_generic/generic_api.h` header file, delivering only
the most basic platform-agnostic CMSIS-Core features.

## Details

CMSIS-Core feature                                  | `generic_api.h`
----------------------------------------------------|----------------
Defines for variable, type, and function attributes | Y
Defines for startup and low level init              | Y
Functions for Core Register Access                  | Y
Intrinsic functions for CPU Instruction Interface   | Y
Intrinsic functions for SIMD Instruction Interface  | N
Debug Access                                        | N

Dependencies               | `generic_api.h`
---------------------------|----------------
Required macro definitions | None
