/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef CMSIS_CORE_GENERIC_API_H
#define CMSIS_CORE_GENERIC_API_H

// This header provides the most generic (device-independent) CMSIS-Core API:
// - CMSIS defines for variable, type, and function attributes,
// - CMSIS defines for startup and low level init,
// - CMSIS intrinsic functions for Core Instruction Interface,
// - CMSIS functions for Core Register Access.

// NOTE: All definitions common to Cortex-M cores are available; no need to
// provide any device-specific macros.

// NOTE: Internally this is just a generic subset of cmsis_compiler.h.

#ifdef __cplusplus
extern "C" {
#endif

#include "cmsis_compiler.h"

#ifdef __cplusplus
} // extern "C"
#endif

#endif // CMSIS_CORE_GENERIC_API_H
