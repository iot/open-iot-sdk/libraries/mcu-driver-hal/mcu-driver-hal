# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

add_library(mbed-trace src/mbed_trace.c)
target_include_directories(mbed-trace PUBLIC inc)
