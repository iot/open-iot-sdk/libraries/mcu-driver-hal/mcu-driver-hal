## Index

## Getting started.

See the [Getting started guide](./user/README.md).

## HAL API documentation

See the [HAL API Doxygen documentation](<HostingLocation>) (not currently available)

## Porting guide

See the [HAL porting guide](./porting/README.md) for help with supporting new targets.
