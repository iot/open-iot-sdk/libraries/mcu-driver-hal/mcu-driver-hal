<h1 id="quadspi-port">QuadSPI (QSPI) </h1>

Implementing QSPI enables MCU-Driver-HAL to communicate with compliant external SPI devices much faster than with standalone SPI due to the inclusion of up to four data lines between the host and a device.

The most common use case is for external memory to use as additional data storage.

## Assumptions

### Defined behavior

- An MCU implementation covers most of the QSPI frame format (some MCUs might not provide the flexibility for setting all frame parameters).
- Command transfer - An MCU might provide additional functions for sending device specific commands. If it does not, you can implement it using read and write functions. This is MCU or driver dependent.


## Implementing QSPI

You can implement your own QSPI by pulling in the following API header file:

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal__qspi.html)



Use `mdh_qspi_write` and `mdh_qspi_read` for data transfers.
To communicate with a device, use `mdh_qspi_transfer`.


## Testing

MCU-Driver-HAL provides a set of conformance tests for the QSPI interface.

You can use these tests to validate the correctness of your implementation.

Steps to run the QSPI HAL tests will be provided in the future.
