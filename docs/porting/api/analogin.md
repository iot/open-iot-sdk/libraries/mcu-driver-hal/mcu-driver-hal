# Analog Input

## Assumptions

### Defined behavior

- `mdh_analogin_read` reads the input voltage, represented as a float in the range [0.0 (GND), 1.0 (VCC)].
- `mdh_analogin_read_u16` reads the value from analogin pin, represented as an unsigned 16bit value [0.0 (GND), MAX_UINT16 (VCC)].
- The accuracy of the ADC is +/- 10%.
- The ADC operations `mdh_analogin_read`, `mdh_analogin_read_u16` take less than 20us to complete.

## Implementing the Analog Input API

You can find the API and specification for the Analogin API in the following header file:

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal__analogin.html)

## Testing

MCU-Driver-HAL provides a set of conformance tests for the Analog Input:

- `mcu-driver-hal/tests/mbed_hal_fpga_ci_test_shield/analogin` -- verify the defined behavior of the Analogin driver, see [paragraph above](#defined-behavior).

You can use these tests to validate the correctness of your implementation.

Visit the driver implementation for a step-by-step guide on building and running the Analog Input tests.
