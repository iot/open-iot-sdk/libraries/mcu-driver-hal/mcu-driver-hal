<h1 id="dma-port">Direct memory access (DMA) </h1>

## Defined behaviors

- `mdh_dma_memmove`:
   - Copies given amount of bytes from one memory address to an other address, using the DMA. The memory regions can overlap.
   - The function returns after the DMA is setup. If a callback function was passed as an argument then it's going to be called
     after the operation has finished.
   - Returns an error code, which shows if an error has happened during DMA setup.
- `mdh_dma_get_state`:
   - Gets the state of the DMA.
   - The state describe if the DMA is busy, ready for next command, or an error has happened.

## Undefined behaviors
- Passing an invalid `cbk` to `mdh_dma_memmove`.
- Calling `mdh_dma_memmove` when the state is not Ready.

## Dependencies

Hardware DMA capabilities.

## Implementing the DMA API

You can find the API and specification for the DMA API in the header file:

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal__dma.html)


## Testing

MCU-Driver-HAL provides a set of conformance tests for DMA. You can use these tests to validate the correctness of your implementation. The tests can be built with CMake and ran with CTest.
