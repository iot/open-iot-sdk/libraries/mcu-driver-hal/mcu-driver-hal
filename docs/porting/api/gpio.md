<h1 id="gpio-port">General Purpose Input Output (GPIO) </h1>

GPIO ports are digital input-output pins widely used in microcontrollers. They are placed on an electronic circuit in order to communicate with electronic components and external circuits. These components and external circuits include buttons, LEDs or sensors to capture data.

MCU-Driver-HAL provides a synchronous and asynchronous API to configure, read and write port pins as well as entire GPIO ports.

## General functionalities
The following set of functions provides the basic functionalities needed to interact with the GPIO pins of a microcontroller in synchronous mode. It is available without the need to specify any compiler definition.

- `mdh_gpio_is_connected`: Checks if a `mdh_gpio_t` instance is connected to an existing port pin on a given platform.
- `mdh_gpio_set_direction`: Sets whether the pin is an input or output.
- `mdh_gpio_set_mode`: Sets the behaviour of the pins when configured as inputs.
- `mdh_gpio_write`: Outputs a digital value to a pin configured as an output.
- `mdh_gpio_read`: Reads the digital value of a pin configured as an input.
- `mdh_gpio_get_capabilities`: Retrieves the behaviour supported by the pins of a given platform

## Asynchronous functionalities
These functions are to be used if an application uses an asynchronous model. These are enabled by adding a compiler definition for `DEVICE_INTERRUPTIN`.

- `mdh_gpio_set_irq_callback`: Registers a function to be executed from a GPIO Interrupt Service Routine (ISR).
- `mdh_gpio_enable_irq`: Enables the GPIO IRQ.
- `mdh_gpio_disable_irq`: Disables the GPIO IRQ.
- `mdh_gpio_set_irq_availability`: Enables or Disables a given event to be reported by a GPIO IRQ.

## Port functionalities
These functions provide simultaneous manipulation of multiple port pins. These are enabled by adding a compiler definition for `DEVICE_PORTIN` or `DEVICE_PORTOUT`.

- `mdh_gpio_set_port_direction`: Sets whether the group of pins is an input or output.
- `mdh_gpio_set_port_mode`: Sets the behaviour of a group of pins when configured as input pins
- `mdh_gpio_port_write`: Outputs a value up to 32-bit long where each pin of the group of pins represents a bit.
- `mdh_gpio_port_read`: Reads a value up to 32-bit long where each pin of the group of pins represents a bit.

## Implementing the GPIO API
You can find the API and specification for the GPIO API at:

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal___gpio.html)

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal___gpioirq.html)

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal___port.html)

## Testing
MCU-Driver-HAL provides a set of conformance tests for GPIO. You can use these tests to validate the correctness of your implementation. The tests can be built with CMake and ran with CTest.
