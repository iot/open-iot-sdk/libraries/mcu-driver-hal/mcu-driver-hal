<h1 id="i2c-port">Inter-integrated circuit (I2C) </h1>

---
**Note**

The _Master/Slave_ terminology for I2C is obsolete. _Master_ is now _Controller_ and _Slave_ is now _Peripheral_, this is reflected in the API.

---

I2C is a serial protocol for two-wire interface to connect low-speed devices in embedded systems. The I2C API allows control and configuration of this interface.

The interface is made up of two lines for all communication:
- Serial Clock (SCL).
- Serial Data (SDA).

## Defined behaviors

- `mdh_i2c_set_frequency`:
   - Sets the frequency to use for the transfer.
   - Must leave all other configuration unchanged.
- `mdh_i2c_write`:
   - Writes `length` number of symbols to the bus.
   - Returns the number of symbols sent to the bus.
   - Returns an negative integer if transfer fails.
   - Generates a stop condition on the bus at the end of the transfer if `add_stop` parameter is `true`.
   - Handles transfer collisions and loss of arbitration if the platform supports multi-controller in hardware.
   - The transfer times out and returns a negative integer if the transfer takes longer than the configured timeout duration.
- `mdh_i2c_read`:
   - Reads `length` symbols from the bus.
   - Returns the number of symbols received from the bus.
   - Returns a negative integer if transfer fails.
   - Generates a stop condition on the bus at the end of the transfer if the `add_stop` parameter is `true`.
   - Handles transfer collisions and loss of arbitration if the platform supports multi-controller in hardware.
   - The transfer times out and returns a negative integer value if the transfer takes longer than the configured timeout duration.
- `mdh_i2c_send_start`:
   - Generates I2C START condition on the bus in Controller mode.
   - Does nothing if called when configured in Peripheral mode.
- `mdh_i2c_send_stop`:
   - Generates I2C STOP condition on the bus in Controller mode.
   - Does nothing if configured in Peripheral mode.
- `mdh_i2c_set_peripheral_address`:
   - Sets the address of the peripheral to the `address` parameter.
   - Does nothing if called in Controller mode.
- `mdh_i2c_asynch_transfer`:
   - The callback given to `mdh_i2c_asynch_transfer` is invoked when the transfer finishes or error occurs.
   - Must save the handler and context pointers inside the `self` pointer.
   - The context pointer is passed to the callback on transfer completion.
   - The callback must be invoked on completion unless the transfer is aborted.
   - May handle transfer collisions and loss of arbitration if the platform supports multi-controller in hardware and enabled in API.
- `mdh_i2c_asynch_abort`:
   - Aborts any ongoing async transfers.

## Undefined behaviors

- Use of a null pointer as an argument to any function.
- Initializing the I2C peripheral with invalid `SDA` and `SCL` pins.
- Initializing in Peripheral mode if Peripheral mode is not supported.
- Operating in Peripheral mode without first specifying and address using `mdh_i2c_set_peripheral_address`.
- Setting an address using `mdh_i2c_set_peripheral_address` after initializing in Controller mode.
- Setting an address to an I2C reserved value.
- Setting an address larger than the 7-bit supported maximum if 10-bit addressing is not supported.
- Setting an address larger than the 10-bit supported maximum.
- Setting a frequency outside the supported range.
- Specifying an invalid address when calling read or write functions.
- Setting the length of the transfer or receive buffers to larger than the buffers are.
- Passing an invalid `cbk` to `mdh_i2c_asynch_transfer`.
- Calling `mdh_i2c_asynch_abort` when no transfer is currently in progress.

## Dependencies

Hardware I2C capabilities.

## Implementing the I2C API

You can find the API and specification for the I2C API in the header file:

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal___general_i2_c.html)

To enable the asynchronous API, add `DEVICE_I2C_ASYNCH=1` to your device CMake
target's `target_compile_definitions()`. To enable the I2C Peripheral API, add
`DEVICE_I2C_PERIPHERAL=1` to your device CMake target's
`target_compile_definitions()`.

## Testing

MCU-Driver-HAL provides a set of conformance tests for I2C. You can use these tests to validate the correctness of your implementation.

Steps to run the I2C HAL tests will be provided in the future.
