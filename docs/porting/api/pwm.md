# Pulse-width modulation digital output (PWM output)

## Assumptions

### Defined behavior

- `mdh_pwmout_write` sets the output duty-cycle in range <0.0f, 1.0f>.
- `mdh_pwmout_read` returns the current float-point output duty-cycle in range <0.0f, 1.0f>.
- `mdh_pwmout_set_period` sets the PWM period specified in seconds, keeping the duty cycle the same.
- `mdh_pwmout_set_period_ms` sets the PWM period specified in milliseconds, keeping the duty cycle the same.
- `mdh_pwmout_set_period_us` sets the PWM period specified in microseconds, keeping the duty cycle the same.
- `mdh_pwmout_get_period_us` reads the PWM period specified in microseconds.
- `mdh_pwmout_set_pulsewidth` sets the PWM pulsewidth specified in seconds, keeping the period the same.
- `mdh_pwmout_set_pulsewidth_ms` sets the PWM pulsewidth specified in milliseconds, keeping the period the same.
- `mdh_pwmout_set_pulsewidth_us` sets the PWM pulsewidth specified in microseconds, keeping the period the same.
- `mdh_pwmout_get_pulsewidth_us` read the PWM pulsewidth specified in microseconds.
- The accuracy of the PWM is +/- 10%.
- The PWM operations `mdh_pwmout_write`, `mdh_pwmout_read`, `mdh_pwmout_read`, `mdh_pwmout_set_period_ms`, `mdh_pwmout_set_period_us`, `mdh_pwmout_set_pulsewidth`, `mdh_pwmout_set_pulsewidth_ms`, `mdh_pwmout_set_pulsewidth_us` take less than 20us to complete.

## Implementing the PWM digital output API

You can find the API and specification for the PWM output API in the following header file:

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal__pwmout.html)

## Testing

MCU-Driver-HAL provides a set of conformance tests for the pulse-width modulation digital output:

- `mcu-driver-hal/tests/mbed_hal_fpga_ci_test_shield/pwm` -- verify the defined behavior of the PWM output driver, see [paragraph above](#defined-behavior).

You can use these tests to validate the correctness of your implementation.

Visit the driver implementation for a step-by-step guide on building and running the PWM output tests.
