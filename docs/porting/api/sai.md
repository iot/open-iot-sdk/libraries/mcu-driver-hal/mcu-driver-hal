<h1 id="sai">Serial Audio Interface (SAI)</h1>

## Implementing the SAI API

You can find the API and specification for the SAI API at:

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal___general_sai.html)


## Testing

A loopback test is provided here as best-effort. Its integration may require extensive support code
and/or specific hardware configuration.
