<h1 id="serial-port">Serial</h1>

## Implementing the Serial API

You can find the API and specification for the Serial API in the following header file:

[![View code](../../images/view_library_button.png)](<HostingLocation>/mcu-driver-hal/doxygen/html/group__hal___general_serial.html)


## Testing

MCU_Driver-HAL provides a set of conformance tests for the serial API. You can use these tests to validate the correctness of your implementation.

Steps to run the Serial HAL tests will be provided in the future.

## Known issues

* The size, alignment and endianness of the data words within the `buffer` parameter of `mdh_serial_asynch_receive()` and `mdh_serial_asynch_send()` is not specified by the API. Users of the API need to consult specific implementations of the `.asynch_receive` and `.asynch_send` functions to find out more details.
* When used in asynchronous mode, some microcontrollers include an address matching function that includes character matching functions that compares match registers to incoming data words for special framing characters.`mdh_serial_asynch_receive()` only support character matching for 8 bits data words.
