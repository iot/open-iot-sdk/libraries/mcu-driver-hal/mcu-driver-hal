# Migrating Mbed OS HAL to MCU-Driver-HAL

## Introduction
The MCU-Driver-HAL API is an evolution of Mbed OS HAL API.

Mbed OS HAL APIs required the implementation to do the following:
* implement functions declared in HAL header files
* create a header file that provide definitions for peripheral data types (e.g., `device.h`, `objects.h`, etc)
* create a header file that provide port pin related data types and `enum`s (e.g., `PinNames.h`)

Therefore a dependency cycle existed to allow customisation by silicon partners (SiPs). MCU-Driver-HAL does not have dependency cycles and uses a different programming model.

Please ensure you read the [programming model](../programming_model.md) before proceeding with the migration.

## Migration steps
Here are the steps to convert Mbed OS HAL implementations in SiPs repositories to use the new programming model used in MCU-Driver-HAL:

1. Remove inclusion of `pinmap.h` module in all source/header files as it no longer exists.
1. Convert HAL implementation function definitions to static function definitions if they define operations specified in the API base class. Function definitions that are not part of the base class operations should remain as public functions and are up to the SiP to modify as desired.
1. Create a static instance of the subclass virtual table with each member pointing to one of the static functions mentioned above.
1. Create a C header file for the subclass to expose operations for the API that are specific to the SiP (such as peripheral initialisation/deinitialisation).
