# MCU-Driver-HAL programming model
MCU-Driver-HAL uses an object-oriented programming (OOP) approach where customisation is achieved via inheritance and polymorphism is used to define a contract all implementations must commit to.

For the remainder of this guide, MCU-Driver-HAL APIs will be refered to as base classes and the implementation in Silicon partners (SiPs) repositories will be referred as subclasses.
Each base class define operations with Virtual Functions which receive as an argument a base class object that is a member of a subclass object.

Each Virtual Function determines which implementation to call at run-time (late binding/virtual call).

MCU-Driver-HAL APIs do not define virtual functions for initialising periperals or for operations that require port pin manipulation. These have to be done in SiP's repositories however seen fit.

## Example for MCU-Driver-HAL Serial API

**Listing: Declaration of the Serial API base class interface in `serial_api.h`**
```c
// Fordward declaration for the Serial API base class data type
typedef struct mdh_serial_s mdh_serial_t;

// Serial's Virtual Table declaration containing all possible operations
typedef struct mdh_serial_vtable_s {
    void (*put_data)(mdh_serial_t *self, uint32_t data);
    uint32_t (*get_data)(mdh_serial_t *self)
    // ...
} mdh_serial_vtable_t;

// The Serial API base class definition
struct mdh_serial_s {
    mdh_serial_vtable_t const *vfptr;
};

// Serial's `put_data` operation Virtual Call
static inline void mdh_serial_put_data(mdh_serial_t *self, uint32_t data) {
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->put_data));
    self->vfptr->put_data(self, c);
}

// Serial's `get_data` Virtual Call
static inline uint32_t mdh_serial_get_data(mdh_serial_t *self) {
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_data));
    return self->vfptr->get_data(self);
}
```
This is the only file contained in MCU-Driver-HAL for the Serial API. The other files below are only examples and can be implemented however SiPs would like provided the contract defined by the Serial API is respected.

**Listing: SiP specific header file (`sip_serial.h`) to expose SiP specific serial operations.**
```c
// Import the Serial API base class interface
#include "hal/serial_api.h"

// SiP's Serial API subclass declaration
typedef struct sip_serial_s {
    mdh_serial_t serial; // Inherit the Serial API base class

    // custom attributes can be added here
    sip_uart_t* device;
} sip_serial_t;

// SiP specific Serial initialisation; not determined by Serial API base class
void sip_serial_init(
    sip_serial_t** serial, uint8_t uart_number, uint16_t tx_pin, uint16_t rx_pin
);
```

**Listing: Definition of the Serial API in a SiP's repository `sip_serial.c`.**
```c
#include "sip_serial.h"
// Import SiP UART device driver containing UART instances, definitions and functions
#include "sip_uart.h"

#ifndef get_struct_instance
#define get_struct_instance(member_instance, type, member)             \
    ({                                                                 \
        const typeof(((type *)0)->member) *__mptr = (member_instance); \
        (type *)((char *)__mptr - offsetof(type, member));             \
    })
#endif // get_struct_instance

// Private SiP's implementation specific `put_data` declaration
// Must only be accessed via MCU-Driver-HAL Serial API
static void put_data(mdh_serial_t *self, uint32_t data);

// Virtual Table of the SiP's Serial API subclass
// Must only be accessed via MCU-Driver-HAL Serial API
static mdh_serial_vtable_t const vtbl = {
    .put_data = put_data
};

// Instances of SiP's Serial API subclass
static sip_serial_t sip_serial_instances[SIP_PLATFORM_MAX_UART_PORT] = {
    [0] = {.serial.vfptr = &vtbl, .device = &sip_uart_0_device},
    [1] = {.serial.vfptr = &vtbl, .device = &sip_uart_1_device}
};

static void put_data(mdh_serial_t *self, uint32_t data) {
    // Subclass instance is recovered using base class instance
    sip_serial_t* sip_serial_instance = get_struct_instance(
        self, sip_serial_t, sip_serial_t.serial
    );

    sip_uart_write(sip_serial_instance->device, (uint8_t)c);
}

void sip_serial_init(
    sip_serial_t** serial, uint8_t uart_number, uint16_t tx_pin, uint16_t rx_pin
) {
    if (NULL == serial && uart_number >= SIP_UART_MAX_PORT) {
        return;
    }

    sip_uart_init(sip_serial_instances[uart_number].device, tx_pin, rx_pin);
    *serial = &(sip_serial_instances[uart_number]);
}
```


The instance passed to the MCU-Driver-HAL Virtual Functions is a C `struct` that contains a pre-initialised Virtual Pointer to the corresponding Virtual Table in a subclass.

**Listing: Application making use of a SiP's repository with a reference to MCU-Driver-HAL.**
```c
#include "hal/serial_api.h"

#include "sip_serial.h"

void main(void) {
    sip_serial_t *console = NULL;
    // Must initialise the peripheral if required before first call of MCU-Driver-HAL virtual functions
    // Whether the peripheral must be initialised or not is up to the SiP.
    sip_serial_init(&console, 0, CONSOLE_UART_TX_PIN, CONSOLE_UART_RX_PIN);
    mdh_serial_put_data(&(console->serial), 0x00UL);
}
```
