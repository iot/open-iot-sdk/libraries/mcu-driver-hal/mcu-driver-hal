# Developer guide

## Deprecated APIs

### `wait_us()` is not available

The `wait_us()` and `wait_ns()` APIs have been removed.

#### `wait_us()` migration guide

For the microsecond delays use the microsecond ticker API directly. A new utility function has been added to provide a wait functionality; see [`us_ticker_util_wait()`](../../hal/source/mbed_us_ticker_api.c).

Additionally, see the microsecond ticker examples:

- [us_ticker_delay](../../examples/us_ticker_delay/main.c) -- a blocking delay timed with a ticker.
- [us_ticker_interrupt](../../examples/us_ticker_interrupt/main.c) -- setting a future event with a ticker interrupt.

### Ticker middleware layer is not available

The generic ticker middleware has been removed and APIs like the `us_timestamp_t ticker_read_us(const ticker_data_t *const ticker)` are no longer available.

#### Ticker middleware migration guide

The MCU-Driver-HAL microsecond ticker API is a low level HAL. Features that were provided by the ticker middleware have to be implemented in the layers above the HAL:

- handling multiple ticker counter overflows while conducting a time measurement,
- reading time duration in the units of time (seconds, microseconds).

Special care must be taken when dealing with tick values or converting ticks to time. The ticker counter bit width and the ticker frequency are platform-dependent variables. Both are available from the `ticker_info_t` type; this can be read with the use of `us_ticker_get_info()` API.

#### Ticker middleware migration guide -- timing

The most robust method for time measurement requires handling ticker counter overflows. This can be achieved by setting a ticker interrupt at overflow. For reference see the extended version of the [us_ticker_stopwatch](../../examples/us_ticker_stopwatch/main.c) example.

For timing, use the microsecond ticker API directly. It is essential to remember that `us_ticker_read()` returns ticks and these have be converted to time with the use of `ticker_info_t` value available via `us_ticker_get_info()`. See the code below:

```C
#include "hal/us_ticker_api.h"

int main(void)
{
    us_ticker_init();
    const ticker_info_t *ticker_info = us_ticker_get_info();

    // The `ticker_info->bits` has a max value of 32 (see the us ticker
    // docs/defined behavior). As a result, the `ticker_counter_mask` has a max
    // value of UINT32_MAX (0xffffffff). `uint32_t` type is sufficient.
    uint32_t ticker_counter_mask = (1UL << ticker_info->bits) - 1UL;

    uint32_t ticks1, ticks2, ticks_diff;
    uint64_t duration_us;

    // Measure the execution time of `foo()` by reading the microsecond
    // ticker counter value.
    ticks1 = us_ticker_read();
    foo();
    ticks2 = us_ticker_read();

    // The `ticker_counter_mask` must be applied to obtain a correct difference.
    ticks_diff = (ticks2 - ticks1) & ticker_counter_mask;

    // Each operand below has to be `uint64_t`.
    duration_us = 1000000ULL * (uint64_t)ticks_diff / (uint64_t)ticker_info->frequency;
}
```

Additionally, see the microsecond ticker examples:

- [us_ticker_stopwatch](../../examples/us_ticker_stopwatch/main.c) -- a timing example. Basic and extended versions are available.

## Removed mcu-driver-bootstrap library

The deprecated `mcu-driver-bootstrap` library has been removed, and since most of its modules were not used, they are no longer available via `mcu-driver-hal` project. However, a few modules have been converted to standalone CMake targets and are now located in the `components` dir.

### mcu-driver-bootstrap migration guide

Convert the code according to the below table.

- Remove `mcu-driver-bootstrap` from CMake target dependencies, and replace it with a new CMake target where available.
- Remove `#include "bootstrap/<header-file>.h"` from include directives, and replace with the new include path where available.

mcu-driver-bootstrap module | Old CMake target | Old include path | New CMake target | New include path
---|---|---|---|---
mbed_application | `mcu-driver-bootstrap` | `bootstrap/mbed_application.h` | - | -
mbed_assert | `mcu-driver-bootstrap` | `bootstrap/mbed_assert.h` | - (libc) | `assert.h`
mbed_atomic | `mcu-driver-bootstrap` | `bootstrap/mbed_atomic.h` | `mbed-atomic-api` | `mbed_atomic/mbed_atomic.h`
mbed_boot | `mcu-driver-bootstrap` | `bootstrap/mbed_boot.h` | - | -
mbed_critical | `mcu-driver-bootstrap` | `bootstrap/mbed_critical.h` | `mbed-critical` | `mbed_critical/mbed_critical.h`
mbed_debug | `mcu-driver-bootstrap` | `bootstrap/mbed_debug.h` | - | -
mbed_error | `mcu-driver-bootstrap` | `bootstrap/mbed_error.h` | - (may be available in the implementation repository) | -
mbed_preprocessor | `mcu-driver-bootstrap` | `bootstrap/mbed_preprocessor.h` | - | -
mbed_retarget | `mcu-driver-bootstrap` | `bootstrap/mbed_retarget.h` | - | -
mbed_toolchain | `mcu-driver-bootstrap` | `bootstrap/mbed_toolchain.h` | `cmsis-core-generic` | `cmsis_core_generic/generic_api.h`
mbed_trace | `mcu-driver-bootstrap` | `bootstrap/mbed_trace.h` | `mbed-trace` | `mbed_trace/mbed_trace.h`
mbed_wait_api | `mcu-driver-bootstrap` | `bootstrap/mbed_wait_api.h` | - (may be available in the implementation repository) | -
