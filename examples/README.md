# MCU-Driver-HAL examples

The MCU-Driver-HAL examples demonstrate the usage of the APIs. They give a starting point on top of which more complex logic can be built.
The examples are categorised by API and can be found in this directory.

## Required tools

- [CMake](https://cmake.org/install/), ensure you select a version that fulfills the minimum requirement in the main [`CMakeLists.txt`](../CMakeLists.txt) file.
- [Mbed-tools](https://github.com/ARMmbed/mbed-tools) version `7.16.0`.

CMake uses the `Make` build automation by default, however, [`Ninja`](https://github.com/ninja-build/ninja/wiki/Pre-built-Ninja-packages) is also supported.
`Ninja` can optionally be used to build faster.

The examples can be configured to be built for any hardware platform implementing the MCU Driver HAL API. The configuration involves providing a CMake module that must contain CMake variables defining C language macros and other CMake variables that are expected by the MCU-Driver-HAL. The configuration module must be included by the implementation of the HAL. See [here](../docs/porting/cmake.md) for more information.

## Provided Examples

The following table details the currently supported MCU-Driver-HAL examples.

| API                    | Description                                                       |
| ---------------------- | ----------------------------------------------------------------- |
| analogin               | Reads an analog input pin and reports the voltage with a trace    |
| crc                    | Calculates the CRC of an array of a data stream                   |
| flash                  | Erases, reads, writes the flash at various locations              |
| gpio                   | Blinks two LEDs in alternation using a loop as a delay            |
| i2c                    | Writes then reads a byte from an I2C peripheral                   |
| pwm                    | Ramps two LEDs in alternation using a loop as a delay             |
| serial                 | Prints some text to the console                                   |
| spi                    | Initialises the SPI interface and write a byte                    |
| trace                  | Prints traces for the supported tracing levels                    |
| trng                   | Gets random data from the true random number generator            |
| us_ticker -- delay     | Blinks an LED using the microsecond ticker as a delay             |
| us_ticker -- interrupt | Toggles an LED in a microsecond ticker callback                   |
| us_ticker -- stopwatch | Uses the microsecond ticker to time a function call               |

## Build

1. From a terminal, navigate to the directory containing the `CMakeLists.txt` file to build the example executable.
1. From the directory in the previous step, run the following comand to configure the target and generate the build directory:

    ```
    cmake -S . -B cmake_build -GNinja -DCMAKE_BUILD_TYPE=debug -DCMAKE_TOOLCHAIN_FILE=<tools/cmake/toolchain/toolchain-{armclang|arm-none-eabi-gcc}.cmake>
    ```

1. Run the following command to build the artefacts:

    ```
    cmake --build cmake_build
    ```

Expected configuration output:

```
-- The C compiler identification is GNU 9.2.1
-- The ASM compiler identification is GNU
-- Found assembler: /usr/local/bin/arm-none-eabi-gcc
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/local/bin/arm-none-eabi-gcc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Found Python3: /usr/local/Frameworks/Python.framework/Versions/3.9/bin/python3.9 (found version "3.9.1") found components: Interpreter
-- Checking for Python package prettytable -- found
-- Checking for Python package future -- found
-- Checking for Python package jinja2 -- found
-- Checking for Python package intelhex -- found
-- The CXX compiler identification is GNU 9.2.1
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - failed
-- Check for working CXX compiler: /usr/local/bin/arm-none-eabi-g++
-- Check for working CXX compiler: /usr/local/bin/arm-none-eabi-g++ - works
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /path/to/cmake_build
```

Expected build output:

```
[ 99%] Built target <EXAMPLE_LIBRARY_CMAKE_NAME>
Scanning dependencies of target <EXAMPLE_EXECUTABLE_CMAKE_NAME>
[100%] Linking C executable <~EXAMPLE_EXECUTABLE_CMAKE_NAME>.elf
executable:
-- built: /path/to/cmake_build/<EXAMPLE_EXECUTABLE_CMAKE_NAME>.bin
-- built: /path/to/cmake_build/<EXAMPLE_EXECUTABLE_CMAKE_NAME>.hex
Displaying memory map for <EXAMPLE_EXECUTABLE_CMAKE_NAME>
[100%] Built target <EXAMPLE_EXECUTABLE_CMAKE_NAME>
```
