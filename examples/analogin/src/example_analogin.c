/* Copyright (c) 2021-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_TRACE_MAX_LEVEL        TRACE_LEVEL_INFO
#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "A_IN"

#include "hal/analogin_api.h"

#include "mbed_trace/mbed_trace.h"

void example_analogin(mdh_analogin_t *analog_input, float reference_voltage_in_volts)
{
    tr_info("The voltage on the analog input pin is %f V.",
            reference_voltage_in_volts * mdh_analogin_read(analog_input));
}
