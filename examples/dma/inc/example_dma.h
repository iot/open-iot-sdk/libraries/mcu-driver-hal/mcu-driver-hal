/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXAMPLE_DMA_H
#define EXAMPLE_DMA_H

#include "hal/dma_api.h"

/** Copy data between two arrays using IRQ
 *
 *  @param dma_device The DMA object
 *  @param src Source data buffer
 *  @param des Destination data buffer
 *  @param size Number of bytes to be copied from src buffer to des buffer
 */
void example_dma(mdh_dma_t *dma_device, uint8_t *src, uint8_t *des, uint32_t size);

#endif /* EXAMPLE_DMA_H */
