/* Copyright (c) 2022-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "DMA"
#define MBED_TRACE_MAX_LEVEL        TRACE_LEVEL_DEBUG

#include "example_dma.h"
#include "cmsis_core_generic/generic_api.h"
#include "mbed_trace/mbed_trace.h"

static void dma_callback(void *context);

static volatile uint8_t callback_happend = 0;

void example_dma(mdh_dma_t *dma_device, uint8_t *src, uint8_t *des, uint32_t size)
{
    mdh_dma_error_t error = mdh_dma_memmove(dma_device, src, des, size, dma_callback, NULL);

    /* Waiting for the DMA ready interrupt */
    while (callback_happend == 0) {
        __WFE();
    }

    if (error == MDH_DMA_ERROR_NO_ERROR) {
        tr_info("%ld bytes copied by the DMA", size);
    } else {
        tr_info("DMA memcpy failed");
    }
}

static void dma_callback(void *context)
{
    callback_happend = 1;
}
