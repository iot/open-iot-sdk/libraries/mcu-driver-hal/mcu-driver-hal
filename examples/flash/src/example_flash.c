/* Copyright (c) 2020-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/flash_api.h"

#define MBED_TRACE_GROUP "EXPL"

#include "mbed_trace/mbed_trace.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#ifndef ALIGN_DOWN
#define ALIGN_DOWN(x, a) ((x) & ~((a)-1))
#endif

void mdh_example_flash(mdh_flash_t *flash_device)
{
    int32_t ret;
    // silence unused variable warning
    (void)ret;

    uint32_t test_size = mdh_flash_get_page_size(flash_device);

    uint8_t *data = (uint8_t *)malloc(test_size);
    uint8_t *data_flashed = (uint8_t *)malloc(test_size);

    memset(data, 0xCE, test_size);

    // the one before the last page in the system
    uint32_t address = mdh_flash_get_start_address(flash_device) + mdh_flash_get_size(flash_device) - (2 * test_size);

    // sector size might not be same as page size
    uint32_t erase_sector_boundary = ALIGN_DOWN(address, mdh_flash_get_sector_size(flash_device, address));
    tr_info("erase sector boundary: %" PRIu32, erase_sector_boundary);

    ret = mdh_flash_erase_sector(flash_device, erase_sector_boundary);
    assert(0 == ret);

    ret = mdh_flash_program_page(flash_device, address, data, test_size);
    assert(0 == ret);

    ret = mdh_flash_read(flash_device, address, data_flashed, test_size);
    assert(0 == ret);

    // sector size might not be same as page size
    erase_sector_boundary = ALIGN_DOWN(address, mdh_flash_get_sector_size(flash_device, address));
    ret = mdh_flash_erase_sector(flash_device, erase_sector_boundary);
    assert(0 == ret);

    // write another data to be certain we are re-flashing
    memset(data, 0xAC, test_size);

    ret = mdh_flash_program_page(flash_device, address, data, test_size);
    assert(0 == ret);

    ret = mdh_flash_read(flash_device, address, data_flashed, test_size);
    assert(0 == ret);

    free(data);
    free(data_flashed);
}
