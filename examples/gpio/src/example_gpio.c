/* Copyright (c) 2020-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "example_gpio.h"

void example_gpio(mdh_gpio_t *led_1, mdh_gpio_t *led_2, void (*one_second_delay)(void))
{
    mdh_gpio_write(led_1, 1U);
    mdh_gpio_write(led_2, 0U);
    one_second_delay();

    mdh_gpio_write(led_1, 0U);
    mdh_gpio_write(led_2, 1U);
    one_second_delay();
}
