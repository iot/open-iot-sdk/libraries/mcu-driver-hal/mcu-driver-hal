/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "example_gpio_input.h"

void example_gpio_input(mdh_gpio_t *button, mdh_gpio_t *leds[], size_t number_of_leds, void (*one_second_delay)(void))
{
    for (size_t i = 0U; i < number_of_leds; i++) {
        mdh_gpio_write(leds[i], mdh_gpio_read(button));
        one_second_delay();
    }
}
