/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXAMPLE_I2C_H
#define EXAMPLE_I2C_H

#include "hal/i2c_api.h"

/** Write data to a peripheral device then read from it
 *
 *  @param i2c_device The I2C object
 *  @param peripheral_address The 7-bit address in the lower 7 bits of the byte (does not include R/W bit)
 *  @param data data to write to the I2C peripheral
 */
void example_i2c(mdh_i2c_t *i2c_device, uint8_t peripheral_address, uint8_t data);

#endif /* EXAMPLE_I2C_H */
