/* Copyright (c) 2021-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "I2C"

#include "example_i2c.h"

#include "mbed_trace/mbed_trace.h"

void example_i2c(mdh_i2c_t *i2c_device, uint8_t peripheral_address, uint8_t data)
{
    int n_written = mdh_i2c_write(i2c_device, peripheral_address << 1, &data, sizeof(data), false);
    tr_info("%d byte(s) written to I2C address 0x%.2x. Byte value: 0x%.2x", n_written, peripheral_address << 1, data);

    uint8_t read_data = 0x0F;
    int n_read = mdh_i2c_read(i2c_device, (peripheral_address << 1) | 1, &read_data, sizeof(read_data), true);
    tr_info("%d byte(s) read from I2C address 0x%.2x. Byte value: 0x%.2x",
            n_read,
            (peripheral_address << 1) | 1,
            read_data);
}
