/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXAMPLE_SAI_LOOPBACK_H
#define EXAMPLE_SAI_LOOPBACK_H

#include "hal/sai_api.h"
#include "hal/us_ticker_api.h"

typedef void (*example_complete_f)(int32_t left, int32_t right);

/**
 * Runs a demonstration of the SAI HAL API.
 *
 * This demo emits a 1KHz sine wave on sai_out and reads the signal coming in.
 *
 * It analyses the signal from sai_in and prints the peak-peak amplitude of the captured signal.
 *
 * @Note: The example requires us_ticker, mbed_trace and two SAI instances to be initialised.
 */
void example_sai_loopback(mdh_sai_t *sai_in, mdh_sai_t *sai_out, example_complete_f callback, mdh_ticker_t *us_ticker);

#endif /* EXAMPLE_SAI_LOOPBACK_H */
