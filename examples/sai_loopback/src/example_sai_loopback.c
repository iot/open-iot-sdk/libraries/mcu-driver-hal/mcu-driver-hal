/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "LPBK"

#include "example_sai_loopback.h"

#include "hal/sai_api.h"
#include "hal/us_ticker_api.h"
#include "cmsis_core_generic/generic_api.h"
#include "hal-toolbox/us_ticker_util.h"
#include "mbed_trace/mbed_trace.h"

#include <string.h>

// Private types
typedef enum demo_state_e {
    DEMO_STATE_INIT,
    DEMO_STATE_EMIT_1KHz_SINE,
    DEMO_STATE_SAMPLE,
    DEMO_STATE_DONE
} demo_state_t;

// Private function declarations
static void sai_handler_in(mdh_sai_t *self, void *ctx, mdh_sai_transfer_complete_t code);
static void sai_handler_out(mdh_sai_t *self, void *ctx, mdh_sai_transfer_complete_t code);
static void sai_handler_error(mdh_sai_t *self, mdh_sai_event_t code);

static void emit_silence_for_10ms(mdh_ticker_t *us_ticker);
static void process_sample(const int16_t *samples, uint32_t sample_count, example_complete_f callback);
static void run_demo(mdh_ticker_t *us_ticker, example_complete_f callback);

// Private variables & constants
// A 48 point, 16 bit sine table
static const int16_t gsc_sinewave[48] = {0x0000, 0x10B5, 0x2120, 0x30FB, 0x3FFF, 0x4DEB, 0x5A82, 0x658C, 0x6ED9, 0x7641,
                                         0x7BA3, 0x7EE7, 0x7FFF, 0x7EE7, 0x7BA3, 0x7641, 0x6ED9, 0x658C, 0x5A82, 0x4DEB,
                                         0x3FFF, 0x30FB, 0x2120, 0x10B5, 0x0000, 0xEF4A, 0xDEDF, 0xCF04, 0xC000, 0xB214,
                                         0xA57D, 0x9A73, 0x9126, 0x89BE, 0x845C, 0x8118, 0x8000, 0x8118, 0x845C, 0x89BE,
                                         0x9126, 0x9A73, 0xA57D, 0xB214, 0xC000, 0xCF04, 0xDEDF, 0xEF4A};
static int16_t gs_interlaced_channels[48 * 2] = {0};
static mdh_sai_t *gs_psai_in = NULL;
static mdh_sai_t *gs_psai_out = NULL;

static volatile demo_state_t gs_state = DEMO_STATE_INIT;

// Private function definitions.
static void sai_handler_in(mdh_sai_t *self, void *ctx, mdh_sai_transfer_complete_t code)
{
    gs_state = DEMO_STATE_DONE;
}

static void sai_handler_out(mdh_sai_t *self, void *ctx, mdh_sai_transfer_complete_t code)
{
    if (code == MDH_SAI_TRANSFER_COMPLETE_CANCELLED) {
        return;
    }
    // generate a 1KHz sinewave
    mdh_sai_transfer(gs_psai_out, (uint8_t *)gs_interlaced_channels, sizeof(gs_interlaced_channels), NULL);
}

static void sai_handler_error(mdh_sai_t *self, mdh_sai_event_t code)
{
    tr_error("%p errored with %" PRIx32, self, (uint32_t)code);
    while (true) {
    }
}

// generates 10ms of silence.
static void emit_silence_for_10ms(mdh_ticker_t *us_ticker)
{
    memset(gs_interlaced_channels, 0, sizeof(gs_interlaced_channels));
    mdh_sai_transfer(gs_psai_out, (uint8_t *)gs_interlaced_channels, sizeof(gs_interlaced_channels), NULL);

    us_ticker_util_wait(us_ticker, 10 * 1000);

    mdh_sai_cancel_transfer(gs_psai_out);
}

// Computes the peak-peak signal amplitude
static void process_sample(const int16_t *samples, uint32_t sample_count, example_complete_f callback)
{
    int32_t lmin = INT32_MAX;
    int32_t rmin = INT32_MAX;
    int32_t lmax = INT32_MIN;
    int32_t rmax = INT32_MIN;
    int32_t lold = samples[0];
    int32_t rold = samples[1];

    for (uint32_t i = 1; i < (sample_count - 1); i++) {
        int32_t left = samples[i * 2];
        int32_t right = samples[i * 2 + 1];

        lmin = (left < lmin) ? (left + lold) / 2 : lmin;
        rmin = (right < rmin) ? (right + rold) / 2 : rmin;
        lmax = (left > lmax) ? (left + lold) / 2 : lmax;
        rmax = (right > rmax) ? (right + rold) / 2 : rmax;

        lold = left;
        rold = right;
    }

    if (callback != NULL) {
        (callback)(lmax - lmin, rmax - rmin);
    }
}

static void run_demo(mdh_ticker_t *us_ticker, example_complete_f callback)
{
    static int16_t s_looped_back[48 * 5 * 2];

    memset(s_looped_back, 0, sizeof(s_looped_back));
    gs_state = DEMO_STATE_EMIT_1KHz_SINE;
    mdh_sai_transfer(gs_psai_out, (uint8_t *)gs_interlaced_channels, sizeof(gs_interlaced_channels), NULL);

    us_ticker_util_wait(us_ticker, 500 * 1000);

    gs_state = DEMO_STATE_SAMPLE;
    mdh_sai_transfer(gs_psai_in, (uint8_t *)s_looped_back, sizeof(s_looped_back), NULL);
    while (gs_state == DEMO_STATE_SAMPLE) {
        __WFI();
    }

    us_ticker_util_wait(us_ticker, 500 * 1000);

    mdh_sai_cancel_transfer(gs_psai_out);
    process_sample(s_looped_back, 48 * 5, callback);
}

// Public function definitions.
void example_sai_loopback(mdh_sai_t *sai_in, mdh_sai_t *sai_out, example_complete_f callback, mdh_ticker_t *us_ticker)
{
    gs_psai_in = sai_in;
    gs_psai_out = sai_out;

    mdh_sai_set_transfer_complete_callback(gs_psai_in, sai_handler_in);
    mdh_sai_set_event_callback(gs_psai_in, sai_handler_error);
    mdh_sai_set_transfer_complete_callback(gs_psai_out, sai_handler_out);
    mdh_sai_set_event_callback(gs_psai_out, sai_handler_error);

    emit_silence_for_10ms(us_ticker);

    tr_info("Starting demo on the left side");
    for (uint32_t i = 0; i < 48; i++) {
        gs_interlaced_channels[i * 2] = gsc_sinewave[i];
        gs_interlaced_channels[i * 2 + 1] = 0;
    }
    run_demo(us_ticker, callback);

    emit_silence_for_10ms(us_ticker);

    tr_info("Starting demo on the right side");
    for (uint32_t i = 0; i < 48; i++) {
        gs_interlaced_channels[i * 2] = 0;
        gs_interlaced_channels[i * 2 + 1] = gsc_sinewave[i];
    }
    run_demo(us_ticker, callback);
}
