/* Copyright (c) 2020-2022, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/serial_api.h"

void example_serial(mdh_serial_t *stdio)
{
    mdh_serial_set_baud(stdio, 115200);

    const char str[] = "Hello world!\n";
    for (unsigned int i = 0; i < sizeof(str); ++i) {
        mdh_serial_put_data(stdio, str[i]);
    }
}
