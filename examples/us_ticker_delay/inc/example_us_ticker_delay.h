/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXAMPLE_US_TICKER_DELAY_H
#define EXAMPLE_US_TICKER_DELAY_H

#include "hal/gpio_api.h"

/** Toggles an LED after some delay.
 *
 * @param led The LED to toggle
 */
void example_us_ticker_delay(mdh_gpio_t *led);

#endif /* EXAMPLE_US_TICKER_DELAY_H */
