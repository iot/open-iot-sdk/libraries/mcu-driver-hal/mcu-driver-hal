/* Copyright (c) 2020-2022, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/gpio_api.h"
#include "hal/us_ticker_api.h"

// LED1 blinking period in microseconds.
#define LED_PERIOD_US 100000UL

static void toggle_gpio(mdh_gpio_t *gpio);

void example_us_ticker_delay(mdh_gpio_t *led)
{
    toggle_gpio(led);

    // <led_time_on> == <led_time_off> == LED_PERIOD_US / 2
    us_ticker_util_wait((LED_PERIOD_US) / 2UL);
}

static void toggle_gpio(mdh_gpio_t *gpio)
{
    mdh_gpio_write(gpio, mdh_gpio_read(gpio) ? 0UL : 1UL);
}
