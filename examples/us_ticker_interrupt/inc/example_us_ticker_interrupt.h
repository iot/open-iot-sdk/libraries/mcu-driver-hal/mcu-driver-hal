/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EXAMPLE_US_TICKER_INTERRUPT_H
#define EXAMPLE_US_TICKER_INTERRUPT_H

#include "hal/gpio_api.h"

/** Initiates a self-scheduling interrupt loop toggling an LED
 *
 * @note
 * The example never stops and keeps a pointer to the given LED instance. Therefore
 * the led object must live forever.
 *
 * @param led The GPIO object associated with the LED to toggle.
 */

void example_us_ticker_interrupt_start(mdh_gpio_t *led);

#endif /* EXAMPLE_US_TICKER_INTERRUPT_H */
