/* Copyright (c) 2020-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/gpio_api.h"
#include "hal/us_ticker_api.h"

// LED1 toggle time in seconds, [1..42].
#define LED_TOGGLE_TIME_S 1UL

static mdh_gpio_t *gs_led = NULL;

static void toggle_led(void);
static void irq_handler(const ticker_data_t *const ticker);

void example_us_ticker_interrupt_start(mdh_gpio_t *led)
{
    gs_led = led;
    us_ticker_init();
    set_us_ticker_irq_handler(irq_handler);

    // Force the first interrupt. The following ones will be sheduled
    // by the handler code.
    us_ticker_fire_interrupt();
}

static void toggle_led(void)
{
    mdh_gpio_write(gs_led, mdh_gpio_read(gs_led) ? 0UL : 1UL);
}

// Clear interrupt flag, toggle led, set next interrupt.
static void irq_handler(const ticker_data_t *const ticker)
{
    uint32_t current_tick = ticker->interface->read();
    ticker->interface->clear_interrupt();

    toggle_led();

    const ticker_info_t *ticker_info = ticker->interface->get_info();

    // The `ticker_info->bits` has a max value of 32 (see the us ticker docs/defined behavior).
    // As a result, the `ticker_counter_mask` has a max value of UINT32_MAX (0xffffffff).
    uint32_t ticker_counter_mask = (1UL << ticker_info->bits) - 1UL;

    // The `ticker_info->frequency` max value is 100 MHz (see the us ticker
    // docs/defined behavior). As a result, LED_TOGGLE_TIME_S has to be less
    // than 43 not to overflow the `uint32_t` type when multiplying (below).
    // This is enough for the purpose of this example.
    uint32_t delay_in_ticks = (LED_TOGGLE_TIME_S)*ticker_info->frequency;

    // The `ticker_counter_mask` must be applied to obtain a valid tick.
    uint32_t next_irq_tick = (current_tick + delay_in_ticks) & ticker_counter_mask;
    ticker->interface->set_interrupt(next_irq_tick);
}
