/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_TRACE_MAX_LEVEL        TRACE_LEVEL_DEBUG
#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "US_TICKER_STPWTCH"

#include "hal/us_ticker_api.h"

#include "mbed_trace/mbed_trace.h"

// Set to 0 for a basic example.
// Set to 1 for a extended example.
#define MULTIPLE_OVERFLOW_HANDLING 1

// A function we want to time.
static void foo(void)
{
#if MULTIPLE_OVERFLOW_HANDLING
    // NOTE: Extend the execution of this function to more than
    // 2 * <ticker_counter_overflow_time> to observe multiple overflow handling.
    // This is not really feasible for platforms with 32-bit counter and 1 MHz
    // ticker frequency; in these cases the execution time should be longer
    // than 2 * 72 minutes.
#endif // MULTIPLE_OVERFLOW_HANDLING
    for (volatile uint32_t i = 0; i < 87654321UL; i++)
        ;
}

#if MULTIPLE_OVERFLOW_HANDLING
#include "mbed_atomic/mbed_atomic.h"
#include "mbed_critical/mbed_critical.h"

static volatile uint32_t us_ticker_overflows;

// Clear interrupt flag, increment counter, set next interrupt at counter overflow.
void irq_handler(const ticker_data_t *const ticker)
{
    ticker->interface->clear_interrupt();

    core_util_atomic_incr_u32(&us_ticker_overflows, 1UL);

    const ticker_info_t *ticker_info = ticker->interface->get_info();

    // The `ticker_info->bits` has a max value of 32 (see the us ticker docs/defined behavior).
    // As a result, the `ticker_counter_max` has a max value of UINT32_MAX (0xffffffff).
    uint32_t ticker_counter_max = (1UL << ticker_info->bits) - 1UL;

    // Set an interrupt 1 tick before the overflow.
    ticker->interface->set_interrupt(ticker_counter_max);
}
#endif // MULTIPLE_OVERFLOW_HANDLING

void example_us_ticker_stopwatch(void)
{
#if MULTIPLE_OVERFLOW_HANDLING
    tr_info("Timing example (extended):");
    tr_info("- Handles multiple overflow events during the measurement.");
#else  // MULTIPLE_OVERFLOW_HANDLING
    tr_info("Timing example (basic):");
    tr_info("- Returns correct timing result for up to one overflow.");
    tr_info("- Has a duration limit equal to the the ticker overflow time.");
#endif // MULTIPLE_OVERFLOW_HANDLING

    // Set up the microsecond ticker.
    us_ticker_init();
    const ticker_info_t *ticker_info = us_ticker_get_info();

    // The `ticker_info->bits` has a max value of 32 (see the us ticker docs/defined behavior).
    // As a result, the `ticker_counter_mask` has a max value of UINT32_MAX (0xffffffff).
    uint32_t ticker_counter_mask = (1UL << ticker_info->bits) - 1UL;

#if MULTIPLE_OVERFLOW_HANDLING
    set_us_ticker_irq_handler(irq_handler);
    us_ticker_set_interrupt(ticker_counter_mask);
#endif // MULTIPLE_OVERFLOW_HANDLING

    uint32_t ticker_overflow_s = ticker_counter_mask / ticker_info->frequency;
    tr_info("The ticker counter will overflow in about %" PRIu32 " s (that is about"
            " %" PRIu32 " minutes).",
            ticker_overflow_s,
            ticker_overflow_s / 60UL);

    while (1) {
        uint32_t ticks1 = 0, ticks2 = 0;
        // Measure the execution time of `foo()` by reading the microsecond
        // ticker counter value.

#if MULTIPLE_OVERFLOW_HANDLING
        uint32_t ticker_overflows1 = 0, ticker_overflows2 = 0, ticker_overflows = 0;

        core_util_critical_section_enter();
        ticks1 = us_ticker_read();
        ticker_overflows1 = core_util_atomic_load_u32(&us_ticker_overflows);
        core_util_critical_section_exit();
        foo();
        core_util_critical_section_enter();
        ticks2 = us_ticker_read();
        ticker_overflows2 = core_util_atomic_load_u32(&us_ticker_overflows);
        core_util_critical_section_exit();
#else  // MULTIPLE_OVERFLOW_HANDLING
        ticks1 = us_ticker_read();
        foo();
        ticks2 = us_ticker_read();
#endif // MULTIPLE_OVERFLOW_HANDLING

        // The `ticker_counter_mask` must be applied to obtain a correct difference.
        uint32_t ticks_diff = (ticks2 - ticks1) & ticker_counter_mask;

        // Each operand below has to be `uint64_t`.
        uint64_t duration_us = 1000000ULL * (uint64_t)ticks_diff / (uint64_t)ticker_info->frequency;

#if MULTIPLE_OVERFLOW_HANDLING
        ticker_overflows = ticker_overflows2 - ticker_overflows1;
        tr_debug("ticks1 = 0x%08" PRIx32 ", ticks2 = 0x%08" PRIx32 ", ticks_diff = %" PRIu32 ", "
                 "ticker_overflows = %" PRIu32,
                 ticks1,
                 ticks2,
                 ticks_diff,
                 ticker_overflows);

        // The first detected overflow does not add any duration.
        while (ticker_overflows > 1) {
            // Each operand below has to be `uint64_t`.
            duration_us += 1000000ULL * (uint64_t)ticker_counter_mask / (uint64_t)ticker_info->frequency;
            ticker_overflows--;
        }
#else  // MULTIPLE_OVERFLOW_HANDLING
        tr_debug(
            "ticks1 = 0x%08" PRIx32 ", ticks2 = 0x%08" PRIx32 ", ticks_diff = %" PRIu32, ticks1, ticks2, ticks_diff);
#endif // MULTIPLE_OVERFLOW_HANDLING

        if (ticks2 > ticks1) {
            tr_info("elapsed time: %" PRIu64 " us.", duration_us);
        } else {
            tr_warn("elapsed time: %" PRIu64 " us.", duration_us);
        }
    }
}
