# MCU-Driver-HAL Toolbox

This library provides the following utilities:

- `container_of` preprocessor macro,
- `us_ticker_util_wait` function.
