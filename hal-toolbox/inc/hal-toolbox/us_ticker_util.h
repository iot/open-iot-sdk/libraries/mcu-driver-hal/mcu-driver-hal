/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal-toolbox */
/** @{*/

#ifndef MDH_US_TICKER_UTIL_H
#define MDH_US_TICKER_UTIL_H

#include "hal/us_ticker_api.h"
#include <stdint.h>

/** Wait a number of microseconds.
 *
 * @param us number of microseconds to wait
 *
 * @note The implementation of this utility function spins by repeatedly
 * calling the underlying microsecond ticker API. This method gets the exact
 * number of ticks, but impacts the power and multithread performance.
 * Therefore, spinning for millisecond wait is not recommended; sleep API
 * should be used instead.
 *
 * @note You may call this function from ISR context, but large delays may
 * impact system stability - interrupt handlers should take less than 50us.
 */
void us_ticker_util_wait(mdh_ticker_t *us_ticker, uint32_t us);

#endif // MDH_US_TICKER_UTIL_H

/** @}*/
