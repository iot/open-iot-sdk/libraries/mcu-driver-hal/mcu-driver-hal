/* Copyright (c) 2022-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal-toolbox/us_ticker_util.h"
#include "hal/us_ticker_api.h"
#include <stdint.h>

void us_ticker_util_wait(mdh_ticker_t *us_ticker, uint32_t us)
{
    uint32_t ticks1 = mdh_us_ticker_read(us_ticker);
    const ticker_info_t *ticker_info = mdh_us_ticker_get_info(us_ticker);

    // The `ticker_info->bits` has a max value of 32 (see the us ticker
    // docs/defined behavior). As a result, the `ticker_counter_mask` has a max
    // value of UINT32_MAX (0xffffffff). `uint32_t` type is sufficient.
    uint32_t ticker_counter_mask = (1UL << ticker_info->bits) - 1UL;

    // Each operand below has to be `uint64_t`.
    uint64_t ticks_delay = (uint64_t)us * (uint64_t)ticker_info->frequency / 1000000ULL;
    uint64_t ticks_passed = 0ULL;

    // Spin to wait the exact number of ticks.
    while (ticks_passed < ticks_delay) {
        uint32_t ticks2 = mdh_us_ticker_read(us_ticker);

        // The `ticker_counter_mask` must be applied.
        ticks_passed += (ticks2 - ticks1) & ticker_counter_mask;
        ticks1 = ticks2;
    }
}
