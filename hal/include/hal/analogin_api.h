/* Copyright (c) 2006-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_ANALOGIN_API_H
#define MDH_ANALOGIN_API_H

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

/** Analogin hal structure.
 */
typedef struct mdh_analogin_s mdh_analogin_t;

typedef struct mdh_analogin_vtable_s {
    float (*read)(mdh_analogin_t *self);
    uint16_t (*read_u16)(mdh_analogin_t *self);
} mdh_analogin_vtable_t;

struct mdh_analogin_s {
    const mdh_analogin_vtable_t *vfptr;
};

/**
 * \defgroup hal_analogin Analogin hal functions
 *
 * # Defined behaviour
 * * The function ::mdh_analogin_read reads the input voltage, represented as a float in the range [0.0 (GND), 1.0
 * (VCC)]
 * * The function ::mdh_analogin_read_u16 reads the value from analogin pin, represented as an unsigned 16bit value [0.0
 * (GND), MAX_UINT16 (VCC)]
 * * The accuracy of the ADC is +/- 10%
 * * The ADC operations ::mdh_analogin_read, ::mdh_analogin_read_u16 take less than 20us to complete
 * @{
 */

/** Read the input voltage, represented as a float in the range [0.0, 1.0]
 *
 * @param self The analogin object
 * @return A floating value representing the current input voltage
 */
static inline float mdh_analogin_read(mdh_analogin_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read));
    return self->vfptr->read(self);
}

/** Read the value from analogin pin, represented as an unsigned 16bit value
 *
 * @param self The analogin object
 * @return An unsigned 16bit value representing the current input voltage
 */
static inline uint16_t mdh_analogin_read_u16(mdh_analogin_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read_u16));
    return self->vfptr->read_u16(self);
}

/**@}*/

#endif // MDH_ANALOGIN_API_H

/** @}*/
