/* Copyright (c) 2006-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_ANALOGOUT_API_H
#define MDH_ANALOGOUT_API_H

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

/** Analogout hal structure.
 */
typedef struct mdh_analogout_s mdh_analogout_t;

typedef struct mdh_analogout_vtable_s {
    void (*write)(mdh_analogout_t *self, float value);
    void (*write_u16)(mdh_analogout_t *self, uint16_t value);
    float (*read)(mdh_analogout_t *self);
    uint16_t (*read_u16)(mdh_analogout_t *self);
} mdh_analogout_vtable_t;

struct mdh_analogout_s {
    const mdh_analogout_vtable_t *vfptr;
};

/**
 * \defgroup hal_analogout Analogout hal functions
 *
 * # Defined behaviour
 * * The function ::mdh_analogout_write sets the output voltage, specified as a percentage (float) in range [0.0
 * (GND), 1.0 (VCC)]
 * * The function ::mdh_analogout_write_u16 sets the output voltage, specified as unsigned 16-bit value [0 (GND),
 * MAX_UINT16 (VCC)]
 * * The function ::mdh_analogout_read reads the current voltage value on the pin and returns a floating-point value
 * representing the current voltage in range [0.0 (GND), 1.0 (VCC)]
 * * The function ::mdh_analogout_read_u16 reads the current voltage value on the pin and returns the output voltage,
 * specified as unsigned 16-bit value [0 (GND), MAX_UINT16 (VCC)]
 * * The accuracy of the DAC is +/- 10%
 * * The DAC operations ::mdh_analogout_write, ::mdh_analogout_write_u16, ::mdh_analogout_read, ::mdh_analogout_read_u16
 * take less than 20us to complete
 *
 * @{
 */

/** Set the output voltage, specified as a percentage (float)
 *
 * @param self The analogout object
 * @param value The floating-point output voltage to be set
 */
static inline void mdh_analogout_write(mdh_analogout_t *self, float value)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->write));
    self->vfptr->write(self, value);
}

/** Set the output voltage, specified as unsigned 16-bit
 *
 * @param self The analogout object
 * @param value The unsigned 16-bit output voltage to be set
 */
static inline void mdh_analogout_write_u16(mdh_analogout_t *self, uint16_t value)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->write_u16));
    self->vfptr->write_u16(self, value);
}

/** Read the current voltage value on the pin
 *
 * @param self The analogout object
 * @return A floating-point value representing the current voltage on the pin,
 *     measured as a percentage
 */
static inline float mdh_analogout_read(mdh_analogout_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read));
    return self->vfptr->read(self);
}

/** Read the current voltage value on the pin, as a normalized unsigned 16bit value
 *
 * @param self The analogout object
 * @return An unsigned 16-bit value representing the current voltage on the pin
 */
static inline uint16_t mdh_analogout_read_u16(mdh_analogout_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read_u16));
    return self->vfptr->read_u16(self);
}

/**@}*/

#endif // MDH_ANALOGOUT_API_H

/** @}*/
