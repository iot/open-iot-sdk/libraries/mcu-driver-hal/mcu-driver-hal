/* Copyright (c) 2006-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_CAN_API_H
#define MDH_CAN_API_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 *
 * \enum    mdh_can_format_t
 *
 * \brief   Values that represent CAN Format
 **/
typedef enum mdh_can_format_e {
    MDH_CAN_FORMAT_STANDARD = 0,
    MDH_CAN_FORMAT_EXTENDED = 1,
    MDH_CAN_FORMAT_ANY = 2
} mdh_can_format_t;

/**
 *
 * \enum    mdh_mdh_can_type_t
 *
 * \brief   Values that represent CAN Type
 **/
typedef enum mdh_mdh_can_type_e { MDH_CAN_TYPE_DATA = 0, MDH_CAN_TYPE_REMOTE = 1 } mdh_mdh_can_type_t;

/**
 *
 * \struct  mdh_can_message_t
 *
 * \brief   Holder for single CAN message.
 *
 **/

typedef struct mdh_can_message_s {
    uint32_t id;             // 29 bit identifier
    unsigned char data[8];   // Data field
    size_t len;              // Length of data field in bytes
    mdh_can_format_t format; // Format ::mdh_can_format_t
    mdh_mdh_can_type_t type; // Type ::mdh_mdh_can_type_t
} mdh_can_message_t;

typedef enum mdh_can_irq_e {
    MDH_CAN_IRQ_RX,
    MDH_CAN_IRQ_TX,
    MDH_CAN_IRQ_ERROR,
    MDH_CAN_IRQ_OVERRUN,
    MDH_CAN_IRQ_WAKEUP,
    MDH_CAN_IRQ_PASSIVE,
    MDH_CAN_IRQ_ARB,
    MDH_CAN_IRQ_BUS,
    MDH_CAN_IRQ_READY
} mdh_can_irq_t;

typedef enum {
    MDH_CAN_MODE_RESET,
    MDH_CAN_MODE_NORMAL,
    MDH_CAN_MODE_SILENT,
    MDH_CAN_MODE_TEST_LOCAL,
    MDH_CAN_MODE_TEST_GLOBAL,
    MDH_CAN_MODE_TEST_SILENT
} mdh_can_mode_t;

typedef void (*mdh_can_callback_f)(void *ctx, mdh_can_irq_t type);

typedef struct mdh_can_s mdh_can_t;

typedef struct mdh_can_vtable_s {
    int (*set_frequency)(mdh_can_t *self, int hz);
    void (*set_irq_callback)(mdh_can_t *self, mdh_can_callback_f cbk, void *ctx);
    void (*clear_irq_callback)(mdh_can_t *self);
    void (*set_irq_availability)(mdh_can_t *self, mdh_can_irq_t irq, bool enable);
    int (*write)(mdh_can_t *self, mdh_can_message_t message, int cc);
    int (*read)(mdh_can_t *self, mdh_can_message_t *message, int handle);
    int (*set_mode)(mdh_can_t *self, mdh_can_mode_t mode);
    int (*filter)(mdh_can_t *self, uint32_t id, uint32_t mask, mdh_can_format_t format, int32_t handle);
    void (*reset)(mdh_can_t *self);
    unsigned char (*rderror)(mdh_can_t *self);
    unsigned char (*tderror)(mdh_can_t *self);
    void (*monitor)(mdh_can_t *self, int silent);
} mdh_can_vtable_t;

struct mdh_can_s {
    const mdh_can_vtable_t *vfptr;
};

static inline int mdh_can_set_frequency(mdh_can_t *self, int hz)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_frequency));
    self->vfptr->set_frequency(self, hz);
}

static inline void mdh_can_set_irq_callback(mdh_can_t *self, mdh_can_callback_f cbk, void *ctx)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_irq_callback));
    self->vfptr->set_irq_callback(self, cbk, ctx);
}

static inline void mdh_can_clear_irq_callback(mdh_can_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->clear_irq_callback));
    self->vfptr->clear_irq_callback(self);
}

static inline void mdh_can_set_irq_availability(mdh_can_t *self, mdh_can_irq_t irq, bool enable)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_irq_availability));
    self->vfptr->set_irq_availability(self, irq, enable);
}

static inline int mdh_can_write(mdh_can_t *self, mdh_can_message_t message, int cc)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->write));
    self->vfptr->write(self, message, cc);
}

static inline int mdh_can_read(mdh_can_t *self, mdh_can_message_t *message, int handle)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read));
    self->vfptr->read(self, message, handle);
}

static inline int mdh_can_set_mode(mdh_can_t *self, mdh_can_mode_t mode)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_mode));
    self->vfptr->set_mode(self, mode);
}

static inline int mdh_can_filter(mdh_can_t *self, uint32_t id, uint32_t mask, mdh_can_format_t format, int32_t handle)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->filter));
    self->vfptr->filter(self, id, mask, format, handle);
}

static inline void mdh_can_reset(mdh_can_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->reset));
    self->vfptr->reset(self);
}

static inline unsigned char mdh_can_rderror(mdh_can_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->rderror));
    self->vfptr->rderror(self);
}

static inline unsigned char mdh_can_tderror(mdh_can_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->tderror));
    self->vfptr->tderror(self);
}

static inline void mdh_can_monitor(mdh_can_t *self, int silent)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->monitor));
    self->vfptr->monitor(self, silent);
}

#endif // MDH_CAN_API_H

/** @}*/
