/* Copyright (c) 2021-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_EMAC_API_H
#define MDH_EMAC_API_H

#include "hal/network_stack_memory_manager.h"

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * \defgroup hal_emac Ethernet Media Access Controller hal functions
 * Possible events to be notified with the mdh_emac_event_callback_f callback
 * include link going up or link going down as well as other events
 * that may be implementation specific by extending mdh_emac_event_t
 * # Undefined behaviour
 * * Calling mdh_emac_get_mac_addr() before calling mdh_emac_power_up()
 * * Calling mdh_emac_get_mac_addr() with a memory location with insufficiently allocated
 * space to store a number of bytes equal to the value returned by mdh_emac_get_mac_addr_size()
 * * Calling mdh_emac_set_mac_addr() before calling mdh_emac_power_up()
 * * Calling mdh_emac_set_mac_addr() with a memory location with insufficiently allocated
 * space to copy a number of bytes equal to the value returned by mdh_emac_get_mac_addr_size()
 * * Calling mdh_emac_add_to_multicast_group() before calling mdh_emac_power_up()
 * * Calling mdh_emac_remove_from_multicast_group() before calling mdh_emac_power_up()
 * * Calling mdh_emac_remove_from_multicast_group() before calling mdh_emac_add_to_multicast_group()
 * * Calling mdh_emac_config_multicast_reception() before calling mdh_emac_power_up()
 * * Calling mdh_emac_transmit() before mdh_emac_power_up()
 * * Calling any function other than mdh_emac_power_up() after calling mdh_emac_power_down()
 *
 * @{
 */

typedef struct mdh_emac_s mdh_emac_t;
typedef enum mdh_emac_status_e {
    MDH_EMAC_STATUS_NO_ERROR,
    MDH_EMAC_STATUS_INVALID_ARGS,
    MDH_EMAC_STATUS_INTERNAL_ERROR,
    MDH_EMAC_STATUS_LINK_DOWN,
    MDH_EMAC_STATUS_BUSY,
    MDH_EMAC_STATUS_UNAVAILABLE,

    // Defined for extension purposes.
    MDH_EMAC_STATUS_LAST
} mdh_emac_status_t;

typedef enum mdh_emac_event_e {
    MDH_EMAC_EVENT_LINK_STATUS_CHANGE,

    // Defined for extension purposes.
    MDH_EMAC_EVENT_LINK_STATUS_LAST
} mdh_emac_event_t;

typedef enum mdh_emac_transfer_e { MDH_EMAC_TRANSFER_DONE, MDH_EMAC_TRANSFER_ERROR } mdh_emac_transfer_t;
typedef enum mdh_emac_receive_e { MDH_EMAC_RECEIVE_DONE, MDH_EMAC_RECEIVE_ERROR } mdh_emac_receive_t;

typedef void (*mdh_emac_event_callback_f)(mdh_emac_t *self, void *ctx, mdh_emac_event_t event);

typedef void (*mdh_emac_receive_complete_callback_f)(mdh_emac_t *self, void *ctx, mdh_emac_receive_t status);

typedef void (*mdh_emac_transmit_complete_callback_f)(mdh_emac_t *self,
                                                      void *ctx,
                                                      mdh_emac_transfer_t status,
                                                      const mdh_network_stack_buffer_t *buffer);

typedef struct mdh_emac_callbacks_s {
    mdh_emac_receive_complete_callback_f rx;
    mdh_emac_transmit_complete_callback_f tx;
    mdh_emac_event_callback_f event;
} mdh_emac_callbacks_t;

typedef struct mdh_emac_vtable_s {
    mdh_emac_status_t (*power_up)(const mdh_emac_t *self,
                                  mdh_emac_callbacks_t *cbks,
                                  mdh_network_stack_memory_manager_t *memory_manager,
                                  void *ctx);
    mdh_emac_status_t (*power_down)(mdh_emac_t *const self);

    size_t (*get_mtu)(const mdh_emac_t *const self);
    size_t (*get_align)(const mdh_emac_t *const self);
    size_t (*get_interface_name)(const mdh_emac_t *const self, char *name, size_t name_size);
    size_t (*get_mac_addr_size)(const mdh_emac_t *const self);
    mdh_emac_status_t (*get_mac_addr)(const mdh_emac_t *const self, uint8_t *addr);
    mdh_emac_status_t (*set_mac_addr)(mdh_emac_t *const self, const uint8_t *addr);

    mdh_emac_status_t (*add_to_multicast_group)(mdh_emac_t *const self, const uint8_t *addr);
    mdh_emac_status_t (*remove_from_multicast_group)(mdh_emac_t *const self, const uint8_t *addr);
    mdh_emac_status_t (*config_multicast_reception)(mdh_emac_t *const self, bool receive_all);

    mdh_emac_status_t (*transmit)(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf);
    mdh_emac_status_t (*receive)(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf);
} mdh_emac_vtable_t;

struct mdh_emac_s {
    const mdh_emac_vtable_t *vtable;
};

/** Initializes the EMAC device
 *
 * @note
 * The function is expected to do the following:
 * - bring up the clock
 * - initialise the Ethernet device to a default known state
 * - configure the port pins to be used
 * - notify that the link went up
 * - enable the Ethernet IRQ for Tx and Rx completion as well as overrun and
 *   underrun events. Other interrupts can be also be enabled here and can be
 *   reported to the upper stack layer with the @a cbks->event callback.
 *
 * The callbacks (cbks) are typically called in the Ethernet controller ISR. These
 * callbacks should be short and at most just set flags that are then later on
 * actioned on in an idle loop or thread.
 *
 * The memory manager is an instance mdh_network_stack_memory_manager_t which
 * the network stack must implement and provide to the EMAC HAL to handle data
 * to be transmitted or data received. The EMAC driver should not be responsible
 * for allocating or deallocating memory.
 *
 * @param[in] self EMAC instance to operate with
 * @param[in] cbks the callbacks to execute from the emac driver to notify the application
 * @param[in] memory_manager The memory manager to use
 * @param[in] ctx is an application defined data passed to the callbacks.
 * @returns
 *  - MDH_EMAC_STATUS_NO_ERROR on success;
 *  - MDH_EMAC_STATUS_INTERNAL_ERROR on error;
 *  - MDH_EMAC_STATUS_INVALID_ARGS if NULL arguments have been passed.
 */
static inline mdh_emac_status_t mdh_emac_power_up(mdh_emac_t *const self,
                                                  mdh_emac_callbacks_t *cbks,
                                                  mdh_network_stack_memory_manager_t *memory_manager,
                                                  void *ctx)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->power_up));
    return self->vtable->power_up(self, cbks, memory_manager, ctx);
}

/** Deinitializes the EMAC device
 *
 * @note
 * The function is expected to do the following:
 * - de-initialise the Ethernet device
 * - disable all Ethernet IRQs
 * - notify that the link went down
 *
 * @param[in] self EMAC instance to operate with
 * @returns
 *  - MDH_EMAC_STATUS_NO_ERROR on success;
 *  - MDH_EMAC_STATUS_INTERNAL_ERROR on error.
 */
static inline mdh_emac_status_t mdh_emac_power_down(mdh_emac_t *const self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->power_down));
    return self->vtable->power_down(self);
}

/** Gets the maximum transmission unit (MTU)
 * @note
 * Returns the maximum amount of protocol data transmittable by the peripheral
 * in a single transaction.
 *
 * @param[in] self EMAC instance to operate with
 * @returns The MTU in bytes
 */
static inline size_t mdh_emac_get_mtu(const mdh_emac_t *const self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->get_mtu));
    return self->vtable->get_mtu(self);
}

/** Gets the memory buffer alignment requirement of the Ethernet controller device
 *
 * @note
 * Returns the buffer memory alignment requirement in bytes.
 * The IP stack should align transmit buffers using this (or higher) alignment.
 * If not, the Ethernet Peripheral may not function properly.
 *
 * @param[in] self EMAC instance to operate with
 * @returns The memory alignment requirement in bytes
 */
static inline size_t mdh_emac_get_align(const mdh_emac_t *const self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->get_align));
    return self->vtable->get_align(self);
}

/** Gets the interface name
 *
 * @note
 * Returns a null-terminated string representing the name of the Ethernet interface.
 * If the interface name length exceeds the size of the memory provided to
 * retrieve it, the interface name returned is cropped but still null terminated.
 * The size of the buffer passed must be at least two bytes long to retrieve a
 * minimum of one character in addition to the null terminator.
 *
 * @param[in] self EMAC instance to operate with
 * @param[out] name Pointer to memory location to store the name
 * @param[in] size Maximum number of character to copy
 * @returns The actual size of the interface name retrieved
 */
static inline size_t mdh_emac_get_interface_name(const mdh_emac_t *const self, char *name, size_t size)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->get_interface_name));
    return self->vtable->get_interface_name(self, name, size);
}

/** Gets the size of the underlying interface MAC address.
 *
 * @param[in] self EMAC instance to operate with
 * @returns The MAC address size in bytes
 */
static inline size_t mdh_emac_get_mac_addr_size(const mdh_emac_t *const self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->get_mac_addr_size));
    return self->vtable->get_mac_addr_size(self);
}

/** Gets the interface supplied MAC address
 *
 * @note
 * @param addr must be the correct size, see @a mdh_emac_get_mac_addr_size.
 * The memory location provided must be able to contain the entire PHY address
 * starting with the LSB
 *
 * @param[in] self EMAC instance to operate with
 * @param[out] addr Pointer to memory location to write the address
 * @returns
 *  - MDH_EMAC_STATUS_NO_ERROR on success;
 *  - MDH_EMAC_STATUS_UNAVAILABLE if the unavailable.
 */
static inline mdh_emac_status_t mdh_emac_get_mac_addr(const mdh_emac_t *const self, uint8_t *addr)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->get_mac_addr));
    return self->vtable->get_mac_addr(self, addr);
}

/** Sets the Ethernet device MAC address
 *
 * @note
 * The implementation of this function is optional.
 * The provided @param addr must be of correct size, see @a mdh_emac_get_mac_addr_size.
 * Call it to set the MAC address to use as the new Ethernet device MAC address
 *
 * @param[in] self EMAC instance to operate with
 * @param[in] addr Pointer to memory location containing the address to set to
 * @returns
 *  - MDH_EMAC_STATUS_NO_ERROR on success (the MAC address of the Ethernet controller matches @param addr);
 *  - MDH_EMAC_STATUS_INTERNAL_ERROR on error or not implemented.
 */
static inline mdh_emac_status_t mdh_emac_set_mac_addr(mdh_emac_t *const self, const uint8_t *addr)
{
    assert((NULL != self) && (NULL != self->vtable));

    if (NULL != self->vtable->set_mac_addr) {
        return self->vtable->set_mac_addr(self, addr);
    } else {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }
}

/** Adds an EMAC device to a multicast group
 *
 * @note
 * The implementation of this function is optional.
 * The device must accept all frames addressed to the multicast group identified by @param addr
 *
 * @param[in] self EMAC instance to operate with
 * @param[in] addr Pointer to memory location containing the MAC address of the multicast group
 * @returns
 *  - MDH_EMAC_STATUS_NO_ERROR on success (The Ethernet controller is in to the multicast group);
 *  - MDH_EMAC_STATUS_INTERNAL_ERROR on error or not implemented.
 */
static inline mdh_emac_status_t mdh_emac_add_to_multicast_group(mdh_emac_t *const self, const uint8_t *addr)
{
    assert((NULL != self) && (NULL != self->vtable));

    if (NULL != self->vtable->add_to_multicast_group) {
        return self->vtable->add_to_multicast_group(self, addr);
    } else {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }
}

/** Removes an EMAC device from a multicast group
 *
 * @note
 * The implementation of this function is optional.
 * The device must reject all frames addressed to the multicast group identified by @param addr
 *
 * @param[in] self EMAC instance to operate with
 * @param[in] addr Pointer to memory location containing the MAC address of the multicast group
 * @returns
 *  - MDH_EMAC_STATUS_NO_ERROR on success (The Ethernet controller is not in the multicast group);
 *  - MDH_EMAC_STATUS_INTERNAL_ERROR on erroror not implemented.
 */
static inline mdh_emac_status_t mdh_emac_remove_from_multicast_group(mdh_emac_t *const self, const uint8_t *addr)
{
    assert((NULL != self) && (NULL != self->vtable));

    if (NULL != self->vtable->remove_from_multicast_group) {
        return self->vtable->remove_from_multicast_group(self, addr);
    } else {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }
}

/** Enable or disable reception of all multicast packets
 *
 * @note
 * The implementation of this function is optional.
 * Accept all multicast frames if @param receive_all is true,
 * otherwise revert to filtering and accept only multicast frames for the group
 * the Ethernet controller device was added to with @a mdh_emac_add_to_multicast_group
 *
 * @param[in] self EMAC instance to operate with
 * @param[in] receive_all
 *  - true to receive all multicasts;
 *  - false to receive only multicasts addressed to specified groups;
 * @returns
 *  - MDH_EMAC_STATUS_NO_ERROR on success (The Ethernet controller is in a multicast group and is filtering messages or
 * not);
 *  - MDH_EMAC_STATUS_INTERNAL_ERROR on error or not implemented.
 */
static inline mdh_emac_status_t mdh_emac_config_multicast_reception(mdh_emac_t *const self, bool receive_all)
{
    assert((NULL != self) && (NULL != self->vtable));

    if (NULL != self->vtable->config_multicast_reception) {
        return self->vtable->config_multicast_reception(self, receive_all);
    } else {
        return MDH_EMAC_STATUS_INTERNAL_ERROR;
    }
}

/** Kicks off the transmission of a frame over the link
 *
 * @note
 * This is not a blocking call. The caller is informed of the completion of
 * the transmission asynchronously via the callback registered with the
 * @a mdh_emac_power_up() function, i.e. @a cbks->tx.
 * @param buf refers to a pre-allocated memory buffer, this function is not
 * responsible for freeing the allocated memory. Memory deallocation must be
 * handled by the allocator. The payload of the buffer (referenced
 * by mdh_network_stack_memory_manager_get_payload()) must be memory aligned
 * according to the value returned by mdh_emac_get_align().
 *
 * @param[in] self EMAC instance to operate with
 * @param[in] buf  Frame to be send
 * @returns
 *  - MDH_EMAC_STATUS_NO_ERROR on success;
 *  - MDH_EMAC_STATUS_LINK_DOWN if the link is down
 *  - MDH_EMAC_STATUS_BUSY if a transfer is ongoing or pending,
 *
 */
static inline mdh_emac_status_t mdh_emac_transmit(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->transmit));
    return self->vtable->transmit(self, buf);
}

/** Retrieves received data from the Ethernet controller.
 *
 * @param buf refers to a pre-allocated memory buffer, this function is not
 * responsible for freeing the allocated memory. Memory deallocation must be
 * handled by the allocator. The payload of the buffer (referenced
 * by mdh_network_stack_memory_manager_get_payload()) must be memory aligned
 * according to the value returned by mdh_emac_get_align().
 * In an async design, this function is responsible for enabling the Rx IRQ which
 * would have been disabled in an ISR upon notification of data reception.
 *
 * @param[in] self EMAC instance to operate with
 * @param[in] buf  Buffer to store the frame received
 * @returns
 *  - MDH_EMAC_STATUS_NO_ERROR on success;
 *  - MDH_EMAC_STATUS_INTERNAL_ERROR if no data was retrieved
 */
static inline mdh_emac_status_t mdh_emac_receive(mdh_emac_t *const self, const mdh_network_stack_buffer_t *buf)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->receive));
    return self->vtable->receive(self, buf);
}

/**@}*/

#endif // MDH_EMAC_API_H

/** @}*/
