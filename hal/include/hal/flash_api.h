/* Copyright (c) 2017-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_FLASH_API_H
#define MDH_FLASH_API_H

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

#define MDH_FLASH_INVALID_SIZE 0xFFFFFFFFUL

typedef struct mdh_flash_s mdh_flash_t;

typedef struct mdh_flash_vtable_s {
    int32_t (*erase_sector)(mdh_flash_t *self, uint32_t address);
    int32_t (*read)(mdh_flash_t *self, uint32_t address, uint8_t *data, uint32_t size);
    int32_t (*program_page)(mdh_flash_t *self, uint32_t address, const uint8_t *data, uint32_t size);
    uint32_t (*get_sector_size)(const mdh_flash_t *self, uint32_t address);
    uint32_t (*get_page_size)(const mdh_flash_t *self);
    uint32_t (*get_start_address)(const mdh_flash_t *self);
    uint32_t (*get_size)(const mdh_flash_t *self);
    uint8_t (*get_erase_value)(const mdh_flash_t *self);
} mdh_flash_vtable_t;

struct mdh_flash_s {
    const mdh_flash_vtable_t *vfptr;
};

/**
 * \defgroup flash_hal Flash HAL API
 * @{
 */

/** Erase one sector starting at defined address
 *
 * The address should be at sector boundary. This function does not do any check for address alignments
 * @param self The flash object
 * @param address The sector starting address
 * @return 0 for success, -1 for error
 */
static inline int32_t mdh_flash_erase_sector(mdh_flash_t *self, uint32_t address)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->erase_sector));
    return self->vfptr->erase_sector(self, address);
}

/** Read data starting at defined address
 *
 * @param self The flash object
 * @param address Address to begin reading from
 * @param data The buffer to read data into
 * @param size The number of bytes to read
 * @return 0 for success, -1 for error
 */
static inline int32_t mdh_flash_read(mdh_flash_t *self, uint32_t address, uint8_t *data, uint32_t size)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read));
    return self->vfptr->read(self, address, data, size);
}

/** Program pages starting at defined address
 *
 * The pages should not cross multiple sectors.
 * This function does not do any check for address alignments or if size is aligned to a page size.
 * @param self The flash object
 * @param address The sector starting address
 * @param data The data buffer to be programmed
 * @param size The number of bytes to program
 * @return 0 for success, -1 for error
 */
static inline int32_t mdh_flash_program_page(mdh_flash_t *self, uint32_t address, const uint8_t *data, uint32_t size)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->program_page));
    return self->vfptr->program_page(self, address, data, size);
}

/** Get sector size
 *
 * @param self The flash object
 * @param address The sector starting address
 * @return The size of a sector
 */
static inline uint32_t mdh_flash_get_sector_size(const mdh_flash_t *self, uint32_t address)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_sector_size));
    return self->vfptr->get_sector_size(self, address);
}

/** Get page size
 *
 * The page size defines the writable page size
 * @param self The flash object
 * @return The size of a page
 */
static inline uint32_t mdh_flash_get_page_size(const mdh_flash_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_page_size));
    return self->vfptr->get_page_size(self);
}

/** Get start address for the flash region
 *
 * @param self The flash object
 * @return The start address for the flash region
 */
static inline uint32_t mdh_flash_get_start_address(const mdh_flash_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_start_address));
    return self->vfptr->get_start_address(self);
}

/** Get the flash region size
 *
 * @param self The flash object
 * @return The flash region size
 */
static inline uint32_t mdh_flash_get_size(const mdh_flash_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_size));
    return self->vfptr->get_size(self);
}

/** Get the flash erase value
 *
 * @param self The flash object
 * @return The flash erase value
 */
static inline uint8_t mdh_flash_get_erase_value(const mdh_flash_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_erase_value));
    return self->vfptr->get_erase_value(self);
}

/**@}*/

#endif // MDH_FLASH_API_H

/** @}*/
