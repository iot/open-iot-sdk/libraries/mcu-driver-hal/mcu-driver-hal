/* Copyright (c) 2006-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_GPIO_API_H
#define MDH_GPIO_API_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifndef DEVICE_INTERRUPTIN
#define DEVICE_INTERRUPTIN 0
#endif // DEVICE_INTERRUPTIN
#ifndef DEVICE_PORTIN
#define DEVICE_PORTIN 0
#endif // DEVICE_PORTIN
#ifndef DEVICE_PORTOUT
#define DEVICE_PORTOUT 0
#endif // DEVICE_PORTOUT

typedef struct mdh_gpio_s mdh_gpio_t;

typedef enum mdh_gpio_direction_e {
    MDH_GPIO_DIRECTION_IN = 0,
    MDH_GPIO_DIRECTION_OUT,

    // Defined for extension purposes.
    MDH_GPIO_DIRECTION_LAST
} mdh_gpio_direction_t;

typedef enum {
    MDH_GPIO_MODE_PULL_NONE = 0,
    MDH_GPIO_MODE_PULL_DOWN,
    MDH_GPIO_MODE_PULL_UP,
    MDH_GPIO_MODE_OPEN_DRAIN,

    // Defined for extension purposes.
    MDH_GPIO_MODE_LAST
} mdh_gpio_mode_t;

/** GPIO IRQ events
 */
typedef enum mdh_gpio_irq_event_s {
    MDH_GPIO_IRQ_EVENT_NONE = 0,
    MDH_GPIO_IRQ_EVENT_RISE,
    MDH_GPIO_IRQ_EVENT_FALL,
    MDH_GPIO_IRQ_EVENT_BOTH
} mdh_gpio_irq_event_t;

typedef void (*mdh_gpio_irq_callback_f)(void *ctx, mdh_gpio_irq_event_t event);

/** GPIO capabilities for a given pin
 */
typedef struct mdh_gpio_capabilities_s {
    uint8_t pull_none : 1;
    uint8_t pull_down : 1;
    uint8_t pull_up : 1;
} mdh_gpio_capabilities_t;

typedef struct mdh_gpio_vtable_s {
    bool (*is_connected)(const mdh_gpio_t *self);
    void (*set_mode)(mdh_gpio_t *self, mdh_gpio_mode_t mode);
    void (*set_direction)(mdh_gpio_t *self, mdh_gpio_direction_t direction);
    void (*write)(mdh_gpio_t *self, uint32_t value);
    uint32_t (*read)(mdh_gpio_t *self);
    void (*get_capabilities)(mdh_gpio_t *gpio, mdh_gpio_capabilities_t *cap);
#if DEVICE_INTERRUPTIN
    void (*set_irq_callback)(mdh_gpio_t *self, mdh_gpio_irq_callback_f cbk, void *ctx);
    void (*set_irq_availability)(mdh_gpio_t *self, mdh_gpio_irq_event_t event, bool enable);
    void (*enable_irq)(mdh_gpio_t *self);
    void (*disable_irq)(mdh_gpio_t *self);
#endif // DEVICE_INTERRUPTIN
#if DEVICE_PORTIN || DEVICE_PORTOUT
    void (*set_port_mode)(mdh_gpio_t *self, mdh_gpio_mode_t mode);
    void (*set_port_direction)(mdh_gpio_t *self, mdh_gpio_direction_t dir);
    void (*port_write)(mdh_gpio_t *self, uint32_t value);
    uint32_t (*port_read)(mdh_gpio_t *self);
#endif // DEVICE_PORTIN || DEVICE_PORTOUT
} mdh_gpio_vtable_t;

struct mdh_gpio_s {
    const mdh_gpio_vtable_t *vfptr;
};

#if DEVICE_PORTIN || DEVICE_PORTOUT
/**
 * \defgroup hal_port Port HAL functions
 *
 * # Undefined behavior
 * * Using a GPIO object connected to a single pin with a port pin function
 *
 * @{
 */

/** Set the input port mode
 *
 * @param self  The GPIO object
 * @param mode The port mode to be set
 */
static inline void mdh_gpio_set_port_mode(mdh_gpio_t *self, mdh_gpio_mode_t mode)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_port_mode));
    self->vfptr->set_port_mode(self, mode);
}

/** Set port direction (in/out)
 *
 * @param self The GPIO object
 * @param dir The port direction to be set
 */
static inline void mdh_gpio_set_port_direction(mdh_gpio_t *self, mdh_gpio_direction_t direction)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_port_direction));
    self->vfptr->set_port_direction(self, direction);
}

/** Write value to the port
 *
 * @param self   The GPIO object
 * @param value The value to write
 */
static inline void mdh_gpio_port_write(mdh_gpio_t *self, uint32_t value)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->port_write));
    self->vfptr->port_write(self, value);
}

/** Read the current value on the port
 *
 * @param self The GPIO object
 * @return An integer with each bit corresponding to an associated port pin setting
 */
static inline uint32_t mdh_gpio_port_read(mdh_gpio_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->port_read));
    return self->vfptr->port_read(self);
}

#endif // DEVICE_PORTIN || DEVICE_PORTOUT

#if DEVICE_INTERRUPTIN

/**
 * \defgroup hal_gpioirq GPIO IRQ HAL functions
 *
 * # Defined behavior
 * * ::mdh_gpio_set_irq_callback registers the function to be invoked when the interrupt fires.
 * * ::mdh_gpio_set_irq_availability enables/disables pin IRQ event
 * * ::mdh_gpio_enable_irq enables GPIO IRQ
 * * ::mdh_gpio_disable_irq disables GPIO IRQ
 *
 * @{
 */

/** Sets the callback to execute in the IRQ handler
 *
 * @param self     The GPIO object
 * @param cbk      The callbacks to execute when the interrupt fires
 * @param ctx      is an application defined data passed to the callbacks
 */
static inline void mdh_gpio_set_irq_callback(mdh_gpio_t *self, mdh_gpio_irq_callback_f cbk, void *ctx)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_irq_callback));
    self->vfptr->set_irq_callback(self, cbk, ctx);
}

/** Enable/disable pin IRQ event
 *
 * @param self    The GPIO object
 * @param event  The GPIO IRQ event
 * @param enable The enable flag
 */
static inline void mdh_gpio_set_irq_availability(mdh_gpio_t *self, mdh_gpio_irq_event_t event, bool enable)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_irq_availability));
    self->vfptr->set_irq_availability(self, event, enable);
}

/** Enable GPIO IRQ
 *
 * This is target dependent, as it might enable the entire port or just a pin
 * @param self The GPIO object
 */
static inline void mdh_gpio_enable_irq(mdh_gpio_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->enable_irq));
    self->vfptr->enable_irq(self);
}

/** Disable GPIO IRQ
 *
 * This is target dependent, as it might disable the entire port or just a pin
 * @param self The GPIO object
 */
static inline void mdh_gpio_disable_irq(mdh_gpio_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->disable_irq));
    self->vfptr->disable_irq(self);
}

/**@}*/

#endif // DEVICE_INTERRUPTIN

/**
 * \defgroup hal_gpio GPIO HAL functions
 *
 * # Defined behavior
 * * ::mdh_gpio_is_connected can be used to test whether a mdh_gpio_t object was initialized with NC
 * * ::mdh_gpio_set_mode sets the mode of the given pin
 * * ::mdh_gpio_set_direction sets the direction of the given pin
 * * ::mdh_gpio_write sets the gpio output value
 * * ::mdh_gpio_read reads the input value
 * * The GPIO operations ::mdh_gpio_write, ::mdh_gpio_read take less than 20us to complete
 * * The function ::mdh_gpio_get_capabilities fills the given
 * `mdh_gpio_capabilities_t` instance according to pin capabilities.
 *
 * # Undefined behavior
 * * Calling any ::mdh_gpio_set_mode, ::mdh_gpio_set_direction, ::mdh_gpio_write or ::mdh_gpio_read on a mdh_gpio_t
 * object that is not connected to a pin.
 *
 * @{
 */

/** Checks if gpio object is connected (pin was not initialized with NC)
 * @param self The GPIO object
 * @return false if object was initialized with NC
 * @return non-zero if object was initialized with a valid PinName
 **/
static inline bool mdh_gpio_is_connected(const mdh_gpio_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->is_connected));
    return self->vfptr->is_connected(self);
}

/** Set the input pin mode
 *
 * @param self  The GPIO object (must be connected)
 * @param mode The pin mode to be set
 */
static inline void mdh_gpio_set_mode(mdh_gpio_t *self, mdh_gpio_mode_t mode)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_mode));
    self->vfptr->set_mode(self, mode);
}

/** Set the pin direction
 *
 * @param self       The GPIO object (must be connected)
 * @param direction The pin direction to be set
 */
static inline void mdh_gpio_set_direction(mdh_gpio_t *self, mdh_gpio_direction_t direction)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_direction));
    self->vfptr->set_direction(self, direction);
}

/** Set the output value
 *
 * @param self   The GPIO object (must be connected)
 * @param value The value to be set
 */
static inline void mdh_gpio_write(mdh_gpio_t *self, uint32_t value)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->write));
    self->vfptr->write(self, value);
}

/** Read the input value
 *
 * @param self The GPIO object (must be connected)
 * @return An integer value 1 or 0
 */
static inline uint32_t mdh_gpio_read(mdh_gpio_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read));
    return self->vfptr->read(self);
}

/** Fill the given mdh_gpio_capabilities_t instance according to pin capabilities.
 */
static inline void mdh_gpio_get_capabilities(mdh_gpio_t *self, mdh_gpio_capabilities_t *capabilities)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_capabilities));
    self->vfptr->get_capabilities(self, capabilities);
}

/**@}*/

#endif // MDH_GPIO_API_H

/** @}*/
