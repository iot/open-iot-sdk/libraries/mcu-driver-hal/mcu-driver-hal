/* Copyright (c) 2006-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_I2C_API_H
#define MDH_I2C_API_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifndef DEVICE_I2C_PERIPHERAL
#define DEVICE_I2C_PERIPHERAL 0
#endif // DEVICE_I2C_PERIPHERAL
#ifndef DEVICE_I2C_ASYNCH
#define DEVICE_I2C_ASYNCH 0
#endif // DEVICE_I2C_ASYNCH

/**
 * @defgroup hal_I2CEvents I2C Events Macros
 *
 * @{
 */
#define MDH_I2C_EVENT_ERROR               (1 << 1)
#define MDH_I2C_EVENT_ERROR_NO_PERIPHERAL (1 << 2)
#define MDH_I2C_EVENT_TRANSFER_COMPLETE   (1 << 3)
#define MDH_I2C_EVENT_TRANSFER_EARLY_NACK (1 << 4)
#define MDH_I2C_EVENT_ALL                                                                      \
    (MDH_I2C_EVENT_ERROR | MDH_I2C_EVENT_TRANSFER_COMPLETE | MDH_I2C_EVENT_ERROR_NO_PERIPHERAL \
     | MDH_I2C_EVENT_TRANSFER_EARLY_NACK)

/**@}*/
/** I2C HAL structure
 */
typedef struct mdh_i2c_s mdh_i2c_t;

typedef enum mdh_i2c_error_e {
    MDH_I2C_ERROR_NO_ERROR = 0,
    MDH_I2C_ERROR_INVALID_ARG = -1,
    MDH_I2C_ERROR_INTERNAL_ERROR = -2,
    MDH_I2C_ERROR_NO_PERIPHERAL = -3,
    MDH_I2C_ERROR_BUS_BUSY = -4,

    // Defined for extension purposes.
    MDH_I2C_ERROR_LAST = -5
} mdh_i2c_error_t;

typedef struct mdh_i2c_vtable_s {
    mdh_i2c_error_t (*set_frequency)(mdh_i2c_t *self, uint32_t hz);
    int (*send_start)(mdh_i2c_t *self);
    int (*send_stop)(mdh_i2c_t *self);
    int (*read)(mdh_i2c_t *self, uint16_t address, uint8_t *buffer, size_t length, bool add_stop);
    int (*write)(mdh_i2c_t *self, uint16_t address, const uint8_t *buffer, size_t length, bool add_stop);
    void (*reset)(mdh_i2c_t *self);
    int (*read_byte)(mdh_i2c_t *self, uint8_t ack_bit);
    int (*write_byte)(mdh_i2c_t *self, uint8_t data);
#if DEVICE_I2C_PERIPHERAL
    void (*set_mode)(mdh_i2c_t *self, bool is_peripheral);
    int (*was_addressed)(mdh_i2c_t *self);
    int (*peripheral_read)(mdh_i2c_t *self, uint8_t *buffer, size_t length);
    int (*peripheral_write)(mdh_i2c_t *self, const uint8_t *buffer, size_t length);
    void (*set_peripheral_address)(mdh_i2c_t *self, uint32_t address);
#endif // DEVICE_I2C_PERIPHERAL
#if DEVICE_I2C_ASYNCH
    void (*asynch_transfer)(mdh_i2c_t *self,
                            const void *tx,
                            size_t tx_length,
                            void *rx,
                            size_t rx_length,
                            uint32_t address,
                            uint32_t stop,
                            uint32_t cbk,
                            uint32_t event);
    uint32_t (*asynch_irq_handler)(mdh_i2c_t *self);
    bool (*asynch_is_active)(mdh_i2c_t *self);
    void (*asynch_abort)(mdh_i2c_t *self);
#endif // DEVICE_I2C_ASYNCH
} mdh_i2c_vtable_t;

struct mdh_i2c_s {
    const mdh_i2c_vtable_t *vfptr;
};

/**
 * \defgroup hal_GeneralI2C I2C Configuration Functions
 *
 * # Defined behavior
 * * ::mdh_i2c_set_frequency configures the I2C frequency
 * * ::mdh_i2c_send_start sends START command
 * * ::mdh_i2c_read reads `length` bytes from the I2C peripheral specified by `address` to the `buffer`
 * * ::mdh_i2c_read reads generates a stop condition on the bus at the end of the transfer if `add_stop` parameter is
 * non-zero
 * * ::mdh_i2c_read reads returns the number of symbols received from the bus
 * * ::mdh_i2c_write writes `length` bytes to the I2C peripheral specified by `address` from the `buffer`
 * * ::mdh_i2c_write generates a stop condition on the bus at the end of the transfer if `add_stop` parameter is `true`
 * * ::mdh_i2c_write returns zero on success, error code otherwise
 * * ::mdh_i2c_reset resets the I2C peripheral
 * * ::mdh_i2c_read_byte reads and return one byte from the specfied I2C peripheral
 * * ::mdh_i2c_read_byte uses the `ack_bit` parameter to inform the peripheral that all bytes have been read
 * * ::mdh_i2c_write_byte writes one byte to the specified I2C peripheral
 * * ::mdh_i2c_write_byte returns 0 if NAK was received, 1 if ACK was received, 2 for timeout
 * * ::mdh_i2c_set_mode enables/disables I2S peripheral mode
 * * ::mdh_i2c_was_addressed returns: 1 - read addresses, 2 - write to all peripherals, 3 write addressed, 0 - the
 * peripheral has not been addressed
 * * ::mdh_i2c_peripheral_read reads `length` bytes from the I2C controller to the `data` buffer
 * * ::mdh_i2c_peripheral_read returns non-zero if a value is available, 0 otherwise
 * * ::mdh_i2c_peripheral_write writes `length` bytes to the I2C controller from the `data` buffer
 * * ::mdh_i2c_peripheral_write returns non-zero if a value is available, 0 otherwise
 * * ::mdh_i2c_set_peripheral_address configures I2C peripheral address
 * * ::mdh_i2c_asynch_transfer starts I2C asynchronous transfer
 * * ::mdh_i2c_asynch_transfer writes `tx_length` bytes to the I2C peripheral specified by `address` from the `tx`
 * buffer
 * * ::mdh_i2c_asynch_transfer reads `rx_length` bytes from the I2C peripheral specified by `address` to the `rx` buffer
 * * ::mdh_i2c_asynch_transfer generates a stop condition on the bus at the end of the transfer if `add_stop` parameter
 * is `true`
 * * The callback given to ::mdh_i2c_asynch_transfer is invoked when the transfer completes
 * * ::mdh_i2c_asynch_transfer specifies the logical OR of events to be registered
 * * ::mdh_i2c_asynch_irq_handler returns event flags if a transfer termination condition was met, otherwise returns 0.
 * * ::mdh_i2c_asynch_is_active returns non-zero if the I2C module is active or 0 if it is not
 * * ::mdh_i2c_asynch_abort aborts an on-going async transfer
 *
 * # Undefined behavior
 * * Passing an invalid pointer as `self` to any function
 * * Use of a `null` pointer as an argument to any function.
 * * Initialising the peripheral in peripheral mode if peripheral mode is not supported
 * * Operating the peripheral in peripheral mode without first specifying and address using
 * ::mdh_i2c_set_peripheral_address
 * * Setting an address using mdh_i2c_set_peripheral_address after initialising the peripheral in controller mode
 * * Setting an address to an `I2C` reserved value
 * * Specifying an invalid address when calling any `read` or `write` functions
 * * Setting the length of the transfer or receive buffers to larger than the buffers are
 * * Passing an invalid pointer as `cbk`
 * * Calling ::mdh_i2c_abort_async when no transfer is currently in progress
 *
 *
 * @{
 */

/** Configure the I2C frequency
 *
 *  @param self The I2C object
 *  @param hz  Frequency in Hz
 *  @returns
 *  - MDH_I2C_ERROR_NO_ERROR on success;
 *  - Otherwise, one of the other mdh_i2c_error_t values.
 */
static inline mdh_i2c_error_t mdh_i2c_set_frequency(mdh_i2c_t *self, uint32_t hz)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_frequency));
    return self->vfptr->set_frequency(self, hz);
}

/** Send START command
 *
 *  @param self The I2C object
 */
static inline int mdh_i2c_send_start(mdh_i2c_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->send_start));
    return self->vfptr->send_start(self);
}

/** Send STOP command
 *
 *  @param self The I2C object
 */
static inline int mdh_i2c_send_stop(mdh_i2c_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->send_stop));
    return self->vfptr->send_stop(self);
}

/** Blocking reading data
 *
 *  @note
 *  The address must be received already formatted for addressing a peripheral with the R/W bit set
 *
 *  @param self The I2C object
 *  @param address 7-bit or 10-bit I2C address
 *  @param buffer The buffer for receiving
 *  @param length Number of bytes to read
 *  @param add_stop Whether or not a stop bit must be added after the transfer is done
 *  @return zero or non-zero - Number of bytes read
 *          negative - mdh_i2c_error_t status
 */
static inline int mdh_i2c_read(mdh_i2c_t *self, uint16_t address, uint8_t *buffer, size_t length, bool add_stop)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read));
    return self->vfptr->read(self, address, buffer, length, add_stop);
}

/** Blocking sending data
 *
 *  @note
 *  The address must be received already formatted for addressing a peripheral with the R/W bit clear
 *
 *  @param self The I2C object
 *  @param address 7-bit or 10-bit I2C address
 *  @param buffer The buffer for sending
 *  @param length Number of bytes to write
 *  @param add_stop Whether or not a stop bit must be added after the transfer is done
 *  @return zero or non-zero - Number of written bytes
 *          negative - mdh_i2c_error_t status
 */
static inline int mdh_i2c_write(mdh_i2c_t *self, uint16_t address, const uint8_t *buffer, size_t length, bool add_stop)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->write));
    return self->vfptr->write(self, address, buffer, length, add_stop);
}

/** Reset I2C peripheral. TODO: The action here. Most of the implementation sends stop()
 *
 *  @param self The I2C object
 */
static inline void mdh_i2c_reset(mdh_i2c_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->reset));
    self->vfptr->reset(self);
}

/** Read one byte
 *
 *  @param self The I2C object
 *  @param ack_bit The Acknowledge (ACK) bit
 *  @return The byte read
 */
static inline int mdh_i2c_read_byte(mdh_i2c_t *self, uint8_t ack_bit)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read_byte));
    return self->vfptr->read_byte(self, ack_bit);
}

/** Write one byte
 *
 *  @param self The I2C object
 *  @param data Byte to be written
 *  @return 0 if NAK was received, 1 if ACK was received, 2 for timeout.
 */
static inline int mdh_i2c_write_byte(mdh_i2c_t *self, uint8_t data)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->write_byte));
    return self->vfptr->write_byte(self, data);
}

/**@}*/

#if DEVICE_I2C_PERIPHERAL

/**
 * \defgroup SynchI2C Synchronous I2C Hardware Abstraction Layer for peripheral
 * @{
 */

/** Configure I2C as peripheral or controller.
 *  @param self The I2C object
 *  @param is_peripheral Enable i2c hardware so you can receive events with ::mdh_i2c_was_addressed
 *  @return non-zero if a value is available
 */
static inline void mdh_i2c_set_mode(mdh_i2c_t *self, bool is_peripheral)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_mode));
    self->vfptr->set_mode(self, is_peripheral);
}

/** Check to see if the I2C peripheral has been addressed.
 *  @param self The I2C object
 *  @return The status - 1 - read addresses, 2 - write to all peripherals,
 *         3 write addressed, 0 - the peripheral has not been addressed
 */
static inline int mdh_i2c_was_addressed(mdh_i2c_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->was_addressed));
    return self->vfptr->was_addressed(self);
}

/** Read data as a peripheral.
 *  @param self The I2C object
 *  @param buffer The buffer for receiving
 *  @param length Number of bytes to read
 *  @return non-zero if a value is available
 */
static inline int mdh_i2c_peripheral_read(mdh_i2c_t *self, uint8_t *buffer, size_t length)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->peripheral_read));
    return self->vfptr->peripheral_read(self, buffer, length);
}

/** Write data as a peripheral.
 *  @param self The I2C object
 *  @param buffer The buffer for sending
 *  @param length Number of bytes to write
 *  @return non-zero if a value is available
 */
static inline int mdh_i2c_peripheral_write(mdh_i2c_t *self, const uint8_t *buffer, size_t length)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->peripheral_write));
    return self->vfptr->peripheral_write(self, buffer, length);
}

/** Configure I2C address.
 *  @param self     The I2C object
 *  @param address The address to be set
 */
static inline void mdh_i2c_set_peripheral_address(mdh_i2c_t *self, uint32_t address)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_peripheral_address));
    self->vfptr->set_peripheral_address(self, address);
}

#endif // DEVICE_I2C_PERIPHERAL

/**@}*/

#if DEVICE_I2C_ASYNCH

/**
 * \defgroup hal_AsynchI2C Asynchronous I2C Hardware Abstraction Layer
 * @{
 */

/** Start I2C asynchronous transfer
 *
 *  @param self The I2C object
 *  @param tx The transmit buffer
 *  @param tx_length The number of bytes to transmit
 *  @param rx The receive buffer
 *  @param rx_length The number of bytes to receive
 *  @param address The address to be set - 7bit or 9bit
 *  @param stop If true, stop will be generated after the transfer is done
 *  @param cbk Memory address location of the callback to execute from an I2C IRQ
 *  @param event Event mask for the transfer. See \ref hal_I2CEvents
 */
static inline void mdh_i2c_asynch_transfer(mdh_i2c_t *self,
                                           const void *tx,
                                           size_t tx_length,
                                           void *rx,
                                           size_t rx_length,
                                           uint32_t address,
                                           uint32_t stop,
                                           uint32_t cbk,
                                           uint32_t event)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_transfer));
    self->vfptr->asynch_transfer(self, tx, tx_length, rx, rx_length, address, stop, cbk, event);
}

/** The asynchronous IRQ handler
 *
 *  @param self The I2C object which holds the transfer information
 *  @return Event flags if a transfer termination condition was met, otherwise return 0.
 */
static inline uint32_t mdh_i2c_asynch_irq_handler(mdh_i2c_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_irq_handler));
    return self->vfptr->asynch_irq_handler(self);
}

/** Attempts to determine if the I2C peripheral is already in use
 *
 *  @param self The I2C object
 *  @return Non-zero if the I2C module is active or zero if it is not
 */
static inline bool mdh_i2c_asynch_is_active(mdh_i2c_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_is_active));
    return self->vfptr->asynch_is_active(self);
}

/** Abort asynchronous transfer
 *
 *  This function does not perform any check - that should happen in upper layers.
 *  @param self The I2C object
 */
static inline void mdh_i2c_asynch_abort(mdh_i2c_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_abort));
    self->vfptr->asynch_abort(self);
}

#endif // DEVICE_I2C_ASYNCH

/**@}*/

#endif // MDH_I2C_API_H

/** @}*/
