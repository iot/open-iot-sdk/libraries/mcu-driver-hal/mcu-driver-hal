/* Copyright (c) 2015-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_LP_TICKER_API_H
#define MDH_LP_TICKER_API_H

#include "hal/ticker_types.h"

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

/**
 * \defgroup hal_lp_ticker Low Power Ticker
 * Low level interface to the low power ticker of a target
 *
 * # Defined behavior
 * * Has a reported frequency between 4KHz and 64KHz - verified by ::lp_ticker_info_test
 * * Has a counter that is at least 12 bits wide - verified by ::lp_ticker_info_test
 * * Continues operating in deep sleep mode
 * * All behavior defined by the @ref hal_ticker_shared "ticker specification"
 *
 * # Undefined behavior
 * * See the @ref hal_ticker_shared "ticker specification"
 *
 * # Potential bugs
 * * Glitches due to ripple counter - Verified by ::lp_ticker_glitch_test
 * @{
 */

/* HAL lp ticker */

/** Read the current tick
 *
 * If no rollover has occurred, the seconds passed since ticker was initialized
 * can be found by dividing the ticks returned by this function
 * by the frequency returned by ::lp_ticker_get_info.
 *
 * @return The current timer's counter value in ticks
 *
 * Pseudo Code:
 * @code
 * uint32_t lp_ticker_read()
 * {
 *     uint16_t count;
 *     uint16_t last_count;
 *
 *     // Loop until the same tick is read twice since this
 *     // is ripple counter on a different clock domain.
 *     count = LPTMR_COUNT;
 *     do {
 *         last_count = count;
 *         count = LPTMR_COUNT;
 *     } while (last_count != count);
 *
 *     return count;
 * }
 * @endcode
 */
static inline uint32_t mdh_lp_ticker_read(mdh_ticker_t *self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->read));
    return self->vtable->read(self);
}

/** Set interrupt for specified timestamp
 *
 * @param timestamp The time in ticks to be set
 *
 * @note calling this function with timestamp of more than the supported
 * number of bits returned by ::lp_ticker_get_info results in undefined
 * behavior.
 *
 * Pseudo Code:
 * @code
 * void lp_ticker_set_interrupt(timestamp_t timestamp)
 * {
 *     LPTMR_COMPARE = timestamp;
 *     LPTMR_CTRL |= LPTMR_CTRL_COMPARE_ENABLE_Msk;
 * }
 * @endcode
 */
static inline void mdh_lp_ticker_set_interrupt(mdh_ticker_t *self, timestamp_t timestamp)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->set_interrupt));
    self->vtable->set_interrupt(self, timestamp);
}

/** Disable low power ticker interrupt
 *
 * Pseudo Code:
 * @code
 * void lp_ticker_disable_interrupt(void)
 * {
 *     // Disable the compare interrupt
 *     LPTMR_CTRL &= ~LPTMR_CTRL_COMPARE_ENABLE_Msk;
 * }
 * @endcode
 */
static inline void mdh_lp_ticker_disable_interrupt(mdh_ticker_t *self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->disable_interrupt));
    self->vtable->disable_interrupt(self);
}

/** Clear the low power ticker interrupt
 *
 * Pseudo Code:
 * @code
 * void lp_ticker_clear_interrupt(void)
 * {
 *     // Write to the ICR (interrupt clear register) of the LPTMR
 *     LPTMR_ICR = LPTMR_ICR_COMPARE_Msk;
 * }
 * @endcode
 */
static inline void mdh_lp_ticker_clear_interrupt(mdh_ticker_t *self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->clear_interrupt));
    self->vtable->clear_interrupt(self);
}

/** Set pending interrupt that should be fired right away.
 *
 * Pseudo Code:
 * @code
 * void lp_ticker_fire_interrupt(void)
 * {
 *     NVIC_SetPendingIRQ(LPTMR_IRQn);
 * }
 * @endcode
 */
static inline void mdh_lp_ticker_fire_interrupt(mdh_ticker_t *self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->fire_interrupt));
    self->vtable->fire_interrupt(self);
}

/** Set low power ticker IRQ handler
 *
 * @param ticker_irq_handler IRQ handler to be connected
 *
 * @return previous ticker IRQ handler
 *
 * @note by default IRQ handler is set to ::ticker_irq_handler
 * @note this function is primarily for testing purposes and it's not required part of HAL implementation
 *
 */
static inline ticker_irq_callback_f mdh_lp_ticker_set_irq_handler(mdh_ticker_t *self, ticker_irq_callback_f callback)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->set_irq_handler));
    return self->vtable->set_irq_handler(self, callback);
}

/** Get frequency and counter bits of this ticker.
 *
 * Pseudo Code:
 * @code
 * const ticker_info_t* lp_ticker_get_info()
 * {
 *     static const ticker_info_t info = {
 *         32768,      // 32KHz
 *         16          // 16 bit counter
 *     };
 *     return &info;
 * }
 * @endcode
 */
static inline const ticker_info_t *mdh_lp_ticker_get_info(mdh_ticker_t *self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->get_info));
    return self->vtable->get_info(self);
}

/**@}*/

#endif // MDH_LP_TICKER_API_H

/** @}*/
