/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_NETWORK_STACK_MEMORY_MANAGER_H
#define MDH_NETWORK_STACK_MEMORY_MANAGER_H

#include <assert.h>
#include <stddef.h>

typedef struct mdh_network_stack_memory_manager_s mdh_network_stack_memory_manager_t;

typedef void mdh_network_stack_buffer_t;

typedef struct mdh_network_stack_memory_manager_vtable_s {
    mdh_network_stack_buffer_t *(*alloc_from_heap)(mdh_network_stack_memory_manager_t *const self,
                                                   size_t size,
                                                   size_t align);
    mdh_network_stack_buffer_t *(*alloc_from_static_pool)(mdh_network_stack_memory_manager_t *const self,
                                                          size_t size,
                                                          size_t align);
    size_t (*get_static_pool_alloc_unit)(mdh_network_stack_memory_manager_t *const self, size_t align);
    void (*free)(mdh_network_stack_memory_manager_t *const self, const mdh_network_stack_buffer_t *buf);
    void (*copy)(mdh_network_stack_memory_manager_t *const self,
                 const mdh_network_stack_buffer_t *const source,
                 const mdh_network_stack_buffer_t *destination);
    size_t (*copy_to_buf)(mdh_network_stack_memory_manager_t *const self,
                          const void *const ptr,
                          size_t size,
                          const mdh_network_stack_buffer_t *buf);
    size_t (*copy_from_buf)(mdh_network_stack_memory_manager_t *const self,
                            const mdh_network_stack_buffer_t *const buf,
                            void *const ptr,
                            size_t size);
    void *(*get_payload)(mdh_network_stack_memory_manager_t *const self, const mdh_network_stack_buffer_t *const buf);
    size_t (*get_payload_len)(mdh_network_stack_memory_manager_t *const self,
                              const mdh_network_stack_buffer_t *const buf);
    void (*set_payload_len)(mdh_network_stack_memory_manager_t *const self,
                            const mdh_network_stack_buffer_t *buf,
                            size_t size);
} mdh_network_stack_memory_manager_vtable_t;

struct mdh_network_stack_memory_manager_s {
    const mdh_network_stack_memory_manager_vtable_t *vtable;
};

/** Allocates memory buffer from the heap
 *
 * Memory buffer allocated from heap is always contiguous and can have arbitrary size.
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in] size Size of the memory to allocate in bytes
 * @param[in] align Memory alignment requirement in bytes
 * @return Allocated memory buffer, or NULL in case of error
 */
static inline mdh_network_stack_buffer_t *mdh_network_stack_memory_manager_alloc_from_heap(
    mdh_network_stack_memory_manager_t *const self, size_t size, size_t align)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->alloc_from_heap));
    return self->vtable->alloc_from_heap(self, size, align);
}

/** Allocates memory buffer from a static pool
 *
 * Memory allocated from pool is contiguous and will typically come from
 * fixed-size packet pool memory.
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in]  size Total size of the memory to allocate in bytes
 * @param[in]  align Memory alignment requirement for each buffer in bytes
 * @return Allocated memory buffer, or NULL in case of error
 */
static inline mdh_network_stack_buffer_t *mdh_network_stack_memory_manager_alloc_from_static_pool(
    mdh_network_stack_memory_manager_t *const self, size_t size, size_t align)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->alloc_from_static_pool));
    return self->vtable->alloc_from_static_pool(self, size, align);
}

/** Get memory buffer pool allocation unit
 *
 * Returns the maximum size of contiguous memory that can be allocated from a pool.
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in] align Memory alignment requirement in bytes
 * @return Contiguous memory size
 */
static inline size_t
mdh_network_stack_memory_manager_get_static_pool_alloc_unit(mdh_network_stack_memory_manager_t *const self,
                                                            size_t align)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->get_static_pool_alloc_unit));
    return self->vtable->get_static_pool_alloc_unit(self, align);
}

/** Free memory buffer
 *
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in] buf Memory buffer to be freed.
 */
static inline void mdh_network_stack_memory_manager_free(mdh_network_stack_memory_manager_t *const self,
                                                         const mdh_network_stack_buffer_t *buf)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->free));
    return self->vtable->free(self, buf);
}

/** Copies data from one buffer to another.
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in] source Memory buffer to copy from
 * @param[in, out] destination Memory buffer to copy to
 */
static inline void mdh_network_stack_memory_manager_copy(mdh_network_stack_memory_manager_t *const self,
                                                         const mdh_network_stack_buffer_t *const source,
                                                         const mdh_network_stack_buffer_t *destination)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->copy));
    self->vtable->copy(self, source, destination);
}

/** Copies data to a buffer.
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in] ptr Pointer to data
 * @param[in] size Data length
 * @param[out] buf Memory buffer to copy to
 * @return Total number of bytes copied
 */
static inline size_t mdh_network_stack_memory_manager_copy_to_buf(mdh_network_stack_memory_manager_t *const self,
                                                                  const void *const ptr,
                                                                  size_t size,
                                                                  const mdh_network_stack_buffer_t *buf)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->copy_to_buf));
    return self->vtable->copy_to_buf(self, ptr, size, buf);
}

/** Copies data from a memory buffer.
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in] buf Memory buffer to copy from
 * @param[out] ptr Pointer to data
 * @param[out] size Data length
 * @return Length of the data that was copied
 */
static inline size_t mdh_network_stack_memory_manager_copy_from_buf(mdh_network_stack_memory_manager_t *const self,
                                                                    const mdh_network_stack_buffer_t *const buf,
                                                                    void *const ptr,
                                                                    size_t size)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->copy_from_buf));
    return self->vtable->copy_from_buf(self, buf, ptr, size);
}

/** Returns pointer to the payload of the buffer
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in] buf Memory buffer
 * @return Pointer to the payload
 */
static inline void *mdh_network_stack_memory_manager_get_payload(mdh_network_stack_memory_manager_t *const self,
                                                                 const mdh_network_stack_buffer_t *const buf)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->get_payload));
    return self->vtable->get_payload(self, buf);
}

/** Return payload size of the buffer
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in] buf Memory buffer
 * @return Size in bytes
 */
static inline size_t mdh_network_stack_memory_manager_get_payload_len(mdh_network_stack_memory_manager_t *const self,
                                                                      const mdh_network_stack_buffer_t *const buf)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->get_payload_len));
    return self->vtable->get_payload_len(self, buf);
}

/** Sets the payload size of the buffer
 *
 * The allocated payload size will not change.
 *
 * @param[in] self Network stack memory manager instance to operate with
 * @param[in, out] buf Memory buffer
 * @param[in] size Payload size, must be less or equal to the allocated size
 */
static inline void mdh_network_stack_memory_manager_set_payload_len(mdh_network_stack_memory_manager_t *const self,
                                                                    const mdh_network_stack_buffer_t *buf,
                                                                    size_t size)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->set_payload_len));
    return self->vtable->set_payload_len(self, buf, size);
}

#endif // MDH_NETWORK_STACK_MEMORY_MANAGER_H

/** @}*/
