/* Copyright (c) 2006-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_PWMOUT_API_H
#define MDH_PWMOUT_API_H

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

/** Pwmout hal structure. mdh_pwmout_s is declared in the target's hal
 */
typedef struct mdh_pwmout_s mdh_pwmout_t;

typedef struct mdh_pwmout_vtable_s {
    void (*write)(mdh_pwmout_t *self, float percent);
    float (*read)(mdh_pwmout_t *self);
    void (*set_period)(mdh_pwmout_t *self, float seconds);
    void (*set_period_ms)(mdh_pwmout_t *self, int ms);
    void (*set_period_us)(mdh_pwmout_t *self, int us);
    int (*get_period_us)(mdh_pwmout_t *self);
    void (*set_pulsewidth)(mdh_pwmout_t *self, float seconds);
    void (*set_pulsewidth_ms)(mdh_pwmout_t *self, int ms);
    void (*set_pulsewidth_us)(mdh_pwmout_t *self, int us);
    int (*get_pulsewidth_us)(mdh_pwmout_t *self);
} mdh_pwmout_vtable_t;

struct mdh_pwmout_s {
    const mdh_pwmout_vtable_t *vfptr;
};

/**
 * \defgroup hal_pwmout Pwmout hal functions
 *
 * # Defined behavior
 * * ::mdh_pwmout_write sets the output duty-cycle in range <0.0f, 1.0f>
 * * ::mdh_pwmout_read returns the current float-point output duty-cycle in range <0.0f, 1.0f>
 * * ::mdh_pwmout_period sets the PWM period specified in seconds, keeping the duty cycle the same
 * * ::mdh_pwmout_period_ms sets the PWM period specified in milliseconds, keeping the duty cycle the same
 * * ::mdh_pwmout_period_us sets the PWM period specified in microseconds, keeping the duty cycle the same
 * * ::mdh_pwmout_get_period_us reads the PWM period specified in microseconds
 * * ::mdh_pwmout_pulsewidth sets the PWM pulsewidth specified in seconds, keeping the period the same
 * * ::mdh_pwmout_pulsewidth_ms sets the PWM pulsewidth specified in milliseconds, keeping the period the same
 * * ::mdh_pwmout_pulsewidth_us sets the PWM pulsewidth specified in microseconds, keeping the period the same
 * * ::mdh_pwmout_get_pulsewidth_us read the PWM pulsewidth specified in microseconds
 * * The accuracy of the PWM is +/- 10%
 * * The PWM operations ::mdh_pwmout_write, ::mdh_pwmout_read, ::mdh_pwmout_read, ::mdh_pwmout_period_ms,
 * ::mdh_pwmout_period_us
 *   ::mdh_pwmout_pulsewidth, ::mdh_pwmout_pulsewidth_ms, ::mdh_pwmout_pulsewidth_us take less than 20us to complete
 *
 * @{
 */

/** Set the output duty-cycle in range <0.0f, 1.0f>
 *
 * Value 0.0f represents 0 percentage, 1.0f represents 100 percent.
 * @param self     The pwmout object
 * @param percent The floating-point percentage number
 */
static inline void mdh_pwmout_write(mdh_pwmout_t *self, float percent)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->write));
    self->vfptr->write(self, percent);
}

/** Read the current float-point output duty-cycle
 *
 * @param self The pwmout object
 * @return A floating-point output duty-cycle
 */
static inline float mdh_pwmout_read(mdh_pwmout_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read));
    return self->vfptr->read(self);
}

/** Set the PWM period specified in seconds, keeping the duty cycle the same
 *
 * Periods smaller than microseconds (the lowest resolution) are set to zero.
 * @param self     The pwmout object
 * @param seconds The floating-point seconds period
 */
static inline void mdh_pwmout_set_period(mdh_pwmout_t *self, float seconds)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_period));
    self->vfptr->set_period(self, seconds);
}

/** Set the PWM period specified in milliseconds, keeping the duty cycle the same
 *
 * @param self The pwmout object
 * @param ms  The milisecond period
 */
static inline void mdh_pwmout_set_period_ms(mdh_pwmout_t *self, int ms)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_period_ms));
    self->vfptr->set_period_ms(self, ms);
}

/** Set the PWM period specified in microseconds, keeping the duty cycle the same
 *
 * @param self The pwmout object
 * @param us  The microsecond period
 */
static inline void mdh_pwmout_set_period_us(mdh_pwmout_t *self, int us)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_period_us));
    self->vfptr->set_period_us(self, us);
}

/** Read the PWM period specified in microseconds
 *
 * @param self The pwmout object
 * @return A int output period
 */
static inline int mdh_pwmout_get_period_us(mdh_pwmout_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_period_us));
    return self->vfptr->get_period_us(self);
}

/** Set the PWM pulsewidth specified in seconds, keeping the period the same.
 *
 * @param self     The pwmout object
 * @param seconds The floating-point pulsewidth in seconds
 */
static inline void mdh_pwmout_set_pulsewidth(mdh_pwmout_t *self, float seconds)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_pulsewidth));
    self->vfptr->set_pulsewidth(self, seconds);
}

/** Set the PWM pulsewidth specified in milliseconds, keeping the period the same.
 *
 * @param self The pwmout object
 * @param ms  The floating-point pulsewidth in milliseconds
 */
static inline void mdh_pwmout_set_pulsewidth_ms(mdh_pwmout_t *self, int ms)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_pulsewidth_ms));
    self->vfptr->set_pulsewidth_ms(self, ms);
}

/** Set the PWM pulsewidth specified in microseconds, keeping the period the same.
 *
 * @param self The pwmout object
 * @param us  The floating-point pulsewidth in microseconds
 */
static inline void mdh_pwmout_set_pulsewidth_us(mdh_pwmout_t *self, int us)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_pulsewidth_us));
    self->vfptr->set_pulsewidth_us(self, us);
}

/** Read the PWM pulsewidth specified in microseconds
 *
 * @param self The pwmout object
 * @return A int output pulsewitdth
 */
static inline int mdh_pwmout_get_pulsewidth_us(mdh_pwmout_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_pulsewidth_us));
    return self->vfptr->get_pulsewidth_us(self);
}

/**@}*/

#endif // MDH_PWMOUT_API_H

/** @}*/
