/* Copyright (c) 2017-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_QSPI_API_H
#define MDH_QSPI_API_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * \defgroup hal_qspi QSPI HAL
 * @{
 */

/** QSPI HAL object
 */
typedef struct mdh_qspi_s mdh_qspi_t;

/** QSPI Bus width
 *
 * Some parts of commands provide variable bus width
 */
typedef enum mdh_qspi_bus_width_e {
    MDH_QSPI_BUS_WIDTH_SINGLE,
    MDH_QSPI_BUS_WIDTH_DUAL,
    MDH_QSPI_BUS_WIDTH_QUAD,
} mdh_qspi_bus_width_t;

/** Address size in bits
 */
typedef enum mdh_qspi_address_size_e {
    MDH_QSPI_ADDRESS_SIZE_8,
    MDH_QSPI_ADDRESS_SIZE_16,
    MDH_QSPI_ADDRESS_SIZE_24,
    MDH_QSPI_ADDRESS_SIZE_32,
} mdh_qspi_address_size_t;

/** Alternative size in bits
 */
typedef uint8_t mdh_qspi_alt_size_t;

// The following defines are provided for backwards compatibilty. New code should explicitly
// specify the required number of alt bits.
#define MDH_QSPI_ALT_ADDRESS_SIZE_8  8U
#define MDH_QSPI_ALT_ADDRESS_SIZE_16 16U
#define MDH_QSPI_ALT_ADDRESS_SIZE_24 24U
#define MDH_QSPI_ALT_ADDRESS_SIZE_32 32U

/** QSPI command
 *
 * Defines a frame format. It consists of instruction, address, alternative, dummy count and data
 */
typedef struct mdh_qspi_command_s {
    struct {
        mdh_qspi_bus_width_t bus_width; /**< Bus width for the instruction >*/
        uint8_t value;                  /**< Instruction value >*/
        bool disabled;                  /**< Instruction phase skipped if disabled is set to true >*/
    } instruction;
    struct {
        mdh_qspi_bus_width_t bus_width; /**< Bus width for the address >*/
        mdh_qspi_address_size_t size;   /**< Address size >*/
        uint32_t value;                 /**< Address value >*/
        bool disabled;                  /**< Address phase skipped if disabled is set to true >*/
    } address;
    struct {
        mdh_qspi_bus_width_t bus_width; /**< Bus width for alternative  >*/
        mdh_qspi_alt_size_t size;       /**< Alternative size >*/
        uint32_t value;                 /**< Alternative value >*/
        bool disabled;                  /**< Alternative phase skipped if disabled is set to true >*/
    } alt;
    uint8_t dummy_count; /**< Dummy cycles count >*/
    struct {
        mdh_qspi_bus_width_t bus_width; /**< Bus width for data >*/
    } data;
} mdh_qspi_command_t;

/** QSPI return status
 */
typedef enum mdh_qspi_status_e {
    MDH_QSPI_STATUS_ERROR = -1,             /**< Generic error >*/
    MDH_QSPI_STATUS_INVALID_PARAMETER = -2, /**< The parameter is invalid >*/
    MDH_QSPI_STATUS_OK = 0,                 /**< Function executed sucessfully  >*/
} mdh_qspi_status_t;

typedef struct mdh_qspi_vtable_s {
    mdh_qspi_status_t (*set_mode)(mdh_qspi_t *self, uint8_t mode);
    mdh_qspi_status_t (*set_frequency)(mdh_qspi_t *self, uint32_t hz);
    mdh_qspi_status_t (*write)(mdh_qspi_t *self, const mdh_qspi_command_t *command, const void *data, size_t *length);
    mdh_qspi_status_t (*read)(mdh_qspi_t *self, const mdh_qspi_command_t *command, void *data, size_t *length);
    mdh_qspi_status_t (*transfer)(mdh_qspi_t *self,
                                  const mdh_qspi_command_t *command,
                                  const void *tx_data,
                                  size_t tx_size,
                                  void *rx_data,
                                  size_t rx_size);
} mdh_qspi_vtable_t;

struct mdh_qspi_s {
    const mdh_qspi_vtable_t *vfptr;
};

/** Set the QSPI mode
 *
 * Sets clock polarity and phase mode. The clock for the peripheral should be enabled
 *
 * @param self QSPI object
 * @param mode Clock polarity and phase mode (0 - 3)
 * @return MDH_QSPI_STATUS_OK if initialisation successfully executed
           MDH_QSPI_STATUS_INVALID_PARAMETER if invalid parameter found
           MDH_QSPI_STATUS_ERROR otherwise
 */
static inline mdh_qspi_status_t mdh_qspi_set_mode(mdh_qspi_t *self, uint8_t mode)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_mode));
    return self->vfptr->set_mode(self, mode);
}

/** Set the QSPI baud rate
 *
 * Actual frequency may differ from the desired frequency due to available dividers and the bus clock
 * Configures the QSPI peripheral's baud rate
 * @param self The QSPI object to configure
 * @param hz  The baud rate in Hz
 * @return MDH_QSPI_STATUS_OK if frequency was set
           MDH_QSPI_STATUS_INVALID_PARAMETER if invalid parameter found
           MDH_QSPI_STATUS_ERROR otherwise
 */
static inline mdh_qspi_status_t mdh_qspi_set_frequency(mdh_qspi_t *self, uint32_t hz)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_frequency));
    return self->vfptr->set_frequency(self, hz);
}

/** Send a command and block of data
 *
 * @param self QSPI object
 * @param command QSPI command
 * @param data TX buffer
 * @param[in,out] length in - TX buffer length in bytes, out - number of bytes written
 * @return MDH_QSPI_STATUS_OK if the data has been succesfully sent
           MDH_QSPI_STATUS_INVALID_PARAMETER if invalid parameter found
           MDH_QSPI_STATUS_ERROR otherwise
 */
static inline mdh_qspi_status_t
mdh_qspi_write(mdh_qspi_t *self, const mdh_qspi_command_t *command, const void *data, size_t *length)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->write));
    return self->vfptr->write(self, command, data, length);
}

/** Send a command and receive a block of data
 *
 * @param self QSPI object
 * @param command QSPI command
 * @param data RX buffer
 * @param[in,out] length in - RX buffer length in bytes, out - number of bytes read
 * @return MDH_QSPI_STATUS_OK if data has been succesfully received
           MDH_QSPI_STATUS_INVALID_PARAMETER if invalid parameter found
           MDH_QSPI_STATUS_ERROR otherwise
 */
static inline mdh_qspi_status_t
mdh_qspi_read(mdh_qspi_t *self, const mdh_qspi_command_t *command, void *data, size_t *length)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->read));
    return self->vfptr->read(self, command, data, length);
}

/** Send a command (and optionally data) and get the response. Can be used to send/receive device specific commands
 *
 * @param self QSPI object
 * @param command QSPI command
 * @param tx_data TX buffer
 * @param tx_size TX buffer length in bytes
 * @param rx_data RX buffer
 * @param rx_size RX buffer length in bytes
 * @return MDH_QSPI_STATUS_OK if the data has been succesfully sent
           MDH_QSPI_STATUS_INVALID_PARAMETER if invalid parameter found
           MDH_QSPI_STATUS_ERROR otherwise
 */
static inline mdh_qspi_status_t mdh_qspi_transfer(mdh_qspi_t *self,
                                                  const mdh_qspi_command_t *command,
                                                  const void *tx_data,
                                                  size_t tx_size,
                                                  void *rx_data,
                                                  size_t rx_size)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->transfer));
    return self->vfptr->transfer(self, command, tx_data, tx_size, rx_data, rx_size);
}

/**@}*/

#endif // MDH_QSPI_API_H

/** @}*/
