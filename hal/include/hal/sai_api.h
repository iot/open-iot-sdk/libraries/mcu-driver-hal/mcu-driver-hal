/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_SAI_API_H
#define MDH_SAI_API_H

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

/**
 * \defgroup hal_sai Serial audio interface hal functions
 * Low level interface to the serial audio interface of a target.
 *
 * # Common patterns
 * `self` parameters represent the driver's instance.
 * `ctx` is an application defined data passed to the completion callback. It may be used to identify
 * the transfer and/or wake a blocked task.
 * `cbk` is a callback used to notify completion or out of band events.
 *
 * # Defined behaviours
 * - Samples must be aligned to the nearest power of 2 of bytes capable to hold them (e.g. 24bit
 *   samples in 4bytes/32bits integers). Only the least significant bits are used.
 * - `sai_transfer()` returns `MDH_SAI_STATUS_NO_ERROR` if it accepts the request.
 *   The transfer may be enqueued and processed later. any buffer passed must live until the completion
 *   callback is invoked.
 * - `sai_transfer()` returns `MDH_SAI_STATUS_BUSY` if the driver cannot accept the request, for
 *   example if its queue is full.
 * - `len` of 0 may be used to check if the driver is ready to accept a new transfer.
 *
 * # Undefined behaviours
 * - `NULL` value for `psample` is a programming error and the driver may either ignore the call or
 *   assert (relying on the system assert implementation).
 *
 * @{
 */

typedef struct mdh_sai_s mdh_sai_t;
typedef enum mdh_sai_status_e {
    MDH_SAI_STATUS_NO_ERROR,
    MDH_SAI_STATUS_BUSY,

    // defined for extension purposes
    MDH_SAI_STATUS_LAST
} mdh_sai_status_t;

typedef enum mdh_sai_event_e { MDH_SAI_EVENT_UNDERRUN, MDH_SAI_EVENT_OVERRUN } mdh_sai_event_t;

typedef enum mdh_sai_transfer_complete_e {
    MDH_SAI_TRANSFER_COMPLETE_DONE,
    MDH_SAI_TRANSFER_COMPLETE_CANCELLED,
} mdh_sai_transfer_complete_t;

typedef void (*mdh_sai_transfer_complete_f)(mdh_sai_t *self, void *ctx, mdh_sai_transfer_complete_t code);

typedef void (*mdh_sai_event_f)(mdh_sai_t *self, mdh_sai_event_t code);

// vtable
typedef struct mdh_sai_vtable_s {
    mdh_sai_status_t (*set_transfer_complete_callback)(mdh_sai_t *self, mdh_sai_transfer_complete_f cbk);
    mdh_sai_status_t (*set_event_callback)(mdh_sai_t *self, mdh_sai_event_f cbk);

    mdh_sai_status_t (*transfer)(mdh_sai_t *self, uint8_t *samples, uint32_t len, void *ctx);
    mdh_sai_status_t (*cancel_transfer)(mdh_sai_t *self);
} mdh_sai_vtable_t;

// interface definition
struct mdh_sai_s {
    const mdh_sai_vtable_t *vtable;
};

/**
 * Set up the callback to be invoked when a synchronous event occurs.
 *
 * @warning Must only be called when the device is idle (no active or pending transfer).
 *
 * @return MDH_SAI_STATUS_BUSY if a transfer is ongoing or pending,
 *         MDH_SAI_STATUS_NO_ERROR otherwise.
 */
static inline mdh_sai_status_t mdh_sai_set_event_callback(mdh_sai_t *self, mdh_sai_event_f cbk)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->set_event_callback));

    return self->vtable->set_event_callback(self, cbk);
}

/**
 * Set up the callback to be invoked at the end of transfers.
 *
 * @warning Must only be called when the device is idle (no active or pending transfer).
 *
 * @return MDH_SAI_STATUS_BUSY if a transfer is ongoing or pending,
 *         MDH_SAI_STATUS_NO_ERROR otherwise.
 */
static inline mdh_sai_status_t mdh_sai_set_transfer_complete_callback(mdh_sai_t *self, mdh_sai_transfer_complete_f cbk)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->set_transfer_complete_callback));

    return self->vtable->set_transfer_complete_callback(self, cbk);
}

/**
 * Attempt to transmit or receive a sample depending of the mode of this instance.
 * @param psample   Pointer to store or fetch a sample.
 * @param len       Number of bytes in the buffer. Must be a multiple of the sample size in bytes.
 * @return MDH_SAI_STATUS_BUSY if the device is not ready to process the request.
 */
static inline mdh_sai_status_t mdh_sai_transfer(mdh_sai_t *self, uint8_t *psample, uint32_t len, void *ctx)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->transfer));

    return self->vtable->transfer(self, psample, len, ctx);
}

/**
 * Cancel the next pending transfer.
 * Do not affect any on going transfer.
 *
 * @return MDH_SAI_STATUS_NO_ERROR if a transfer was cancelled,
 *         MDH_SAI_STATUS_BUSY otherwise.
 */
static inline mdh_sai_status_t mdh_sai_cancel_transfer(mdh_sai_t *self)
{
    assert((NULL != self) && (NULL != self->vtable) && (NULL != self->vtable->cancel_transfer));

    return self->vtable->cancel_transfer(self);
}

/**@}*/

#endif // HAL_SAI_API_H

/** @}*/
