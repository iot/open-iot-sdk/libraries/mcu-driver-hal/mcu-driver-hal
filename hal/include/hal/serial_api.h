/* Copyright (c) 2006-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_SERIAL_API_H
#define MDH_SERIAL_API_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifndef DEVICE_SERIAL_ASYNCH
#define DEVICE_SERIAL_ASYNCH 0
#endif // DEVICE_SERIAL_ASYNCH

#define SERIAL_EVENT_TX_SHIFT (2)
#define SERIAL_EVENT_RX_SHIFT (8)

#define SERIAL_EVENT_TX_MASK (0x00FC)
#define SERIAL_EVENT_RX_MASK (0x3F00)

#define SERIAL_EVENT_ERROR (1 << 1)

/**
 * @defgroup SerialTXEvents Serial TX Events Macros
 *
 * @{
 */
#define SERIAL_EVENT_TX_COMPLETE (1 << (SERIAL_EVENT_TX_SHIFT + 0))
#define SERIAL_EVENT_TX_ALL      (SERIAL_EVENT_TX_COMPLETE)
/**@}*/

/**
 * @defgroup SerialRXEvents Serial RX Events Macros
 *
 * @{
 */
#define SERIAL_EVENT_RX_COMPLETE        (1 << (SERIAL_EVENT_RX_SHIFT + 0))
#define SERIAL_EVENT_RX_OVERRUN_ERROR   (1 << (SERIAL_EVENT_RX_SHIFT + 1))
#define SERIAL_EVENT_RX_FRAMING_ERROR   (1 << (SERIAL_EVENT_RX_SHIFT + 2))
#define SERIAL_EVENT_RX_PARITY_ERROR    (1 << (SERIAL_EVENT_RX_SHIFT + 3))
#define SERIAL_EVENT_RX_OVERFLOW        (1 << (SERIAL_EVENT_RX_SHIFT + 4))
#define SERIAL_EVENT_RX_CHARACTER_MATCH (1 << (SERIAL_EVENT_RX_SHIFT + 5))
#define SERIAL_EVENT_RX_ALL                                                                  \
    (SERIAL_EVENT_RX_OVERFLOW | SERIAL_EVENT_RX_PARITY_ERROR | SERIAL_EVENT_RX_FRAMING_ERROR \
     | SERIAL_EVENT_RX_OVERRUN_ERROR | SERIAL_EVENT_RX_COMPLETE | SERIAL_EVENT_RX_CHARACTER_MATCH)
/**@}*/

// When in asynchronous mode, use this reserved value during character matching
// to check if a data word is not matched to a character.
#define SERIAL_RESERVED_CHAR_MATCH ((uint8_t)(0xFFU))

typedef enum mdh_serial_parity_e {
    MDH_SERIAL_PARITY_NONE = 0,
    MDH_SERIAL_PARITY_ODD = 1,
    MDH_SERIAL_PARITY_EVEN = 2,
    MDH_SERIAL_PARITY_FORCED_1 = 3,
    MDH_SERIAL_PARITY_FORCED_0 = 4
} mdh_serial_parity_t;

/** Serial interrupt sources
 */
typedef enum mdh_serial_irq_type_e {
    MDH_SERIAL_IRQ_TYPE_RX, /**< Receive Data Register Full */
    MDH_SERIAL_IRQ_TYPE_TX  /**< Transmit Data Register Empty */
} mdh_serial_irq_type_t;

typedef void (*mdh_serial_callback_f)(void *ctx, mdh_serial_irq_type_t event);

/** Serial HAL structure
 */
typedef struct mdh_serial_s mdh_serial_t;

typedef struct mdh_serial_vtable_s {
    void (*set_baud)(mdh_serial_t *self, uint32_t baudrate);
    void (*set_format)(mdh_serial_t *self, uint8_t data_bits, mdh_serial_parity_t parity, uint8_t stop_bits);
    void (*set_irq_callback)(mdh_serial_t *self, mdh_serial_callback_f cbk, void *ctx);
    void (*set_irq_availability)(mdh_serial_t *self, mdh_serial_irq_type_t irq, bool enable);
    uint32_t (*get_data)(mdh_serial_t *self);
    void (*put_data)(mdh_serial_t *self, uint32_t data);
    bool (*is_readable)(mdh_serial_t *self);
    bool (*is_writable)(mdh_serial_t *self);
    void (*clear)(mdh_serial_t *self);
    void (*set_break)(mdh_serial_t *self);
    void (*clear_break)(mdh_serial_t *self);

#if DEVICE_SERIAL_ASYNCH
    int (*asynch_send)(mdh_serial_t *self, const void *buffer, size_t buffer_length, uint32_t cbk, uint32_t event);
    void (*asynch_receive)(
        mdh_serial_t *self, void *buffer, size_t buffer_length, uint32_t cbk, uint32_t event, uint8_t char_match);
    bool (*is_tx_active)(mdh_serial_t *self);
    bool (*is_rx_active)(mdh_serial_t *self);
    uint32_t (*is_asynch_transfer_complete)(mdh_serial_t *self);
    void (*abort_asynch_tx)(mdh_serial_t *self);
    void (*abort_asynch_rx)(mdh_serial_t *self);
#endif // DEVICE_SERIAL_ASYNCH
} mdh_serial_vtable_t;

struct mdh_serial_s {
    const mdh_serial_vtable_t *vfptr;
};

/**
 * \defgroup hal_GeneralSerial Serial Configuration Functions
 *
 * # Defined behavior
 * * ::set_baud configures the baud rate
 * * at least 9600 bps the baud rate  must be supported
 * * ::set_format configures the transmission format (number of bits, parity and the number of stop bits)
 * * at least 8N1 format must be supported
 * * ::set_irq_callback registers the interrupt handler which will be invoked when the interrupt fires.
 * * ::set_irq_availability enables or disables the serial RX or TX IRQ.
 * * If `MDH_SERIAL_IRQ_TYPE_RX` is enabled by ::set_irq_availability, The callback registered with ::set_irq_callback
 * will be invoked whenever Receive Data Register Full IRQ is generated.
 * * If `MDH_SERIAL_IRQ_TYPE_TX` is enabled by ::set_irq_availability, The callback registered with ::set_irq_callback
 * will be invoked whenever Transmit Data Register Empty IRQ is generated.
 * * If the interrupt condition holds true, when the interrupt is enabled with ::set_irq_availability,
 * the ::set_irq_callback is called instantly.
 * * ::get_data returns the character from serial buffer.
 * * ::get_data is a blocking call (waits for the character).
 * * ::put_data sends a character.
 * * ::put_data is a blocking call (waits for a peripheral to be available).
 * * ::is_readable returns non-zero value if a character can be read, 0 otherwise.
 * * ::is_writable returns non-zero value if a character can be written, 0 otherwise.
 * * ::clear clears the ::serial_t RX/TX buffers
 * * ::set_break sets the break signal.
 * * ::clear_break clears the break signal.
 * * ::asynch_send starts the serial asynchronous transfer.
 * * ::asynch_send writes `buffer_length` bytes from the `buffer` to the bus.
 * * ::asynch_send must support 8 data bits
 * * The callback given to ::asynch_send is invoked when the transfer completes.
 * * ::asynch_send specifies the logical OR of events to be registered.
 * * ::asynch_receive starts the serial asynchronous transfer.
 * * ::asynch_receive reads `buffer_length` bytes to the `buffer`.
 * * ::asynch_receive must support 8 data bits
 * * The callback given to ::asynch_receive is invoked when the transfer completes.
 * * ::asynch_receive specifies the logical OR of events to be registered.
 * * ::asynch_receive specifies a character in range 0-254 to be matched, 255 is a reserved value.
 * * If SERIAL_EVENT_RX_CHARACTER_MATCH event is not registered, the `char_match` is ignored.
 * * The SERIAL_EVENT_RX_CHARACTER_MATCH event is set in the callback when SERIAL_EVENT_RX_CHARACTER_MATCH event is
 * registered AND `char_match` is present in the received data.
 * * ::is_tx_active returns non-zero if the TX transaction is ongoing, 0 otherwise.
 * * ::is_rx_active returns non-zero if the RX transaction is ongoing, 0 otherwise.
 * * ::is_asynch_transfer_complete returns event flags if a transfer termination condition was met, otherwise returns 0.
 * * ::is_asynch_transfer_complete takes no longer than one packet transfer time (packet_bits / baudrate) to execute.
 * * ::abort_asynch_tx aborts the ongoing TX transaction.
 * * ::abort_asynch_tx disables the enabled interupt for TX.
 * * ::abort_asynch_tx flushes the TX hardware buffer if TX FIFO is used.
 * * ::abort_asynch_rx aborts the ongoing RX transaction.
 * * ::abort_asynch_rx disables the enabled interupt for RX.
 * * ::abort_asynch_rx flushes the TX hardware buffer if RX FIFO is used.
 * * Correct operation guaranteed when interrupt latency is shorter than one packet transfer time (packet_bits /
 * baudrate) if the flow control is not used.
 * * Correct operation guaranteed regardless of interrupt latency if the flow control is used.
 *
 * # Undefined behavior
 * * Calling any function on an uninitialised or freed `mdh_serial_t`.
 * * Passing an invalid pointer as `self` to any function.
 * * Mixing synchronous and asynchronous API calls. The usage of synchronous and asynchronous APIs is mutually
 * exclusive.
 * * Passing an invalid pointer as `handler` to ::set_irq_callback, ::asynch_send, ::asynch_receive.
 * * Calling ::abort_asynch_tx while no async TX transfer is being processed.
 * * Calling ::abort_asynch_rx while no async RX transfer is being processed.
 * * Devices behavior is undefined when the interrupt latency is longer than one packet transfer time
 * (packet_bits / baudrate) if the flow control is not used.
 * @{
 */

/** Configure the baud rate
 *
 * @param self      The serial object
 * @param baudrate The baud rate to be configured
 */
static inline void mdh_serial_set_baud(mdh_serial_t *self, uint32_t baudrate)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_baud));
    self->vfptr->set_baud(self, baudrate);
}

/** Configure the format. Set the number of bits, parity and the number of stop bits
 *
 * @param self       The serial object
 * @param data_bits The number of data bits
 * @param parity    The parity
 * @param stop_bits The number of stop bits
 */
static inline void
mdh_serial_set_format(mdh_serial_t *self, uint8_t data_bits, mdh_serial_parity_t parity, uint8_t stop_bits)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_format));
    self->vfptr->set_format(self, data_bits, parity, stop_bits);
}

/** Sets the callback to execute in the IRQ handler
 *
 * @param self     The serial object
 * @param cbk      The callbacks to execute when the interrupt fires
 * @param ctx      is an application defined data passed to the callbacks
 */
static inline void mdh_serial_set_irq_callback(mdh_serial_t *self, mdh_serial_callback_f cbk, void *ctx)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_irq_callback));
    self->vfptr->set_irq_callback(self, cbk, ctx);
}

/** Configure serial interrupt. This function is used for word-approach
 *
 * @param self    The serial object
 * @param irq    The serial IRQ type (RX or TX)
 * @param enable Set to true to enable events, or false to disable them
 */
static inline void mdh_serial_set_irq_availability(mdh_serial_t *self, mdh_serial_irq_type_t irq, bool enable)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_irq_availability));
    self->vfptr->set_irq_availability(self, irq, enable);
}

/** Get data. This is a blocking call, waiting for a character
 *
 * @param self The serial object
 * @return the data retrieved
 */
static inline uint32_t mdh_serial_get_data(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_data));
    return self->vfptr->get_data(self);
}

/** Send data. This is a blocking call, waiting for a peripheral to be available
 *  for writing
 *
 * @param self The serial object
 * @param c   The data to be sent
 */
static inline void mdh_serial_put_data(mdh_serial_t *self, uint32_t data)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->put_data));
    self->vfptr->put_data(self, data);
}

/** Check if the serial peripheral is readable
 *
 * @param self The serial object
 * @return true value if a character can be read, false if nothing to read
 */
static inline bool mdh_serial_is_readable(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->is_readable));
    return self->vfptr->is_readable(self);
}

/** Check if the serial peripheral is writable
 *
 * @param self The serial object
 * @return true value if a character can be written, false otherwise.
 */
static inline bool mdh_serial_is_writable(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->is_writable));
    return self->vfptr->is_writable(self);
}

/** Clear the serial peripheral
 *
 * @param self The serial object
 */
static inline void mdh_serial_clear(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->clear));
    self->vfptr->clear(self);
}

/** Set the break
 *
 * @param self The serial object
 */
static inline void mdh_serial_set_break(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_break));
    self->vfptr->set_break(self);
}

/** Clear the break
 *
 * @param self The serial object
 */
static inline void mdh_serial_clear_break(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->clear_break));
    self->vfptr->clear_break(self);
}

#if DEVICE_SERIAL_ASYNCH

/**@}*/

/**
 * \defgroup hal_AsynchSerial Asynchronous Serial Hardware Abstraction Layer
 * @{
 */

/** Begin asynchronous TX transfer. The used buffer is specified in the serial object,
 *  tx_buff
 *
 * @param self       The serial object
 * @param tx        The transmit buffer
 * @param tx_length The number of bytes to transmit
 * @param cbk       Transmit callback
 * @param event     The logical OR of events to be registered
 * @return Returns number of data transfered, otherwise returns 0
 */
static inline int
mdh_serial_asynch_send(mdh_serial_t *self, const void *buffer, size_t buffer_length, uint32_t cbk, uint32_t event)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_send));
    return self->vfptr->asynch_send(self, buffer, buffer_length, cbk, event);
}

/** Begin asynchronous RX transfer (enable interrupt for data collecting)
 *  The used buffer is specified in the serial object - rx_buff
 *
 * @param self        The serial object
 * @param buffer         The receive buffer
 * @param buffer_length  The number of bytes to receive
 * @param cbk        Receive callback
 * @param event      The logical OR of events to be registered
 * @param char_match A character in range 0-254 to be matched
 */
static inline void mdh_serial_asynch_receive(
    mdh_serial_t *self, void *buffer, size_t buffer_length, uint32_t cbk, uint32_t event, uint8_t char_match)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_receive));
    self->vfptr->asynch_receive(self, buffer, buffer_length, cbk, event, char_match);
}

/** Attempts to determine if the serial peripheral is already in use for TX
 *
 * @param self The serial object
 * @return true if the TX transaction is ongoing, false otherwise
 */
static inline bool mdh_serial_is_tx_active(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->is_tx_active));
    return self->vfptr->is_tx_active(self);
}

/** Attempts to determine if the serial peripheral is already in use for RX
 *
 * @param self The serial object
 * @return true if the RX transaction is ongoing, false otherwise
 */
static inline bool mdh_serial_is_rx_active(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->is_rx_active));
    return self->vfptr->is_rx_active(self);
}

/** Checks if an asynchronous TX or RX transfer completed
 *
 * @param self The serial object
 * @return Returns event flags if a TX/RX transfer termination condition was met or 0 otherwise
 */
static inline uint32_t mdh_serial_is_asynch_transfer_complete(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->is_asynch_transfer_complete));
    return self->vfptr->is_asynch_transfer_complete(self);
}

/** Abort the ongoing TX transaction. It disables the enabled interupt for TX and
 *  flushes the TX hardware buffer if TX FIFO is used
 *
 * @param self The serial object
 */
static inline void mdh_serial_abort_asynch_tx(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->abort_asynch_tx));
    self->vfptr->abort_asynch_tx(self);
}

/** Abort the ongoing RX transaction. It disables the enabled interrupt for RX and
 *  flushes the RX hardware buffer if RX FIFO is used
 *
 * @param self The serial object
 */
static inline void mdh_serial_abort_asynch_rx(mdh_serial_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->abort_asynch_rx));
    self->vfptr->abort_asynch_rx(self);
}

/**@}*/

#endif // DEVICE_SERIAL_ASYNCH

#endif // MDH_SERIAL_API_H

/** @}*/
