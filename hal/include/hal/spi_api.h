/* Copyright (c) 2006-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef MDH_SPI_API_H
#define MDH_SPI_API_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifndef DEVICE_SPI_ASYNCH
#define DEVICE_SPI_ASYNCH 0
#endif // DEVICE_SPI_ASYNCH

#define MDH_SPI_EVENT_ERROR       (1 << 1)
#define MDH_SPI_EVENT_COMPLETE    (1 << 2)
#define MDH_SPI_EVENT_RX_OVERFLOW (1 << 3)
#define MDH_SPI_EVENT_ALL         (MDH_SPI_EVENT_ERROR | MDH_SPI_EVENT_COMPLETE | MDH_SPI_EVENT_RX_OVERFLOW)

#define MDH_SPI_EVENT_INTERNAL_TRANSFER_COMPLETE (1 << 30) // Internal flag to report that an event occurred

#define MDH_SPI_FILL_WORD (0xFFFF)
#define MDH_SPI_FILL_CHAR (0xFF)

// |  SPI Mode  |  Clock polarity  |   Clock phase    |  Clock edge    |
// |---         |---               |---               |---             |
// |  0         |  0               | 0                | 1              |
// |  1         |  0               | 1                | 0              |
// |  2         |  1               | 0                | 1              |
// |  3         |  1               | 1                | 0              |
typedef enum mdh_spi_mode_e { MDH_SPI_MODE_0 = 0, MDH_SPI_MODE_1, MDH_SPI_MODE_2, MDH_SPI_MODE_3 } mdh_spi_mode_t;

/** SPI HAL structure
 */
typedef struct mdh_spi_s mdh_spi_t;

/**
 * Describes the capabilities of a SPI peripherals
 */
typedef struct {
    /** Minimum frequency supported must be set by target device and it will be assessed during
     *  testing.
     */
    uint32_t minimum_frequency;
    /** Maximum frequency supported must be set by target device and it will be assessed during
     *  testing.
     */
    uint32_t maximum_frequency;
    /** Each bit represents the corresponding word length. lsb => 1bit, msb => 32bit. */
    uint32_t word_lengths;
    uint16_t peripheral_delay_between_symbols_ns; /**< specifies required number of ns between transmission of
                                                successive symbols in peripheral mode. */
    uint8_t
        clk_modes; /**< specifies supported modes from mdh_spi_mode_t. Each bit represents the corresponding mode. */
    bool supports_peripheral_mode; /**< If true, the device can handle SPI peripheral mode using hardware management on
                                the specified csel pin. */
    bool hw_cs_handle;             /**< If true, in SPI controller mode Chip Select can be handled by hardware. */
    bool async_mode;               /**< If true, in async mode is supported. */
    bool buffers_equal_length;     /**< If true, read_buffer and write_buffer must have the same length. */
} mdh_spi_capabilities_t;

typedef struct mdh_spi_vtable_s {
    void (*get_capabilities)(mdh_spi_t *self, bool is_peripheral, mdh_spi_capabilities_t *capabilities);
    void (*set_format)(mdh_spi_t *self, uint8_t word_size, mdh_spi_mode_t mode, bool is_peripheral);
    void (*set_frequency)(mdh_spi_t *self, uint32_t hz);
    uint32_t (*controller_write)(mdh_spi_t *self, uint32_t write_data);
    uint32_t (*controller_block_read_write)(mdh_spi_t *self,
                                            const uint8_t *write_buffer,
                                            size_t write_size,
                                            uint8_t *read_buffer,
                                            size_t read_size,
                                            uint8_t write_fill);
    bool (*is_peripheral_data_available)(mdh_spi_t *self);
    uint32_t (*peripheral_read)(mdh_spi_t *self);
    void (*peripheral_write)(mdh_spi_t *self, uint32_t data);
    bool (*is_busy)(mdh_spi_t *self);
#if DEVICE_SPI_ASYNCH
    void (*asynch_controller_transfer)(mdh_spi_t *self,
                                       const void *write_buffer,
                                       size_t write_size,
                                       void *read_buffer,
                                       size_t read_size,
                                       uint8_t data_word_size,
                                       uint32_t cbk,
                                       uint32_t event);
    uint32_t (*asynch_irq_handler)(mdh_spi_t *self);
    bool (*asynch_is_active)(mdh_spi_t *self);
    void (*asynch_abort_transfer)(mdh_spi_t *self);
#endif // DEVICE_SPI_ASYNCH
} mdh_spi_vtable_t;

struct mdh_spi_s {
    const mdh_spi_vtable_t *vfptr;
};

/**
 * \defgroup hal_GeneralSPI SPI Configuration Functions
 *
 * # Defined behavior
 * * ::mdh_spi_get_capabilities() fills the given `mdh_spi_capabilities_t` instance
 * * At least a symbol width of 8bit must be supported
 * * The supported frequency range must include the range [0.2..2] MHz
 * * ::mdh_spi_set_format sets the number of bits per frame
 * * ::mdh_spi_set_format configures clock polarity and phase
 * * ::mdh_spi_set_format configures controller/peripheral mode
 * * ::mdh_spi_set_frequency sets the SPI baud rate
 * * ::mdh_spi_controller_write writes a symbol out in controller mode and receives a symbol
 * * ::mdh_spi_controller_block_write writes `write_size` words to the bus
 * * ::mdh_spi_controller_block_write reads `read_size` words from the bus
 * * ::mdh_spi_controller_block_write returns the maximum of write_size and read_size
 * * ::mdh_spi_controller_block_write specifies the write_fill which is default data transmitted while performing a read
 * * ::mdh_spi_peripheral_read returns a received value out of the SPI receive buffer in peripheral mode - TBD (SPI
 * peripheral test)
 * * ::mdh_spi_peripheral_read blocks until a value is available - TBD (SPI peripheral test)
 * * ::mdh_spi_peripheral_write writes a value to the SPI peripheral in peripheral mode - TBD (SPI peripheral test)
 * * ::mdh_spi_peripheral_write blocks until the SPI peripheral can be written to - TBD (SPI peripheral test)
 * * ::mdh_spi_busy returns non-zero if the peripheral is currently transmitting, 0 otherwise - TBD (basic test)
 * * ::mdh_spi_controller_transfer starts the SPI asynchronous transfer
 * * ::mdh_spi_controller_transfer writes `tx_len` words to the bus
 * * ::mdh_spi_controller_transfer reads `rx_len` words from the bus
 * * ::mdh_spi_controller_transfer specifies the bit width of buffer words
 * * The callback given to ::mdh_spi_controller_transfer is invoked when the transfer completes (with a success or an
 * error)
 * * ::mdh_spi_controller_transfer specifies the logical OR of events to be registered
 * * ::mdh_spi_asynch_irq_handler reads the received values out of the RX FIFO
 * * ::mdh_spi_asynch_irq_handler writes values into the TX FIFO
 * * ::mdh_spi_asynch_irq_handler checks for transfer termination conditions, such as buffer overflows or transfer
 * complete
 * * ::mdh_spi_asynch_irq_handler returns event flags if a transfer termination condition was met, otherwise 0 -
 * * ::mdh_spi_asynch_abort_transfer aborts an on-going async transfer - TBD (basic test)
 * * ::mdh_spi_active returns non-zero if the SPI port is active or zero if it is not - TBD (basic test)
 *
 * # Undefined behavior
 * * Passing an invalid pointer as `self` to any function
 * * Passing an invalid pointer as `handler` to ::mdh_spi_controller_transfer
 * * Calling ::mdh_spi_abort while no async transfer is being processed (no transfer or a synchronous transfer)
 *
 * @{
 */

/** Returns the SPI capabilities supported for a given SPI device
 *
 * @param[in] self The SPI object to query
 * @param[in] is_peripheral Whether to return the SPI capabilities of the device when in peripheral mode or controller
 * mode
 * @param[out] capabilities Pointer to structure which will hold information about the SPI capabilities of the device
 */
static inline void mdh_spi_get_capabilities(mdh_spi_t *self, bool is_peripheral, mdh_spi_capabilities_t *capabilities)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->get_capabilities));
    self->vfptr->get_capabilities(self, is_peripheral, capabilities);
}

/** Configure the SPI format
 *
 * Set the number of bits per frame, configure clock polarity and phase, shift order and controller/peripheral mode.
 * The default bit order is MSB.
 * @param[in] self The SPI object to configure
 * @param[in] word_size The number of bits per frame
 * @param[in] mode The SPI mode (clock polarity, phase, and shift direction)
 * @param[in] is_peripheral true for peripheral mode, false for controller
 */
static inline void mdh_spi_set_format(mdh_spi_t *self, uint8_t word_size, mdh_spi_mode_t mode, bool is_peripheral)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_format));
    self->vfptr->set_format(self, word_size, mode, is_peripheral);
}

/** Set the SPI clock frequency
 *
 * Actual frequency may differ from the desired frequency due to available dividers and bus clock
 * Configures the SPI peripheral's baud rate
 * @param[in] self The SPI object to configure
 * @param[in] hz The frequency in Hz
 */
static inline void mdh_spi_set_frequency(mdh_spi_t *self, uint32_t hz)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->set_frequency));
    self->vfptr->set_frequency(self, hz);
}

/**@}*/
/**
 * \defgroup SynchSPI Synchronous SPI Hardware Abstraction Layer
 * @{
 */

/** Write a data word out in controller mode and receive a value
 *
 * @param[in] self The SPI peripheral to use for sending
 * @param[in] write_data The data word to send
 * @return Returns the value received during send
 */
static inline uint32_t mdh_spi_controller_write(mdh_spi_t *self, uint32_t write_data)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->controller_write));
    return self->vfptr->controller_write(self, write_data);
}

/** Sends and receives blocks of 1-byte elements data via SPI bus.
 *
 * @note
 * This function only supports 8-bit data words.
 *
 *
 *  The total number of bytes sent and received will be the maximum of
 *  write_size and read_size. The bytes written will be padded with the
 *  value 0xff.
 *
 * @param[in] self The SPI peripheral to use for sending
 * @param[in] write_buffer Pointer to the byte-array of data to write to the device
 * @param[in] write_size Number of bytes to write, may be zero
 * @param[in] read_buffer Pointer to the byte-array of data to read from the device
 * @param[in] read_size Number of bytes to read, may be zero
 * @param[in] write_fill Default data transmitted while performing a read
 * @returns
 *      The number of bytes written and read from the device. This is
 *      maximum of write_size and read_size.
 */
static inline uint32_t mdh_spi_controller_block_read_write(mdh_spi_t *self,
                                                           const uint8_t *write_buffer,
                                                           size_t write_size,
                                                           uint8_t *read_buffer,
                                                           size_t read_size,
                                                           uint8_t write_fill)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->controller_block_read_write));
    return self->vfptr->controller_block_read_write(self, write_buffer, write_size, read_buffer, read_size, write_fill);
}

/** Check if a value is available to read
 *
 * @param[in] self The SPI peripheral to check
 * @return true if data word is available, false otherwise.
 */
static inline bool mdh_spi_is_peripheral_data_available(mdh_spi_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->is_peripheral_data_available));
    return self->vfptr->is_peripheral_data_available(self);
}

/** Read data from the SPI receive buffer in peripheral mode
 *
 * Blocks until data is available
 * @param[in] self The SPI peripheral to read
 * @return The data received
 */
static inline uint32_t mdh_spi_peripheral_read(mdh_spi_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->peripheral_read));
    return self->vfptr->peripheral_read(self);
}

/** Write data to the SPI peripheral in peripheral mode
 *
 * Blocks until the SPI peripheral can be written to
 * @param[in] self The SPI peripheral to write
 * @param[in] data The data to write
 */
static inline void mdh_spi_peripheral_write(mdh_spi_t *self, uint32_t data)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->peripheral_write));
    self->vfptr->peripheral_write(self, data);
}

/** Checks if the specified SPI peripheral is in use
 *
 * @param[in] self The SPI peripheral to check
 * @return true if the peripheral is currently transmitting, false otherwise
 */
static inline bool mdh_spi_is_busy(mdh_spi_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->is_busy));
    return self->vfptr->is_busy(self);
}

/**@}*/

#if DEVICE_SPI_ASYNCH
/**
 * \defgroup AsynchSPI Asynchronous SPI Hardware Abstraction Layer
 * @{
 */

/** Begin the SPI transfer.
 *
 * @param[in] self The SPI object that holds the transfer information
 * @param[in] write_buffer The transmit buffer
 * @param[in] write_size The number of bytes to transmit
 * @param[in] read_buffer The receive buffer
 * @param[in] read_size The number of bytes to receive
 * @param[in] data_word_size The number of bits
 * @param[in] cbk Memory address location of the callback to execute from an SPI IRQ
 * @param[in] event The logical OR of events to be registered
 */
static inline void mdh_spi_asynch_controller_transfer(mdh_spi_t *self,
                                                      const void *write_buffer,
                                                      size_t write_size,
                                                      void *read_buffer,
                                                      size_t read_size,
                                                      uint8_t data_word_size,
                                                      uint32_t cbk,
                                                      uint32_t event)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_controller_transfer));
    self->vfptr->asynch_controller_transfer(
        self, write_buffer, write_size, read_buffer, read_size, data_word_size, cbk, event);
}

/** The asynchronous IRQ handler
 *
 * Reads the received values out of the RX FIFO, writes values into the TX FIFO and checks for transfer termination
 * conditions, such as buffer overflows or transfer complete.
 * @param[in] self The SPI object that holds the transfer information
 * @return Event flags if a transfer termination condition was met; otherwise 0.
 */
static inline uint32_t mdh_spi_asynch_irq_handler(mdh_spi_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_irq_handler));
    return self->vfptr->asynch_irq_handler(self);
}

/** Attempts to determine if the SPI peripheral is already in use
 *
 * If a temporary DMA channel has been allocated, peripheral is in use.
 * If a permanent DMA channel has been allocated, check if the DMA channel is in use.  If not, proceed as though no DMA
 * channel were allocated.
 * If no DMA channel is allocated, check whether write_buffer and read_buffer have been assigned.  For each assigned
 * buffer, check if the corresponding buffer position is less than the buffer length.  If buffers do not indicate
 * activity, check if there are any bytes in the FIFOs.
 * @param[in] self The SPI object to check for activity
 * @return true if the SPI port is active, false if otherwise.
 */
static inline bool mdh_spi_asynch_is_active(mdh_spi_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_is_active));
    return self->vfptr->asynch_is_active(self);
}

/** Abort an SPI transfer
 *
 * @param self The SPI peripheral to stop
 */
static inline void mdh_spi_asynch_abort_transfer(mdh_spi_t *self)
{
    assert((NULL != self) && (NULL != self->vfptr) && (NULL != self->vfptr->asynch_abort_transfer));
    self->vfptr->asynch_abort_transfer(self);
}

#endif // DEVICE_SPI_ASYNCH

/**@}*/

#endif // MDH_SPI_API_H

/** @}*/
