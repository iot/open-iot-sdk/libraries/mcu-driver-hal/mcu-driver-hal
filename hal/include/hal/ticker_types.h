/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal */
/** @{*/

#ifndef TICKER_TYPES_H
#define TICKER_TYPES_H

#include <stdint.h>

/** A timestamp in ticks.
 *
 * @note Use the frequency returned by ::us_ticker_get_info or
 * ::lp_ticker_get_info to divide timestamp_t value and calculate
 * time in seconds.
 */
typedef uint32_t timestamp_t;

/** Information about the ticker implementation
 */
typedef struct {
    uint32_t frequency; /**< Frequency in Hz this ticker runs at */
    uint32_t bits;      /**< Number of bits this ticker supports */
} ticker_info_t;

typedef struct mdh_ticker_s mdh_ticker_t;

typedef void (*ticker_irq_callback_f)(mdh_ticker_t *self);

/** Ticker's interface structure - required API for a ticker
 */
typedef struct mdh_ticker_vtable_s {
    uint32_t (*read)(mdh_ticker_t *self);
    void (*disable_interrupt)(mdh_ticker_t *self);
    void (*clear_interrupt)(mdh_ticker_t *self);
    void (*set_interrupt)(mdh_ticker_t *self, timestamp_t timestamp);
    void (*fire_interrupt)(mdh_ticker_t *self);
    ticker_irq_callback_f (*set_irq_handler)(mdh_ticker_t *self, ticker_irq_callback_f callback);
    const ticker_info_t *(*get_info)(mdh_ticker_t *self);
} mdh_ticker_vtable_t;

struct mdh_ticker_s {
    const mdh_ticker_vtable_t *vtable;
};

#endif /* TICKER_TYPES_H */

/** @}*/
