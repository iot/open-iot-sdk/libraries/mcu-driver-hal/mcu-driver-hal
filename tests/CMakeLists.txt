# Copyright (c) 2021-2022 Arm Limited
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# HAL tests
add_subdirectory(mbed_hal/dma EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal/echo EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal/emac EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal/flash/functional_tests EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal/lp_ticker EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal/ospi EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal/qspi EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal/us_ticker EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal/us_ticker_lp_ticker_common EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal/us_ticker_lp_ticker_frequency EXCLUDE_FROM_ALL)

# HAL tests with the FPGA CI test shield
add_subdirectory(mbed_hal_fpga_ci_test_shield/analogin EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal_fpga_ci_test_shield/gpio EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal_fpga_ci_test_shield/gpio_irq EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal_fpga_ci_test_shield/i2c EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal_fpga_ci_test_shield/pwm EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal_fpga_ci_test_shield/spi EXCLUDE_FROM_ALL)
add_subdirectory(mbed_hal_fpga_ci_test_shield/serial EXCLUDE_FROM_ALL)
