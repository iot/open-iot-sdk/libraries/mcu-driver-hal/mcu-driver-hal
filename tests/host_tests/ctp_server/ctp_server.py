# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

from socket import socket, htons, ntohs, AF_PACKET, SOCK_RAW
from getmac import get_mac_address
from binascii import unhexlify, hexlify

import re, sys, os, logging

ETH_P_ALL = 3

ETHERTYPE_LOOPBACK = 0x0090
ETHER_MTU = 1514
ETHER_HEADER_SIZE = 14

# Offset(s) of various params contained within the input raw
# message. All offsets are from the start of the message.
ETHER_DSTMAC_OFFSET = 0
ETHER_SRCMAC_OFFSET = 6
ETHER_TYPE_OFFSET = 12
ETHER_CTP_SKIPCOUNT_OFFSET = 14
ETHER_CTP_MSG_START = 16

# Offset(s) of various params contained within the input ctp
# message. All offsets are from the start of the ctp message.
CTP_FUNCTYPE_OFFSET = 0
CTP_FWD_ADDR_OFFSET = 2

ETHER_MAC_SIZE = 6
ETHER_TYPE_SIZE = 2

CTP_REPLY_MSG = 0x0001
CTP_FORWARD_MSG = 0x0002

def get_mac_addr(ifname):
    """Gets the MAC address of an interface

    This function asks the get_mac library to get the MAC
    address of the interface and converts the ascii output
    of the library to binary.
    """
    return unhexlify(get_mac_address(ifname).replace(":", ""))

def get_ether_msgtype(raw_msg):
    """Gets the msgtype field a raw network packet

    This function takes a raw ethernet packet as input and extracts
    the msgtype field from it.
    """
    type = raw_msg[ETHER_TYPE_OFFSET:ETHER_TYPE_OFFSET+2]
    return ntohs(int(hexlify(type), 16))

def get_ctp_msg_offset(raw_msg):
    """Gets the actual start of the ctp message

    The ctp header starts right after the ethernet header, and the first 2
    bytes of this header contain the skip count, which tells us the number
    of bytes in the ctp header we should skip. So if the skip count is 2,
    the ctp message starts at: 14 + 2 + 2 = 18 (ethernet header + skip count field +
    bytes to skip).

    This function takes a raw ethernet packet as input and returns the ctp
    message offset.
    """
    skip_count = raw_msg[ETHER_CTP_SKIPCOUNT_OFFSET:ETHER_CTP_SKIPCOUNT_OFFSET+2]

    # Skip the raw message message and skip count.
    return ntohs(int(hexlify(skip_count), 16)) + ETHER_CTP_MSG_START

def get_ctp_function(ctp_msg):
    """Gets the ctp function

    A ctp packet may either be a forward or a reply packet. This function looks
    at the provided ctp message and finds this out.
    """
    func = ctp_msg[CTP_FUNCTYPE_OFFSET:CTP_FUNCTYPE_OFFSET+2]
    return ntohs(int(hexlify(func), 16))

def get_ctp_fwd_addr(ctp_msg):
    """Gets the ctp forward address

    Given a CTP Message, extracts the forward MAC address. This is used as the
    source address if the server wants to reply back.
    """
    fwd_addr = ctp_msg[CTP_FWD_ADDR_OFFSET:CTP_FWD_ADDR_OFFSET+ETHER_MAC_SIZE]
    return fwd_addr

def set_mac_address(raw_msg, src_addr, dest_addr):
    """Sets the MAC address fields of a raw packet appropriately

    This function takes the source and destination mac address provided as arguments
    and copies them to the raw packet.
    """
    raw_msg[ETHER_DSTMAC_OFFSET:ETHER_DSTMAC_OFFSET+ETHER_MAC_SIZE] = dest_addr[0:6]
    raw_msg[ETHER_SRCMAC_OFFSET:ETHER_SRCMAC_OFFSET+ETHER_MAC_SIZE] = src_addr[0:6]

def set_skip_count(raw_msg, skip_count):
    """Sets the ctp skip count field in a raw packet

    This function takes a raw ethernet packet as input and sets the ctp skip count
    field.
    """
    sc = htons(skip_count)
    raw_msg[ETHER_CTP_SKIPCOUNT_OFFSET:ETHER_CTP_SKIPCOUNT_OFFSET+2] = bytearray(sc.to_bytes(2, 'big'))

def check_started_with_elevated_privileges():
    """Perfoms basic sanity checks before starting the server

    This function ensures that the script has super user privileges. If not, it exits
    after printing failure messages.
    """
    if os.getuid() != 0:
        logging.error("Cannot create raw sockets as normal user.")
        logging.error("Rerun this script as super user")
        sys.exit()

def run_ctp_server(ifname, loglevel=logging.WARNING):
    """Runs the CTP server

    This function creates a raw socket on the provided network interface, binds to it
    and starts listening for incoming packet. On reception of every packet, this function:
    .. Checks if the packet is valid. If it is not, we skip it altogether.
    .. If its valid, we check if its a ctp packet (loopback). If not, we skip it.
    .. If it is, then we check the ctp function type.
       .. If the function type is CTP_FORWARD, we create and send a ctp response packet.
       .. Skip otherwise.

    This function will print out frame TX/RX statistics on reception of every 100 valid frames.
    """

    # Set up logging
    logging.getLogger().setLevel(loglevel)

    check_started_with_elevated_privileges()

    logging.info(f"Starting CTP Server on {ifname}")

    ctp = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))
    ctp.bind((ifname, 0))

    src_addr = get_mac_addr(ifname);
    counter = recvd = recvd_ctp = sent = ignored = 0

    while True:
        raw_msg = bytearray(ctp.recv(ETHER_MTU))
        recvd += 1

        if len(raw_msg) < ETHER_HEADER_SIZE:
            logging.debug("Invalid Packet Received, Ignoring")
            ignored += 1
            continue

        # As we have a raw socket, it will capture packets from all
        # apps but we only want to process (and respond to) the ctp
        # packets. Filter all non-essential packets out.
        if get_ether_msgtype(raw_msg) == ETHERTYPE_LOOPBACK:
            logging.debug("CTP Message Recvd")
            recvd_ctp += 1

            # Find start of the CTP Message.
            ctp_msg_offset = get_ctp_msg_offset(raw_msg)
            ctp_msg = raw_msg[ctp_msg_offset:]

            if get_ctp_function(ctp_msg) == CTP_FORWARD_MSG:
                # In this case, we will reply back with the received message except:
                # .. Destination MAC address will be the CTP Forward Address.
                # .. Skip Count will be updated so receiver reads ctp message from correct offset
                set_mac_address(raw_msg, src_addr, get_ctp_fwd_addr(ctp_msg))
                set_skip_count(raw_msg, 8)

                ctp.send(raw_msg)
                sent += 1
            else:
                logging.debug("CTP Reply Message Received")
                logging.debug("Ignored as CTP server never sends a forward message")
                ignored += 1

        counter += 1
        if counter == 100:
            logging.info("CTP Server Stats")
            logging.info(f"Total Packets Received: {recvd}")
            logging.info(f"Total CTP Packets Received: {recvd_ctp}")
            logging.info(f"Total Responses Sent: {sent}")
            logging.info(f"Total Packets Ignored: {ignored}")
            counter = 0
