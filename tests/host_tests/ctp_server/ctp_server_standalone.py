# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

import logging, argparse

from ctp_server import run_ctp_server

if __name__ == "__main__":
    loglevel=logging.INFO

    # Create parser (and parse the recvd args)
    parser = argparse.ArgumentParser()
    parser.add_argument(dest="ifname", help="Network Interface to use for CTP server")
    parser.add_argument("-v", "--verbose", action="store_true", help="Display debug output on the console")

    args = parser.parse_args()
    if args.verbose == True:
        loglevel = logging.DEBUG

    # Start the server on the interface
    run_ctp_server(args.ifname, loglevel)
