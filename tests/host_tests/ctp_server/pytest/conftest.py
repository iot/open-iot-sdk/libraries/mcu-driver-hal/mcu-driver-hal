# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

def pytest_addoption(parser):
    parser.addoption("--ifname", action="store", default="virt0")
    parser.addoption("--is_virtual", action="store", default="1")

def pytest_generate_tests(metafunc):
    # This is called for every test. Only get/set command line arguments
    # if the argument is specified in the list of test "fixturenames".
    option_value = metafunc.config.option.ifname
    if 'ifname' in metafunc.fixturenames and option_value is not None:
        metafunc.parametrize("ifname", [option_value])
    option_value = metafunc.config.option.is_virtual
    if 'is_virtual' in metafunc.fixturenames and option_value is not None:
        metafunc.parametrize("is_virtual", [option_value])
