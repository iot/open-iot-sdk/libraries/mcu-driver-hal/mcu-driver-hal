# Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

from socket import socket, htons, ntohs, AF_PACKET, SOCK_RAW
from getmac import get_mac_address
from binascii import unhexlify, hexlify

import re, sys, os, select, logging
import struct, fcntl
import pytest

ETH_P_ALL = 3

ETHERTYPE_LOOPBACK = 0x0090
ETHER_MTU = 1514
ETHER_HEADER_SIZE = 14

# Offset(s) of various params contained within the input raw
# message. All offsets are from the start of the message.
ETHER_DSTMAC_OFFSET = 0
ETHER_SRCMAC_OFFSET = 6
ETHER_TYPE_OFFSET = 12
ETHER_CTP_SKIPCOUNT_OFFSET = 14
ETHER_CTP_MSG_START = 16

# Offset(s) of various params contained within the input ctp
# message. All offsets are from the start of the ctp message.
CTP_FUNCTYPE_OFFSET = 0
CTP_FORWARDING_ADDR_OFFSET = 2
CTP_OTHERFUNCTYPE_OFFSET = 8
CTP_RECEIPT_OFFSET = 10

ETHER_MAC_SIZE = 6
ETHER_TYPE_SIZE = 2

CTP_REPLY_MSG = 0x0001
CTP_FORWARD_MSG = 0x0002

server_addr = bytearray(6)

def get_mac_addr(ifname):
    """Gets the MAC address of an interface

    This function asks the get_mac library to get the MAC
    address of the interface and converts the ascii output
    of the library to binary.
    """
    return unhexlify(get_mac_address(ifname).replace(":", ""))

def get_src_mac_addr(raw_msg):
    """ Gets the Source MAC address from a raw network packet

    Given a raw network packet, this function will extract the source mac
    address field from the Ethernet header and return it.
    """
    return raw_msg[ETHER_SRCMAC_OFFSET:ETHER_SRCMAC_OFFSET+ETHER_MAC_SIZE]

def set_src_mac_addr(raw_msg, addr):
    """ Sets the Source MAC address of a raw network packet

    This function will copy the "addr" argument to the source mac address field
    of the raw packet.
    """
    raw_msg[ETHER_SRCMAC_OFFSET:ETHER_SRCMAC_OFFSET+ETHER_MAC_SIZE] = addr[0:6]

def set_dst_mac_addr(raw_msg, addr):
    """ Sets the Destination MAC address of a raw network packet

    This function will copy the "addr" argument to the destination mac address field
    of the raw packet.
    """
    raw_msg[ETHER_DSTMAC_OFFSET:ETHER_DSTMAC_OFFSET+ETHER_MAC_SIZE] = addr[0:6]

def get_ether_msgtype(raw_msg):
    """Gets the msgtype field from a raw network packet

    This function takes a raw ethernet packet as input and extracts
    the msgtype field from it.
    """
    type = raw_msg[ETHER_TYPE_OFFSET:ETHER_TYPE_OFFSET+2]
    return ntohs(int(hexlify(type), 16))

def set_ether_msgtype(raw_msg, type):
    """Sets the ethernet msgtype field in a raw packet

    This function takes a raw ethernet packet as input and sets the ethernet msgtype
    field.
    """
    _type = htons(type)
    raw_msg[ETHER_TYPE_OFFSET:ETHER_TYPE_OFFSET+2] = bytearray(_type.to_bytes(2, 'big'))

def get_ctp_msg_offset(raw_msg):
    """Gets the actual start of the ctp message

    The ctp header starts right after the ethernet header, and the first 2
    bytes of this header contain the skip count, which tells us the number
    of bytes in the ctp header we should skip. So if the skip count is 2,
    the ctp message starts at: 14 + 2 + 2 = 18 (ethernet header + skip count field +
    bytes to skip).

    This function takes a raw ethernet packet as input and returns the ctp
    message offset.
    """
    skip_count = raw_msg[ETHER_CTP_SKIPCOUNT_OFFSET:ETHER_CTP_SKIPCOUNT_OFFSET+2]

    # Skip the raw message message and skip count.
    return ntohs(int(hexlify(skip_count), 16)) + ETHER_CTP_MSG_START

def get_ctp_function(ctp_msg):
    """Gets the ctp function

    A ctp packet may either be a forward or a reply packet. This function looks
    at the provided ctp message and finds this out.
    """
    func = ctp_msg[CTP_FUNCTYPE_OFFSET:CTP_FUNCTYPE_OFFSET+2]
    return ntohs(int(hexlify(func), 16))

def set_ctp_function(raw_msg, func):
    """Sets the ctp function type in a raw packet

    This function takes a raw ethernet packet as input and sets the ctp function
    type field.
    """
    offset = ETHER_CTP_MSG_START + CTP_FUNCTYPE_OFFSET

    _func = htons(func)
    raw_msg[offset:offset+2] = bytearray(_func.to_bytes(2, 'big'))

def swap_mac_address(raw_msg, src_addr):
    """Sets the MAC address fields of a raw packet appropriately

    This function takes a raw ethernet packet as input and:
    .. Copies the source mac address field of the raw message to the destination.
    .. Copies the provided "src_addr" mac address to the source mac address field
       of the packet.
    """
    raw_msg[ETHER_DSTMAC_OFFSET:ETHER_DSTMAC_OFFSET+ETHER_MAC_SIZE] = raw_msg[ETHER_SRCMAC_OFFSET:ETHER_SRCMAC_OFFSET+ETHER_MAC_SIZE]
    raw_msg[ETHER_SRCMAC_OFFSET:ETHER_SRCMAC_OFFSET+ETHER_MAC_SIZE] = src_addr[0:6]

def set_skip_count(raw_msg, skip_count):
    """Sets the ctp skip count field in a raw packet

    This function takes a raw ethernet packet as input and sets the ctp skip count
    field.
    """
    sc = htons(skip_count)
    raw_msg[ETHER_CTP_SKIPCOUNT_OFFSET:ETHER_CTP_SKIPCOUNT_OFFSET+2] = bytearray(sc.to_bytes(2, 'big'))

def set_receipt_number(raw_msg, receipt_no):
    """Sets the ctp receipt number in a raw packet

    This function takes a raw ethernet packet as input and sets the ctp receipt
    number field.
    """
    offset = ETHER_CTP_MSG_START + CTP_RECEIPT_OFFSET

    _rno = htons(receipt_no)
    raw_msg[offset:offset+2] = bytearray(_rno.to_bytes(2, 'big'))

def set_ctp_forwarding_addr(raw_msg, src_addr):
    """Sets the ctp forwarding address in a raw packet

    This function takes a raw ethernet packet as input and sets the ctp forwarding
    address field.
    """
    offset = ETHER_CTP_MSG_START + CTP_FORWARDING_ADDR_OFFSET

    raw_msg[offset:offset+6] = src_addr

def create_ctp_packet(src_addr, pkt_len, receipt_no):
    """Creates a CTP packet

    Provided the Source MAC address, the total packet length and the receipt number,
    this function creates a valid CTP message with the following attributes:
      .. Destination MAC address is Broadcast.
      .. Created CTP Message is a forward message (skipcount = 0)
      .. Total Packet size = pkt_len + CTP Header (28 bytes)
    """
    CTP_HEADER = 28
    broadcast = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

    pkt = bytearray(pkt_len+CTP_HEADER)

    # Create Ethernet header:
    # .. Set src addr as provided by caller
    # .. Set broadcast destination address
    # .. Set Ethertype to loopback
    set_src_mac_addr(pkt, src_addr)
    set_dst_mac_addr(pkt, broadcast)
    set_ether_msgtype(pkt, ETHERTYPE_LOOPBACK)

    set_skip_count(pkt, 0)

    # For the CTP packet:
    # .. Set Function to Forward
    # .. Set own mac as Forwarding addr
    # .. Set receipt no per the argument
    set_ctp_function(pkt, CTP_FORWARD_MSG)
    set_ctp_forwarding_addr(pkt, src_addr)
    set_receipt_number(pkt, receipt_no)

    #pkt[24:26] = CTP_REPLY_MSG.to_bytes(2, sys.byteorder)
    pkt[24] = 0x01; pkt[25] = 0x00

    # Fill in the data
    for i in range(28, pkt_len):
        pkt[i] = i % 256

    return pkt

def bind_to_virtual_interface(ifname):
    """Binds to a virtual interface

    Provided a virtual tap interface, this function opens a valid connection to it
    and sets appropriate arguments. Virtual interfaces behave differently from physical
    ones and need special handling.

    This function is not portable and will only work on Linux systems. There is no standard
    way of interfacing with virtual interfaces.
    """
    TUNSETIFF = 0x400454ca
    TUNSETOWNER = TUNSETIFF + 2
    IFF_TUN = 0x0001
    IFF_TAP = 0x0002
    IFF_NO_PI = 0x1000
    LINUX_TAP_DEV = "/dev/net/tun"

    tap = os.open(LINUX_TAP_DEV, os.O_RDWR)
    ifr = struct.pack("16sH", bytes(ifname, "utf-8"), IFF_TAP | IFF_NO_PI)
    fcntl.ioctl(tap, TUNSETIFF, ifr)
    fcntl.ioctl(tap, TUNSETOWNER, 1000)
    return tap

def send_ctp_packet(sock, pkt, is_virtual):
    """Sends a CTP packet

    This function takes an input packet and sends it out on the interface. The packet must
    be handled differently if the interface is physical or virtual.
    """
    if is_virtual == True:
        return os.write(sock, pkt)
    else:
        return sock.send(pkt)

def recv_ctp_packet(sock, is_virtual):
    """Receives a CTP packet

    This function receives a packet from the interface. The packet must be handled differently
    if the interface is physical or virtual.
    """
    WAIT_FOR_PKT = 5 # Wait for 5 seconds for a packet
    ready = select.select([sock], [], [], WAIT_FOR_PKT)
    if ready[0]:
        if is_virtual == True:
            raw_msg = bytearray(os.read(sock, ETHER_MTU))
        else:
            raw_msg = bytearray(sock.recv(ETHER_MTU))
        return raw_msg
    else:
        return None

def check_msg_validity(msg):
    """Checks the validity of input message

    This function takes a raw network packet as input and checks if it is valid.
        .. The packet must be larger than the ethernet header.
        .. The ether msgtype must be loopback (0090).
        .. The message must be a CTP reply.
    If any of the above tests fail, this function returns -1. Otherwise 0.
    """
    if len(msg) < ETHER_HEADER_SIZE:
        logging.debug("Invalid Packet Received, Ignoring")
        return -1

    if get_ether_msgtype(msg) != ETHERTYPE_LOOPBACK:
        logging.debug("CTP Message Recvd")
        return -1

    ctp_msg_offset = get_ctp_msg_offset(msg)
    ctp_msg = msg[ctp_msg_offset:]

    if get_ctp_function(ctp_msg) == CTP_FORWARD_MSG:
        logging.debug("CTP Message Recvd, but its a forward msg. Ignoring")
        return -1

    # All tests have passed
    return 0

def sanity_check(ifname):
    if os.getuid() != 0:
        assert False

    if ifname == None:
        assert False

@pytest.mark.dependency(name="find_ctp_server")
def test_find_ctp_server(ifname, is_virtual):
    """Tries to find a CTP server by sending broadcast CTP messages

    This function tries to find a CTP server by sending broadcast CTP forward
    messages on the provided interface. The packets sent have a fixed length of
    100 (128 including header). For each packet sent, we wait for 5 seconds to
    receive a response from the server. If the response is not received, we try
    up to 10 times before exiting.
    """
    sanity_check(ifname)

    is_virtual = int(is_virtual)
    if is_virtual == True:
        sock = bind_to_virtual_interface(ifname)
    else:
        sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))
        sock.bind((ifname, 0))

    src_addr = get_mac_addr(ifname)
    TOTAL_RETRIES = 10

    for i in range(0, TOTAL_RETRIES):
        ctp_pkt = create_ctp_packet(src_addr, 100, i)

        send_ctp_packet(sock, ctp_pkt, is_virtual)

        raw_msg = recv_ctp_packet(sock, is_virtual)
        if raw_msg == None:
            continue

        if check_msg_validity(raw_msg) != 0:
            continue

        # Valid msg recvd, get the server mac address
        server_addr[0:6] = get_src_mac_addr(raw_msg)
        break

    os.close(sock)
    if i >= (TOTAL_RETRIES-1):
        assert False
    else:
        assert True

@pytest.mark.dependency(depends=["find_ctp_server"])
def test_do_ctp_unicast(ifname, is_virtual):
    """Runs the CTP Unicast test

    This function runs the CTP unicast test by sending ctp messages of
    increasing length to the CTP server and waiting for responses. If the response
    to a message is not received within 5 seconds, we retry up to 10 times before
    exiting.
    """
    sanity_check(ifname)

    is_virtual = int(is_virtual)
    if is_virtual == True:
        sock = bind_to_virtual_interface(ifname)
    else:
        sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))
        sock.bind((ifname, 0))

    src_addr = get_mac_addr(ifname)
    test_passed = True
    TOTAL_RETRIES = 10

    for pkt_size in range(0, ETHER_MTU):
        # Exit if the prevous iteration failed
        if test_passed == False:
            break

        # Create the CTP packet
        receipt = pkt_size
        ctp_pkt = create_ctp_packet(src_addr, pkt_size, receipt)
        ctp_pkt[0:6] = server_addr

        # Try to send the packet up to 10 times
        for i in range(0, TOTAL_RETRIES):
            ctp_pkt = create_ctp_packet(src_addr, pkt_size, pkt_size)
            ctp_pkt[0:6] = server_addr

            send_ctp_packet(sock, ctp_pkt, is_virtual)
            raw_msg = recv_ctp_packet(sock, is_virtual)
            if raw_msg == None:
                test_passed = False
                continue

            test_passed = not(check_msg_validity(raw_msg))
            break

    os.close(sock)
    assert test_passed == True
