"""
Copyright (c) 2022, Arm Limited and affiliates
SPDX-License-Identifier: Apache-2.0

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import sys, pathlib, threading

# Append path to the CTP Server module
sys.path.append(str(pathlib.Path(__file__).parent.resolve()) + "/ctp_server")

import uuid
from htrun import BaseHostTest

# Import CTP Server
from ctp_server import run_ctp_server

class EMAC_CTP_Server(BaseHostTest):

    ifname="armbr0"

    def ctp_server_start_callback(self, key, value, _):
        try:
            threading.Thread(target=run_ctp_server, args = (EMAC_CTP_Server.ifname, ), daemon=True).start()
            self.send_kv("started_ctp_server", value)
        except:
            self.send_kv("fail", value)

    def ctp_server_stop_callback(self, key, value, _):
        """
        No need to explicitly stop the ctp server thread. It will automatically be killed
        once the test has finished executing.
        """
        pass

    def setup(self):
        self.register_callback("start_ctp_server", self.ctp_server_start_callback)
        self.register_callback("stop_ctp_server", self.ctp_server_stop_callback)

    def teardown(self):
        pass
