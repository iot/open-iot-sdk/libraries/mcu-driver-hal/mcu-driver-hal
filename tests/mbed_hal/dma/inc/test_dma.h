/* Copyright (c) 2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MDH_HAL_TEST_DMA_H
#define MDH_HAL_TEST_DMA_H

#include "hal/dma_api.h"

void mdh_dma_test_memmove_irq(mdh_dma_t *dma);
void mdh_dma_test_memmove_polling(mdh_dma_t *dma);

void mdh_dma_test_clear_irq_flag(void);

#endif // MDH_HAL_TEST_DMA_H
