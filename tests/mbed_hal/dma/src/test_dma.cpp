/*
 * Copyright (c) 2013-2023, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "test_dma.h"

#include "cmsis_core_generic/generic_api.h"
#include "greentea-client/test_env.h"
#include "unity.h"
#include "utest/utest.h"

#include <stdio.h>
#include <string.h>

#define TEST_STRING     "Test string"
#define TEST_STRING_LEN (12)

static volatile bool interrupt_flag = false;

static void dma_irq_callback(void *ctx)
{
    /* The DMA's interrupt source flag is cleared in the on_event() implementation */
    interrupt_flag = true;
}

void mdh_dma_test_memmove_irq(mdh_dma_t *dma)
{
    char src[TEST_STRING_LEN] = TEST_STRING;
    char des[TEST_STRING_LEN] = {0};
    mdh_dma_error_t mdh_dma_err = MDH_DMA_ERROR_NO_ERROR;

    TEST_ASSERT(strncmp(src, des, TEST_STRING_LEN) != 0);
    /* The test caller function has to clear the flag */
    TEST_ASSERT(interrupt_flag == false);

    mdh_dma_err = mdh_dma_memmove(dma, src, des, TEST_STRING_LEN, dma_irq_callback, NULL);
    TEST_ASSERT(mdh_dma_err == MDH_DMA_ERROR_NO_ERROR);

    /* Wait until an interrupt has happend */
    while (interrupt_flag != true) {
        __WFE();
    }

    TEST_ASSERT(interrupt_flag == true);
    TEST_ASSERT(strncmp(src, des, TEST_STRING_LEN) == 0);
}

void mdh_dma_test_memmove_polling(mdh_dma_t *dma)
{

    char src[TEST_STRING_LEN] = TEST_STRING;
    char des[TEST_STRING_LEN] = {0};
    mdh_dma_error_t mdh_dma_err = MDH_DMA_ERROR_NO_ERROR;

    TEST_ASSERT(strncmp(src, des, TEST_STRING_LEN) != 0);

    mdh_dma_err = mdh_dma_memmove(dma, src, des, TEST_STRING_LEN, NULL, NULL);
    TEST_ASSERT(mdh_dma_err == MDH_DMA_ERROR_NO_ERROR);

    while (mdh_dma_get_state(dma) != MDH_DMA_STATE_READY) {
        /* The DMA IRQ is not enabled in the NVIC, but the interrupt
           still going to wake the core up */
        __WFE();
    }

    TEST_ASSERT(strncmp(src, des, TEST_STRING_LEN) == 0);
}

void mdh_dma_test_clear_irq_flag(void)
{
    interrupt_flag = false;
}
