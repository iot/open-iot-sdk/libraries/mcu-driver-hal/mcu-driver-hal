/*
 * Copyright (c) 2013-2022, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "greentea-client/test_env.h"
#include "unity.h"
#include "utest/utest.h"

#include <stdio.h>
#include <string.h>

#define PAYLOAD_LENGTH 36U

// Fill a buffer with a slice of the ASCII alphabet.
void fill_buffer(char *buffer, unsigned int length, unsigned int index)
{
    unsigned int start = length * index;
    for (unsigned int i = 0; i < length - 1; i++) {
        buffer[i] = 'a' + ((start + i) % 26);
    }
    buffer[length - 1] = '\0';
}

void test_echo_server(void)
{
    char _key[11] = {};
    char _tx_value[PAYLOAD_LENGTH + 1] = {};
    char _rx_value[PAYLOAD_LENGTH + 1] = {};
    const int echo_count = 16;
    const char _echo_count_key_const[] = "echo_count";
    const char _echo_key_const[] = "echo";
    int expected_key = 1;

    // Send up the echo count
    greentea_send_kv(_echo_count_key_const, echo_count);
    // Handshake with host
    do {
        greentea_parse_kv(_key, _rx_value, sizeof(_key), sizeof(_rx_value));
        // Ensure the key received is "echo_count" and not some old data
        expected_key = strcmp(_echo_count_key_const, _key);
    } while (expected_key);
    TEST_ASSERT_EQUAL_INT(echo_count, strtol(_rx_value, NULL, 10));

    for (int i = 0; i < echo_count; ++i) {
        fill_buffer(_tx_value, PAYLOAD_LENGTH, i);
        greentea_send_kv(_echo_key_const, _tx_value);
        do {
            greentea_parse_kv(_key, _rx_value, sizeof(_key), sizeof(_rx_value));
            // Ensure the key received is "echo" and not some old data
            expected_key = strcmp(_echo_key_const, _key);
        } while (expected_key);
        TEST_ASSERT(strncmp(_tx_value, _rx_value, PAYLOAD_LENGTH) == 0);
    }
}
