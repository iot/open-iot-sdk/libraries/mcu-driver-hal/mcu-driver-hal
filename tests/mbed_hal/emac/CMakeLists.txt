# Copyright (c) 2021-2023 Arm Limited
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

add_library(mdh-hal-test-emac
        src/test_emac.c
        src/test_emac_ctp.c
        src/test_emac_memory_manager.c
)

target_include_directories(mdh-hal-test-emac
    PUBLIC
        inc
)

target_link_libraries(mdh-hal-test-emac
    PRIVATE
        mcu-driver-hal-api
        greentea::client
        custom-greentea-io
        utest
        unity
        mbed-trace
)
