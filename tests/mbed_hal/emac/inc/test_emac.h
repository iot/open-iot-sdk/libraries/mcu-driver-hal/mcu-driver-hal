/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/emac_api.h"
#include "hal/us_ticker_api.h"

#ifndef MDH_HAL_TEST_EMAC_H
#define MDH_HAL_TEST_EMAC_H

#define MAC_48_ADDRESS_SIZE 6U

#define ETH_MAC_ADDRESS_SIZE MAC_48_ADDRESS_SIZE

/* The ethernet frame has 18 extra bytes in addition to the Data field:
 * Destination address[6], Source address[6], Type[2], FCS[4]
 * If VLAN support is enabled then an extra 4 bytes are added for the
 * VLAN Tag Control Information, making it 22 bytes long.
 */
#define ETH_FRAME_OVERHEAD_SIZE 22U

/* Initialises the Ethernet controller peripheral and records details about
 * the EMAC driver.
 *
 * Tests that the Ethernet controller peripheral is initialised correctly and
 * the call to retrieve the peripheral physical address succeeds.
 * This test case must run before any other test as it initialises the
 * Ethernet Controller peripheral.
 */
void test_emac_initialise(mdh_emac_t *emac);

/* Broadcasts a Configuration Test Protocol (CTP) message and expects
 * responses from an echo server.
 *
 * The echo server MAC address is recorded and is to be used for the remaining
 * test cases.
 */
void test_emac_broadcast(mdh_emac_t *emac, mdh_ticker_t *us_ticker);

/* Sends a Unicast Configuration Test Protocol (CTP) message
 * to the echo server and expects responses.
 *
 * This test case cannot be run before the `test_emac_broadcast()` test case
 */
void test_emac_unicast(mdh_emac_t *emac, mdh_ticker_t *us_ticker);

/* Sends a unicast Configuration Test Protocol (CTP) message of increasing size
 * up to the Maximum Transmission Unit (MTU) to an echo server and expects
 * responses from it.
 *
 * This test case cannot be run before the `test_emac_broadcast()` test case
 */
void test_emac_unicast_frame_len(mdh_emac_t *emac, mdh_ticker_t *us_ticker);

/* Sends a unicast Configuration Test Protocol (CTP) message of increasing size
 * up to the Maximum Transmission Unit (MTU) to an echo server and expects
 * responses from it. Starts from Frame of size 0 and goes up to MTU and repeats
 * the process multiple times.
 *
 * This test case cannot be run before the `test_emac_broadcast()` test case
 */
void test_emac_unicast_burst(mdh_emac_t *emac, mdh_ticker_t *us_ticker);

/* Sends multiple unicast Configuration Test Protocol (CTP) messages of random sizes
 * up to the Maximum Transmission Unit (MTU) to an echo server and expects
 * responses from it.
 *
 * This test case cannot be run before the `test_emac_broadcast()` test case
 */
void test_emac_unicast_long(mdh_emac_t *emac, mdh_ticker_t *us_ticker);

#if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
/* Tests that the multicast frames can be filtered in and out.
 * It also tests that the DUT accepts only multicast frames for the group
 * the Ethernet controller device was added to.
 *
 * This test case cannot be run before the `test_emac_broadcast()` test case
 */
void test_emac_multicast_filter(mdh_emac_t *emac, mdh_ticker_t *us_ticker);

/* Sends a Unicast Configuration Test Protocol (CTP) message
 * to the echo server with a broadcast address as the forwarding
 * address to assess the DUT's ability to process broadcast packets.
 *
 * This test case cannot be run before the `test_emac_broadcast()` test case
 */
void test_emac_broadcast_handling(mdh_emac_t *emac, mdh_ticker_t *us_ticker);
#endif // #if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)

#endif // MDH_HAL_TEST_EMAC_H
