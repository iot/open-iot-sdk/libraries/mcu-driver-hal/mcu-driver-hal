/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This module encapsulates all functionalities related to the Ethernet
// Configuration Testing Protocol (aka CTP).

#ifndef MDH_HAL_TEST_EMAC_CTP_H
#define MDH_HAL_TEST_EMAC_CTP_H

// dst_addr(6) + src_addr(6) + ether_type(2) + skipCount(2) + function_code(2) + fwd_addr(6) + function_code(2) +
// receipt_number(2)
#define MINIMUM_CTP_FRAME_LENGTH 28U

// Populates the Ethernet frame for a CTP Reply message encapsulated in a
// Forward Data message
size_t mdh_test_populate_eth_ctp_header(uint8_t *eth_packet,
                                        const uint8_t *dest_addr,
                                        const uint8_t *src_addr,
                                        const uint8_t *fwd_addr);

bool mdh_test_is_ctp_message(const uint8_t *eth_packet);

bool mdh_test_ctp_get_source_mac_addr(const uint8_t *eth_packet, uint8_t *addr);

bool mdh_test_ctp_is_reply_function_code(const uint8_t *eth_packet);

// Check if frame receipt number is of a previously sent message
bool mdh_test_ctp_is_response_to_tx_message(const uint8_t *eth_packet);

const uint8_t *mdh_test_ctp_get_function_code_location(const uint8_t *eth_packet);

#endif // MDH_HAL_TEST_EMAC_CTP_H
