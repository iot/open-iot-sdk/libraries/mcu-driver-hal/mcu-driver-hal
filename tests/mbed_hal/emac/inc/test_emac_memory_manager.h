/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MDH_HAL_TEST_EMAC_MEMORY_MANAGER_H
#define MDH_HAL_TEST_EMAC_MEMORY_MANAGER_H

#include "hal/network_stack_memory_manager.h"

/* Gets the default instance of the emac test memory manager. */
const mdh_network_stack_memory_manager_t *mdh_test_memory_manager_get_instance(void);

#endif // MDH_HAL_TEST_EMAC_MEMORY_MANAGER_H
