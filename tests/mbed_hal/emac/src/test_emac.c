/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "TEST EMAC"

#include "test_emac.h"

#include "hal/emac_api.h"
#include "hal/us_ticker_api.h"

#include "mbed_trace/mbed_trace.h"
#include "test_emac_ctp.h"
#include "test_emac_memory_manager.h"
#include "unity.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

typedef enum test_state_e {
    TEST_STATE_UNKNOWN,
    TEST_STATE_INIT,
    TEST_STATE_ECHO_SERVER_FOUND,
    TEST_STATE_CTP_RESPONSE_RECEIVED,
    TEST_STATE_DONE
} test_state_t;
typedef enum tx_state_e {
    TX_STATE_IDLE,
    TX_STATE_CTP_FRAME_SENDING,
    TX_STATE_CTP_FRAME_SENT,
    TX_STATE_CTP_FRAME_TX_FAILED
} tx_state_t;
typedef enum rx_state_e { RX_STATE_IDLE, RX_STATE_FRAME_RECEIVED } rx_state_t;

static const uint8_t gs_broadcast_mac_addr[ETH_MAC_ADDRESS_SIZE] = {0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU};
static uint8_t gs_echo_server_mac_addr[ETH_MAC_ADDRESS_SIZE] = {0x00U};
static uint8_t gs_dut_mac_addr[ETH_MAC_ADDRESS_SIZE] = {0x00U};
static size_t gs_dut_mtu = 0U;
static size_t gs_dut_max_frame_size = 0U;

static size_t gs_dut_memory_alignment = 0U;
static volatile test_state_t gs_test_state = TEST_STATE_UNKNOWN;
static volatile tx_state_t gs_tx_state = TX_STATE_IDLE;
static volatile rx_state_t gs_rx_state = RX_STATE_IDLE;
static mdh_network_stack_memory_manager_t *gs_network_memory_manager = NULL;

static bool has_time_elapsed(mdh_ticker_t *us_ticker, uint32_t milliseconds, uint32_t us_ticker_at_start);
static void on_receive_complete(mdh_emac_t *self, void *ctx, mdh_emac_receive_t status);
static void
on_transmit_complete(mdh_emac_t *self, void *ctx, mdh_emac_transfer_t status, const mdh_network_stack_buffer_t *buf);
static void send_ctp_message(
    mdh_emac_t *emac, const uint8_t *dest_addr, const uint8_t *src_addr, const uint8_t *fwd_addr, size_t frame_length);
static void process_received_frame(mdh_emac_t *emac, bool emac_server_known);
static void run_broadcast_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker);
static void run_unicast_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker, const uint8_t fwd_addr[ETH_MAC_ADDRESS_SIZE]);
static void run_unicast_frame_len_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker);
static void run_unicast_burst_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker);
static void run_unicast_long_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker);
#if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
static void run_multicast_filter_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker);
#endif // #if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)

static mdh_emac_callbacks_t gs_cbks = {.rx = on_receive_complete, .tx = on_transmit_complete, .event = NULL};

static bool has_time_elapsed(mdh_ticker_t *us_ticker, uint32_t milliseconds, uint32_t us_ticker_at_start)
{
    const ticker_info_t *ticker_info = mdh_us_ticker_get_info(us_ticker);

    uint32_t ticker_counter_mask = (1UL << ticker_info->bits) - 1UL;

    uint32_t ticks = (uint32_t)(((uint64_t)milliseconds * (uint64_t)ticker_info->frequency) / 1000ULL);

    // Higher values are unreachable.
    assert(ticks <= ticker_counter_mask);

    return ((mdh_us_ticker_read(us_ticker) - us_ticker_at_start) & ticker_counter_mask) >= ticks;
}

static void on_receive_complete(mdh_emac_t *self, void *ctx, mdh_emac_receive_t status)
{
    if (MDH_EMAC_RECEIVE_DONE == status) {
        gs_rx_state = RX_STATE_FRAME_RECEIVED;
    }
}

static void
on_transmit_complete(mdh_emac_t *self, void *ctx, mdh_emac_transfer_t status, const mdh_network_stack_buffer_t *buffer)
{
    if (MDH_EMAC_TRANSFER_DONE == status) {
        gs_tx_state = TX_STATE_CTP_FRAME_SENT;
    } else {
        gs_tx_state = TX_STATE_CTP_FRAME_TX_FAILED;
    }

    mdh_network_stack_memory_manager_free(gs_network_memory_manager, buffer);
}

static void send_ctp_message(
    mdh_emac_t *emac, const uint8_t *dest_addr, const uint8_t *src_addr, const uint8_t *fwd_addr, size_t frame_length)
{
    if (frame_length < MINIMUM_CTP_FRAME_LENGTH) {
        frame_length = MINIMUM_CTP_FRAME_LENGTH;
    }

    mdh_network_stack_buffer_t *buffer = mdh_network_stack_memory_manager_alloc_from_heap(
        gs_network_memory_manager, frame_length, gs_dut_memory_alignment);
    TEST_ASSERT_NOT_NULL(buffer);

    uint8_t *buffer_data = (uint8_t *)mdh_network_stack_memory_manager_get_payload(gs_network_memory_manager, buffer);

    size_t header_size = mdh_test_populate_eth_ctp_header(buffer_data, dest_addr, src_addr, fwd_addr);

    size_t buf_payload_len = mdh_network_stack_memory_manager_get_payload_len(gs_network_memory_manager, buffer);

    // Fill the data section of the CTP frame
    for (size_t i = header_size; i < buf_payload_len; i++) {
        buffer_data[i] = i;
    }

    TEST_ASSERT_EQUAL(MDH_EMAC_STATUS_NO_ERROR, mdh_emac_transmit(emac, buffer));
}

static void process_received_frame(mdh_emac_t *emac, bool emac_server_known)
{
    mdh_network_stack_buffer_t *buffer = mdh_network_stack_memory_manager_alloc_from_heap(
        gs_network_memory_manager, gs_dut_max_frame_size, gs_dut_memory_alignment);
    TEST_ASSERT_NOT_NULL(buffer);

    if (MDH_EMAC_STATUS_NO_ERROR != mdh_emac_receive(emac, buffer)) {
        goto done;
    }

    uint8_t *buffer_data = (uint8_t *)mdh_network_stack_memory_manager_get_payload(gs_network_memory_manager, buffer);

    if (!mdh_test_is_ctp_message(buffer_data))
        goto done;

    if (!emac_server_known) {
        mdh_test_ctp_get_source_mac_addr(buffer_data, gs_echo_server_mac_addr);
    } else {
        uint8_t response_mac_addr[ETH_MAC_ADDRESS_SIZE];

        mdh_test_ctp_get_source_mac_addr(buffer_data, response_mac_addr);
        if (memcmp(response_mac_addr, gs_echo_server_mac_addr, ETH_MAC_ADDRESS_SIZE)) {
            goto done;
        }
    }

    if ((mdh_test_ctp_is_reply_function_code(buffer_data)) && (mdh_test_ctp_is_response_to_tx_message(buffer_data))) {
        gs_test_state = TEST_STATE_CTP_RESPONSE_RECEIVED;
    }

done:
    mdh_network_stack_memory_manager_free(gs_network_memory_manager, buffer);
}

static void run_broadcast_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    const uint8_t max_resend_count = 6U;
    const uint8_t max_echo_server_response_count = 3U;

    uint8_t resend_count = max_resend_count;
    uint8_t echo_server_response_count = 0U;

    gs_rx_state = RX_STATE_IDLE;
    gs_tx_state = TX_STATE_IDLE;

    while (gs_test_state != TEST_STATE_DONE) {
        gs_tx_state = TX_STATE_CTP_FRAME_SENDING;
        send_ctp_message(emac, gs_broadcast_mac_addr, gs_dut_mac_addr, gs_dut_mac_addr, 100U);

        // Wait for frame to be transmitted
        uint32_t us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_tx_state == TX_STATE_CTP_FRAME_SENDING) && !has_time_elapsed(us_ticker, 1000U, us_ticker_at_start)) {
        }

        TEST_ASSERT_EQUAL(TX_STATE_CTP_FRAME_SENT, gs_tx_state);
        gs_tx_state = TX_STATE_IDLE;

        // Wait to receive a frame
        us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_rx_state != RX_STATE_FRAME_RECEIVED) && !has_time_elapsed(us_ticker, 3000U, us_ticker_at_start)) {
        }

        if (gs_rx_state == RX_STATE_FRAME_RECEIVED) {
            gs_rx_state = RX_STATE_IDLE;
            process_received_frame(emac, false);
        }

        if (gs_test_state == TEST_STATE_CTP_RESPONSE_RECEIVED) {
            resend_count = max_resend_count;
            echo_server_response_count++;
            tr_info("Echo server response count: %i", echo_server_response_count);
            gs_test_state = TEST_STATE_INIT;
        } else if (resend_count > 0U) {
            tr_info("Retry remaining: %i", resend_count);
            resend_count--;
            continue;
        } else {
            tr_info("Maximum number of retry reached");
            break;
        }

        if (echo_server_response_count == max_echo_server_response_count) {
            gs_test_state = TEST_STATE_DONE;
        }
    }

    tr_info("Echo server MAC address: %02X:%02X:%02X:%02X:%02X:%02X",
            gs_echo_server_mac_addr[0],
            gs_echo_server_mac_addr[1],
            gs_echo_server_mac_addr[2],
            gs_echo_server_mac_addr[3],
            gs_echo_server_mac_addr[4],
            gs_echo_server_mac_addr[5]);
    TEST_ASSERT_EQUAL(TEST_STATE_DONE, gs_test_state);
    gs_test_state = TEST_STATE_ECHO_SERVER_FOUND;
    TEST_ASSERT_EQUAL(max_echo_server_response_count, echo_server_response_count);
}

static void run_unicast_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker, const uint8_t fwd_addr[ETH_MAC_ADDRESS_SIZE])
{
    const uint8_t max_resend_count = 6U;
    const uint8_t max_echo_server_response_count = 3U;

    uint8_t resend_count = max_resend_count;
    uint8_t echo_server_response_count = 0U;

    gs_rx_state = RX_STATE_IDLE;
    gs_tx_state = TX_STATE_IDLE;

    while (gs_test_state != TEST_STATE_DONE) {
        gs_tx_state = TX_STATE_CTP_FRAME_SENDING;

        send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, fwd_addr, 100U);

        // Wait for frame to be transmitted
        uint32_t us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_tx_state == TX_STATE_CTP_FRAME_SENDING) && !has_time_elapsed(us_ticker, 1000U, us_ticker_at_start)) {
        }

        TEST_ASSERT_EQUAL(TX_STATE_CTP_FRAME_SENT, gs_tx_state);
        gs_tx_state = TX_STATE_IDLE;

        // Wait to receive a frame
        us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_rx_state != RX_STATE_FRAME_RECEIVED) && !has_time_elapsed(us_ticker, 3000U, us_ticker_at_start)) {
        }

        if (gs_rx_state == RX_STATE_FRAME_RECEIVED) {
            gs_rx_state = RX_STATE_IDLE;
            process_received_frame(emac, true);
        }

        if (gs_test_state == TEST_STATE_CTP_RESPONSE_RECEIVED) {
            resend_count = max_resend_count;
            echo_server_response_count++;
            tr_info("Echo server response count: %i", echo_server_response_count);
            gs_test_state = TEST_STATE_INIT;
        } else if (resend_count > 0U) {
            tr_info("Retry remaining: %i", resend_count);
            resend_count--;
            continue;
        } else {
            tr_info("Maximum number of retry reached");
            break;
        }

        if (echo_server_response_count == max_echo_server_response_count) {
            gs_test_state = TEST_STATE_DONE;
        }
    }

    TEST_ASSERT_EQUAL(TEST_STATE_DONE, gs_test_state);
    gs_test_state = TEST_STATE_ECHO_SERVER_FOUND;
    TEST_ASSERT_EQUAL(max_echo_server_response_count, echo_server_response_count);
}

static void run_unicast_long_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    const uint8_t max_resend_count = 5U;
    const uint16_t max_echo_server_response_count = 50000U;

    uint8_t resend_count = max_resend_count;
    uint16_t echo_server_response_count = 0U;

    gs_rx_state = RX_STATE_IDLE;
    gs_tx_state = TX_STATE_IDLE;

    size_t frame_length = 0U;

    while (gs_test_state != TEST_STATE_DONE) {
        gs_tx_state = TX_STATE_CTP_FRAME_SENDING;

        send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, gs_dut_mac_addr, frame_length);

        // Wait for frame to be transmitted
        uint32_t us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_tx_state == TX_STATE_CTP_FRAME_SENDING) && !has_time_elapsed(us_ticker, 1000U, us_ticker_at_start)) {
        }

        TEST_ASSERT_EQUAL(TX_STATE_CTP_FRAME_SENT, gs_tx_state);
        gs_tx_state = TX_STATE_IDLE;

        // Wait to receive a frame
        us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_rx_state != RX_STATE_FRAME_RECEIVED) && !has_time_elapsed(us_ticker, 350U, us_ticker_at_start)) {
        }

        if (gs_rx_state == RX_STATE_FRAME_RECEIVED) {
            gs_rx_state = RX_STATE_IDLE;
            process_received_frame(emac, true);
        }

        if (gs_test_state == TEST_STATE_CTP_RESPONSE_RECEIVED) {
            resend_count = max_resend_count;
            echo_server_response_count++;
            if (echo_server_response_count % 2000 == 0) {
                tr_info("Echo server response count: %i", echo_server_response_count);
            }
            gs_test_state = TEST_STATE_INIT;
            frame_length = rand() % gs_dut_mtu;
        } else if (resend_count > 0U) {
            tr_info("Retry remaining: %i", resend_count);
            resend_count--;
            continue;
        } else {
            tr_info("Maximum number of retry reached");
            break;
        }

        if (echo_server_response_count == max_echo_server_response_count) {
            gs_test_state = TEST_STATE_DONE;
        }
    }

    TEST_ASSERT_EQUAL(TEST_STATE_DONE, gs_test_state);
    gs_test_state = TEST_STATE_ECHO_SERVER_FOUND;
    TEST_ASSERT_EQUAL(max_echo_server_response_count, echo_server_response_count);
}

static void run_unicast_frame_len_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    const uint8_t max_resend_count = 5U;

    uint8_t resend_count = max_resend_count;

    gs_rx_state = RX_STATE_IDLE;
    gs_tx_state = TX_STATE_IDLE;

    size_t frame_length = 0U;

    while (gs_test_state != TEST_STATE_DONE) {
        tr_info("frame_length: %i", frame_length);
        gs_tx_state = TX_STATE_CTP_FRAME_SENDING;
        send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, gs_dut_mac_addr, frame_length);

        uint32_t us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_tx_state == TX_STATE_CTP_FRAME_SENDING) && !has_time_elapsed(us_ticker, 1000U, us_ticker_at_start)) {
        }

        TEST_ASSERT_EQUAL(TX_STATE_CTP_FRAME_SENT, gs_tx_state);
        gs_tx_state = TX_STATE_IDLE;

        us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_rx_state != RX_STATE_FRAME_RECEIVED) && !has_time_elapsed(us_ticker, 500U, us_ticker_at_start)) {
        }

        if (gs_rx_state == RX_STATE_FRAME_RECEIVED) {
            gs_rx_state = RX_STATE_IDLE;
            process_received_frame(emac, true);
        }

        if (gs_test_state == TEST_STATE_CTP_RESPONSE_RECEIVED) {
            resend_count = max_resend_count;

            if (frame_length == gs_dut_mtu) {
                gs_test_state = TEST_STATE_DONE;
                break;
            }

            if (frame_length + 50U >= gs_dut_mtu) {
                frame_length = gs_dut_mtu;
            } else {
                frame_length += 50U;
            }

            gs_test_state = TEST_STATE_INIT;
        } else if (resend_count > 0U) {
            tr_info("Retry remaining: %i", resend_count);
            resend_count--;
            continue;
        } else {
            tr_info("Maximum number of retry reached");
            break;
        }
    }

    TEST_ASSERT_EQUAL(TEST_STATE_DONE, gs_test_state);
    gs_test_state = TEST_STATE_ECHO_SERVER_FOUND;
}

static void run_unicast_burst_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    const uint8_t max_resend_count = 5U;
    const uint8_t max_test_repetition = 10U;

    uint8_t resend_count = max_resend_count;
    uint8_t test_repetition = 0U;

    gs_rx_state = RX_STATE_IDLE;
    gs_tx_state = TX_STATE_IDLE;

    size_t frame_length = 0U;

    while (test_repetition < max_test_repetition) {

        if (gs_test_state == TEST_STATE_DONE) {
            frame_length = 0U;
            gs_test_state = TEST_STATE_INIT;
        }

        tr_info("frame_length: %i", frame_length);
        gs_tx_state = TX_STATE_CTP_FRAME_SENDING;
        send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, gs_dut_mac_addr, frame_length);

        uint32_t us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_tx_state == TX_STATE_CTP_FRAME_SENDING) && !has_time_elapsed(us_ticker, 1000U, us_ticker_at_start)) {
        }

        TEST_ASSERT_EQUAL(TX_STATE_CTP_FRAME_SENT, gs_tx_state);
        gs_tx_state = TX_STATE_IDLE;

        us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_rx_state != RX_STATE_FRAME_RECEIVED) && !has_time_elapsed(us_ticker, 500U, us_ticker_at_start)) {
        }

        if (gs_rx_state == RX_STATE_FRAME_RECEIVED) {
            gs_rx_state = RX_STATE_IDLE;
            process_received_frame(emac, true);
        }

        if (gs_test_state == TEST_STATE_CTP_RESPONSE_RECEIVED) {
            resend_count = max_resend_count;

            if (frame_length == gs_dut_mtu) {
                gs_test_state = TEST_STATE_DONE;
                test_repetition++;
                continue;
            }

            if (frame_length + 50U >= gs_dut_mtu) {
                frame_length = gs_dut_mtu;
            } else {
                frame_length += 50U;
            }

            gs_test_state = TEST_STATE_INIT;
        } else if (resend_count > 0U) {
            tr_info("Retry remaining: %i", resend_count);
            resend_count--;
            continue;
        } else {
            tr_info("Maximum number of retry reached");
            break;
        }
    }

    TEST_ASSERT_EQUAL(TEST_STATE_DONE, gs_test_state);
    gs_test_state = TEST_STATE_ECHO_SERVER_FOUND;
}

#if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
static void run_multicast_filter_task(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    // The successful run depends on the order below, do not change.
    enum {
        EMAC_MULTICAST_TEST_STEP_CHECK_UNICAST = 0U,
        EMAC_MULTICAST_TEST_STEP_IPV6_FILTER_OUT,
        EMAC_MULTICAST_TEST_STEP_IPV6_FILTER_IN,
        EMAC_MULTICAST_TEST_STEP_IPV6_REMOVE,
        EMAC_MULTICAST_TEST_STEP_IPV4_FILTER_OUT,
        EMAC_MULTICAST_TEST_STEP_IPV4_FILTER_IN,
        EMAC_MULTICAST_TEST_STEP_RECEIVE_ALL,
        EMAC_MULTICAST_TEST_STEP_END
    } test_step = EMAC_MULTICAST_TEST_STEP_CHECK_UNICAST;

    const uint8_t max_resend_count = 3U;

    uint8_t resend_count = max_resend_count;

    bool wait_for_response = false;
    bool go_to_next_step = false;

    gs_rx_state = RX_STATE_IDLE;
    gs_tx_state = TX_STATE_IDLE;

    while (gs_test_state != TEST_STATE_DONE) {
        gs_tx_state = TX_STATE_CTP_FRAME_SENDING;

        switch (test_step) {
            case EMAC_MULTICAST_TEST_STEP_CHECK_UNICAST: {
                tr_info("Check unicast message functionality");

                wait_for_response = true;

                send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, gs_dut_mac_addr, 100U);

                break;
            }

            case EMAC_MULTICAST_TEST_STEP_IPV6_FILTER_OUT: {
                tr_info("Set ipv6 multicast filter, test if input message is rejected");

                uint8_t multicast_group_addr[] = {0x33U, 0x33U, 0x1BU, 0x1CU, 0x1DU, 0x1EU};
                TEST_ASSERT_EQUAL(MDH_EMAC_STATUS_NO_ERROR,
                                  mdh_emac_add_to_multicast_group(emac, multicast_group_addr));

                wait_for_response = false;
                uint8_t fwd_addr[] = {0x33U, 0x33U, 0x11U, 0x22U, 0x33U, 0x44U};
                send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, fwd_addr, 100U);

                break;
            }

            case EMAC_MULTICAST_TEST_STEP_IPV6_FILTER_IN: {
                tr_info("Set ipv6 multicast filter, test that input message is accepted");

                uint8_t multicast_group_addr[] = {0x33U, 0x33U, 0xAAU, 0xBBU, 0xCCU, 0xDDU};
                TEST_ASSERT_EQUAL(MDH_EMAC_STATUS_NO_ERROR,
                                  mdh_emac_add_to_multicast_group(emac, multicast_group_addr));

                wait_for_response = true;
                send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, multicast_group_addr, 100U);

                break;
            }

            case EMAC_MULTICAST_TEST_STEP_IPV6_REMOVE: {
                tr_info("Remove from previously added ipv6 multicast group, test that input message is rejected");

                uint8_t previous_multicast_group_addr[] = {0x33U, 0x33U, 0xAAU, 0xBBU, 0xCCU, 0xDDU};
                TEST_ASSERT_EQUAL(MDH_EMAC_STATUS_NO_ERROR,
                                  mdh_emac_remove_from_multicast_group(emac, previous_multicast_group_addr));

                wait_for_response = false;
                send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, previous_multicast_group_addr, 100U);

                break;
            }

            case EMAC_MULTICAST_TEST_STEP_IPV4_FILTER_OUT: {
                tr_info("Set ipv4 multicast filter, test if input message is rejected");

                uint8_t multicast_group_addr[] = {0x01U, 0x00U, 0x5EU, 0xA1U, 0xA2U, 0xA3U};
                TEST_ASSERT_EQUAL(MDH_EMAC_STATUS_NO_ERROR,
                                  mdh_emac_add_to_multicast_group(emac, multicast_group_addr));

                wait_for_response = false;
                uint8_t fwd_addr[] = {0x01U, 0x00U, 0x5EU, 0x11U, 0x22U, 0x33U};
                send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, fwd_addr, 100U);

                break;
            }

            case EMAC_MULTICAST_TEST_STEP_IPV4_FILTER_IN: {
                tr_info("Set ipv4 multicast filter, test that input message is accepted");

                uint8_t multicast_group_addr[] = {0x01U, 0x00U, 0x5EU, 0xA5U, 0xA6U, 0xA7U};
                TEST_ASSERT_EQUAL(MDH_EMAC_STATUS_NO_ERROR,
                                  mdh_emac_add_to_multicast_group(emac, multicast_group_addr));

                wait_for_response = true;
                send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, multicast_group_addr, 100U);

                break;
            }

            case EMAC_MULTICAST_TEST_STEP_RECEIVE_ALL: {
                tr_info("Set receive all multicast, verify that input message is accepted");

                TEST_ASSERT_EQUAL(MDH_EMAC_STATUS_NO_ERROR, mdh_emac_config_multicast_reception(emac, true));

                wait_for_response = true;
                uint8_t multicast_group_addr[] = {0x33U, 0x33U, 0x11U, 0x12U, 0x33U, 0x44U};
                send_ctp_message(emac, gs_echo_server_mac_addr, gs_dut_mac_addr, multicast_group_addr, 100U);

                break;
            }

            case EMAC_MULTICAST_TEST_STEP_END: {
                gs_test_state = TEST_STATE_DONE;
                break;
            }

            default: {
                TEST_ASSERT_TRUE_MESSAGE(false, "Unknown EMAC multicast test state");
                break;
            }
        }

        if (gs_test_state == TEST_STATE_DONE) {
            break;
        }

        uint32_t us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_tx_state == TX_STATE_CTP_FRAME_SENDING) && !has_time_elapsed(us_ticker, 100U, us_ticker_at_start)) {
        }

        TEST_ASSERT_EQUAL(TX_STATE_CTP_FRAME_SENT, gs_tx_state);
        gs_tx_state = TX_STATE_IDLE;

        us_ticker_at_start = mdh_us_ticker_read(us_ticker);
        while ((gs_rx_state != RX_STATE_FRAME_RECEIVED) && !has_time_elapsed(us_ticker, 500U, us_ticker_at_start)) {
        }

        if (gs_rx_state == RX_STATE_FRAME_RECEIVED) {
            gs_rx_state = RX_STATE_IDLE;
            process_received_frame(emac, true);
        }

        if (gs_test_state == TEST_STATE_CTP_RESPONSE_RECEIVED) {
            resend_count = max_resend_count;

            gs_test_state = TEST_STATE_INIT;

            TEST_ASSERT_TRUE_MESSAGE(wait_for_response,
                                     "Multicast was not filtered, No response expected but one was received.");
            go_to_next_step = true;

        } else if (resend_count > 0U) {
            tr_info("Retry remaining: %i", resend_count);
            resend_count--;
            continue;
        } else {
            if (wait_for_response == true) {
                tr_info("Maximum number of retry reached");
                break;
            } else {
                go_to_next_step = true;
                resend_count = max_resend_count;
            }
        }

        if (go_to_next_step) {
            test_step++;
#if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_IPV4_MULTICAST_FILTER == 1)
            if (test_step == EMAC_MULTICAST_TEST_STEP_IPV4_FILTER_OUT) {
                // Skip IPV4 related test steps
                test_step = EMAC_MULTICAST_TEST_STEP_RECEIVE_ALL;
            }
#endif // (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_IPV4_MULTICAST_FILTER == 1)

            gs_test_state = TEST_STATE_INIT;
            go_to_next_step = false;
        }
    }

    TEST_ASSERT_EQUAL(TEST_STATE_DONE, gs_test_state);
    gs_test_state = TEST_STATE_ECHO_SERVER_FOUND;
}
#endif // #if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)

void test_emac_initialise(mdh_emac_t *emac)
{
    TEST_ASSERT_NOT_NULL(emac);

    gs_network_memory_manager = (mdh_network_stack_memory_manager_t *)mdh_test_memory_manager_get_instance();

    TEST_ASSERT_NOT_NULL(gs_network_memory_manager);

    TEST_ASSERT_EQUAL(MDH_EMAC_STATUS_NO_ERROR, mdh_emac_power_up(emac, &gs_cbks, gs_network_memory_manager, NULL));

    TEST_ASSERT_EQUAL(ETH_MAC_ADDRESS_SIZE, mdh_emac_get_mac_addr_size(emac));

    TEST_ASSERT_EQUAL(MDH_EMAC_STATUS_NO_ERROR, mdh_emac_get_mac_addr(emac, gs_dut_mac_addr));
    tr_info("DUT MAC address: %02X:%02X:%02X:%02X:%02X:%02X",
            gs_dut_mac_addr[0],
            gs_dut_mac_addr[1],
            gs_dut_mac_addr[2],
            gs_dut_mac_addr[3],
            gs_dut_mac_addr[4],
            gs_dut_mac_addr[5]);

    gs_dut_mtu = mdh_emac_get_mtu(emac);
    gs_dut_max_frame_size = gs_dut_mtu + ETH_FRAME_OVERHEAD_SIZE;
    TEST_ASSERT_NOT_EQUAL(0U, gs_dut_mtu);

    gs_dut_memory_alignment = mdh_emac_get_align(emac);
    TEST_ASSERT_NOT_EQUAL(0U, gs_dut_memory_alignment);

    // Check cropped name can be returned
    char cropped_interface_name[2U] = {0xFF, 0xFF};
    TEST_ASSERT_NOT_EQUAL(0U,
                          mdh_emac_get_interface_name(emac, cropped_interface_name, sizeof(cropped_interface_name)));
    // Check cropped name is null-terminated
    TEST_ASSERT_EQUAL('\0', cropped_interface_name[sizeof(cropped_interface_name) - 1U]);

    char uncropped_interface_name[11U] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    TEST_ASSERT_NOT_EQUAL(
        0U, mdh_emac_get_interface_name(emac, uncropped_interface_name, sizeof(uncropped_interface_name)));
    // Check un-cropped name is null-terminated
    TEST_ASSERT_EQUAL('\0', uncropped_interface_name[strlen(uncropped_interface_name)]);

    gs_test_state = TEST_STATE_INIT;
}

void test_emac_broadcast(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    TEST_ASSERT_EQUAL(TEST_STATE_INIT, gs_test_state);

    run_broadcast_task(emac, us_ticker);
}

void test_emac_unicast(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    TEST_ASSERT_EQUAL(TEST_STATE_ECHO_SERVER_FOUND, gs_test_state);

    run_unicast_task(emac, us_ticker, gs_dut_mac_addr);
}

void test_emac_unicast_frame_len(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    TEST_ASSERT_EQUAL(TEST_STATE_ECHO_SERVER_FOUND, gs_test_state);

    run_unicast_frame_len_task(emac, us_ticker);
}

void test_emac_unicast_long(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    TEST_ASSERT_EQUAL(TEST_STATE_ECHO_SERVER_FOUND, gs_test_state);

    run_unicast_long_task(emac, us_ticker);
}

void test_emac_unicast_burst(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    TEST_ASSERT_EQUAL(TEST_STATE_ECHO_SERVER_FOUND, gs_test_state);

    run_unicast_burst_task(emac, us_ticker);
}

void test_emac_broadcast_handling(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    TEST_ASSERT_EQUAL(TEST_STATE_ECHO_SERVER_FOUND, gs_test_state);

    run_unicast_task(emac, us_ticker, gs_broadcast_mac_addr);
}

#if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
void test_emac_multicast_filter(mdh_emac_t *emac, mdh_ticker_t *us_ticker)
{
    TEST_ASSERT_EQUAL(TEST_STATE_ECHO_SERVER_FOUND, gs_test_state);

    run_multicast_filter_task(emac, us_ticker);
}
#endif // #if (MBED_CONF_NETWORK_EMAC_NO_SUPPORT_FOR_MULTICAST_FILTER == 0)
