/* Copyright (c) 2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_emac.h"
#include "test_emac_memory_manager.h"

#include <stdint.h>
#include <string.h>

#define CTP_FUNC_CODE_REPLY    0x0001U
#define CTP_FUNC_CODE_FWD_DATA 0x0002U

#define CTP_SKIP_COUNT_SIZE     2U
#define CTP_FUNCTION_CODE_SIZE  2U
#define CTP_FWD_ADDR_SIZE       ETH_MAC_ADDRESS_SIZE
#define CTP_RECEIPT_NUMBER_SIZE 2U

#define ETHERNET_DESTINATION_ADDR_SIZE ETH_MAC_ADDRESS_SIZE
#define ETHERNET_SOURCE_ADDR_SIZE      ETH_MAC_ADDRESS_SIZE
#define ETHERNET_ETHER_TYPE_SIZE       2U
#define ETHERNET_HEADER_SIZE           (ETHERNET_DESTINATION_ADDR_SIZE + ETHERNET_SOURCE_ADDR_SIZE + ETHERNET_ETHER_TYPE_SIZE)

#define ETHERNET_SOURCE_ADDRESS_POS    6U
#define ETHERNET_ETHER_TYPE_MSB_POS    12U
#define ETHERNET_ETHER_TYPE_LSB_POS    13U
#define ETHERNET_CTP_SKIPCOUNT_LSB_POS 14U
#define ETHERNET_CTP_SKIPCOUNT_MSB_POS 15U

#define ETHERNET_ETHER_TYPE_CTP 0x9000U

static uint16_t gs_ctp_receipt_number = 0U;

size_t mdh_test_populate_eth_ctp_header(uint8_t *eth_packet,
                                        const uint8_t *dest_addr,
                                        const uint8_t *src_addr,
                                        const uint8_t *fwd_addr)
{
    size_t i = 0U;
    memcpy(&eth_packet[i], dest_addr, ETHERNET_DESTINATION_ADDR_SIZE);
    i += ETHERNET_DESTINATION_ADDR_SIZE;
    memcpy(&eth_packet[i], src_addr, ETHERNET_SOURCE_ADDR_SIZE);
    i += ETHERNET_SOURCE_ADDR_SIZE;

    eth_packet[i++] = (uint8_t)(ETHERNET_ETHER_TYPE_CTP >> 8U);
    eth_packet[i++] = (uint8_t)(ETHERNET_ETHER_TYPE_CTP);

    // CTP fields are encoded in Little Endian unlike the rest of the Ethernet frame

    // skipCount
    eth_packet[i++] = 0x00U;
    eth_packet[i++] = 0x00U;

    // Function code Function
    eth_packet[i++] = (uint8_t)(CTP_FUNC_CODE_FWD_DATA);
    eth_packet[i++] = (uint8_t)(CTP_FUNC_CODE_FWD_DATA >> 8U);
    memcpy(&eth_packet[i], fwd_addr, CTP_FWD_ADDR_SIZE);
    i += CTP_FWD_ADDR_SIZE;

    eth_packet[i++] = (uint8_t)(CTP_FUNC_CODE_REPLY);
    eth_packet[i++] = (uint8_t)(CTP_FUNC_CODE_REPLY >> 8U);
    gs_ctp_receipt_number++;
    eth_packet[i++] = gs_ctp_receipt_number;
    eth_packet[i++] = gs_ctp_receipt_number >> 8U;

    return i;
}

bool mdh_test_is_ctp_message(const uint8_t *eth_packet)
{
    if (NULL == eth_packet) {
        return false;
    }

    return ((eth_packet[ETHERNET_ETHER_TYPE_MSB_POS] == (uint8_t)(ETHERNET_ETHER_TYPE_CTP >> 8U))
            && (eth_packet[ETHERNET_ETHER_TYPE_LSB_POS] == (uint8_t)ETHERNET_ETHER_TYPE_CTP));
}

bool mdh_test_ctp_get_source_mac_addr(const uint8_t *eth_packet, uint8_t *addr)
{
    if (NULL == eth_packet) {
        return false;
    }

    memcpy(addr, &eth_packet[ETHERNET_SOURCE_ADDRESS_POS], ETHERNET_SOURCE_ADDR_SIZE);

    return true;
}

const uint8_t *mdh_test_ctp_get_function_code_location(const uint8_t *eth_packet)
{
    if ((NULL == eth_packet) && !mdh_test_is_ctp_message(eth_packet)) {
        return NULL;
    }

    uint16_t skip_count = (((uint16_t)eth_packet[ETHERNET_CTP_SKIPCOUNT_MSB_POS] << 8U)
                           | ((uint16_t)eth_packet[ETHERNET_CTP_SKIPCOUNT_LSB_POS]));

    return &eth_packet[ETHERNET_CTP_SKIPCOUNT_MSB_POS + 1] + skip_count;
}

bool mdh_test_ctp_is_reply_function_code(const uint8_t *eth_packet)
{
    const uint8_t *function_code_location = mdh_test_ctp_get_function_code_location(eth_packet);

    if (NULL == function_code_location) {
        return false;
    }

    uint16_t function_code = (((uint16_t)function_code_location[1U] << 8U) | ((uint16_t)function_code_location[0U]));

    return (function_code == CTP_FUNC_CODE_REPLY);
}

bool mdh_test_ctp_is_response_to_tx_message(const uint8_t *eth_packet)
{
    const uint8_t *function_code_location = mdh_test_ctp_get_function_code_location(eth_packet);

    if (NULL == function_code_location) {
        return false;
    }

    const uint8_t *packet_receipt_number_location = function_code_location + CTP_FUNCTION_CODE_SIZE;

    uint16_t packet_receipt_number =
        (((uint16_t)packet_receipt_number_location[1U] << 8U) | ((uint16_t)packet_receipt_number_location[0U]));

    return (packet_receipt_number <= gs_ctp_receipt_number);
}
