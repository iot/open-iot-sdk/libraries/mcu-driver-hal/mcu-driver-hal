/* Copyright (c) 2021-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/network_stack_memory_manager.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef struct emac_memory_buf_s {
    void *allocated_memory; /**< Pointer to allocated buffer */
    void *data;             /**< Pointer to data (starting at aligned memory address) */
    uint32_t capacity;      /**< Allocated buffer length. */
    uint32_t length;        /**< Consumed buffer length. */
} emac_memory_buf_t;

static mdh_network_stack_buffer_t *
alloc_from_heap(mdh_network_stack_memory_manager_t *const self, size_t size, size_t align);
static void free_buf(mdh_network_stack_memory_manager_t *const self, const mdh_network_stack_buffer_t *buf);
static void copy(mdh_network_stack_memory_manager_t *const self,
                 const mdh_network_stack_buffer_t *const source,
                 const mdh_network_stack_buffer_t *destination);
static size_t copy_to_buf(mdh_network_stack_memory_manager_t *const self,
                          const void *const ptr,
                          size_t size,
                          const mdh_network_stack_buffer_t *buf);
static size_t copy_from_buf(mdh_network_stack_memory_manager_t *const self,
                            const mdh_network_stack_buffer_t *const buf,
                            void *const ptr,
                            size_t size);
static void *get_payload(mdh_network_stack_memory_manager_t *const self, const mdh_network_stack_buffer_t *const buf);
static size_t get_payload_len(mdh_network_stack_memory_manager_t *const self,
                              const mdh_network_stack_buffer_t *const buf);
static void
set_payload_len(mdh_network_stack_memory_manager_t *const self, const mdh_network_stack_buffer_t *buf, size_t size);

static const mdh_network_stack_memory_manager_vtable_t gsc_vtable = {
    .alloc_from_heap = alloc_from_heap,
    .alloc_from_static_pool = NULL,
    .get_static_pool_alloc_unit = NULL,
    .free = free_buf,
    .copy = copy,
    .copy_to_buf = copy_to_buf,
    .copy_from_buf = copy_from_buf,
    .get_payload = get_payload,
    .get_payload_len = get_payload_len,
    .set_payload_len = set_payload_len,
};

static const mdh_network_stack_memory_manager_t gsc_memory_manager = {
    .vtable = &gsc_vtable,
};

const mdh_network_stack_memory_manager_t *mdh_test_memory_manager_get_instance(void)
{
    return &gsc_memory_manager;
}

static mdh_network_stack_buffer_t *
alloc_from_heap(mdh_network_stack_memory_manager_t *const self, size_t size, size_t align)
{
    emac_memory_buf_t *buf = (emac_memory_buf_t *)malloc(sizeof(emac_memory_buf_t));
    if (!buf) {
        return NULL;
    }

    buf->allocated_memory = malloc(size + align);
    if (!buf->allocated_memory) {
        free(buf);
        return NULL;
    }

    buf->data = buf->allocated_memory;
    buf->capacity = buf->length = size;

    if (align) {
        uint32_t remainder = (uint32_t)buf->data % align;
        if (remainder) {
            uint32_t offset = align - remainder;
            if (offset >= align) {
                offset = align;
            }
            buf->data = (char *)buf->data + offset;
        }
    }

    return (mdh_network_stack_buffer_t *)buf;
}

static void free_buf(mdh_network_stack_memory_manager_t *const self, const mdh_network_stack_buffer_t *buf)
{
    if (!buf)
        return;

    const emac_memory_buf_t *buffer = (const emac_memory_buf_t *)buf;

    free(buffer->allocated_memory);
    free((void *)buffer);
}

static void copy(mdh_network_stack_memory_manager_t *const self,
                 const mdh_network_stack_buffer_t *const source,
                 const mdh_network_stack_buffer_t *destination)
{
    if (!source || !destination)
        return;

    const emac_memory_buf_t *const src_buf = (const emac_memory_buf_t *const)source;
    const emac_memory_buf_t *dst_buf = (const emac_memory_buf_t *)destination;

    if (src_buf->length != dst_buf->length)
        return;

    memcpy(dst_buf->data, src_buf->data, dst_buf->length);
}

static size_t copy_to_buf(mdh_network_stack_memory_manager_t *const self,
                          const void *const ptr,
                          size_t size,
                          const mdh_network_stack_buffer_t *buf)
{
    if (!ptr || !buf)
        return 0;

    const emac_memory_buf_t *const dst_buf = (const emac_memory_buf_t *const)buf;

    if (size > dst_buf->length)
        return 0;

    memcpy(dst_buf->data, (void *)ptr, size);
    return size;
}

static size_t copy_from_buf(mdh_network_stack_memory_manager_t *const self,
                            const mdh_network_stack_buffer_t *const buf,
                            void *const ptr,
                            size_t size)
{
    if (!ptr || !buf)
        return 0;

    const emac_memory_buf_t *const src_buf = (const emac_memory_buf_t *const)buf;

    if (size > src_buf->length)
        size = src_buf->length;

    memcpy((void *)ptr, src_buf->data, size);
    return size;
}

static void *get_payload(mdh_network_stack_memory_manager_t *const self, const mdh_network_stack_buffer_t *const buf)
{
    if (!buf)
        return NULL;

    const emac_memory_buf_t *const buffer = (const emac_memory_buf_t *const)buf;

    return buffer->data;
}

static size_t get_payload_len(mdh_network_stack_memory_manager_t *const self,
                              const mdh_network_stack_buffer_t *const buf)
{
    if (!buf)
        return 0;

    const emac_memory_buf_t *const buffer = (const emac_memory_buf_t *const)buf;

    return buffer->length;
}

static void
set_payload_len(mdh_network_stack_memory_manager_t *const self, const mdh_network_stack_buffer_t *buf, size_t size)
{
    if (!buf)
        return;

    emac_memory_buf_t *buffer = (emac_memory_buf_t *)buf;

    if (size > buffer->capacity)
        return;

    buffer->length = size;
}
