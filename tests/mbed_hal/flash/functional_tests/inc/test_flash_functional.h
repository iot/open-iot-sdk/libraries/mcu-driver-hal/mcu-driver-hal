/* Copyright (c) 2022, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal/flash_api.h"
#include "hal/us_ticker_api.h"

#ifndef MDH_HAL_TEST_FLASH_FUNCTIONAL_H
#define MDH_HAL_TEST_FLASH_FUNCTIONAL_H

/**
 * # Common patterns
 * the `flash_device` instance passed to all the test cases should be already initialized and ready to be tested.
 * The `flash_device` instance should be re-initialised between each test.
 */

/**
 * This test case must be the first to run as it records the start CPU cycle
 * used in `mdh_flash_test_clock_and_cache()`
 */
void mdh_flash_test_mapping_alignment(mdh_flash_t *flash_device, mdh_ticker_t *us_ticker);

/** Erase sector, write one page, erase sector and write new data
 */
void mdh_flash_test_erase_sector(mdh_flash_t *flash_device);

void mdh_flash_test_program_page(mdh_flash_t *flash_device);

/** Check the execution speed at the start and end of the test to make sure
 * cache settings weren't changed
 */
void mdh_flash_test_clock_and_cache(mdh_ticker_t *us_ticker);

#endif // MDH_HAL_TEST_FLASH_FUNCTIONAL_H
