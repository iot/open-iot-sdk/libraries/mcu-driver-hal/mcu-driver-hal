/* Copyright (c) 2017-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mbed_critical/mbed_critical.h"
#include "unity.h"
#include "utest/utest.h"

extern "C" {
#include "hal/flash_api.h"
#include "hal/us_ticker_api.h"
}

#if defined(__ARMCOMPILER_VERSION)
extern uint32_t Load$$LR$$LR_IROM1$$Limit[];
#define FLASHIAP_APP_ROM_END_ADDR ((uint32_t)Load$$LR$$LR_IROM1$$Limit)
#elif defined(__GNUC__)
extern uint32_t __etext;
extern uint32_t __data_start__;
extern uint32_t __data_end__;
#define FLASHIAP_APP_ROM_END_ADDR (((uint32_t)&__etext) + ((uint32_t)&__data_end__) - ((uint32_t)&__data_start__))
#endif

#define TEST_CYCLES 10000000

#define ALLOWED_DRIFT_PPM (1000000U / 5000U) // 0.5%

#define US_TICKER_OV_LIMIT 35000

/*
    return values to be checked are documented at:
        http://arm-software.github.io/CMSIS_5/Pack/html/algorithmFunc.html#Verify
*/

#ifndef ALIGN_DOWN
#define ALIGN_DOWN(x, a) ((x) & ~((a)-1))
#endif

#if defined(__GNUC__) || defined(__ARMCC_VERSION)
static void delay_loop(uint32_t count) __attribute__((noinline));
#endif
static void overflow_protect(mdh_ticker_t *us_ticker);
static uint32_t time_cpu_cycles(mdh_ticker_t *us_ticker, uint32_t cycles) __attribute__((noinline));
void mdh_flash_test_mapping_alignment(mdh_flash_t *flash_device, mdh_ticker_t *us_ticker);
void mdh_flash_test_erase_sector(mdh_flash_t *flash_device);
void mdh_flash_test_program_page(mdh_flash_t *flash_device);
void mdh_flash_test_clock_and_cache(mdh_ticker_t *us_ticker);

static uint32_t timer_diff_start;

#if defined(__GNUC__) || defined(__ARMCC_VERSION)
static void delay_loop(uint32_t count)
{
    __asm__ volatile("%=:\n\t"
#if defined(__thumb__) && !defined(__thumb2__) && !defined(__ARMCC_VERSION)
                     "SUB  %0, #1\n\t"
#else
                     "SUBS %0, %0, #1\n\t"
#endif
                     "BCS  %=b\n\t"
                     : "+l"(count)
                     :
                     : "cc");
}
#endif

/* Since according to the ticker requirements min acceptable counter size is
 * - 12 bits for low power timer - max count = 4095,
 * - 16 bits for high frequency timer - max count = 65535
 * then all test cases must be executed in this time windows.
 * HAL ticker layer handles counter overflow and it is not handled in the target
 * ticker drivers. Ensure we have enough time to execute test case without overflow.
 */
static void overflow_protect(mdh_ticker_t *us_ticker)
{
    uint32_t time_window = US_TICKER_OV_LIMIT;

    const uint32_t ticks_now = mdh_us_ticker_read(us_ticker);
    const ticker_info_t *p_ticker_info = mdh_us_ticker_get_info(us_ticker);

    const uint32_t max_count = ((1 << p_ticker_info->bits) - 1);

    if ((max_count - ticks_now) > time_window) {
        return;
    }

    while (mdh_us_ticker_read(us_ticker) >= ticks_now)
        ;
}

static uint32_t time_cpu_cycles(mdh_ticker_t *us_ticker, uint32_t cycles)
{
    // Disable interrupts and time the execution.
    core_util_critical_section_enter();
    uint32_t pre_delay_tick = mdh_us_ticker_read(us_ticker);
    delay_loop(cycles);
    uint32_t post_delay_tick = mdh_us_ticker_read(us_ticker);
    core_util_critical_section_exit();

    const ticker_info_t *ticker_info = mdh_us_ticker_get_info(us_ticker);
    // The `ticker_info->bits` has a max value of 32 (see the us ticker docs/defined behavior).
    // As a result, the `ticker_counter_mask` has a max value of UINT32_MAX (0xffffffff).
    uint32_t ticker_counter_mask = (1UL << ticker_info->bits) - 1UL;

    // The `ticker_counter_mask` must be applied to obtain a correct difference.
    uint32_t elapsed_ticks = (post_delay_tick - pre_delay_tick) & ticker_counter_mask;

    // Each operand below has to be `uint64_t`.
    uint64_t duration_us = 1000000ULL * (uint64_t)elapsed_ticks / (uint64_t)ticker_info->frequency;

    return (uint32_t)duration_us;
}

void mdh_flash_test_mapping_alignment(mdh_flash_t *flash_device, mdh_ticker_t *us_ticker)
{
    overflow_protect(us_ticker);
    timer_diff_start = time_cpu_cycles(us_ticker, TEST_CYCLES);

    const uint32_t page_size = mdh_flash_get_page_size(flash_device);
    const uint32_t flash_start = mdh_flash_get_start_address(flash_device);
    const uint32_t flash_size = mdh_flash_get_size(flash_device);
    TEST_ASSERT_TRUE(page_size != 0UL);

    uint32_t sector_size = mdh_flash_get_sector_size(flash_device, flash_start);
    for (uint32_t offset = 0; offset < flash_size; offset += sector_size) {
        const uint32_t sector_start = flash_start + offset;
        sector_size = mdh_flash_get_sector_size(flash_device, sector_start);
        const uint32_t sector_end = sector_start + sector_size - 1;
        const uint32_t end_sector_size = mdh_flash_get_sector_size(flash_device, sector_end);

        // Sector size must be a valid value
        TEST_ASSERT_NOT_EQUAL(MDH_FLASH_INVALID_SIZE, sector_size);
        // Sector size must be greater than zero
        TEST_ASSERT_NOT_EQUAL(0, sector_size);
        // All flash sectors must be a multiple of page size
        TEST_ASSERT_EQUAL(0, sector_size % page_size);
        // Sector address must be a multiple of sector size
        TEST_ASSERT_EQUAL(0, sector_start % sector_size);
        // All address in a sector must return the same sector size
        TEST_ASSERT_EQUAL(sector_size, end_sector_size);
    }

    // Make sure unmapped flash is reported correctly
    TEST_ASSERT_EQUAL(MDH_FLASH_INVALID_SIZE, mdh_flash_get_sector_size(flash_device, flash_start - 1));
    TEST_ASSERT_EQUAL(MDH_FLASH_INVALID_SIZE, mdh_flash_get_sector_size(flash_device, flash_start + flash_size));
}

void mdh_flash_test_erase_sector(mdh_flash_t *flash_device)
{
    uint32_t addr_after_last = mdh_flash_get_start_address(flash_device) + mdh_flash_get_size(flash_device);
    uint32_t last_sector_size = mdh_flash_get_sector_size(flash_device, addr_after_last - 1);
    uint32_t last_sector = addr_after_last - last_sector_size;
    TEST_ASSERT_EQUAL_INT32(0, last_sector % last_sector_size);

    utest_printf("ROM ends at 0x%lx, test starts at 0x%lx\n", FLASHIAP_APP_ROM_END_ADDR, last_sector);
    if (last_sector >= FLASHIAP_APP_ROM_END_ADDR) {
        TEST_PASS_MESSAGE("Test skipped. Test region overlaps code.");
    }

    int32_t ret = mdh_flash_erase_sector(flash_device, last_sector);
    TEST_ASSERT_EQUAL_INT32(0, ret);
}

void mdh_flash_test_program_page(mdh_flash_t *flash_device)
{
    uint32_t test_size = mdh_flash_get_page_size(flash_device);
    uint8_t *data = new uint8_t[test_size];
    uint8_t *data_flashed = new uint8_t[test_size];
    for (uint32_t i = 0; i < test_size; i++) {
        data[i] = 0xCE;
    }

    // the one before the last page in the system
    uint32_t address = mdh_flash_get_start_address(flash_device) + mdh_flash_get_size(flash_device) - (2 * test_size);

    // sector size might not be same as page size
    uint32_t erase_sector_boundary = ALIGN_DOWN(address, mdh_flash_get_sector_size(flash_device, address));
    utest_printf("ROM ends at 0x%lx, test starts at 0x%lx\n", FLASHIAP_APP_ROM_END_ADDR, erase_sector_boundary);
    if (erase_sector_boundary >= FLASHIAP_APP_ROM_END_ADDR) {
        TEST_PASS_MESSAGE("Test skipped. Test region overlaps code.");
    }

    int32_t ret = mdh_flash_erase_sector(flash_device, erase_sector_boundary);
    TEST_ASSERT_EQUAL_INT32(0, ret);

    ret = mdh_flash_program_page(flash_device, address, data, test_size);
    TEST_ASSERT_EQUAL_INT32(0, ret);

    ret = mdh_flash_read(flash_device, address, data_flashed, test_size);
    TEST_ASSERT_EQUAL_INT32(0, ret);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(data, data_flashed, test_size);

    // sector size might not be same as page size
    erase_sector_boundary = ALIGN_DOWN(address, mdh_flash_get_sector_size(flash_device, address));
    ret = mdh_flash_erase_sector(flash_device, erase_sector_boundary);
    TEST_ASSERT_EQUAL_INT32(0, ret);

    // write another data to be certain we are re-flashing
    for (uint32_t i = 0; i < test_size; i++) {
        data[i] = 0xAC;
    }
    ret = mdh_flash_program_page(flash_device, address, data, test_size);
    TEST_ASSERT_EQUAL_INT32(0, ret);

    ret = mdh_flash_read(flash_device, address, data_flashed, test_size);
    TEST_ASSERT_EQUAL_INT32(0, ret);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(data, data_flashed, test_size);

    delete[] data;
    delete[] data_flashed;
}

void mdh_flash_test_clock_and_cache(mdh_ticker_t *us_ticker)
{
    overflow_protect(us_ticker);

    const uint32_t timer_diff_end = time_cpu_cycles(us_ticker, TEST_CYCLES);
    const uint32_t acceptable_range = timer_diff_start / (ALLOWED_DRIFT_PPM);
    TEST_ASSERT_UINT32_WITHIN(acceptable_range, timer_diff_start, timer_diff_end);
}
