/* Copyright (c) 2017-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_lp_ticker.h"

#include "mbed_critical/mbed_critical.h"
#include "unity.h"

extern "C" {
#include "hal/lp_ticker_api.h"
#include "hal/us_ticker_api.h"
}

using namespace utest::v1;

volatile int intFlag = 0;

ticker_irq_callback_f prev_handler;

#define US_PER_MS 1000

#define TICKER_GLITCH_TEST_TICKS 1000

#define TICKER_INT_VAL 1500
#define TICKER_DELTA   10

#define LP_TICKER_OV_LIMIT 4000

/* To prevent a loss of Greentea data, the serial buffers have to be flushed
 * before the UART peripheral shutdown. The UART shutdown happens when the
 * device is entering the deepsleep mode or performing a reset.
 *
 * With the current API, it is not possible to check if the hardware buffers
 * are empty. However, it is possible to determine the time required for the
 * buffers to flush.
 *
 * Assuming the biggest Tx FIFO of 128 bytes (as for CY8CPROTO_062_4343W)
 * and a default UART config (9600, 8N1), flushing the Tx FIFO wold take:
 * (1 start_bit + 8 data_bits + 1 stop_bit) * 128 * 1000 / 9600 = 133.3 ms.
 * To be on the safe side, set the wait time to 150 ms.
 */
#define SERIAL_FLUSH_TIME_MS 150

void busy_wait_ms(mdh_ticker_t *us_ticker, int ms)
{
    const ticker_info_t *p_ticker_info = mdh_us_ticker_get_info(us_ticker);
    const uint32_t start = mdh_us_ticker_read(us_ticker);
    const uint32_t stop = start + (p_ticker_info->frequency / (ms * 1000));
    if (start > stop) {
        while (mdh_us_ticker_read(us_ticker) > stop)
            ;
    }

    while (mdh_us_ticker_read(us_ticker) < stop)
        ;
}

/* Since according to the ticker requirements min acceptable counter size is
 * - 12 bits for low power timer - max count = 4095,
 * then all test cases must be executed in this time windows.
 * HAL ticker layer handles counter overflow and it is not handled in the target
 * ticker drivers. Ensure we have enough time to execute test case without overflow.
 */
void overflow_protect(mdh_ticker_t *lp_ticker)
{
    uint32_t time_window;

    time_window = LP_TICKER_OV_LIMIT;

    const uint32_t ticks_now = mdh_lp_ticker_read(lp_ticker);
    const ticker_info_t *p_ticker_info = mdh_lp_ticker_get_info(lp_ticker);

    const uint32_t max_count = ((1 << p_ticker_info->bits) - 1);

    if ((max_count - ticks_now) > time_window) {
        return;
    }

    while (mdh_lp_ticker_read(lp_ticker) >= ticks_now)
        ;
}

void ticker_event_handler_stub(mdh_ticker_t *lp_ticker)
{
    /* Indicate that ISR has been executed in interrupt context. */
    if (core_util_is_isr_active()) {
        intFlag++;
    }

    /* Clear and disable ticker interrupt. */
    mdh_lp_ticker_clear_interrupt(lp_ticker);
    mdh_lp_ticker_disable_interrupt(lp_ticker);
}

/* Test that the ticker has the correct frequency and number of bits. */
void lp_ticker_info_test(mdh_ticker_t *lp_ticker)
{
    const ticker_info_t *p_ticker_info = mdh_lp_ticker_get_info(lp_ticker);

    TEST_ASSERT(p_ticker_info->frequency >= 4000);
    // TEST_ASSERT(p_ticker_info->frequency <= 64000);
    TEST_ASSERT(p_ticker_info->bits >= 12);

#if defined(LP_TICKER_PERIOD_NUM) || defined(CHECK_TICKER_OPTIM)
    TEST_ASSERT_UINT32_WITHIN(
        1, (uint64_t)1000000 * LP_TICKER_PERIOD_DEN / LP_TICKER_PERIOD_NUM, p_ticker_info->frequency);
    TEST_ASSERT_EQUAL_UINT32(LP_TICKER_MASK, ((uint64_t)1 << p_ticker_info->bits) - 1);
#endif
}

/* Test that the ticker does not glitch backwards due to an incorrectly implemented ripple counter driver. */
void lp_ticker_glitch_test(mdh_ticker_t *lp_ticker)
{
    overflow_protect(lp_ticker);

    uint32_t last = mdh_lp_ticker_read(lp_ticker);
    const uint32_t start = last;

    while (last < (start + TICKER_GLITCH_TEST_TICKS)) {
        const uint32_t cur = mdh_lp_ticker_read(lp_ticker);
        TEST_ASSERT(cur >= last);
        last = cur;
    }
}
