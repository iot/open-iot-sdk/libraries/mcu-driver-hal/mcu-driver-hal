/* Copyright (c) 2018-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal_ospi
 *  @{
 *  \defgroup hal_ospi_tests Tests
 *  OSPI tests of the HAL.
 *  @{
 */
#ifndef MDH_OSPI_TEST_H
#define MDH_OSPI_TEST_H

#include "unity.h"
#include "utest/utest.h"

extern "C" {
#include "hal/ospi_api.h"
#include "hal/us_ticker_api.h"
}

#include "test_ospi_utils.h"

// uncomment to enable verbose mode
//#define OSPI_TEST_LOG_DATA
//#define OSPI_TEST_LOG_FLASH_TIME
//#define OSPI_TEST_LOG_FLASH_STATUS

#ifndef OSPI_MIN_FREQUENCY
#define OSPI_MIN_FREQUENCY 1000000
#endif

// write address should be page aligned
#define TEST_FLASH_ADDRESS 0x0

#define TEST_REPEAT_SINGLE   1
#define TEST_REPEAT_MULTIPLE 4

// write block of data in single write operation
#define WRITE_SINGLE 1
// write block of data in adjacent locations in multiple write operations
#define WRITE_MULTIPLE 4

// read block of data in single read operation
#define READ_SINGLE 1
// read block of data in adjacent locations in multiple read operations
#define READ_MULTIPLE 4

static uint32_t gen_flash_address(uint32_t sector_count, uint32_t sector_size)
{
    uint32_t address = (99 % sector_count) * sector_size; // 99 is arbitrarily chosen
    address &= 0xFFFFFF; // Ensure address is within 24 bits so as to not have to deal with 4-byte addressing
    return address;
}

static void log_data(const char *str, uint8_t *data, uint32_t size)
{
    utest_printf("%s: ", str);
    for (uint32_t j = 0; j < size; j++) {
        utest_printf("%02X ", data[j]);
    }
    utest_printf("\r\n");
}

template <mdh_ospi_bus_width_t write_inst_width,
          mdh_ospi_bus_width_t write_addr_width,
          mdh_ospi_bus_width_t write_data_width,
          mdh_ospi_bus_width_t write_alt_width,
          uint32_t write_cmd,
          mdh_ospi_address_size_t write_addr_size,
          mdh_ospi_alt_size_t write_alt_size,
          uint32_t write_count,
          mdh_ospi_bus_width_t read_inst_width,
          mdh_ospi_bus_width_t read_addr_width,
          mdh_ospi_bus_width_t read_data_width,
          mdh_ospi_bus_width_t read_alt_width,
          uint32_t read_cmd,
          int read_dummy_cycles,
          mdh_ospi_address_size_t read_addr_size,
          mdh_ospi_alt_size_t read_alt_size,
          uint32_t read_count,
          uint32_t test_count,
          uint32_t data_size,
          uint32_t erase_cmd,
          uint32_t max_erase_wait_time_us,
          uint32_t max_prog_wait_time_us,
          size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
static void _ospi_write_read_test(Ospi &ospi, uint32_t flash_addr)
{
    mdh_ospi_status_t ret = MDH_OSPI_STATUS_OK;

    Timer timer;
    int erase_time = 0, write_time = 0, read_time = 0;
    size_t buf_len = data_size;

    for (uint32_t tc = 0; tc < test_count; tc++) {

        srand(ticker_read(get_us_ticker_data()));
        for (uint32_t i = 0; i < data_size; i++) {
            tx_buf[i] = (uint8_t)(rand() & 0xFF);
        }

        ret = write_enable<status_reg_size,
                           wrsr_max_time,
                           status_reg_read_cmd,
                           write_enable_cmd,
                           status_bit_write_in_progess,
                           status_bit_write_enable_latch>(ospi);
        TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

        timer.reset();
        timer.start();

        ret = erase<status_reg_read_cmd,
                    read_cfg_reg_2_cmd,
                    read_opi_cmd,
                    read_dopi_cmd,
                    read_fast_dummy_cycle_count,
                    read_8io_dummy_cycle_count>(erase_cmd, flash_addr, ospi);
        TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);
        WAIT_FOR(max_erase_wait_time_us, ospi, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);

        timer.stop();
        erase_time = timer.read_us();

        // switching to extended-SPI/DPI/QPI mode here for write operation
        // for DPI/QPI ospi.cmd is automatically switched to 2_2_2/4_4_4 mode
        ret = mode_enable(ospi, write_inst_width, write_addr_width, write_data_width);
        TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

        const uint32_t write_size = data_size / write_count;
        for (uint32_t wc = 0, write_start = flash_addr; wc < write_count; wc++, write_start += write_size) {
            ret = write_enable<status_reg_size,
                               wrsr_max_time,
                               status_reg_read_cmd,
                               write_enable_cmd,
                               status_bit_write_in_progess,
                               status_bit_write_enable_latch>(ospi);
            TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

            timer.reset();
            timer.start();

            buf_len = write_size;
            ospi.cmd.configure(
                write_inst_width, write_addr_width, write_data_width, write_alt_width, write_addr_size, write_alt_size);
            ospi.cmd.build(write_cmd, write_start);
            ret = ospi_write(&ospi.handle, ospi.cmd.get(), tx_buf + wc * write_size, &buf_len);
            TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);
            TEST_ASSERT_EQUAL(write_size, buf_len);

            if (is_extended_mode(write_inst_width, write_addr_width, write_data_width)) {
                // on some flash chips in extended-SPI mode, control commands works only in 1-1-1 mode
                // so switching back to 1-1-1 mode
                ospi.cmd.configure(MODE_1_1_1, MDH_OSPI_ADDRESS_SIZE_32, MDH_OSPI_ALT_ADDRESS_SIZE_8);
            }

            WAIT_FOR(max_prog_wait_time_us, ospi, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);

            timer.stop();
            write_time = timer.read_us();
        }

        // switching back to single channel SPI
        ret = mode_disable(ospi, write_inst_width, write_addr_width, write_data_width);
        TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

        // switching to extended-SPI/DPI/QPI mode here for read operation
        // for DPI/QPI ospi.cmd is automatically switched to 2_2_2/4_4_4 mode
        ret = mode_enable(ospi, read_inst_width, read_addr_width, read_data_width);
        TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

        memset(rx_buf, 0, sizeof(rx_buf));
        const uint32_t read_size = data_size / read_count;
        ospi.cmd.configure(read_inst_width,
                           read_addr_width,
                           read_data_width,
                           read_alt_width,
                           read_addr_size,
                           read_alt_size,
                           read_dummy_cycles);
        for (uint32_t rc = 0, read_start = flash_addr; rc < read_count; rc++, read_start += read_size) {
            timer.reset();
            timer.start();

            buf_len = read_size;
            ospi.cmd.build(read_cmd, read_start);
            ret = ospi_read(&ospi.handle, ospi.cmd.get(), rx_buf + rc * read_size, &buf_len);
            TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);
            TEST_ASSERT_EQUAL(read_size, buf_len);

            timer.stop();
            read_time = timer.read_us();
        }
        ospi.cmd.set_dummy_cycles(0);

        if (is_extended_mode(read_inst_width, read_addr_width, read_data_width)) {
            // on some flash chips in extended-SPI mode, control commands works only in 1-1-1 mode
            // so switching back to 1-1-1 mode
            ospi.cmd.configure(MODE_1_1_1, MDH_OSPI_ADDRESS_SIZE_32, MDH_OSPI_ALT_ADDRESS_SIZE_8);
        }

        // switching back to single channel SPI
        ret = mode_disable(ospi, read_inst_width, read_addr_width, read_data_width);
        TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

        for (uint32_t i = 0; i < data_size; i++) {
            if (tx_buf[i] != rx_buf[i]) {
                log_data("tx data", tx_buf, data_size);
                log_data("rx data", rx_buf, data_size);
                utest_printf("erase/write/read time: %d/%d/%d [us]\r\n", erase_time, write_time, read_time);
                TEST_ASSERT_EQUAL(tx_buf[i], rx_buf[i]);
            }
        }

#ifdef OSPI_TEST_LOG_FLASH_TIME
        utest_printf("erase/write/read time: %d/%d/%d [us]\r\n", erase_time, write_time, read_time);
#endif

#ifdef OSPI_TEST_LOG_DATA
        log_data("tx data", tx_buf, data_size);
        log_data("rx data", rx_buf, data_size);
        utest_printf("rx/tx data match\r\n");
#endif
    }
}

/** Test ospi frequency setting.
 *
 *  Given board provides OSPI, with OSPI already initialized.
 *  When set OSPI frequency.
 *  Then freguency setting is successfully performed (no exception is generated).
 *
 */
template <mdh_ospi_bus_width_t write_addr_width,
          mdh_ospi_alt_size_t write_alt_size,
          uint32_t write_count,
          uint32_t erase_cmd,
          uint32_t max_erase_wait_time_us,
          uint32_t max_prog_wait_time_us,
          size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t wait_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t status_reg_write_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
void ospi_frequency_test(mdh_ospi_t *ospi_handle,
                         uint32_t max_frequency,
                         uint32_t data_size,
                         uint8_t tx_buf[],
                         size_t tx_size,
                         uint8_t rx_buf[],
                         size_t rx_size)
{
    Ospi ospi;
    ospi.handle = ospi_handle;
    mdh_ospi_status_t ret = mdh_ospi_set_mode(ospi.handle, 0);
    uint32_t freq = max_frequency;
    ret = mdh_ospi_set_frequency(ospi.handle, freq);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

    while (ret == MDH_OSPI_STATUS_OK && freq >= OSPI_MIN_FREQUENCY) {
        // check if the memory is working properly
        ospi.cmd.configure(MODE_1_1_1, MDH_OSPI_ADDRESS_SIZE_32, MDH_OSPI_ALT_ADDRESS_SIZE_8);
        ret = ospi_frequency(&ospi.handle, freq);
        flash_init<status_reg_size,
                   wrsr_max_time,
                   wait_max_time,
                   status_reg_read_cmd,
                   status_reg_write_cmd,
                   write_enable_cmd,
                   status_bit_write_in_progess>(ospi);
        _ospi_write_read_test<MODE_1_1_1,
                              write_addr_width,
                              MDH_OSPI_ADDRESS_SIZE_32,
                              MDH_OSPI_ALT_ADDRESS_SIZE_8,
                              WRITE_SINGLE,
                              MODE_1_1_1,
                              write_alt_size,
                              write_count,
                              MDH_OSPI_ADDRESS_SIZE_32,
                              MDH_OSPI_ALT_ADDRESS_SIZE_8,
                              READ_SINGLE,
                              TEST_REPEAT_SINGLE,
                              data_size,
                              erase_cmd,
                              max_erase_wait_time_us,
                              max_prog_wait_time_us,
                              status_reg_size,
                              wrsr_max_time,
                              status_reg_read_cmd,
                              write_enable_cmd,
                              status_bit_write_in_progess,
                              status_bit_write_enable_latch,
                              read_cfg_reg_2_cmd,
                              read_opi_cmd,
                              read_dopi_cmd,
                              read_fast_dummy_cycle_count,
                              read_8io_dummy_cycle_count>(ospi, TEST_FLASH_ADDRESS, tx_buf, tx_size, rx_buf, rx_size);

        utest_printf("frequency setting %d [Hz] - OK\r\n", freq);

        freq /= 2;
    }
}

/** Template for write/read tests
 *
 *  Test single write/read operation of a block of data to/from the specific memory address
 *  Given board provides OSPI, with OSPI already initialized.
 *  When perform write and then read operations.
 *  Then data is successfully written and then read (no exception is generated) and the read data is valid.
 *
 *  Test multiple write/read operation of a block of data to/from the same specific memory address
 *  Given board provides OSPI, with OSPI already initialized.
 *  When perform write and then read operations.
 *  Then data is successfully written and then read (no exception is generated) and the read data is valid.
 *
 *  Test multiple adjacent write and single read operation of a block of data to/from the specific memory address
 *  Given board provides OSPI, with OSPI already initialized.
 *  When perform write and then read operations.
 *  Then data is successfully written and then read (no exception is generated) and the read data is valid.
 *
 *  Test single write and multiple adjacent read operation of a block of data to/from the specific memory address
 *  Given board provides OSPI, with OSPI already initialized.
 *  When perform write and then read operations.
 *  Then data is successfully written and then read (no exception is generated) and the read data is valid.
 *
 */
template <mdh_ospi_bus_width_t write_inst_width,
          mdh_ospi_bus_width_t write_addr_width,
          mdh_ospi_bus_width_t write_data_width,
          mdh_ospi_bus_width_t write_alt_width,
          uint32_t write_cmd,
          mdh_ospi_address_size_t write_addr_size,
          mdh_ospi_alt_size_t write_alt_size,
          uint32_t write_count,
          mdh_ospi_bus_width_t read_inst_width,
          mdh_ospi_bus_width_t read_addr_width,
          mdh_ospi_bus_width_t read_data_width,
          mdh_ospi_bus_width_t read_alt_width,
          uint32_t read_cmd,
          int read_dummy_cycles,
          mdh_ospi_address_size_t read_addr_size,
          mdh_ospi_alt_size_t read_alt_size,
          int frequency,
          uint32_t read_count,
          uint32_t test_count,
          uint32_t data_size,
          uint32_t flash_addr,
          uint32_t sector_count,
          uint32_t sector_size,
          uint32_t erase_cmd,
          uint32_t max_erase_wait_time_us,
          uint32_t max_prog_wait_time_us,
          size_t status_reg_size,
          size_t max_reg_size,
          uint32_t wrsr_max_time,
          uint32_t wait_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t status_reg_write_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch,
          uint32_t write_disable_cmd,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
void ospi_write_read_test(mdh_ospi_t *ospi_handle, uint8_t *tx_buf, size_t tx_size, uint8_t *rx_buf, size_t rx_size)
{
    uint32_t addr = flash_addr;
    if (addr == 0) {
        // if no specified address selected, use random one to extend flash life
        addr = gen_flash_address(sector_count, sector_size);
    }

    Ospi ospi = {.handle = ospi_handle};

    mdh_qspi_status_t ret = mdh_ospi_set_mode(ospi.handle, 0);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

    ret = mdh_ospi_set_frequency(ospi.handle, frequency);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

    ospi.cmd.configure(MODE_1_1_1, MDH_OSPI_ADDRESS_SIZE_32, MDH_OSPI_ALT_ADDRESS_SIZE_8);
    flash_init<status_reg_size,
               wrsr_max_time,
               wait_max_time,
               status_reg_read_cmd,
               status_reg_write_cmd,
               write_enable_cmd,
               status_bit_write_in_progess>(ospi);

    // switch memory to high performance mode (if available)
    ret = fast_mode_enable(ospi);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

#ifdef OSPI_TEST_LOG_FLASH_STATUS
    log_register(STATUS_REG, OSPI_STATUS_REG_SIZE, ospi, "Status register");
    log_register(CONFIG_REG0, OSPI_CONFIG_REG_0_SIZE, ospi, "Config register 0");
#ifdef CONFIG_REG1
    log_register(CONFIG_REG1, OSPI_CONFIG_REG_1_SIZE, ospi, "Config register 1");
#endif
#ifdef CONFIG_REG2
    log_register(CONFIG_REG2, OSPI_CONFIG_REG_2_SIZE, ospi, "Config register 2");
#endif
#endif

    _ospi_write_read_test<write_inst_width,
                          write_addr_width,
                          write_data_width,
                          write_alt_width,
                          write_cmd,
                          write_addr_size,
                          write_alt_size,
                          write_count,
                          read_inst_width,
                          read_addr_width,
                          read_data_width,
                          read_alt_width,
                          read_cmd,
                          read_dummy_cycles,
                          read_addr_size,
                          read_alt_size,
                          read_count,
                          test_count,
                          data_size,
                          erase_cmd,
                          max_erase_wait_time_us,
                          max_prog_wait_time_us,
                          status_reg_size,
                          wrsr_max_time,
                          status_reg_read_cmd,
                          write_enable_cmd,
                          status_bit_write_in_progess,
                          status_bit_write_enable_latch,
                          read_cfg_reg_2_cmd,
                          read_opi_cmd,
                          read_dopi_cmd,
                          read_fast_dummy_cycle_count,
                          read_8io_dummy_cycle_count>(ospi, addr, tx_buf, tx_size, rx_buf, rx_size);

    ret = fast_mode_disable(ospi);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);
}

void ospi_memory_id_test(const char *chip_name)
{
    utest_printf("*** %s memory config loaded ***\r\n", chip_name);
}

#endif // MDH_OSPI_TEST_H

/** @}*/
/** @}*/
