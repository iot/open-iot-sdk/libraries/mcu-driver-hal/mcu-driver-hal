/* Copyright (c) 2018-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MDH_OSPI_TEST_UTILS_H
#define MDH_OSPI_TEST_UTILS_H

#include "unity.h"
#include "utest/utest.h"

#include <string.h>

extern "C" {
#include "hal/ospi_api.h"
#include "hal/us_ticker_api.h"
}

#define OSPI_NONE (-1)

#define WAIT_FOR(timeout, q, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess)          \
    TEST_ASSERT_EQUAL_MESSAGE(                                                                           \
        sOK,                                                                                             \
        (flash_wait_for<status_reg_size, status_reg_read_cmd, status_bit_write_in_progess>(timeout, q)), \
        "flash_wait_for failed!!!")

// MODE_Command_Address_Data_Alt
#define MODE_1_1_1 \
    MDH_OSPI_BUS_WIDTH_SINGLE, MDH_OSPI_BUS_WIDTH_SINGLE, MDH_OSPI_BUS_WIDTH_SINGLE, MDH_OSPI_BUS_WIDTH_SINGLE
#define MODE_1_1_2 \
    MDH_OSPI_BUS_WIDTH_SINGLE, MDH_OSPI_BUS_WIDTH_SINGLE, MDH_OSPI_BUS_WIDTH_DUAL, MDH_OSPI_BUS_WIDTH_DUAL
#define MODE_1_2_2 MDH_OSPI_BUS_WIDTH_SINGLE, MDH_OSPI_BUS_WIDTH_DUAL, MDH_OSPI_BUS_WIDTH_DUAL, MDH_OSPI_BUS_WIDTH_DUAL
#define MODE_2_2_2 MDH_OSPI_BUS_WIDTH_DUAL, MDH_OSPI_BUS_WIDTH_DUAL, MDH_OSPI_BUS_WIDTH_DUAL, MDH_OSPI_BUS_WIDTH_DUAL
#define MODE_1_1_4 \
    MDH_OSPI_BUS_WIDTH_SINGLE, MDH_OSPI_BUS_WIDTH_SINGLE, MDH_OSPI_BUS_WIDTH_QUAD, MDH_OSPI_BUS_WIDTH_QUAD
#define MODE_1_4_4 MDH_OSPI_BUS_WIDTH_SINGLE, MDH_OSPI_BUS_WIDTH_QUAD, MDH_OSPI_BUS_WIDTH_QUAD, MDH_OSPI_BUS_WIDTH_QUAD
#define MODE_4_4_4 MDH_OSPI_BUS_WIDTH_QUAD, MDH_OSPI_BUS_WIDTH_QUAD, MDH_OSPI_BUS_WIDTH_QUAD, MDH_OSPI_BUS_WIDTH_QUAD
#define MODE_8_8_8 MDH_OSPI_BUS_WIDTH_OCTA, MDH_OSPI_BUS_WIDTH_OCTA, MDH_OSPI_BUS_WIDTH_OCTA, MDH_OSPI_BUS_WIDTH_OCTA
#define MODE_8D_8D_8D \
    MDH_OSPI_BUS_WIDTH_OCTA_DTR, MDH_OSPI_BUS_WIDTH_OCTA_DTR, MDH_OSPI_BUS_WIDTH_OCTA_DTR, MDH_OSPI_BUS_WIDTH_OCTA_DTR

enum OspiStatus { sOK, sError, sTimeout, sUnknown };

class OspiCommand {
public:
    void configure(mdh_ospi_bus_width_t inst_width,
                   mdh_ospi_bus_width_t addr_width,
                   mdh_ospi_bus_width_t data_width,
                   mdh_ospi_bus_width_t alt_width,
                   mdh_ospi_address_size_t addr_size,
                   mdh_ospi_alt_size_t alt_size,
                   int dummy_cycles = 0);

    void set_dummy_cycles(int dummy_cycles);

    template <uint32_t status_reg_read_cmd,
              uint32_t read_cfg_reg_2_cmd,
              uint32_t read_opi_cmd,
              uint32_t read_dopi_cmd,
              uint8_t read_fast_dummy_cycle_count,
              uint8_t read_8io_dummy_cycle_count>
    void build(int instruction, int address = OSPI_NONE, int alt = OSPI_NONE);

    mdh_ospi_command_t *get();

private:
    mdh_ospi_command_t _cmd;
};

template <uint32_t status_reg_read_cmd,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
void OspiCommand::build(int instruction, int address, int alt)
{
    _cmd.instruction.disabled = (instruction == -1);
    if (!_cmd.instruction.disabled) {
        _cmd.instruction.value = instruction;
    }

    _cmd.address.disabled = (address == OSPI_NONE);
    if (!_cmd.address.disabled) {
        _cmd.address.value = address;
    }

    _cmd.alt.disabled = (alt == OSPI_NONE);
    if (!_cmd.alt.disabled) {
        _cmd.alt.value = alt;
    }

    if ((_cmd.instruction.bus_width == MDH_OSPI_BUS_WIDTH_OCTA)
        || (_cmd.instruction.bus_width == MDH_OSPI_BUS_WIDTH_OCTA_DTR)) {
        if (instruction == status_reg_read_cmd) {
            _cmd.address.disabled = 0;
            _cmd.address.value = 0;
            _cmd.dummy_count = read_fast_dummy_cycle_count;
        } else if (instruction == read_cfg_reg_2_cmd) {
            _cmd.dummy_count = read_fast_dummy_cycle_count;
        } else if ((instruction == read_opi_cmd) || (instruction == read_dopi_cmd)) {
            _cmd.dummy_count = read_8io_dummy_cycle_count;
        } else {
            _cmd.dummy_count = 0;
        }
    }
}

struct Ospi {
    mdh_ospi_t *handle;
    OspiCommand cmd;
};

template <uint32_t status_reg_read_cmd,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
mdh_ospi_status_t read_register(uint32_t cmd, uint8_t *buf, uint32_t size, Ospi &q)
{
    q.cmd.build<status_reg_read_cmd,
                read_cfg_reg_2_cmd,
                read_opi_cmd,
                read_dopi_cmd,
                read_fast_dummy_cycle_count,
                read_8io_dummy_cycle_count>(cmd);
    return mdh_ospi_transfer(q.handle, q.cmd.get(), NULL, 0, buf, size);
}

template <uint32_t status_reg_read_cmd,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
mdh_ospi_status_t write_register(uint32_t cmd, uint8_t *buf, uint32_t size, Ospi &q)
{
    q.cmd.build<status_reg_read_cmd,
                read_cfg_reg_2_cmd,
                read_opi_cmd,
                read_dopi_cmd,
                read_fast_dummy_cycle_count,
                read_8io_dummy_cycle_count>(cmd);
    return mdh_ospi_transfer(q.handle, q.cmd.get(), buf, size, NULL, 0);
}

template <uint32_t status_reg_read_cmd,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
mdh_ospi_status_t read_config_register_2(uint32_t cmd, uint32_t addr, uint8_t *buf, uint32_t size, Ospi &q)
{
    q.cmd.build<status_reg_read_cmd,
                read_cfg_reg_2_cmd,
                read_opi_cmd,
                read_dopi_cmd,
                read_fast_dummy_cycle_count,
                read_8io_dummy_cycle_count>(cmd, addr);
    return mdh_ospi_transfer(q.handle, q.cmd.get(), NULL, 0, buf, size);
}

template <uint32_t status_reg_read_cmd,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
mdh_ospi_status_t write_config_register_2(uint32_t cmd, uint32_t addr, uint8_t *buf, uint32_t size, Ospi &q)
{
    q.cmd.build<status_reg_read_cmd,
                read_cfg_reg_2_cmd,
                read_opi_cmd,
                read_dopi_cmd,
                read_fast_dummy_cycle_count,
                read_8io_dummy_cycle_count>(cmd, addr);
    return mdh_ospi_transfer(q.handle, q.cmd.get(), buf, size, NULL, 0);
}

template <size_t status_reg_size, uint32_t status_reg_read_cmd, uint32_t status_bit_write_in_progess>
OspiStatus flash_wait_for(uint32_t time_us, Ospi &ospi)
{
    uint8_t reg[status_reg_size];
    mdh_ospi_status_t ret;

    const ticker_info_t *ticker_info = us_ticker_get_info();

    const uint32_t delay_in_ticks = time_us * (ticker_info->frequency / 1000000UL);

    // The expected tick is the current tick plus the desired delay in ticks.
    // When the current tick (value returned by us_ticker_read()) becomes larger
    // than the expected tick, the desired delay/timeout has elapsed.
    const uint32_t expected_tick = us_ticker_read() + delay_in_ticks;

    memset(reg, 255, status_reg_size);
    do {
        ret = read_register(status_reg_read_cmd, reg, status_reg_size, ospi);
    } while (((reg[0] & status_bit_write_in_progess) != 0) && (us_ticker_read() < expected_tick));

    if (((reg[0] & status_bit_write_in_progess) == 0) && (ret == MDH_OSPI_STATUS_OK)) {
        return sOK;
    } else if (ret != MDH_OSPI_STATUS_OK) {
        return sError;
    } else if (us_ticker_read() >= expected_tick) {
        return sTimeout;
    }
    return sUnknown;
}

template <size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t wait_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t status_reg_write_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
void flash_init(Ospi &ospi)
{
    uint8_t status[status_reg_size];
    mdh_ospi_status_t ret;

    ospi.cmd.build<status_reg_read_cmd,
                   read_cfg_reg_2_cmd,
                   read_opi_cmd,
                   read_dopi_cmd,
                   read_fast_dummy_cycle_count,
                   read_8io_dummy_cycle_count>(status_reg_read_cmd);
    ret = mdh_ospi_transfer(ospi.handle, ospi.cmd.get(), NULL, 0, status, status_reg_size);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

#ifdef QSPI_CMD_RSTEN
    // Only do reset enable if device needs it
    if (OSPI_CMD_RSTEN != 0) {
        ospi.cmd.build<status_reg_read_cmd,
                       read_cfg_reg_2_cmd,
                       read_opi_cmd,
                       read_dopi_cmd,
                       read_fast_dummy_cycle_count,
                       read_8io_dummy_cycle_count>(OSPI_CMD_RSTEN);
        ret = mdh_ospi_transfer(ospi.handle, ospi.cmd.get(), NULL, 0, NULL, 0);
        TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

        WAIT_FOR(wrsr_max_time, ospi, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);
    }
#endif

#ifdef QSPI_CMD_RST
    ospi.cmd.build<status_reg_read_cmd,
                   read_cfg_reg_2_cmd,
                   read_opi_cmd,
                   read_dopi_cmd,
                   read_fast_dummy_cycle_count,
                   read_8io_dummy_cycle_count>(OSPI_CMD_RST);
    ret = mdh_ospi_transfer(ospi.handle, ospi.cmd.get(), NULL, 0, NULL, 0);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

    WAIT_FOR(wait_max_time, ospi, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);
#endif

    // Zero out status register to attempt to clear block protection bits
    uint8_t blanks[status_reg_size] = {0};

    ospi.cmd.build<status_reg_read_cmd,
                   read_cfg_reg_2_cmd,
                   read_opi_cmd,
                   read_dopi_cmd,
                   read_fast_dummy_cycle_count,
                   read_8io_dummy_cycle_count>(write_enable_cmd);
    ret = mdh_ospi_transfer(ospi.handle, ospi.cmd.get(), NULL, 0, NULL, 0);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

    ospi.cmd.build<status_reg_read_cmd,
                   read_cfg_reg_2_cmd,
                   read_opi_cmd,
                   read_dopi_cmd,
                   read_fast_dummy_cycle_count,
                   read_8io_dummy_cycle_count>(status_reg_write_cmd);
    ret = mdh_ospi_transfer(ospi.handle, ospi.cmd.get(), blanks, 1, NULL, 0);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

    WAIT_FOR(wrsr_max_time, ospi, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);
}

template <size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
mdh_ospi_status_t write_enable(Ospi &ospi)
{
    uint8_t reg[status_reg_size];
    ospi.cmd.build<status_reg_read_cmd,
                   read_cfg_reg_2_cmd,
                   read_opi_cmd,
                   read_dopi_cmd,
                   read_fast_dummy_cycle_count,
                   read_8io_dummy_cycle_count>(write_enable_cmd);

    if (mdh_ospi_transfer(ospi.handle, ospi.cmd.get(), NULL, 0, NULL, 0) != MDH_OSPI_STATUS_OK) {
        return MDH_OSPI_STATUS_ERROR;
    }
    WAIT_FOR(wrsr_max_time, ospi, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);

    memset(reg, 0, status_reg_size);
    if (read_register(status_reg_read_cmd, reg, status_reg_size, ospi) != MDH_OSPI_STATUS_OK) {
        return MDH_OSPI_STATUS_ERROR;
    }

    return ((reg[0] & status_bit_write_enable_latch) != 0 ? MDH_OSPI_STATUS_OK : MDH_OSPI_STATUS_ERROR);
}

template <size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch,
          uint32_t write_disable_cmd,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
mdh_ospi_status_t write_disable(Ospi &ospi)
{
    uint8_t reg[status_reg_size];
    ospi.cmd.build<status_reg_read_cmd,
                   read_cfg_reg_2_cmd,
                   read_opi_cmd,
                   read_dopi_cmd,
                   read_fast_dummy_cycle_count,
                   read_8io_dummy_cycle_count>(write_disable_cmd);

    if (mdh_ospi_transfer(ospi.handle, ospi.cmd.get(), NULL, 0, NULL, 0) != MDH_OSPI_STATUS_OK) {
        return MDH_OSPI_STATUS_ERROR;
    }
    WAIT_FOR(wrsr_max_time, ospi, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);

    memset(reg, 0, status_reg_size);
    if (read_register(status_reg_read_cmd, reg, status_reg_size, ospi) != MDH_OSPI_STATUS_OK) {
        return MDH_OSPI_STATUS_ERROR;
    }

    return ((reg[0] & status_bit_write_enable_latch) == 0 ? MDH_OSPI_STATUS_OK : MDH_OSPI_STATUS_ERROR);
}

template <size_t max_reg_size> void log_register(uint32_t cmd, uint32_t reg_size, Ospi &ospi, const char *str)
{
    mdh_ospi_status_t ret;
    static uint8_t reg[max_reg_size];

    ret = read_register(cmd, reg, reg_size, ospi);
    TEST_ASSERT_EQUAL(MDH_OSPI_STATUS_OK, ret);

    for (uint32_t j = 0; j < reg_size; j++) {
        utest_printf("%s byte %u (MSB first): ", str != NULL ? str : "", j);
        for (int i = 0; i < 8; i++) {
            utest_printf("%s ", ((reg[j] & (1 << (7 - i))) & 0xFF) == 0 ? "0" : "1");
        }
        utest_printf("\r\n");
    }
}

mdh_ospi_status_t mode_enable(Ospi &ospi,
                              mdh_ospi_bus_width_t inst_width,
                              mdh_ospi_bus_width_t addr_width,
                              mdh_ospi_bus_width_t data_width);
mdh_ospi_status_t mode_disable(Ospi &ospi,
                               mdh_ospi_bus_width_t inst_width,
                               mdh_ospi_bus_width_t addr_width,
                               mdh_ospi_bus_width_t data_width);

mdh_ospi_status_t fast_mode_enable(Ospi &ospi);
mdh_ospi_status_t fast_mode_disable(Ospi &ospi);

template <uint32_t status_reg_read_cmd,
          uint32_t read_cfg_reg_2_cmd,
          uint32_t read_opi_cmd,
          uint32_t read_dopi_cmd,
          uint8_t read_fast_dummy_cycle_count,
          uint8_t read_8io_dummy_cycle_count>
mdh_ospi_status_t erase(uint32_t erase_cmd, uint32_t flash_addr, Ospi &ospi)
{
    ospi.cmd.build<status_reg_read_cmd,
                   read_cfg_reg_2_cmd,
                   read_opi_cmd,
                   read_dopi_cmd,
                   read_fast_dummy_cycle_count,
                   read_8io_dummy_cycle_count>(erase_cmd, flash_addr);
    return mdh_ospi_transfer(ospi.handle, ospi.cmd.get(), NULL, 0, NULL, 0);
}

bool is_extended_mode(mdh_ospi_bus_width_t inst_width,
                      mdh_ospi_bus_width_t addr_width,
                      mdh_ospi_bus_width_t data_width);
bool is_dual_mode(mdh_ospi_bus_width_t inst_width, mdh_ospi_bus_width_t addr_width, mdh_ospi_bus_width_t data_width);
bool is_quad_mode(mdh_ospi_bus_width_t inst_width, mdh_ospi_bus_width_t addr_width, mdh_ospi_bus_width_t data_width);
bool is_octa_mode(mdh_ospi_bus_width_t inst_width, mdh_ospi_bus_width_t addr_width, mdh_ospi_bus_width_t data_width);
bool is_octa_dtr_mode(mdh_ospi_bus_width_t inst_width,
                      mdh_ospi_bus_width_t addr_width,
                      mdh_ospi_bus_width_t data_width);

#endif // MDH_OSPI_TEST_UTILS_H
