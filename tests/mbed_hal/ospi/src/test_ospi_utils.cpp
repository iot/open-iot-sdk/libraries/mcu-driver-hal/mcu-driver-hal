/* Copyright (c) 2018-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_ospi_utils.h"

static mdh_ospi_status_t extended_enable(Ospi &ospi);
static mdh_ospi_status_t extended_disable(Ospi &ospi);
static mdh_ospi_status_t dual_enable(Ospi &ospi);
static mdh_ospi_status_t dual_disable(Ospi &ospi);
static mdh_ospi_status_t quad_enable(Ospi &ospi);
static mdh_ospi_status_t quad_disable(Ospi &ospi);
static mdh_ospi_status_t octa_enable(Ospi &ospi);
static mdh_ospi_status_t octa_disable(Ospi &ospi);
static mdh_ospi_status_t octa_dtr_enable(Ospi &ospi);
static mdh_ospi_status_t octa_dtr_disable(Ospi &ospi);

void OspiCommand::configure(mdh_ospi_bus_width_t inst_width,
                            mdh_ospi_bus_width_t addr_width,
                            mdh_ospi_bus_width_t data_width,
                            mdh_ospi_bus_width_t alt_width,
                            mdh_ospi_address_size_t addr_size,
                            mdh_ospi_alt_size_t alt_size,
                            int dummy_cycles)
{
    memset(&_cmd, 0, sizeof(mdh_ospi_command_t));
    _cmd.instruction.disabled = _cmd.address.disabled = _cmd.alt.disabled = true;

    _cmd.instruction.bus_width = inst_width;
    _cmd.address.bus_width = addr_width;
    _cmd.address.size = addr_size;
    _cmd.alt.bus_width = alt_width;
    _cmd.alt.size = alt_size;
    _cmd.data.bus_width = data_width;
    _cmd.dummy_count = dummy_cycles;
}

void OspiCommand::set_dummy_cycles(int dummy_cycles)
{
    _cmd.dummy_count = dummy_cycles;
}

mdh_ospi_command_t *OspiCommand::get()
{
    return &_cmd;
}

mdh_ospi_status_t mode_enable(Ospi &ospi,
                              mdh_ospi_bus_width_t inst_width,
                              mdh_ospi_bus_width_t addr_width,
                              mdh_ospi_bus_width_t data_width)
{
    if (is_extended_mode(inst_width, addr_width, data_width)) {
        return extended_enable(ospi);
    } else if (is_dual_mode(inst_width, addr_width, data_width)) {
        return dual_enable(ospi);
    } else if (is_quad_mode(inst_width, addr_width, data_width)) {
        return quad_enable(ospi);
    } else if (is_octa_mode(inst_width, addr_width, data_width)) {
        return octa_enable(ospi);
    } else if (is_octa_dtr_mode(inst_width, addr_width, data_width)) {
        return octa_dtr_enable(ospi);
    } else {
        return MDH_OSPI_STATUS_OK;
    }
}

mdh_ospi_status_t mode_disable(Ospi &ospi,
                               mdh_ospi_bus_width_t inst_width,
                               mdh_ospi_bus_width_t addr_width,
                               mdh_ospi_bus_width_t data_width)
{
    if (is_extended_mode(inst_width, addr_width, data_width)) {
        return extended_disable(ospi);
    } else if (is_dual_mode(inst_width, addr_width, data_width)) {
        return dual_disable(ospi);
    } else if (is_quad_mode(inst_width, addr_width, data_width)) {
        return quad_disable(ospi);
    } else if (is_octa_mode(inst_width, addr_width, data_width)) {
        return octa_disable(ospi);
    } else if (is_octa_dtr_mode(inst_width, addr_width, data_width)) {
        return octa_dtr_disable(ospi);
    } else {
        return MDH_OSPI_STATUS_OK;
    }
}

static mdh_ospi_status_t extended_enable(Ospi &ospi)
{
#ifdef EXTENDED_SPI_ENABLE
    EXTENDED_SPI_ENABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

static mdh_ospi_status_t extended_disable(Ospi &ospi)
{
#ifdef EXTENDED_SPI_DISABLE
    EXTENDED_SPI_DISABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

static mdh_ospi_status_t dual_enable(Ospi &ospi)
{
#ifdef DUAL_ENABLE
    DUAL_ENABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

static mdh_ospi_status_t dual_disable(Ospi &ospi)
{
#ifdef DUAL_DISABLE
    DUAL_DISABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

static mdh_ospi_status_t quad_enable(Ospi &ospi)
{
#ifdef QUAD_ENABLE
    QUAD_ENABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

static mdh_ospi_status_t quad_disable(Ospi &ospi)
{
#ifdef QUAD_DISABLE
    QUAD_DISABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

static mdh_ospi_status_t octa_enable(Ospi &ospi)
{
#ifdef OCTA_ENABLE
    OCTA_ENABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

static mdh_ospi_status_t octa_disable(Ospi &ospi)
{
#ifdef OCTA_DISABLE
    OCTA_DISABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

static mdh_ospi_status_t octa_dtr_enable(Ospi &ospi)
{
#ifdef OCTA_DTR_ENABLE
    OCTA_DTR_ENABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

static mdh_ospi_status_t octa_dtr_disable(Ospi &ospi)
{
#ifdef OCTA_DTR_DISABLE
    OCTA_DTR_DISABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

mdh_ospi_status_t fast_mode_enable(Ospi &ospi)
{
#ifdef FAST_MODE_ENABLE
    FAST_MODE_ENABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

mdh_ospi_status_t fast_mode_disable(Ospi &ospi)
{
#ifdef FAST_MODE_DISABLE
    FAST_MODE_DISABLE();
#else
    return MDH_OSPI_STATUS_OK;
#endif
}

bool is_extended_mode(mdh_ospi_bus_width_t inst_width, mdh_ospi_bus_width_t addr_width, mdh_ospi_bus_width_t data_width)
{
    return (inst_width == MDH_OSPI_BUS_WIDTH_SINGLE)
           && ((addr_width != MDH_OSPI_BUS_WIDTH_SINGLE) || (data_width != MDH_OSPI_BUS_WIDTH_SINGLE));
}

bool is_dual_mode(mdh_ospi_bus_width_t inst_width, mdh_ospi_bus_width_t addr_width, mdh_ospi_bus_width_t data_width)
{
    return (inst_width == MDH_OSPI_BUS_WIDTH_DUAL) && (addr_width == MDH_OSPI_BUS_WIDTH_DUAL)
           && (data_width == MDH_OSPI_BUS_WIDTH_DUAL);
}

bool is_quad_mode(mdh_ospi_bus_width_t inst_width, mdh_ospi_bus_width_t addr_width, mdh_ospi_bus_width_t data_width)
{
    return (inst_width == MDH_OSPI_BUS_WIDTH_QUAD) && (addr_width == MDH_OSPI_BUS_WIDTH_QUAD)
           && (data_width == MDH_OSPI_BUS_WIDTH_QUAD);
}

bool is_octa_mode(mdh_ospi_bus_width_t inst_width, mdh_ospi_bus_width_t addr_width, mdh_ospi_bus_width_t data_width)
{
    return (inst_width == MDH_OSPI_BUS_WIDTH_OCTA) && (addr_width == MDH_OSPI_BUS_WIDTH_OCTA)
           && (data_width == MDH_OSPI_BUS_WIDTH_OCTA);
}

bool is_octa_dtr_mode(mdh_ospi_bus_width_t inst_width, mdh_ospi_bus_width_t addr_width, mdh_ospi_bus_width_t data_width)
{
    return (inst_width == MDH_OSPI_BUS_WIDTH_OCTA_DTR) && (addr_width == MDH_OSPI_BUS_WIDTH_OCTA_DTR)
           && (data_width == MDH_OSPI_BUS_WIDTH_OCTA_DTR);
}
