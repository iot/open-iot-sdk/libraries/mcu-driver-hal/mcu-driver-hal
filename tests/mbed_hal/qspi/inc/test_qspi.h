/* Copyright (c) 2018-2022, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal_qspi
 *  @{
 *  \defgroup hal_qspi_tests Tests
 *  QSPI tests of the HAL.
 *  @{
 */
#ifndef MDH_QSPI_TEST_H
#define MDH_QSPI_TEST_H

#include "unity.h"
#include "utest/utest.h"

#include <string.h>

extern "C" {
#include "hal/qspi_api.h"
#include "hal/us_ticker_api.h"
}

#include "test_qspi_utils.h"

// uncomment to enable verbose mode
//#define QSPI_TEST_LOG_DATA
//#define QSPI_TEST_LOG_FLASH_TIME
//#define QSPI_TEST_LOG_FLASH_STATUS

#ifndef QSPI_MIN_FREQUENCY
#define QSPI_MIN_FREQUENCY 1000000
#endif

// write address should be page aligned
#define TEST_FLASH_ADDRESS 0x0

#define TEST_REPEAT_SINGLE   1
#define TEST_REPEAT_MULTIPLE 4

// write block of data in single write operation
#define WRITE_SINGLE 1
// write block of data in adjacent locations in multiple write operations
#define WRITE_MULTIPLE 4

// read block of data in single read operation
#define READ_SINGLE 1
// read block of data in adjacent locations in multiple read operations
#define READ_MULTIPLE 4

static uint32_t gen_flash_address(uint32_t sector_count, uint32_t sector_size)
{
    uint32_t address = (99 % sector_count) * sector_size; // 99 is arbitrarily chosen
    address &= 0xFFFFFF; // Ensure address is within 24 bits so as to not have to deal with 4-byte addressing
    return address;
}

static void log_data(const char *str, uint8_t *data, uint32_t size)
{
    utest_printf("%s: ", str);
    for (uint32_t j = 0; j < size; j++) {
        utest_printf("%02X ", data[j]);
    }
    utest_printf("\r\n");
}

template <mdh_qspi_bus_width_t write_inst_width,
          mdh_qspi_bus_width_t write_addr_width,
          mdh_qspi_bus_width_t write_data_width,
          mdh_qspi_bus_width_t write_alt_width,
          uint32_t write_cmd,
          mdh_qspi_address_size_t write_addr_size,
          mdh_qspi_alt_size_t write_alt_size,
          uint32_t write_count,
          mdh_qspi_bus_width_t read_inst_width,
          mdh_qspi_bus_width_t read_addr_width,
          mdh_qspi_bus_width_t read_data_width,
          mdh_qspi_bus_width_t read_alt_width,
          uint32_t read_cmd,
          int read_dummy_cycles,
          mdh_qspi_address_size_t read_addr_size,
          mdh_qspi_alt_size_t read_alt_size,
          uint32_t read_count,
          uint32_t test_count,
          uint32_t data_size,
          uint32_t erase_cmd,
          uint32_t max_erase_wait_time_us,
          uint32_t max_prog_wait_time_us,
          size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch>
static void _qspi_write_read_test(Qspi &qspi,
                                  mdh_ticker_t *us_ticker,
                                  uint32_t flash_addr,
                                  uint8_t *tx_buf,
                                  size_t tx_size,
                                  uint8_t *rx_buf,
                                  size_t rx_size)
{
    mdh_qspi_status_t ret = MDH_QSPI_STATUS_OK;

    const ticker_info_t *ticker_info = mdh_us_ticker_get_info(us_ticker);

    uint32_t start_tick = 0, end_tick = 0;
    int erase_time_us = 0, write_time_us = 0, read_time_us = 0;
    size_t buf_len = data_size;

    for (uint32_t tc = 0; tc < test_count; tc++) {

        for (uint32_t i = 0; i < data_size; i++) {
            tx_buf[i] = (uint8_t)(99 & 0xFF); // 99 is arbitrarily chosen
        }

        ret = write_enable<status_reg_size,
                           wrsr_max_time,
                           status_reg_read_cmd,
                           write_enable_cmd,
                           status_bit_write_in_progess,
                           status_bit_write_enable_latch>(qspi, us_ticker);
        TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

        start_tick = mdh_us_ticker_read(us_ticker);

        ret = erase(erase_cmd, flash_addr, qspi);
        TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);
        WAIT_FOR(
            max_erase_wait_time_us, qspi, us_ticker, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);

        end_tick = mdh_us_ticker_read(us_ticker);

        erase_time_us = (end_tick - start_tick) / (ticker_info->frequency / 1000000UL);

        // switching to extended-SPI/DPI/QPI mode here for write operation
        // for DPI/QPI qspi.cmd is automatically switched to 2_2_2/4_4_4 mode
        ret = mode_enable(qspi, write_inst_width, write_addr_width, write_data_width);
        TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

        const uint32_t write_size = data_size / write_count;
        for (uint32_t wc = 0, write_start = flash_addr; wc < write_count; wc++, write_start += write_size) {
            ret = write_enable<status_reg_size,
                               wrsr_max_time,
                               status_reg_read_cmd,
                               write_enable_cmd,
                               status_bit_write_in_progess,
                               status_bit_write_enable_latch>(qspi, us_ticker);
            TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

            start_tick = mdh_us_ticker_read(us_ticker);

            buf_len = write_size;
            qspi.cmd.configure(
                write_inst_width, write_addr_width, write_data_width, write_alt_width, write_addr_size, write_alt_size);
            qspi.cmd.build(write_cmd, write_start);
            ret = mdh_qspi_write(qspi.handle, qspi.cmd.get(), tx_buf + wc * write_size, &buf_len);
            TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);
            TEST_ASSERT_EQUAL(write_size, buf_len);

            if (is_extended_mode(write_inst_width, write_addr_width, write_data_width)) {
                // on some flash chips in extended-SPI mode, control commands works only in 1-1-1 mode
                // so switching back to 1-1-1 mode
                qspi.cmd.configure(MODE_1_1_1, MDH_QSPI_ADDRESS_SIZE_24, MDH_QSPI_ALT_ADDRESS_SIZE_8);
            }

            WAIT_FOR(max_prog_wait_time_us,
                     qspi,
                     us_ticker,
                     status_reg_size,
                     status_reg_read_cmd,
                     status_bit_write_in_progess);

            end_tick = mdh_us_ticker_read(us_ticker);
            write_time_us = (end_tick - start_tick) / (ticker_info->frequency / 1000000UL);
        }

        // switching back to single channel SPI
        ret = mode_disable(qspi, write_inst_width, write_addr_width, write_data_width);
        TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

        // switching to extended-SPI/DPI/QPI mode here for read operation
        // for DPI/QPI qspi.cmd is automatically switched to 2_2_2/4_4_4 mode
        ret = mode_enable(qspi, read_inst_width, read_addr_width, read_data_width);
        TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

        memset(rx_buf, 0, rx_size);
        const uint32_t read_size = data_size / read_count;
        qspi.cmd.configure(read_inst_width,
                           read_addr_width,
                           read_data_width,
                           read_alt_width,
                           read_addr_size,
                           read_alt_size,
                           read_dummy_cycles);
        for (uint32_t rc = 0, read_start = flash_addr; rc < read_count; rc++, read_start += read_size) {
            start_tick = mdh_us_ticker_read(us_ticker);

            buf_len = read_size;
            qspi.cmd.build(read_cmd, read_start);
            ret = mdh_qspi_read(qspi.handle, qspi.cmd.get(), rx_buf + rc * read_size, &buf_len);
            TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);
            TEST_ASSERT_EQUAL(read_size, buf_len);

            end_tick = mdh_us_ticker_read(us_ticker);
            read_time_us = (end_tick - start_tick) / (ticker_info->frequency / 1000000UL);
        }
        qspi.cmd.set_dummy_cycles(0);

        if (is_extended_mode(read_inst_width, read_addr_width, read_data_width)) {
            // on some flash chips in extended-SPI mode, control commands works only in 1-1-1 mode
            // so switching back to 1-1-1 mode
            qspi.cmd.configure(MODE_1_1_1, MDH_QSPI_ADDRESS_SIZE_24, MDH_QSPI_ALT_ADDRESS_SIZE_8);
        }

        // switching back to single channel SPI
        ret = mode_disable(qspi, read_inst_width, read_addr_width, read_data_width);
        TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

        for (uint32_t i = 0; i < data_size; i++) {
            if (tx_buf[i] != rx_buf[i]) {
                log_data("tx data", tx_buf, data_size);
                log_data("rx data", rx_buf, data_size);
                utest_printf("erase/write/read time: %d/%d/%d [us]\r\n", erase_time_us, write_time_us, read_time_us);
                TEST_ASSERT_EQUAL(tx_buf[i], rx_buf[i]);
            }
        }

#ifdef QSPI_TEST_LOG_FLASH_TIME
        utest_printf("erase/write/read time: %d/%d/%d [us]\r\n", erase_time_us, write_time_us, read_time_us);
#endif

#ifdef QSPI_TEST_LOG_DATA
        log_data("tx data", tx_buf, data_size);
        log_data("rx data", rx_buf, data_size);
        utest_printf("rx/tx data match\r\n");
#endif
    }
}

/** Test qspi frequency setting.
 *
 *  Given board provides QSPI, with QSPI already initialized.
 *  When set QSPI frequency.
 *  Then freguency setting is successfully performed (no exception is generated).
 *
 */
template <mdh_qspi_bus_width_t write_addr_width,
          mdh_qspi_alt_size_t write_alt_size,
          uint32_t write_count,
          uint32_t erase_cmd,
          uint32_t max_erase_wait_time_us,
          uint32_t max_prog_wait_time_us,
          size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t wait_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t status_reg_write_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch>
void qspi_frequency_test(mdh_qspi_t *qspi_handle,
                         mdh_ticker_t *us_ticker,
                         uint32_t max_frequency,
                         uint32_t data_size,
                         uint8_t tx_buf[],
                         size_t tx_size,
                         uint8_t rx_buf[],
                         size_t rx_size)
{
#ifdef QSPI_FREQUENCY_SETTING_NOT_SUPPORTED
    utest_printf("QSPI_FREQUENCY_SETTING_NOT_SUPPORTED set.\r\n");
    utest_printf("Test has been skipped.\r\n");
    return;
#endif // QSPI_FREQUENCY_SETTING_NOT_SUPPORTED
    Qspi qspi;
    qspi.handle = qspi_handle;
    mdh_qspi_status_t ret = mdh_qspi_set_mode(qspi.handle, 0);
    uint32_t freq = max_frequency;
    ret = mdh_qspi_set_frequency(qspi.handle, freq);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

    while (ret == MDH_QSPI_STATUS_OK && freq >= QSPI_MIN_FREQUENCY) {
        // check if the memory is working properly
        qspi.cmd.configure(MODE_1_1_1, MDH_QSPI_ADDRESS_SIZE_24, MDH_QSPI_ALT_ADDRESS_SIZE_8);
        ret = mdh_qspi_set_frequency(qspi.handle, freq);
        flash_init<status_reg_size,
                   wrsr_max_time,
                   wait_max_time,
                   status_reg_read_cmd,
                   status_reg_write_cmd,
                   write_enable_cmd,
                   status_bit_write_in_progess>(qspi, us_ticker);
        _qspi_write_read_test<MODE_1_1_1,
                              write_addr_width,
                              MDH_QSPI_ADDRESS_SIZE_24,
                              MDH_QSPI_ALT_ADDRESS_SIZE_8,
                              WRITE_SINGLE,
                              MODE_1_1_1,
                              write_alt_size,
                              write_count,
                              MDH_QSPI_ADDRESS_SIZE_24,
                              MDH_QSPI_ALT_ADDRESS_SIZE_8,
                              READ_SINGLE,
                              TEST_REPEAT_SINGLE,
                              data_size,
                              erase_cmd,
                              max_erase_wait_time_us,
                              max_prog_wait_time_us,
                              status_reg_size,
                              wrsr_max_time,
                              status_reg_read_cmd,
                              write_enable_cmd,
                              status_bit_write_in_progess,
                              status_bit_write_enable_latch>(
            qspi, us_ticker, TEST_FLASH_ADDRESS, tx_buf, tx_size, rx_buf, rx_size);

        utest_printf("frequency setting %d [Hz] - OK\r\n", freq);

        freq /= 2;
    }
}

/** Template for write/read tests
 *
 *  Test single write/read operation of a block of data to/from the specific memory address
 *  Given board provides QSPI, with QSPI already initialized.
 *  When perform write and then read operations.
 *  Then data is successfully written and then read (no exception is generated) and the read data is valid.
 *
 *  Test multiple write/read operation of a block of data to/from the same specific memory address
 *  Given board provides QSPI, with QSPI already initialized.
 *  When perform write and then read operations.
 *  Then data is successfully written and then read (no exception is generated) and the read data is valid.
 *
 *  Test multiple adjacent write and single read operation of a block of data to/from the specific memory address
 *  Given board provides QSPI, with QSPI already initialized.
 *  When perform write and then read operations.
 *  Then data is successfully written and then read (no exception is generated) and the read data is valid.
 *
 *  Test single write and multiple adjacent read operation of a block of data to/from the specific memory address
 *  Given board provides QSPI, with QSPI already initialized.
 *  When perform write and then read operations.
 *  Then data is successfully written and then read (no exception is generated) and the read data is valid.
 *
 */
template <mdh_qspi_bus_width_t write_inst_width,
          mdh_qspi_bus_width_t write_addr_width,
          mdh_qspi_bus_width_t write_data_width,
          mdh_qspi_bus_width_t write_alt_width,
          uint32_t write_cmd,
          mdh_qspi_address_size_t write_addr_size,
          mdh_qspi_alt_size_t write_alt_size,
          uint32_t write_count,
          mdh_qspi_bus_width_t read_inst_width,
          mdh_qspi_bus_width_t read_addr_width,
          mdh_qspi_bus_width_t read_data_width,
          mdh_qspi_bus_width_t read_alt_width,
          uint32_t read_cmd,
          int read_dummy_cycles,
          mdh_qspi_address_size_t read_addr_size,
          mdh_qspi_alt_size_t read_alt_size,
          int frequency,
          uint32_t read_count,
          uint32_t test_count,
          uint32_t data_size,
          uint32_t flash_addr,
          uint32_t sector_count,
          uint32_t sector_size,
          uint32_t erase_cmd,
          uint32_t max_erase_wait_time_us,
          uint32_t max_prog_wait_time_us,
          size_t status_reg_size,
          size_t max_reg_size,
          uint32_t wrsr_max_time,
          uint32_t wait_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t status_reg_write_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch,
          uint32_t write_disable_cmd>
void qspi_write_read_test(
    mdh_qspi_t *qspi_handle, mdh_ticker_t *us_ticker, uint8_t *tx_buf, size_t tx_size, uint8_t *rx_buf, size_t rx_size)
{
    uint32_t addr = flash_addr;
    if (addr == 0) {
        // if no specified address selected, use random one to extend flash life
        addr = gen_flash_address(sector_count, sector_size);
    }

    Qspi qspi = {.handle = qspi_handle};

    mdh_qspi_status_t ret = mdh_qspi_set_mode(qspi.handle, 0);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

    ret = mdh_qspi_set_frequency(qspi.handle, frequency);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

    qspi.cmd.configure(MODE_1_1_1, MDH_QSPI_ADDRESS_SIZE_24, MDH_QSPI_ALT_ADDRESS_SIZE_8);
    flash_init<status_reg_size,
               wrsr_max_time,
               wait_max_time,
               status_reg_read_cmd,
               status_reg_write_cmd,
               write_enable_cmd,
               status_bit_write_in_progess>(qspi, us_ticker);

    // switch memory to high performance mode (if available)
    ret = fast_mode_enable(qspi);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

#ifdef QSPI_TEST_LOG_FLASH_STATUS
    log_register<max_reg_size>(STATUS_REG, MDH_QSPI_STATUS_REG_SIZE, qspi, "Status register");
    log_register<max_reg_size>(CONFIG_REG0, QSPI_CONFIG_REG_0_SIZE, qspi, "Config register 0");
#ifdef CONFIG_REG1
    log_register<max_reg_size>(CONFIG_REG1, QSPI_CONFIG_REG_1_SIZE, qspi, "Config register 1");
#endif
#ifdef CONFIG_REG2
    log_register<max_reg_size>(CONFIG_REG2, QSPI_CONFIG_REG_2_SIZE, qspi, "Config register 2");
#endif
#endif

    _qspi_write_read_test<write_inst_width,
                          write_addr_width,
                          write_data_width,
                          write_alt_width,
                          write_cmd,
                          write_addr_size,
                          write_alt_size,
                          write_count,
                          read_inst_width,
                          read_addr_width,
                          read_data_width,
                          read_alt_width,
                          read_cmd,
                          read_dummy_cycles,
                          read_addr_size,
                          read_alt_size,
                          read_count,
                          test_count,
                          data_size,
                          erase_cmd,
                          max_erase_wait_time_us,
                          max_prog_wait_time_us,
                          status_reg_size,
                          wrsr_max_time,
                          status_reg_read_cmd,
                          write_enable_cmd,
                          status_bit_write_in_progess,
                          status_bit_write_enable_latch>(qspi, us_ticker, addr, tx_buf, tx_size, rx_buf, rx_size);

    ret = fast_mode_disable(qspi);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);
}

void qspi_memory_id_test(const char *chip_name)
{
    utest_printf("*** %s memory config loaded ***\r\n", chip_name);
}

#endif // MDH_QSPI_TEST_H

/** @}*/
/** @}*/
