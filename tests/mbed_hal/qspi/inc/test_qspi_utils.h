/* Copyright (c) 2018-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MDH_QSPI_TEST_UTILS_H
#define MDH_QSPI_TEST_UTILS_H

#include "unity.h"
#include "utest/utest.h"

#include <string.h>

extern "C" {
#include "hal/qspi_api.h"
#include "hal/us_ticker_api.h"
}

#define QSPI_NONE (-1)

#define WAIT_FOR(timeout, q, us_ticker, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess)          \
    TEST_ASSERT_EQUAL_MESSAGE(                                                                                      \
        sOK,                                                                                                        \
        (flash_wait_for<status_reg_size, status_reg_read_cmd, status_bit_write_in_progess>(timeout, q, us_ticker)), \
        "flash_wait_for failed!!!")

// MODE_Command_Address_Data_Alt
#define MODE_1_1_1 \
    MDH_QSPI_BUS_WIDTH_SINGLE, MDH_QSPI_BUS_WIDTH_SINGLE, MDH_QSPI_BUS_WIDTH_SINGLE, MDH_QSPI_BUS_WIDTH_SINGLE
#define MODE_1_1_2 \
    MDH_QSPI_BUS_WIDTH_SINGLE, MDH_QSPI_BUS_WIDTH_SINGLE, MDH_QSPI_BUS_WIDTH_DUAL, MDH_QSPI_BUS_WIDTH_DUAL
#define MODE_1_2_2 MDH_QSPI_BUS_WIDTH_SINGLE, MDH_QSPI_BUS_WIDTH_DUAL, MDH_QSPI_BUS_WIDTH_DUAL, MDH_QSPI_BUS_WIDTH_DUAL
#define MODE_2_2_2 MDH_QSPI_BUS_WIDTH_DUAL, MDH_QSPI_BUS_WIDTH_DUAL, MDH_QSPI_BUS_WIDTH_DUAL, MDH_QSPI_BUS_WIDTH_DUAL
#define MODE_1_1_4 \
    MDH_QSPI_BUS_WIDTH_SINGLE, MDH_QSPI_BUS_WIDTH_SINGLE, MDH_QSPI_BUS_WIDTH_QUAD, MDH_QSPI_BUS_WIDTH_QUAD
#define MODE_1_4_4 MDH_QSPI_BUS_WIDTH_SINGLE, MDH_QSPI_BUS_WIDTH_QUAD, MDH_QSPI_BUS_WIDTH_QUAD, MDH_QSPI_BUS_WIDTH_QUAD
#define MODE_4_4_4 MDH_QSPI_BUS_WIDTH_QUAD, MDH_QSPI_BUS_WIDTH_QUAD, MDH_QSPI_BUS_WIDTH_QUAD, MDH_QSPI_BUS_WIDTH_QUAD

enum QspiStatus { sOK, sError, sTimeout, sUnknown };

class QspiCommand {
public:
    void configure(mdh_qspi_bus_width_t inst_width,
                   mdh_qspi_bus_width_t addr_width,
                   mdh_qspi_bus_width_t data_width,
                   mdh_qspi_bus_width_t alt_width,
                   mdh_qspi_address_size_t addr_size,
                   mdh_qspi_alt_size_t alt_size,
                   int dummy_cycles = 0);

    void set_dummy_cycles(int dummy_cycles);

    void build(int instruction, int address = QSPI_NONE, int alt = QSPI_NONE);

    mdh_qspi_command_t *get();

private:
    mdh_qspi_command_t _cmd;
};

struct Qspi {
    mdh_qspi_t *handle;
    QspiCommand cmd;
};

mdh_qspi_status_t fast_mode_enable(Qspi &qspi);

mdh_qspi_status_t fast_mode_disable(Qspi &qspi);

mdh_qspi_status_t read_register(uint32_t cmd, uint8_t *buf, uint32_t size, Qspi &q);

mdh_qspi_status_t write_register(uint32_t cmd, uint8_t *buf, uint32_t size, Qspi &q);

template <size_t status_reg_size, uint32_t status_reg_read_cmd, uint32_t status_bit_write_in_progess>
QspiStatus flash_wait_for(uint32_t time_us, Qspi &qspi, mdh_ticker_t *us_ticker)
{
    uint8_t reg[status_reg_size];
    mdh_qspi_status_t ret;

    const ticker_info_t *ticker_info = mdh_us_ticker_get_info(us_ticker);

    const uint32_t delay_in_ticks = time_us * (ticker_info->frequency / 1000000UL);

    // The expected tick is the current tick plus the desired delay in ticks.
    // When the current tick (value returned by us_ticker_read()) becomes larger
    // than the expected tick, the desired delay/timeout has elapsed.
    const uint32_t expected_tick = mdh_us_ticker_read(us_ticker) + delay_in_ticks;

    memset(reg, 255, status_reg_size);
    do {
        ret = read_register(status_reg_read_cmd, reg, status_reg_size, qspi);
    } while (((reg[0] & status_bit_write_in_progess) != 0) && (mdh_us_ticker_read(us_ticker) < expected_tick));

    if (((reg[0] & status_bit_write_in_progess) == 0) && (ret == MDH_QSPI_STATUS_OK)) {
        return sOK;
    } else if (ret != MDH_QSPI_STATUS_OK) {
        return sError;
    } else if (mdh_us_ticker_read(us_ticker) >= expected_tick) {
        return sTimeout;
    }
    return sUnknown;
}

template <size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t wait_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t status_reg_write_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess>
void flash_init(Qspi &qspi, mdh_ticker_t *us_ticker)
{
    uint8_t status[status_reg_size];
    mdh_qspi_status_t ret;

    qspi.cmd.build(status_reg_read_cmd);
    ret = mdh_qspi_transfer(qspi.handle, qspi.cmd.get(), NULL, 0, status, status_reg_size);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

#ifdef QSPI_CMD_RSTEN
    // Only do reset enable if device needs it
    if (QSPI_CMD_RSTEN != 0) {
        qspi.cmd.build(QSPI_CMD_RSTEN);
        ret = mdh_qspi_transfer(qspi.handle, qspi.cmd.get(), NULL, 0, NULL, 0);
        TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

        WAIT_FOR(wrsr_max_time, qspi, us_ticker, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);
    }
#endif

#ifdef QSPI_CMD_RST
    qspi.cmd.build(QSPI_CMD_RST);
    ret = mdh_qspi_transfer(qspi.handle, qspi.cmd.get(), NULL, 0, NULL, 0);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

    WAIT_FOR(wait_max_time, qspi, us_ticker, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);
#endif

    // Zero out status register to attempt to clear block protection bits
    uint8_t blanks[status_reg_size] = {0};

    qspi.cmd.build(write_enable_cmd);
    ret = mdh_qspi_transfer(qspi.handle, qspi.cmd.get(), NULL, 0, NULL, 0);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

    qspi.cmd.build(status_reg_write_cmd);
    ret = mdh_qspi_transfer(qspi.handle, qspi.cmd.get(), blanks, 1, NULL, 0);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

    WAIT_FOR(wrsr_max_time, qspi, us_ticker, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);
}

template <size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t write_enable_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch>
mdh_qspi_status_t write_enable(Qspi &qspi, mdh_ticker_t *us_ticker)
{
    uint8_t reg[status_reg_size];
    qspi.cmd.build(write_enable_cmd);

    if (mdh_qspi_transfer(qspi.handle, qspi.cmd.get(), NULL, 0, NULL, 0) != MDH_QSPI_STATUS_OK) {
        return MDH_QSPI_STATUS_ERROR;
    }
    WAIT_FOR(wrsr_max_time, qspi, us_ticker, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);

    memset(reg, 0, status_reg_size);
    if (read_register(status_reg_read_cmd, reg, status_reg_size, qspi) != MDH_QSPI_STATUS_OK) {
        return MDH_QSPI_STATUS_ERROR;
    }

    return ((reg[0] & status_bit_write_enable_latch) != 0 ? MDH_QSPI_STATUS_OK : MDH_QSPI_STATUS_ERROR);
}

template <size_t status_reg_size,
          uint32_t wrsr_max_time,
          uint32_t status_reg_read_cmd,
          uint32_t status_bit_write_in_progess,
          uint32_t status_bit_write_enable_latch,
          uint32_t write_disable_cmd>
mdh_qspi_status_t write_disable(Qspi &qspi, mdh_ticker_t *us_ticker)
{
    uint8_t reg[status_reg_size];
    qspi.cmd.build(write_disable_cmd);

    if (mdh_qspi_transfer(qspi.handle, qspi.cmd.get(), NULL, 0, NULL, 0) != MDH_QSPI_STATUS_OK) {
        return MDH_QSPI_STATUS_ERROR;
    }
    WAIT_FOR(wrsr_max_time, qspi, us_ticker, status_reg_size, status_reg_read_cmd, status_bit_write_in_progess);

    memset(reg, 0, status_reg_size);
    if (read_register(status_reg_read_cmd, reg, status_reg_size, qspi) != MDH_QSPI_STATUS_OK) {
        return MDH_QSPI_STATUS_ERROR;
    }

    return ((reg[0] & status_bit_write_enable_latch) == 0 ? MDH_QSPI_STATUS_OK : MDH_QSPI_STATUS_ERROR);
}

template <size_t max_reg_size> void log_register(uint32_t cmd, uint32_t reg_size, Qspi &qspi, const char *str)
{
    mdh_qspi_status_t ret;
    static uint8_t reg[max_reg_size];

    ret = read_register(cmd, reg, reg_size, qspi);
    TEST_ASSERT_EQUAL(MDH_QSPI_STATUS_OK, ret);

    for (uint32_t j = 0; j < reg_size; j++) {
        utest_printf("%s byte %u (MSB first): ", str != NULL ? str : "", j);
        for (int i = 0; i < 8; i++) {
            utest_printf("%s ", ((reg[j] & (1 << (7 - i))) & 0xFF) == 0 ? "0" : "1");
        }
        utest_printf("\r\n");
    }
}

bool is_extended_mode(mdh_qspi_bus_width_t inst_width,
                      mdh_qspi_bus_width_t addr_width,
                      mdh_qspi_bus_width_t data_width);

bool is_dual_mode(mdh_qspi_bus_width_t inst_width, mdh_qspi_bus_width_t addr_width, mdh_qspi_bus_width_t data_width);

bool is_quad_mode(mdh_qspi_bus_width_t inst_width, mdh_qspi_bus_width_t addr_width, mdh_qspi_bus_width_t data_width);

mdh_qspi_status_t mode_enable(Qspi &qspi,
                              mdh_qspi_bus_width_t inst_width,
                              mdh_qspi_bus_width_t addr_width,
                              mdh_qspi_bus_width_t data_width);

mdh_qspi_status_t mode_disable(Qspi &qspi,
                               mdh_qspi_bus_width_t inst_width,
                               mdh_qspi_bus_width_t addr_width,
                               mdh_qspi_bus_width_t data_width);

mdh_qspi_status_t erase(uint32_t erase_cmd, uint32_t flash_addr, Qspi &qspi);

#endif // MDH_QSPI_TEST_UTILS_H
