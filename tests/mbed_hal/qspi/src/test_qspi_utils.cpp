/* Copyright (c) 2018-2022, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_qspi_utils.h"

void QspiCommand::configure(mdh_qspi_bus_width_t inst_width,
                            mdh_qspi_bus_width_t addr_width,
                            mdh_qspi_bus_width_t data_width,
                            mdh_qspi_bus_width_t alt_width,
                            mdh_qspi_address_size_t addr_size,
                            mdh_qspi_alt_size_t alt_size,
                            int dummy_cycles)
{
    memset(&_cmd, 0, sizeof(mdh_qspi_command_t));
    _cmd.instruction.disabled = _cmd.address.disabled = _cmd.alt.disabled = true;

    _cmd.instruction.bus_width = inst_width;
    _cmd.address.bus_width = addr_width;
    _cmd.address.size = addr_size;
    _cmd.alt.bus_width = alt_width;
    _cmd.alt.size = alt_size;
    _cmd.data.bus_width = data_width;
    _cmd.dummy_count = dummy_cycles;
}

void QspiCommand::set_dummy_cycles(int dummy_cycles)
{
    _cmd.dummy_count = dummy_cycles;
}

void QspiCommand::build(int instruction, int address, int alt)
{
    _cmd.instruction.disabled = (instruction == QSPI_NONE);
    if (!_cmd.instruction.disabled) {
        _cmd.instruction.value = instruction;
    }

    _cmd.address.disabled = (address == QSPI_NONE);
    if (!_cmd.address.disabled) {
        _cmd.address.value = address;
    }

    _cmd.alt.disabled = (alt == QSPI_NONE);
    if (!_cmd.alt.disabled) {
        _cmd.alt.value = alt;
    }
}

mdh_qspi_command_t *QspiCommand::get()
{
    return &_cmd;
}

static mdh_qspi_status_t extended_enable(Qspi &qspi)
{
#ifdef EXTENDED_SPI_ENABLE
    EXTENDED_SPI_ENABLE();
#else
    return MDH_QSPI_STATUS_OK;
#endif
}

static mdh_qspi_status_t extended_disable(Qspi &qspi)
{
#ifdef EXTENDED_SPI_DISABLE
    EXTENDED_SPI_DISABLE();
#else
    return MDH_QSPI_STATUS_OK;
#endif
}

static mdh_qspi_status_t dual_enable(Qspi &qspi)
{
#ifdef DUAL_ENABLE
    DUAL_ENABLE();
#else
    return MDH_QSPI_STATUS_OK;
#endif
}

static mdh_qspi_status_t dual_disable(Qspi &qspi)
{
#ifdef DUAL_DISABLE
    DUAL_DISABLE();
#else
    return MDH_QSPI_STATUS_OK;
#endif
}

static mdh_qspi_status_t quad_enable(Qspi &qspi)
{
#ifdef QUAD_ENABLE
    QUAD_ENABLE();
#else
    return MDH_QSPI_STATUS_OK;
#endif
}

static mdh_qspi_status_t quad_disable(Qspi &qspi)
{
#ifdef QUAD_DISABLE
    QUAD_DISABLE();
#else
    return MDH_QSPI_STATUS_OK;
#endif
}

mdh_qspi_status_t fast_mode_enable(Qspi &qspi)
{
#ifdef FAST_MODE_ENABLE
    FAST_MODE_ENABLE();
#else
    return MDH_QSPI_STATUS_OK;
#endif
}

mdh_qspi_status_t fast_mode_disable(Qspi &qspi)
{
#ifdef FAST_MODE_DISABLE
    FAST_MODE_DISABLE();
#else
    return MDH_QSPI_STATUS_OK;
#endif
}

mdh_qspi_status_t read_register(uint32_t cmd, uint8_t *buf, uint32_t size, Qspi &q)
{
    q.cmd.build(cmd);
    return mdh_qspi_transfer(q.handle, q.cmd.get(), NULL, 0, buf, size);
}

mdh_qspi_status_t write_register(uint32_t cmd, uint8_t *buf, uint32_t size, Qspi &q)
{
    q.cmd.build(cmd);
    return mdh_qspi_transfer(q.handle, q.cmd.get(), buf, size, NULL, 0);
}

bool is_extended_mode(mdh_qspi_bus_width_t inst_width, mdh_qspi_bus_width_t addr_width, mdh_qspi_bus_width_t data_width)
{
    return (inst_width == MDH_QSPI_BUS_WIDTH_SINGLE)
           && ((addr_width != MDH_QSPI_BUS_WIDTH_SINGLE) || (data_width != MDH_QSPI_BUS_WIDTH_SINGLE));
}

bool is_dual_mode(mdh_qspi_bus_width_t inst_width, mdh_qspi_bus_width_t addr_width, mdh_qspi_bus_width_t data_width)
{
    return (inst_width == MDH_QSPI_BUS_WIDTH_DUAL) && (addr_width == MDH_QSPI_BUS_WIDTH_DUAL)
           && (data_width == MDH_QSPI_BUS_WIDTH_DUAL);
}

bool is_quad_mode(mdh_qspi_bus_width_t inst_width, mdh_qspi_bus_width_t addr_width, mdh_qspi_bus_width_t data_width)
{
    return (inst_width == MDH_QSPI_BUS_WIDTH_QUAD) && (addr_width == MDH_QSPI_BUS_WIDTH_QUAD)
           && (data_width == MDH_QSPI_BUS_WIDTH_QUAD);
}

mdh_qspi_status_t mode_enable(Qspi &qspi,
                              mdh_qspi_bus_width_t inst_width,
                              mdh_qspi_bus_width_t addr_width,
                              mdh_qspi_bus_width_t data_width)
{
    if (is_extended_mode(inst_width, addr_width, data_width)) {
        return extended_enable(qspi);
    } else if (is_dual_mode(inst_width, addr_width, data_width)) {
        return dual_enable(qspi);
    } else if (is_quad_mode(inst_width, addr_width, data_width)) {
        return quad_enable(qspi);
    } else {
        return MDH_QSPI_STATUS_OK;
    }
}

mdh_qspi_status_t mode_disable(Qspi &qspi,
                               mdh_qspi_bus_width_t inst_width,
                               mdh_qspi_bus_width_t addr_width,
                               mdh_qspi_bus_width_t data_width)
{
    if (is_extended_mode(inst_width, addr_width, data_width)) {
        return extended_disable(qspi);
    } else if (is_dual_mode(inst_width, addr_width, data_width)) {
        return dual_disable(qspi);
    } else if (is_quad_mode(inst_width, addr_width, data_width)) {
        return quad_disable(qspi);
    } else {
        return MDH_QSPI_STATUS_OK;
    }
}

mdh_qspi_status_t erase(uint32_t erase_cmd, uint32_t flash_addr, Qspi &qspi)
{
    qspi.cmd.build(erase_cmd, flash_addr);
    return mdh_qspi_transfer(qspi.handle, qspi.cmd.get(), NULL, 0, NULL, 0);
}
