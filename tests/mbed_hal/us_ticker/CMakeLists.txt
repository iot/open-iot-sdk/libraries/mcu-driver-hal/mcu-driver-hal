# Copyright (c) 2020-2023 Arm Limited
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

add_library(mdh-test-us_ticker
    src/test_us_ticker.cpp
)

target_include_directories(mdh-test-us_ticker
    PUBLIC
        inc
)

target_link_libraries(mdh-test-us_ticker
    PRIVATE
        mcu-driver-hal-api
        greentea::client
        utest
        unity
)
