/* Copyright (c) 2017-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_us_ticker.h"

#include "unity.h"
#include "utest/utest.h"

extern "C" {
#include "hal/us_ticker_api.h"
}

/* Test that the ticker has the correct frequency and number of bits. */
void us_ticker_info_test(mdh_ticker_t *us_ticker)
{
    const ticker_info_t *p_ticker_info = mdh_us_ticker_get_info(us_ticker);

    TEST_ASSERT(p_ticker_info->frequency >= 250000);

    switch (p_ticker_info->bits) {
        case 32:
            TEST_ASSERT(p_ticker_info->frequency <= 100000000);
            break;

        default:
            TEST_ASSERT(p_ticker_info->frequency <= 8000000);
            break;
    }

    TEST_ASSERT(p_ticker_info->bits >= 16);
}
