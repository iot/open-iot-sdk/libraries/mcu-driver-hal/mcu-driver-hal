/* Copyright (c) 2017-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "greentea-client/test_env.h"
#include "mbed_critical/mbed_critical.h"
#include "unity.h"
#include "utest/utest.h"

extern "C" {
#include "hal/lp_ticker_api.h"
#include "hal/us_ticker_api.h"
}

#include "test_us_ticker_lp_ticker_common.h"

#ifdef MBED_CONF_RTOS_PRESENT
#ifdef __cplusplus
extern "C" {
#endif
#include "os_tick.h"
#ifdef __cplusplus
}
#endif // __cplusplus
#endif

#define US_PER_S 1000000

#define FORCE_OVERFLOW_TEST (false)
#define TICKER_INT_VAL      2000
#define TICKER_DELTA        10

#define LP_TICKER_OVERFLOW_DELTA1 0 // this will allow to detect that ticker counter rollovers to 0
#define LP_TICKER_OVERFLOW_DELTA2 0
#define US_TICKER_OVERFLOW_DELTA1 50
#define US_TICKER_OVERFLOW_DELTA2 60

#define TICKER_100_TICKS 100
#define TICKER_500_TICKS 500

#define MAX_FUNC_EXEC_TIME_US   20
#define DELTA_FUNC_EXEC_TIME_US 5
#define NUM_OF_CALLS            100

#define NUM_OF_CYCLES 100000

#define US_TICKER_OV_LIMIT 35000
#define LP_TICKER_OV_LIMIT 4000

#define TICKS_TO_US(ticks, freq) ((uint32_t)((uint64_t)ticks * US_PER_S / freq))

using namespace utest::v1;

volatile int intFlag = 0;
static uint32_t time_window = 0;
ticker_irq_callback_f prev_irq_handler;

uint32_t count_ticks(mdh_ticker_t *ticker, uint32_t cycles, uint32_t step) __attribute__((noinline));
void overflow_protect(mdh_ticker_t *ticker);
void ticker_event_handler_stub(mdh_ticker_t *ticker);
void wait_cycles(volatile unsigned int cycles);
uint32_t diff_us(uint32_t start_ticks, uint32_t stop_ticks, const ticker_info_t *info);
void ticker_info_test(mdh_ticker_t *ticker);
void ticker_interrupt_test(mdh_ticker_t *ticker);
void ticker_past_test(mdh_ticker_t *ticker);
void ticker_repeat_reschedule_test(mdh_ticker_t *ticker);
void ticker_fire_now_test(mdh_ticker_t *ticker);
void ticker_overflow_test(mdh_ticker_t *ticker);
void ticker_increment_test(mdh_ticker_t *ticker);
void ticker_speed_test(mdh_ticker_t *ticker, mdh_ticker_t *usticker);
void us_ticker_test_setup_handler(mdh_ticker_t *us_ticker);
void us_ticker_test_teardown_handler(mdh_ticker_t *us_ticker);
void lp_ticker_test_setup_handler(mdh_ticker_t *lp_ticker);
void lp_ticker_test_teardown_handler(mdh_ticker_t *lp_ticker);

/* Some targets might fail overflow test uncertainly due to getting trapped in busy
 * intf->read() loop. In the loop, some ticker values wouldn't get caught in time
 * because of:
 * 1. Lower CPU clock
 * 2. Compiled code with worse performance
 * 3. Interrupt at that time
 *
 * We fix it by checking small ticker value range rather than one exact ticker point
 * in near overflow check.
 */
unsigned int ticker_overflow_delta1;
unsigned int ticker_overflow_delta2;

/* Auxiliary function to count ticker ticks elapsed during execution of N cycles of empty while loop.
 * Parameter <step> is used to disable compiler optimisation. */
uint32_t count_ticks(mdh_ticker_t *ticker, uint32_t cycles, uint32_t step)
{
    register uint32_t reg_cycles = cycles;

    const ticker_info_t *p_ticker_info = ticker->vtable->get_info(ticker);
    const uint32_t max_count = ((1 << p_ticker_info->bits) - 1);

    core_util_critical_section_enter();

    const uint32_t start = ticker->vtable->read(ticker);

    while (reg_cycles -= step) {
        /* Just wait. */
    }

    const uint32_t stop = ticker->vtable->read(ticker);

    core_util_critical_section_exit();

    /* Handle overflow - overflow protection may not work in this case. */
    uint32_t diff = (start <= stop) ? (stop - start) : (uint32_t)(max_count - start + stop + 1);

    return (diff);
}

/* Since according to the ticker requirements min acceptable counter size is
 * - 12 bits for low power timer - max count = 4095,
 * - 16 bits for high frequency timer - max count = 65535
 * then all test cases must be executed in this time windows.
 * HAL ticker layer handles counter overflow and it is not handled in the target
 * ticker drivers. Ensure we have enough time to execute test case without overflow.
 */
void overflow_protect(mdh_ticker_t *ticker)
{
    const uint32_t ticks_now = ticker->vtable->read(ticker);
    const ticker_info_t *p_ticker_info = ticker->vtable->get_info(ticker);

    const uint32_t max_count = ((1 << p_ticker_info->bits) - 1);

    if ((max_count - ticks_now) > time_window) {
        return;
    }

    while (ticker->vtable->read(ticker) >= ticks_now)
        ;
}

void ticker_event_handler_stub(mdh_ticker_t *ticker)
{
    ticker->vtable->clear_interrupt(ticker);

    /* Indicate that ISR has been executed in interrupt context. */
    if (core_util_is_isr_active()) {
        intFlag++;
    }
}

void wait_cycles(volatile unsigned int cycles)
{
    while (cycles--)
        ;
}

/* Auxiliary function to determine how long ticker function are executed.
 * This function returns number of us between <start_ticks> and <stop_ticks>
 * taking into account counter roll-over, counter size and frequency.
 */
uint32_t diff_us(uint32_t start_ticks, uint32_t stop_ticks, const ticker_info_t *info)
{
    uint32_t counter_mask = ((1 << info->bits) - 1);

    uint32_t diff_ticks = ((stop_ticks - start_ticks) & counter_mask);

    return (uint32_t)((uint64_t)diff_ticks * US_PER_S / info->frequency);
}

/* Test that ticker frequency is non-zero and counter is at least 8 bits */
void ticker_info_test(mdh_ticker_t *ticker)
{
    const ticker_info_t *p_ticker_info = ticker->vtable->get_info(ticker);

    TEST_ASSERT(p_ticker_info->frequency != 0);
    TEST_ASSERT(p_ticker_info->bits >= 8);
}

/* Test that ticker interrupt fires only when the ticker counter increments to the value set by ticker_set_interrupt. */
void ticker_interrupt_test(mdh_ticker_t *ticker)
{
    uint32_t ticker_timeout[] = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200};
    uint8_t run_count = 0;
    const ticker_info_t *p_ticker_info = ticker->vtable->get_info(ticker);

    overflow_protect(ticker);

    for (uint32_t i = 0; i < (sizeof(ticker_timeout) / sizeof(uint32_t)); i++) {

        /* Skip timeout if less than max allowed execution time of set_interrupt() - 20 us */
        if (TICKS_TO_US(ticker_timeout[i], p_ticker_info->frequency)
            < (MAX_FUNC_EXEC_TIME_US + DELTA_FUNC_EXEC_TIME_US)) {
            continue;
        }

        run_count++;
        intFlag = 0;
        const uint32_t tick_count = ticker->vtable->read(ticker);

        /* Set interrupt. Interrupt should be fired when tick count is equal to:
         * tick_count + ticker_timeout[i]. */
        ticker->vtable->set_interrupt(ticker, tick_count + ticker_timeout[i]);

        /* Wait until ticker count reach value: tick_count + ticker_timeout[i] - TICKER_DELTA.
         * Interrupt should not be fired. */
        while (ticker->vtable->read(ticker) < (tick_count + ticker_timeout[i] - TICKER_DELTA)) {
            /* Indicate failure if interrupt has fired earlier. */
            TEST_ASSERT_EQUAL_INT_MESSAGE(0, intFlag, "Interrupt fired too early");
        }

        /* Wait until ticker count reach value: tick_count + ticker_timeout[i] + TICKER_DELTA.
         * Interrupt should be fired after this time. */
        while (ticker->vtable->read(ticker) < (tick_count + ticker_timeout[i] + TICKER_DELTA)) {
            /* Just wait. */
        }

        TEST_ASSERT_EQUAL(1, intFlag);

        /* Wait until ticker count reach value: tick_count + 2 * ticker_timeout[i] + TICKER_DELTA.
         * Interrupt should not be triggered again. */
        while (ticker->vtable->read(ticker) < (tick_count + 2 * ticker_timeout[i] + TICKER_DELTA)) {
            /* Just wait. */
        }

        TEST_ASSERT_EQUAL(1, intFlag);
    }

    /* At least 3 sub test cases must be executed. */
    TEST_ASSERT_MESSAGE(run_count >= 3, "At least 3 sub test cases must be executed");
}

/* Test that ticker interrupt is not triggered when ticker_set_interrupt */
void ticker_past_test(mdh_ticker_t *ticker)
{
    intFlag = 0;

    const uint32_t tick_count = ticker->vtable->read(ticker);

    /* Set interrupt tick count to value in the past.
     * Interrupt should not be fired. */
    ticker->vtable->set_interrupt(ticker, tick_count - TICKER_DELTA);

    wait_cycles(1000);

    TEST_ASSERT_EQUAL(0, intFlag);
}

/* Test that ticker can be rescheduled repeatedly before the handler has been called. */
void ticker_repeat_reschedule_test(mdh_ticker_t *ticker)
{
    overflow_protect(ticker);

    intFlag = 0;

    const uint32_t tick_count = ticker->vtable->read(ticker);

    /* Set interrupt. Interrupt should be fired when tick count is equal to:
     * tick_count + TICKER_INT_VAL. */
    ticker->vtable->set_interrupt(ticker, tick_count + TICKER_INT_VAL);

    /* Reschedule interrupt - it should not be fired yet.
     * Re-schedule interrupt. */
    ticker->vtable->set_interrupt(ticker, tick_count + (2 * TICKER_INT_VAL));
    ticker->vtable->set_interrupt(ticker, tick_count + (3 * TICKER_INT_VAL));

    /* Wait until ticker count reach value: tick_count + 3*TICKER_INT_VAL - TICKER_DELTA.
     * Interrupt should not be fired. */
    while (ticker->vtable->read(ticker) < (tick_count + 3 * TICKER_INT_VAL - TICKER_DELTA)) {
        /* Indicate failure if interrupt has fired earlier. */
        TEST_ASSERT_EQUAL_INT_MESSAGE(0, intFlag, "Interrupt fired too early");
    }

    /* Wait until ticker count reach value: tick_count + 3*TICKER_INT_VAL + TICKER_DELTA.
     * Interrupt should be fired after this time. */
    while (ticker->vtable->read(ticker) < (tick_count + 3 * TICKER_INT_VAL + TICKER_DELTA)) {
        /* Just wait. */
    }

    TEST_ASSERT_EQUAL(1, intFlag);
}

/* Test that ticker_fire_interrupt causes and interrupt to get fired immediately. */
void ticker_fire_now_test(mdh_ticker_t *ticker)
{
    intFlag = 0;

    ticker->vtable->fire_interrupt(ticker);

    /* On some platforms set_interrupt function sets interrupt in the nearest feature. */
    wait_cycles(1000);

    TEST_ASSERT_EQUAL(1, intFlag);
}

/* Test that the ticker correctly handles overflow. */
void ticker_overflow_test(mdh_ticker_t *ticker)
{
    const ticker_info_t *p_ticker_info = ticker->vtable->get_info(ticker);

    /* We need to check how long it will take to overflow.
     * We will perform this test only if this time is no longer than 30 sec.
     */
    const uint32_t max_count = (1 << p_ticker_info->bits) - 1;
    const uint32_t required_time_sec = (max_count / p_ticker_info->frequency);

    if (required_time_sec > 30 && !FORCE_OVERFLOW_TEST) {
        TEST_ASSERT_TRUE(true);
        utest_printf("Test has been skipped.\n");
        return;
    }

    intFlag = 0;

    /* Wait for max count. */
    while (ticker->vtable->read(ticker) >= (max_count - ticker_overflow_delta2)
           && ticker->vtable->read(ticker) <= (max_count - ticker_overflow_delta1)) {
        /* Just wait. */
    }

    /* Now we are near/at the overflow point. Detect rollover. */
    while (ticker->vtable->read(ticker) > ticker_overflow_delta1)
        ;

    const uint32_t after_overflow = ticker->vtable->read(ticker);

    /* Now we are just after overflow. Wait a while assuming that ticker still counts. */
    while (ticker->vtable->read(ticker) < TICKER_100_TICKS) {
        /* Just wait. */
    }

    const uint32_t next_after_overflow = ticker->vtable->read(ticker);

    /* Check that after the overflow ticker continue count. */
    TEST_ASSERT(after_overflow <= ticker_overflow_delta1);
    TEST_ASSERT(next_after_overflow >= TICKER_100_TICKS);
    TEST_ASSERT_EQUAL(0, intFlag);

    const uint32_t tick_count = ticker->vtable->read(ticker);

    /* Check if interrupt scheduling still works. */
    ticker->vtable->set_interrupt(ticker, tick_count + TICKER_INT_VAL);

    /* Wait for the interrupt. */
    while (ticker->vtable->read(ticker) < (tick_count + TICKER_INT_VAL + TICKER_DELTA)) {
        /* Just wait. */
    }

    TEST_ASSERT_EQUAL(1, intFlag);
}

/* Test that the ticker increments by one on each tick. */
void ticker_increment_test(mdh_ticker_t *ticker)
{
    const ticker_info_t *p_ticker_info = ticker->vtable->get_info(ticker);

    /* Perform test based on ticker speed. */
    if (p_ticker_info->frequency <= 250000) { // low frequency tickers
        const uint32_t base_tick_count = ticker->vtable->read(ticker);
        uint32_t next_tick_count = base_tick_count;

        while (next_tick_count == base_tick_count) {
            next_tick_count = ticker->vtable->read(ticker);
        }

        TEST_ASSERT_UINT32_WITHIN(1, next_tick_count, base_tick_count);
    } else { // high frequency tickers

        uint32_t num_of_cycles = NUM_OF_CYCLES;
        const uint32_t repeat_count = 20;
        const uint32_t max_inc_val = 100;

        uint32_t base_tick_count = count_ticks(ticker, num_of_cycles, 1);
        uint32_t next_tick_count = base_tick_count;
        uint32_t inc_val = 0;
        uint32_t repeat_cnt = 0;

        while (inc_val < max_inc_val) {
            next_tick_count = count_ticks(ticker, num_of_cycles + inc_val, 1);

            if (next_tick_count == base_tick_count) {

                /* Same tick count, so repeat 20 times and than
                 * increase num of cycles by 1.
                 */
                if (repeat_cnt == repeat_count) {
                    inc_val++;
                    repeat_cnt = 0;
                }

                repeat_cnt++;
            } else {
                /* Check if we got less than 2 ticks. */
                if ((abs((int32_t)(next_tick_count - base_tick_count)) < 2) || (num_of_cycles < 2)) {
                    break;
                }

                /* It is possible that the difference between base and next
                 * tick count on some platforms is greater that 1, in this case we need
                 * to repeat counting with the reduced number of cycles (for slower boards).
                 * In cases if difference is exactly 1 we can exit the loop.
                 */
                num_of_cycles /= 2;
                inc_val = 0;
                repeat_cnt = 0;
                base_tick_count = count_ticks(ticker, num_of_cycles, 1);
            }
        }

        if (num_of_cycles >= 2) {
            /* Since we are here we know that next_tick_count != base_tick_count.
             * The accuracy of our measurement method is +/- 1 tick, so it is possible that
             * next_tick_count == base_tick_count - 1. This is also valid result.
             */
            TEST_ASSERT_UINT32_WITHIN(1, next_tick_count, base_tick_count);
        } else {
            TEST_IGNORE_MESSAGE("core is too slow compare to the timer");
        }
    }
}

/* Test that common ticker functions complete with the required amount of time. */
void ticker_speed_test(mdh_ticker_t *ticker, mdh_ticker_t *usticker)
{
    int counter = NUM_OF_CALLS;
    uint32_t start;
    uint32_t stop;

    const ticker_info_t *us_ticker_info = mdh_us_ticker_get_info(usticker);

    /* ---- Test ticker_read function. ---- */
    start = mdh_us_ticker_read(usticker);
    while (counter--) {
        ticker->vtable->read(ticker);
    }
    stop = mdh_us_ticker_read(usticker);

    TEST_ASSERT(diff_us(start, stop, us_ticker_info)
                < (NUM_OF_CALLS * (MAX_FUNC_EXEC_TIME_US + DELTA_FUNC_EXEC_TIME_US)));

    /* ---- Test ticker_clear_interrupt function. ---- */
    counter = NUM_OF_CALLS;
    start = mdh_us_ticker_read(usticker);
    while (counter--) {
        ticker->vtable->clear_interrupt(ticker);
    }
    stop = mdh_us_ticker_read(usticker);

    TEST_ASSERT(diff_us(start, stop, us_ticker_info)
                < (NUM_OF_CALLS * (MAX_FUNC_EXEC_TIME_US + DELTA_FUNC_EXEC_TIME_US)));

    /* ---- Test ticker_set_interrupt function. ---- */
    counter = NUM_OF_CALLS;
    start = mdh_us_ticker_read(usticker);
    while (counter--) {
        ticker->vtable->set_interrupt(ticker, 0);
    }
    stop = mdh_us_ticker_read(usticker);

    TEST_ASSERT(diff_us(start, stop, us_ticker_info)
                < (NUM_OF_CALLS * (MAX_FUNC_EXEC_TIME_US + DELTA_FUNC_EXEC_TIME_US)));

    /* ---- Test fire_interrupt function. ---- */
    counter = NUM_OF_CALLS;
    /* Disable ticker interrupt which would interfere with speed test */
    core_util_critical_section_enter();
    start = mdh_us_ticker_read(usticker);
    while (counter--) {
        ticker->vtable->fire_interrupt(ticker);
    }
    stop = mdh_us_ticker_read(usticker);
    core_util_critical_section_exit();

    TEST_ASSERT(diff_us(start, stop, us_ticker_info)
                < (NUM_OF_CALLS * (MAX_FUNC_EXEC_TIME_US + DELTA_FUNC_EXEC_TIME_US)));

    /* ---- Test disable_interrupt function. ---- */
    counter = NUM_OF_CALLS;
    start = mdh_us_ticker_read(usticker);
    while (counter--) {
        ticker->vtable->disable_interrupt(ticker);
    }
    stop = mdh_us_ticker_read(usticker);

    TEST_ASSERT(diff_us(start, stop, us_ticker_info)
                < (NUM_OF_CALLS * (MAX_FUNC_EXEC_TIME_US + DELTA_FUNC_EXEC_TIME_US)));
}

void us_ticker_test_setup_handler(mdh_ticker_t *us_ticker)
{
    prev_irq_handler = mdh_us_ticker_set_irq_handler(us_ticker, ticker_event_handler_stub);

    ticker_overflow_delta1 = US_TICKER_OVERFLOW_DELTA1;
    ticker_overflow_delta2 = US_TICKER_OVERFLOW_DELTA2;
    time_window = US_TICKER_OV_LIMIT;
}

void us_ticker_test_teardown_handler(mdh_ticker_t *us_ticker)
{
    mdh_us_ticker_set_irq_handler(us_ticker, prev_irq_handler);
    prev_irq_handler = NULL;
}

void lp_ticker_test_setup_handler(mdh_ticker_t *lp_ticker)
{
    prev_irq_handler = mdh_lp_ticker_set_irq_handler(lp_ticker, ticker_event_handler_stub);

    ticker_overflow_delta1 = LP_TICKER_OVERFLOW_DELTA1;
    ticker_overflow_delta2 = LP_TICKER_OVERFLOW_DELTA2;
    time_window = LP_TICKER_OV_LIMIT;
}

void lp_ticker_test_teardown_handler(mdh_ticker_t *lp_ticker)
{
    mdh_lp_ticker_set_irq_handler(lp_ticker, prev_irq_handler);
    prev_irq_handler = NULL;
}
