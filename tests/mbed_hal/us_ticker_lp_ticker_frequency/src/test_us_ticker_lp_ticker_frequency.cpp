/* Copyright (c) 2013-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This is the mbed device part of the test to verify if mbed board ticker
 * freqency is valid.
 */

#include "greentea-client/test_env.h"
#include "mbed_critical/mbed_critical.h"
#include "unity.h"
#include "utest/utest.h"

#include <string.h>

extern "C" {
#include "hal/lp_ticker_api.h"
#include "hal/us_ticker_api.h"
}

#include "test_us_ticker_lp_ticker_frequency.h"

#if defined(SKIP_TIME_DRIFT_TESTS)
#error[NOT_SUPPORTED] timing accuracy tests skipped
#endif // defined(SKIP_TIME_DRIFT_TESTS)

#define US_PER_S 1000000

uint32_t intf_mask;
uint32_t intf_last_tick;
uint32_t intf_elapsed_ticks;
ticker_irq_callback_f prev_handler;

uint32_t ticks_to_us(uint32_t ticks, uint32_t freq)
{
    return (uint32_t)((uint64_t)ticks * US_PER_S / freq);
}

void elapsed_ticks_reset(mdh_ticker_t *ticker)
{
    core_util_critical_section_enter();

    const uint32_t ticker_bits = ticker->vtable->get_info(ticker)->bits;

    intf_mask = (1 << ticker_bits) - 1;
    intf_last_tick = ticker->vtable->read(ticker);
    intf_elapsed_ticks = 0;

    core_util_critical_section_exit();
}

uint32_t elapsed_ticks_update(mdh_ticker_t *ticker)
{
    core_util_critical_section_enter();

    const uint32_t current_tick = ticker->vtable->read(ticker);
    intf_elapsed_ticks += (current_tick - intf_last_tick) & intf_mask;
    intf_last_tick = current_tick;

    /* Schedule next interrupt half way to overflow */
    uint32_t next = (current_tick + intf_mask / 2) & intf_mask;
    ticker->vtable->set_interrupt(ticker, next);

    uint32_t elapsed = intf_elapsed_ticks;

    core_util_critical_section_exit();
    return elapsed;
}

void ticker_event_handler_stub(mdh_ticker_t *ticker)
{
    ticker->vtable->clear_interrupt(ticker);
    elapsed_ticks_update(ticker);
}

/* Test that the ticker is operating at the frequency it specifies. */
void ticker_frequency_test(mdh_ticker_t *ticker)
{
    char _key[11] = {};
    char _value[128] = {};
    int expected_key = 1;
    const uint32_t ticker_freq = ticker->vtable->get_info(ticker)->frequency;

    elapsed_ticks_reset(ticker);

    /* Detect overflow for tickers with lower counters width. */
    elapsed_ticks_update(ticker);

    greentea_send_kv("timing_drift_check_start", 0);

    /* Wait for 1st signal from host. */
    do {
        greentea_parse_kv(_key, _value, sizeof(_key), sizeof(_value));
        expected_key = strcmp(_key, "base_time");
    } while (expected_key);

    const uint32_t begin_ticks = elapsed_ticks_update(ticker);

    /* Assume that there was no overflow at this point - we are just after init. */
    greentea_send_kv(_key, ticks_to_us(begin_ticks, ticker_freq));

    /* Wait for 2nd signal from host. */
    greentea_parse_kv(_key, _value, sizeof(_key), sizeof(_value));

    const uint32_t end_ticks = elapsed_ticks_update(ticker);

    greentea_send_kv(_key, ticks_to_us(end_ticks, ticker_freq));

    /* Get the results from host. */
    greentea_parse_kv(_key, _value, sizeof(_key), sizeof(_value));

    TEST_ASSERT_EQUAL_STRING_MESSAGE("pass", _key, "Host side script reported a fail...");

    ticker->vtable->disable_interrupt(ticker);
}

void us_ticker_test_setup_handler(mdh_ticker_t *us_ticker)
{
    prev_handler = mdh_us_ticker_set_irq_handler(us_ticker, ticker_event_handler_stub);
}

void us_ticker_test_teardown_handler(mdh_ticker_t *us_ticker)
{
    mdh_us_ticker_set_irq_handler(us_ticker, prev_handler);
}

void lp_ticker_test_setup_handler(mdh_ticker_t *lp_ticker)
{
    prev_handler = mdh_lp_ticker_set_irq_handler(lp_ticker, ticker_event_handler_stub);
}

void lp_ticker_test_teardown_handler(mdh_ticker_t *lp_ticker)
{
    mdh_lp_ticker_set_irq_handler(lp_ticker, prev_handler);
}
