# Testing with FPGA CI Test shield

## Setup

![30% center](fpga_test_shield.jpg)

## Considerations

The `FPGA_FORCE_ALL_PORTS` macro can be defined to force all pinouts of all peripherals to be tested. Some FPGA tests only test one pinout of one peripheral by default, to save time.

Tested from factor is defined by MBED_CONF_TARGET_DEFAULT_FORM_FACTOR "default-form-factor" default value is null.

When "default-form-factor" is not set, ARDUINO form factor is used.

## Known issues


## LINKS

https://github.com/ARMmbed/fpga-ci-test-shield

https://github.com/ARMmbed/fpga-ci-test-shield-updater

https://github.com/ARMmbed/fpga-ci-test-shield-terminal
