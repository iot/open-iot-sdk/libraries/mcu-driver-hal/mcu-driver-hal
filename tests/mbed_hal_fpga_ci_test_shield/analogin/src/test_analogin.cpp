/* Copyright (c) 2019-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_analogin.h"

#include "unity.h"

#define DELTA_FLOAT (0.1f)     // 10%
#define DELTA_U16   (2 * 3277) // 10%

void test_analogin_read(MbedTester &tester, mdh_analogin_t *pin_under_test)
{
    tester.select_peripheral(MbedTester::PeripheralGPIO);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);

    TEST_ASSERT_FLOAT_WITHIN(DELTA_FLOAT, 1.0f, mdh_analogin_read(pin_under_test));
    TEST_ASSERT_UINT16_WITHIN(DELTA_U16, 65535, mdh_analogin_read_u16(pin_under_test));

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    TEST_ASSERT_FLOAT_WITHIN(DELTA_FLOAT, 0.0f, mdh_analogin_read(pin_under_test));
    TEST_ASSERT_UINT16_WITHIN(DELTA_U16, 0, mdh_analogin_read_u16(pin_under_test));

    /* Set gpio back to Hi-Z */
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, false);
}
