/* Copyright (c) 2019-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal_gpio_tests */
/** @{*/

#ifndef MDH_FPGA_GPIO_TEST_H
#define MDH_FPGA_GPIO_TEST_H

#ifdef __cplusplus
extern "C" {
#endif

#include "hal/gpio_api.h"

#include "fpga_ci_test_shield/MbedTester.h"

#include <stdint.h>

/* Test basic input & output operations.
 *
 * Given a GPIO instance,
 * when basic input and output operations are performed,
 * then all operations succeed.
 */
void test_gpio_basic_input_output(MbedTester &tester, mdh_gpio_t *pin_under_test, void (*wait_us)(uint32_t us));

/* Test input pull modes.
 *
 * Given a GPIO instance configured with an input pull mode,
 * when basic input operations are performed,
 * then all operations succeed.
 *
 * A delay is used when reading a floating input that has an internal pull-up
 * or pull-down resistor. The voltage response is much slower when the input
 * is not driven externally.
 */
void test_gpio_input_pull_modes(MbedTester &tester, mdh_gpio_t *pin_under_test, void (*wait_us)(uint32_t us));

/**@}*/

#ifdef __cplusplus
}
#endif

#endif // MDH_FPGA_GPIO_TEST_H

/**@}*/
