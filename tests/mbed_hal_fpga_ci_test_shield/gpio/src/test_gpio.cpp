/* Copyright (c) 2019-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_gpio.h"

#include "unity.h"
#include "utest/utest_print.h"

extern "C" {
#include "hal/gpio_api.h"
#include "hal/us_ticker_api.h"
}

#include <stdint.h>

void test_gpio_basic_input_output(MbedTester &tester, mdh_gpio_t *pin_under_test, void (*wait_us)(uint32_t us))
{
    (void)wait_us;
    TEST_ASSERT_TRUE(mdh_gpio_is_connected(pin_under_test));

    // Test GPIO used as an input.
    mdh_gpio_set_direction(pin_under_test, MDH_GPIO_DIRECTION_IN);

    uint32_t values_to_read[] = {1U, 0U, 1U};
    for (uint8_t i = 0; i < sizeof(values_to_read) / sizeof(values_to_read[0]); i++) {
        tester.gpio_write(MbedTester::LogicalPinGPIO0, (int)(values_to_read[i]), true);
        TEST_ASSERT_EQUAL_UINT32(values_to_read[i], mdh_gpio_read(pin_under_test));
    }

    // Test GPIO used as an output.
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, false);
    mdh_gpio_set_direction(pin_under_test, MDH_GPIO_DIRECTION_OUT);

    uint32_t values_to_write[] = {0U, 1U, 0U};
    for (uint8_t i = 0; i < sizeof(values_to_write) / sizeof(values_to_write[0]); i++) {
        mdh_gpio_write(pin_under_test, values_to_write[i]);
        TEST_ASSERT_EQUAL_INT((int)values_to_write[i], tester.gpio_read(MbedTester::LogicalPinGPIO0));
    }
}

void test_gpio_input_pull_modes(MbedTester &tester, mdh_gpio_t *pin_under_test, void (*wait_us)(uint32_t us))
{
    TEST_ASSERT_TRUE(mdh_gpio_is_connected(pin_under_test));

    mdh_gpio_set_direction(pin_under_test, MDH_GPIO_DIRECTION_IN);
    mdh_gpio_capabilities_t capabilities = {};
    mdh_gpio_get_capabilities(pin_under_test, &capabilities);

    // Test input, pull-up mode.
    if (capabilities.pull_up) {
        mdh_gpio_set_mode(pin_under_test, MDH_GPIO_MODE_PULL_UP);

        tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, false);
        wait_us(5U);
        TEST_ASSERT_EQUAL_UINT32(1U, mdh_gpio_read(pin_under_test)); // hi-Z, pulled up

        uint32_t values_to_read[] = {0U, 1U, 0U};
        for (uint8_t i = 0; i < sizeof(values_to_read) / sizeof(values_to_read[0]); i++) {
            tester.gpio_write(MbedTester::LogicalPinGPIO0, (int)values_to_read[i], true);
            TEST_ASSERT_EQUAL_UINT32(values_to_read[i], mdh_gpio_read(pin_under_test));
        }

        tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, false);
        wait_us(5U);
        TEST_ASSERT_EQUAL_UINT32(1U, mdh_gpio_read(pin_under_test)); // hi-Z, pulled up
    } else {
        utest_printf("skipped MDH_GPIO_MODE_PULL_UP\n");
    }

    // Test input, pull-down mode.
    if (capabilities.pull_down) {
        mdh_gpio_set_mode(pin_under_test, MDH_GPIO_MODE_PULL_DOWN);

        tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, false);
        wait_us(5U);
        TEST_ASSERT_EQUAL_UINT32(0U, mdh_gpio_read(pin_under_test)); // hi-Z, pulled down

        uint32_t values_to_read[] = {1U, 0U, 1U};
        for (uint8_t i = 0; i < sizeof(values_to_read) / sizeof(values_to_read[0]); i++) {
            tester.gpio_write(MbedTester::LogicalPinGPIO0, (int)values_to_read[i], true);
            TEST_ASSERT_EQUAL_UINT32(values_to_read[i], mdh_gpio_read(pin_under_test));
        }

        tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, false);
        wait_us(5U);
        TEST_ASSERT_EQUAL_UINT32(0U, mdh_gpio_read(pin_under_test)); // hi-Z, pulled down
    } else {
        utest_printf("skipped MDH_GPIO_MODE_PULL_DOWN\n");
    }

    // Test input, pull-none mode.
    if (capabilities.pull_none) {
        mdh_gpio_set_mode(pin_under_test, MDH_GPIO_MODE_PULL_NONE);

        uint32_t values_to_read[] = {1U, 0U, 1U};
        for (uint8_t i = 0; i < sizeof(values_to_read) / sizeof(values_to_read[0]); i++) {
            tester.gpio_write(MbedTester::LogicalPinGPIO0, (int)values_to_read[i], true);
            TEST_ASSERT_EQUAL_UINT32(values_to_read[i], mdh_gpio_read(pin_under_test));
        }
    } else {
        utest_printf("skipped MDH_GPIO_MODE_PULL_NONE\n");
    }
}
