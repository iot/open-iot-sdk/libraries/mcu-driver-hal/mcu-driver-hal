/* Copyright (c) 2019-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal_gpioirq_tests */
/** @{*/

#ifndef MDH_FPGA_GPIO_IRQ_TEST_H
#define MDH_FPGA_GPIO_IRQ_TEST_H

#include "hal/gpio_api.h"

#include "fpga_ci_test_shield/MbedTester.h"

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Test that the GPIO IRQ can be initialized/de-initialized using all possible
 *  GPIO IRQ pins.
 *
 * Given board provides GPIO IRQ support.
 * When GPIO IRQ is initialized (and then de-initialized) using valid GPIO IRQ pin.
 * Then the operation is successfull.
 *
 */
void test_gpio_irq(MbedTester &tester, mdh_gpio_t *gpio, void (*wait_us)(uint32_t us));

/**@}*/

#ifdef __cplusplus
}
#endif

#endif // MDH_FPGA_GPIO_IRQ_TEST_H
/**@}*/
