/* Copyright (c) 2019-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_gpio_irq.h"

#include "unity.h"

extern "C" {
#include "hal/gpio_api.h"
}

#include <stdint.h>

#define US_DELAY 10U

static void irq_handler(void *context, mdh_gpio_irq_event_t event);

static volatile uint32_t call_counter;

void test_gpio_irq(MbedTester &tester, mdh_gpio_t *gpio, void (*wait_us)(uint32_t us))
{
    mdh_gpio_set_irq_callback(gpio, irq_handler, nullptr);

    mdh_gpio_set_irq_availability(gpio, MDH_GPIO_IRQ_EVENT_RISE, true);

    mdh_gpio_enable_irq(gpio);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);

    // test irq on rising edge
    call_counter = 0;
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);

    mdh_gpio_disable_irq(gpio);

    call_counter = 0;
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    mdh_gpio_enable_irq(gpio);

    call_counter = 0;
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);

    // test irq on both rising and falling edge
    mdh_gpio_set_irq_availability(gpio, MDH_GPIO_IRQ_EVENT_FALL, true);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);

    call_counter = 0;
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(3, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(4, call_counter);

    mdh_gpio_disable_irq(gpio);

    call_counter = 0;
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    mdh_gpio_enable_irq(gpio);

    call_counter = 0;
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(3, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(4, call_counter);

    // test irq on falling edge
    mdh_gpio_set_irq_availability(gpio, MDH_GPIO_IRQ_EVENT_RISE, false);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);

    call_counter = 0;
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);

    mdh_gpio_disable_irq(gpio);

    call_counter = 0;
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(0, call_counter);

    mdh_gpio_enable_irq(gpio);

    call_counter = 0;
    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(1, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 0, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);

    tester.gpio_write(MbedTester::LogicalPinGPIO0, 1, true);
    wait_us(US_DELAY);
    TEST_ASSERT_EQUAL(2, call_counter);
}

static void irq_handler(void *context, mdh_gpio_irq_event_t event)
{
    call_counter++;
}
