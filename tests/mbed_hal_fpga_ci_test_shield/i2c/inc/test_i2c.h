/* Copyright (c) 2019-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal_GeneralI2C_tests */
/** @{*/

#ifndef MDH_FPGA_I2C_TEST_H
#define MDH_FPGA_I2C_TEST_H

#include "hal/i2c_api.h"

#include "fpga_ci_test_shield/I2CTester.h"

/** Test that I2C controller is able to read data from I2C bus using i2c_byte_read.
 *
 * Given board provides I2C controller support.
 * When I2C controller reads data from I2C bus using i2c_byte_read.
 * Then data is successfully read.
 *
 */
void test_i2c_read_byte(I2CTester &tester, mdh_i2c_t *i2c);

/** Test that I2C controller is able to write data to I2C bus using mdh_i2c_write.
 *
 * Given board provides I2C controller support.
 * When I2C controller writes data to the I2C bus using mdh_i2c_write.
 * Then data is successfully transmitted.
 *
 */
void test_i2c_write_byte(I2CTester &tester, mdh_i2c_t *i2c);

/** Test that I2C controller is able to read data from I2C bus using mdh_i2c_read.
 *
 * Given board provides I2C controller support.
 * When I2C controller reads data from I2C bus using mdh_i2c_read.
 * Then data is successfully read.
 *
 */
void test_i2c_read(I2CTester &tester, mdh_i2c_t *i2c);

/** Test that I2C controller is able to write data to I2C bus using i2c_write.
 *
 * Given board provides I2C controller support.
 * When I2C controller writes data to the I2C bus using i2c_write.
 * Then data is successfully transmitted.
 *
 */
void test_i2c_write(I2CTester &tester, mdh_i2c_t *i2c);

/**@}*/

#endif // MDH_FPGA_I2C_TEST_H

/**@}*/
