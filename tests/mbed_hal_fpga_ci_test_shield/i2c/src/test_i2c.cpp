/* Copyright (c) 2019-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define MBED_CONF_MBED_TRACE_ENABLE 1
#define TRACE_GROUP                 "I2CT"

#include "test_i2c.h"

#include "unity.h"

extern "C" {
#include "hal/i2c_api.h"
#include "hal/us_ticker_api.h"

#include "mbed_trace/mbed_trace.h"
}

#define NACK         0
#define ACK          1
#define TIMEOUT      2
#define I2C_DEV_ADDR 0x98 // default i2c peripheral address on FPGA is 0x98 until modified
const int TRANSFER_COUNT = 300;

void test_i2c_write(I2CTester &tester, mdh_i2c_t *i2c)
{
    tester.peripherals_reset();
    tester.select_peripheral(I2CTester::PeripheralI2C);

    mdh_i2c_set_frequency(i2c, 100000UL);

    uint8_t buffer[TRANSFER_COUNT];
    for (unsigned int i = 0; i < TRANSFER_COUNT; i++) {
        buffer[i] = i & 0xFF;
    }

    // Write data for I2C complete transaction
    // Will write 0-(TRANSFER_COUNT-1) to FPGA, checksum must match checksum calculated in parallel on FPGA
    int num_writes = mdh_i2c_write(i2c, I2C_DEV_ADDR, buffer, TRANSFER_COUNT, true);

    uint32_t checksum = 0;
    for (unsigned int i = 0; i < TRANSFER_COUNT; i++) {
        checksum += buffer[i];
    }

    TEST_ASSERT_EQUAL(1U, tester.num_dev_addr_matches());
    TEST_ASSERT_EQUAL(TRANSFER_COUNT, num_writes);
    TEST_ASSERT_EQUAL(num_writes + 1, tester.transfer_count());
    TEST_ASSERT_EQUAL(1U, tester.num_starts());
    TEST_ASSERT_EQUAL(1U, tester.num_stops());
    TEST_ASSERT_EQUAL(num_writes + 1U, tester.num_acks());
    TEST_ASSERT_EQUAL(0U, tester.num_nacks());
    TEST_ASSERT_EQUAL(checksum, tester.get_receive_checksum());
    TEST_ASSERT_EQUAL(0U, tester.state_num());
    TEST_ASSERT_EQUAL(buffer[TRANSFER_COUNT - 4], tester.get_prev_to_peripheral_4());
    TEST_ASSERT_EQUAL(buffer[TRANSFER_COUNT - 3], tester.get_prev_to_peripheral_3());
    TEST_ASSERT_EQUAL(buffer[TRANSFER_COUNT - 2], tester.get_prev_to_peripheral_2());
    TEST_ASSERT_EQUAL(buffer[TRANSFER_COUNT - 1], tester.get_prev_to_peripheral_1());
    TEST_ASSERT_EQUAL(num_writes, tester.num_writes());
    TEST_ASSERT_EQUAL(0U, tester.num_reads());

    mdh_i2c_reset(i2c);
}

void test_i2c_read(I2CTester &tester, mdh_i2c_t *i2c)
{
    tester.peripherals_reset();
    tester.select_peripheral(I2CTester::PeripheralI2C);

    mdh_i2c_set_frequency(i2c, 100000UL);

    uint8_t buffer[TRANSFER_COUNT];
    for (int i = 0; i < TRANSFER_COUNT; i++) {
        buffer[i] = 0;
    }

    // Read data for I2C complete transaction
    // Will read <TRANSFER_COUNT> bytes, checksum must match checksum calculated in parallel on FPGA
    int num_reads = mdh_i2c_read(i2c, (I2C_DEV_ADDR | 1), buffer, TRANSFER_COUNT, true);

    uint32_t checksum = 0;
    for (unsigned int i = 0; i < TRANSFER_COUNT; i++) {
        checksum += buffer[i];
    }

    TEST_ASSERT_EQUAL(1U, tester.num_dev_addr_matches());
    TEST_ASSERT_EQUAL(TRANSFER_COUNT, num_reads);
    TEST_ASSERT_EQUAL(num_reads + 1, tester.transfer_count());
    TEST_ASSERT_EQUAL(1U, tester.num_starts());
    TEST_ASSERT_EQUAL(1U, tester.num_stops());
    TEST_ASSERT_EQUAL(TRANSFER_COUNT, tester.num_acks());
    TEST_ASSERT_EQUAL(1U, tester.num_nacks());
    TEST_ASSERT_EQUAL(checksum, tester.get_send_checksum());
    TEST_ASSERT_EQUAL(0, tester.state_num());
    TEST_ASSERT_EQUAL(((TRANSFER_COUNT + 1) & 0xFF), tester.get_next_from_peripheral());
    TEST_ASSERT_EQUAL(0U, tester.num_writes());
    TEST_ASSERT_EQUAL(num_reads, tester.num_reads());

    mdh_i2c_reset(i2c);
}

void test_i2c_write_byte(I2CTester &tester, mdh_i2c_t *i2c)
{
    tester.peripherals_reset();
    tester.select_peripheral(I2CTester::PeripheralI2C);

    mdh_i2c_set_frequency(i2c, 100000UL);

    uint8_t buffer[TRANSFER_COUNT];
    for (unsigned int i = 0; i < TRANSFER_COUNT; i++) {
        buffer[i] = i & 0xFF;
    }

    // Write data for I2C single byte transfers
    // Will write 0-(TRANSFER_COUNT-1) to FPGA, checksum must match checksum calculated in parallel on FPGA
    mdh_i2c_send_start(i2c);
    mdh_i2c_write_byte(i2c, I2C_DEV_ADDR);

    unsigned int num_acks = 1;
    unsigned int num_writes = 0;
    unsigned int num_nacks = 0;
    uint32_t checksum = 0;
    for (int i = 0; i < TRANSFER_COUNT; i++) {
        int ack_nack = mdh_i2c_write_byte(i2c, buffer[i]);
        if (ack_nack == ACK) {
            num_acks += 1;
        } else if (ack_nack == NACK) {
            num_nacks += 1;
        } else {
            tr_info("Timeout error\n\r");
        }
        checksum += buffer[i];
        num_writes += 1;
    }

    mdh_i2c_send_stop(i2c);

    TEST_ASSERT_EQUAL(1U, tester.num_dev_addr_matches());
    TEST_ASSERT_EQUAL(TRANSFER_COUNT, num_writes);
    TEST_ASSERT_EQUAL(num_writes + 1, tester.transfer_count());
    TEST_ASSERT_EQUAL(1U, tester.num_starts());
    TEST_ASSERT_EQUAL(1U, tester.num_stops());
    TEST_ASSERT_EQUAL(num_acks, tester.num_acks());
    TEST_ASSERT_EQUAL(num_nacks, tester.num_nacks());
    TEST_ASSERT_EQUAL(checksum, tester.get_receive_checksum());
    TEST_ASSERT_EQUAL(0U, tester.state_num());
    TEST_ASSERT_EQUAL(buffer[TRANSFER_COUNT - 4], tester.get_prev_to_peripheral_4());
    TEST_ASSERT_EQUAL(buffer[TRANSFER_COUNT - 3], tester.get_prev_to_peripheral_3());
    TEST_ASSERT_EQUAL(buffer[TRANSFER_COUNT - 2], tester.get_prev_to_peripheral_2());
    TEST_ASSERT_EQUAL(buffer[TRANSFER_COUNT - 1], tester.get_prev_to_peripheral_1());
    TEST_ASSERT_EQUAL(num_writes, tester.num_writes());
    TEST_ASSERT_EQUAL(0U, tester.num_reads());

    mdh_i2c_reset(i2c);
}

void test_i2c_read_byte(I2CTester &tester, mdh_i2c_t *i2c)
{
    tester.peripherals_reset();
    tester.select_peripheral(I2CTester::PeripheralI2C);

    mdh_i2c_set_frequency(i2c, 100000UL);

    tester.set_next_from_peripheral(0);
    uint8_t buffer[TRANSFER_COUNT] = {0};

    // Read data for I2C single byte transfers
    // Will read <TRANSFER_COUNT> bytes, checksum must match checksum calculated in parallel on FPGA
    mdh_i2c_send_start(i2c);
    mdh_i2c_write_byte(i2c, (I2C_DEV_ADDR | 1));

    unsigned int num_reads = 0;
    unsigned int num_acks = 1;
    unsigned int num_nacks = 0;
    uint32_t checksum = 0;
    // The value read from the tester I2C peripheral increments from the
    // starting value set with MbedTester::set_next_from_peripheral() and wraps
    // around `0xFF`.
    for (size_t i = 0; i < TRANSFER_COUNT; i++) {
        if (num_reads == (TRANSFER_COUNT - 1)) {
            // Send NAK
            buffer[i] = mdh_i2c_read_byte(i2c, 1U);
            checksum += buffer[i];
            num_reads += 1;
            num_nacks += 1;
        } else {
            // Send ACK
            buffer[i] = mdh_i2c_read_byte(i2c, 0U);
            checksum += buffer[i];
            num_reads += 1;
            num_acks += 1;
        }
    }

    mdh_i2c_send_stop(i2c);

    TEST_ASSERT_EQUAL(1U, tester.num_dev_addr_matches());
    TEST_ASSERT_EQUAL(TRANSFER_COUNT, num_reads);
    TEST_ASSERT_EQUAL(num_reads + 1, tester.transfer_count());
    TEST_ASSERT_EQUAL(1U, tester.num_starts());
    TEST_ASSERT_EQUAL(1U, tester.num_stops());
    TEST_ASSERT_EQUAL(num_acks, tester.num_acks());
    TEST_ASSERT_EQUAL(num_nacks, tester.num_nacks());
    TEST_ASSERT_EQUAL(checksum, tester.get_send_checksum());
    TEST_ASSERT_EQUAL(0, tester.state_num());
    TEST_ASSERT_EQUAL(((TRANSFER_COUNT)&0xFF), tester.get_next_from_peripheral());
    TEST_ASSERT_EQUAL(0U, tester.num_writes());
    TEST_ASSERT_EQUAL(num_reads, tester.num_reads());

    mdh_i2c_reset(i2c);
}
