/* Copyright (c) 2019-2022 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal_pwmout_tests */
/** @{*/

#ifndef MDH_FPGA_PWM_TEST_H
#define MDH_FPGA_PWM_TEST_H

#ifdef __cplusplus
extern "C" {
#endif

#include "hal/pwmout_api.h"

#include "fpga_ci_test_shield/MbedTester.h"

typedef enum {
    PERIOD_WRITE,
    PERIOD_MS_WRITE,
    PERIOD_US_WRITE,
    PERIOD_PULSEWIDTH,
    PERIOD_PULSEWIDTH_MS,
    PERIOD_PULSEWIDTH_US
} pwm_api_test_t;

/** Test that pwmout_period, pwmout_period_ms, pwmout_period_us functions sets the
 * PWM period correctly and pwmout_write, pwmout_pulsewidth, pwmout_pulsewidth_ms,
 * pwmout_pulsewidth_us functions sets the pulse width correctly.
 *
 * Given board provides PWM support.
 * When PWM period/width is set using pwmout_period, pwmout_period_ms,
 * pwmout_period_us/pwmout_write, pwmout_pulsewidth, pwmout_pulsewidth_ms,
 * pwmout_pulsewidth_us.
 * Then the valid PWM puswidth and period is on output.
 *
 */
void test_pwm_period_fill(
    MbedTester &tester, mdh_pwmout_t *pin_under_test, uint32_t period_ms, uint32_t fill_prc, pwm_api_test_t api_test);

#ifdef __cplusplus
}
#endif

#endif // MDH_FPGA_PWM_TEST_H

/**@}*/
