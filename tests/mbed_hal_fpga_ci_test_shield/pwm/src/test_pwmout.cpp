/* Copyright (c) 2019-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_pwmout.h"

#include "mbed_critical/mbed_critical.h"
#include "unity.h"

extern "C" {
#include "hal/us_ticker_api.h"
}

#define pwm_debug_printf(...)

#define NUM_OF_PERIODS 10
#define US_PER_SEC     1000000
#define US_PER_MS      1000
#define MS_PER_SEC     1000

#define DELTA_FACTOR 20 // 5% delta

#define PERIOD_US(PERIOD_MS)          (((PERIOD_MS)*US_PER_MS))
#define PERIOD_FLOAT(PERIOD_MS)       (((float)(PERIOD_MS) / US_PER_MS))
#define FILL_FLOAT(PRC)               ((float)(PRC) / 100)
#define PULSE_HIGH_US(PERIOD_US, PRC) ((uint32_t)((PERIOD_US)*FILL_FLOAT(PRC)))
#define PULSE_LOW_US(PERIOD_US, PRC)  ((uint32_t)((PERIOD_US) * (1.0f - FILL_FLOAT(PRC))))

void test_pwm_period_fill(
    MbedTester &tester, mdh_pwmout_t *pin_under_test, uint32_t period_ms, uint32_t fill_prc, pwm_api_test_t api_test)
{
    pwm_debug_printf("Testing period = %lu ms, duty-cycle = %lu %%\r\n", period_ms, fill_prc);
    pwm_debug_printf("Testing APIs = %d\r\n", (int)api_test);

    MbedTester::LogicalPin logical_pin = (MbedTester::LogicalPin)(MbedTester::LogicalPinIOMetrics0);

    core_util_critical_section_enter();

    switch (api_test) {
        case PERIOD_WRITE:
            mdh_pwmout_set_period(pin_under_test, PERIOD_FLOAT(period_ms));
            mdh_pwmout_write(pin_under_test, FILL_FLOAT(fill_prc));
            break;

        case PERIOD_MS_WRITE:
            mdh_pwmout_set_period_ms(pin_under_test, (int)period_ms);
            mdh_pwmout_write(pin_under_test, FILL_FLOAT(fill_prc));
            break;

        case PERIOD_US_WRITE:
            mdh_pwmout_set_period_us(pin_under_test, PERIOD_US(period_ms));
            mdh_pwmout_write(pin_under_test, FILL_FLOAT(fill_prc));
            break;

        case PERIOD_PULSEWIDTH:
            mdh_pwmout_set_period(pin_under_test, PERIOD_FLOAT(period_ms));
            mdh_pwmout_set_pulsewidth(pin_under_test,
                                      (float)PULSE_HIGH_US(PERIOD_US(period_ms), fill_prc) / US_PER_SEC);
            break;

        case PERIOD_PULSEWIDTH_MS:
            mdh_pwmout_set_period(pin_under_test, PERIOD_FLOAT(period_ms));
            mdh_pwmout_set_pulsewidth_ms(pin_under_test,
                                         (int)PULSE_HIGH_US(PERIOD_US(period_ms), fill_prc) / MS_PER_SEC);
            break;

        case PERIOD_PULSEWIDTH_US:
            mdh_pwmout_set_period(pin_under_test, PERIOD_FLOAT(period_ms));
            mdh_pwmout_set_pulsewidth_us(pin_under_test, (int)PULSE_HIGH_US(PERIOD_US(period_ms), fill_prc));
            break;
    }

    us_ticker_util_wait(PERIOD_US(period_ms));
    tester.io_metrics_start();
    us_ticker_util_wait(NUM_OF_PERIODS * PERIOD_US(period_ms));
    tester.io_metrics_stop();
    core_util_critical_section_exit();

    const uint32_t expected_low_pulse_us = PULSE_LOW_US(PERIOD_US(period_ms), fill_prc);
    const uint32_t expected_high_pulse_us = PULSE_HIGH_US(PERIOD_US(period_ms), fill_prc);
    const uint32_t delta_low_pulse = (expected_low_pulse_us / DELTA_FACTOR);
    const uint32_t delta_high_pulse = (expected_high_pulse_us / DELTA_FACTOR);

    pwm_debug_printf("Minimum pulse low %lu us\r\n", tester.io_metrics_min_pulse_low(logical_pin) / 100);
    pwm_debug_printf("Minimum pulse high %lu us\r\n", tester.io_metrics_min_pulse_high(logical_pin) / 100);
    pwm_debug_printf("Maximum pulse low %lu us\r\n", tester.io_metrics_max_pulse_low(logical_pin) / 100);
    pwm_debug_printf("Maximum pulse high %lu us\r\n", tester.io_metrics_max_pulse_high(logical_pin) / 100);
    pwm_debug_printf("Rising edges %lu\r\n", tester.io_metrics_rising_edges(logical_pin));
    pwm_debug_printf("Falling edges %lu\r\n", tester.io_metrics_falling_edges(logical_pin));

    TEST_ASSERT_FLOAT_WITHIN(
        FILL_FLOAT(fill_prc) / DELTA_FACTOR, FILL_FLOAT(fill_prc), mdh_pwmout_read(pin_under_test));

    TEST_ASSERT_UINT32_WITHIN(
        delta_low_pulse, expected_low_pulse_us, tester.io_metrics_min_pulse_low(logical_pin) / 100);
    TEST_ASSERT_UINT32_WITHIN(
        delta_low_pulse, expected_low_pulse_us, tester.io_metrics_max_pulse_low(logical_pin) / 100);
    TEST_ASSERT_UINT32_WITHIN(
        delta_high_pulse, expected_high_pulse_us, tester.io_metrics_min_pulse_high(logical_pin) / 100);
    TEST_ASSERT_UINT32_WITHIN(
        delta_high_pulse, expected_high_pulse_us, tester.io_metrics_max_pulse_high(logical_pin) / 100);

    TEST_ASSERT_UINT32_WITHIN(1, NUM_OF_PERIODS, tester.io_metrics_rising_edges(logical_pin));
    TEST_ASSERT_UINT32_WITHIN(1, NUM_OF_PERIODS, tester.io_metrics_falling_edges(logical_pin));
}
