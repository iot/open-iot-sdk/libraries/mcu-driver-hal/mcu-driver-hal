/* Copyright (c) 2019-2023, Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MDH_HAL_TEST_SERIAL_H
#define MDH_HAL_TEST_SERIAL_H

#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"

#include "fpga_ci_test_shield/UARTTester.h"

/** Tests various frame transfers on a given serial interface.
 *
 * Call the function with variations of its parameters to test various serial
 * peripherals and configurations
 *
 * The following transfers are tested:
 * - Transmission
 * - Reception
 * - Asynchronous transmission
 * - Asynchronous reception
 *
 * * @param tester The tester object to exercise the DUT with
 * * @param serial_device The serial interface under test
 * * @param baudrate The baudrate
 * * @param data_bits The number of data bits for each serial frame
 * * @param parity  The parity
 * * @param stop_bits The number of stop bits
 * * @param use_flow_control `true` if the flow control is used, `false` otherwise
 */
void test_serial_transfers(UARTTester &tester,
                           mdh_serial_t *serial_device,
                           uint32_t baudrate,
                           uint8_t data_bits,
                           mdh_serial_parity_t parity,
                           uint8_t stop_bits,
                           bool use_flow_control,
                           mdh_ticker_t *us_ticker);

#endif // MDH_HAL_TEST_SERIAL_H
