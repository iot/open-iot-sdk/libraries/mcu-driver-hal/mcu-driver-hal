/* Copyright (c) 2019-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_serial.h"

#include "mbed_atomic/mbed_atomic.h"
#include "mbed_critical/mbed_critical.h"
#include "unity.h"

#include <cassert>
#include <stdlib.h>

extern "C" {
#include "hal/serial_api.h"
#include "hal/us_ticker_api.h"

#include "hal-toolbox/us_ticker_util.h"
}

#define BUFFER_SIZE 16U

// In the UART RX test, the request for the FPGA to start sending data is sent
// first. Then the execution is blocked at mdh_serial_get_data() call. Since the DUT
// is not ready to receive UART data instantly after the request, the start of
// the actual transmission has to be delayed.
// A measured delay for NUCLEO_F070RB is 193 us.
// A measured delay for ARM_AN552_MPS3 is 600 us.
#define TX_START_DELAY_NS 800000UL

typedef struct {
    mdh_serial_t *serial_device;
    struct {
        uint32_t *data;
        size_t num_elements;
    } rx_buff;
    uint32_t rx_cnt;
    struct {
        uint32_t *data;
        size_t num_elements;
    } tx_buff;
    uint32_t tx_cnt;
    uint32_t tx_checksum;
    uint32_t verification_delay_us;
    bool use_flow_control;
} test_data_t;

static void test_irq_handler(void *context, mdh_serial_irq_type_t type)
{
    test_data_t *test_data = static_cast<test_data_t *>(context);
    uint32_t data = 0xFFFFFFFFUL; // arbitrary, non-zero value
    if (type == MDH_SERIAL_IRQ_TYPE_RX) {
        data = mdh_serial_get_data(test_data->serial_device);
        core_util_critical_section_enter();
        if (test_data->rx_cnt < test_data->rx_buff.num_elements) {
            test_data->rx_buff.data[test_data->rx_cnt++] = data;
        }
        core_util_critical_section_exit();
    } else if (type == MDH_SERIAL_IRQ_TYPE_TX) {
        core_util_critical_section_enter();
        if (test_data->tx_cnt < test_data->tx_buff.num_elements) {
            data = test_data->tx_buff.data[test_data->tx_cnt++];
            test_data->tx_checksum += data;
        }
        core_util_critical_section_exit();
        mdh_serial_put_data(test_data->serial_device, data);
    }
}

static void configure_fpga_ci_test_shield_serial(UARTTester &tester,
                                                 uint32_t baudrate,
                                                 uint8_t data_bits,
                                                 mdh_serial_parity_t parity,
                                                 uint8_t stop_bits,
                                                 bool use_flow_control)
{
    tester.peripherals_reset();
    tester.select_peripheral(MbedTester::PeripheralUART);

    tester.set_baud(baudrate);
    tester.set_bits(data_bits);
    tester.set_stops(stop_bits);

    switch (parity) {
        case MDH_SERIAL_PARITY_ODD:
            tester.set_parity(true, true);
            break;
        case MDH_SERIAL_PARITY_EVEN:
            tester.set_parity(true, false);
            break;
        case MDH_SERIAL_PARITY_NONE:
        default:
            tester.set_parity(false, false);
            break;
    }

    if (use_flow_control) {
        tester.cts_deassert_delay(0);
    }
}

static void test_transmit(UARTTester &tester, volatile test_data_t *test_data, mdh_ticker_t *us_ticker)
{
    uint32_t initial_tester_rx_cnt = tester.rx_get_count();
    tester.rx_start();
    for (size_t i = 0; i < test_data->tx_buff.num_elements; i++) {
        uint32_t tx_val = test_data->tx_buff.data[i];
        mdh_serial_put_data(test_data->serial_device, tx_val);
        test_data->tx_cnt += 1UL;
        test_data->tx_checksum += tx_val;
    }
    us_ticker_util_wait(us_ticker, test_data->verification_delay_us);
    tester.rx_stop();

    TEST_ASSERT_EQUAL_UINT32(initial_tester_rx_cnt + test_data->tx_buff.num_elements, tester.rx_get_count());
    TEST_ASSERT_EQUAL_UINT32(test_data->tx_checksum, tester.rx_get_checksum());
    TEST_ASSERT_EQUAL(0U, tester.rx_get_parity_errors());
    TEST_ASSERT_EQUAL(0U, tester.rx_get_stop_errors());
    TEST_ASSERT_EQUAL(0U, tester.rx_get_framing_errors());
    TEST_ASSERT_EQUAL(test_data->tx_buff.data[test_data->tx_buff.num_elements - 1U], tester.rx_get_data());
}

static void test_asynchronous_transmit(UARTTester &tester, volatile test_data_t *test_data, mdh_ticker_t *us_ticker)
{
    uint32_t initial_tester_rx_cnt = tester.rx_get_count();
    test_data->tx_cnt = 0UL;
    tester.rx_start();
    core_util_critical_section_enter();
    mdh_serial_set_irq_availability(test_data->serial_device, MDH_SERIAL_IRQ_TYPE_TX, true);
    core_util_critical_section_exit();
    // Wait until the last byte is written to UART TX reg.
    while (core_util_atomic_load_u32(&(test_data->tx_cnt)) < test_data->tx_buff.num_elements) {
    };
    core_util_critical_section_enter();
    mdh_serial_set_irq_availability(test_data->serial_device, MDH_SERIAL_IRQ_TYPE_TX, false);
    core_util_critical_section_exit();
    us_ticker_util_wait(us_ticker, test_data->verification_delay_us);
    tester.rx_stop();

    TEST_ASSERT_EQUAL_UINT32(initial_tester_rx_cnt + test_data->tx_buff.num_elements, tester.rx_get_count());
    TEST_ASSERT_EQUAL_UINT32(test_data->tx_checksum, tester.rx_get_checksum());
    TEST_ASSERT_EQUAL(0, tester.rx_get_parity_errors());
    TEST_ASSERT_EQUAL(0, tester.rx_get_stop_errors());
    TEST_ASSERT_EQUAL(0, tester.rx_get_framing_errors());
    TEST_ASSERT_EQUAL(test_data->tx_buff.data[test_data->tx_buff.num_elements - 1U], tester.rx_get_data());
}

static void test_receive(UARTTester &tester, volatile test_data_t *test_data)
{
    uint16_t initial_tester_tx_data = 0;
    tester.tx_set_next(initial_tester_tx_data);
    tester.tx_set_count(test_data->rx_buff.num_elements);
    if (!test_data->use_flow_control) {
        tester.tx_set_delay(TX_START_DELAY_NS);
    }
    tester.tx_start(test_data->use_flow_control);
    for (size_t i = 0; i < test_data->rx_buff.num_elements; i++) {
        test_data->rx_buff.data[i] = mdh_serial_get_data(test_data->serial_device);
        test_data->rx_cnt += 1UL;
    }
    tester.tx_stop();

    for (size_t i = 0; i < test_data->rx_buff.num_elements; i++) {
        TEST_ASSERT_EQUAL_UINT32(initial_tester_tx_data + i, test_data->rx_buff.data[i]);
    }
}

static void test_asynchronous_receive(UARTTester &tester, volatile test_data_t *test_data)
{
    uint32_t initial_tester_rx_cnt = tester.rx_get_count();
    uint16_t initial_tester_tx_data = 0;
    tester.tx_set_next(initial_tester_tx_data);
    tester.tx_set_count(test_data->rx_buff.num_elements);
    if (!test_data->use_flow_control) {
        tester.tx_set_delay(TX_START_DELAY_NS);
    }
    core_util_critical_section_enter();
    test_data->rx_cnt = 0UL;
    test_data->tx_cnt = test_data->tx_buff.num_elements;
    mdh_serial_set_irq_availability(test_data->serial_device, MDH_SERIAL_IRQ_TYPE_RX, true);
    core_util_critical_section_exit();
    tester.rx_start();
    tester.tx_start(test_data->use_flow_control);
    // Wait until the last byte is received to UART RX reg.
    while (core_util_atomic_load_u32(&(test_data->rx_cnt)) < test_data->rx_buff.num_elements) {
    };
    core_util_critical_section_enter();
    mdh_serial_set_irq_availability(test_data->serial_device, MDH_SERIAL_IRQ_TYPE_RX, false);
    core_util_critical_section_exit();
    tester.tx_stop();
    tester.rx_stop();

    for (size_t i = 0; i < test_data->rx_buff.num_elements; i++) {
        TEST_ASSERT_EQUAL_UINT32(initial_tester_tx_data + i, test_data->rx_buff.data[i]);
    }
    // Make sure TX IRQ was disabled during this test.
    TEST_ASSERT_EQUAL_UINT32(initial_tester_rx_cnt, tester.rx_get_count());
    TEST_ASSERT_EQUAL_UINT32(test_data->tx_checksum, tester.rx_get_checksum());
}

void test_serial_transfers(UARTTester &tester,
                           mdh_serial_t *serial_device,
                           uint32_t baudrate,
                           uint8_t data_bits,
                           mdh_serial_parity_t parity,
                           uint8_t stop_bits,
                           bool use_flow_control,
                           mdh_ticker_t *us_ticker)
{
    // The FPGA CI shield only supports None, Odd & Even.
    // Forced parity is not supported on many targets
    assert(parity != MDH_SERIAL_PARITY_FORCED_1 && parity != MDH_SERIAL_PARITY_FORCED_0);
    configure_fpga_ci_test_shield_serial(tester, baudrate, data_bits, parity, stop_bits, use_flow_control);
    mdh_serial_set_baud(serial_device, baudrate);
    mdh_serial_set_format(serial_device, data_bits, parity, stop_bits);

    // frame_len = start_bit + data_bits + parity_bit + stop_bits
    const uint32_t num_frame_bits = 1 + data_bits + stop_bits + (parity == MDH_SERIAL_PARITY_NONE ? 0 : 1);
    const uint32_t frame_transmission_time_us = (uint32_t)(1000000ULL * (uint64_t)num_frame_bits / (uint64_t)baudrate);
    uint32_t buffer_1[BUFFER_SIZE] = {0};
    uint32_t buffer_2[BUFFER_SIZE] = {0};
    volatile test_data_t test_data = {.serial_device = serial_device,
                                      .rx_buff = {.data = buffer_1, .num_elements = BUFFER_SIZE},
                                      .rx_cnt = 0UL,
                                      .tx_buff = {.data = buffer_2, .num_elements = BUFFER_SIZE},
                                      .tx_cnt = 0UL,
                                      .tx_checksum = 0UL,
                                      .verification_delay_us = frame_transmission_time_us * 2,
                                      .use_flow_control = use_flow_control};
    for (size_t i = 0; i < test_data.tx_buff.num_elements; i++) {
        test_data.tx_buff.data[i] = i;
    }
    mdh_serial_set_irq_callback(serial_device, test_irq_handler, (void *)&test_data);

    test_transmit(tester, &test_data, us_ticker);
    test_receive(tester, &test_data);
    test_asynchronous_transmit(tester, &test_data, us_ticker);
    test_asynchronous_receive(tester, &test_data);
}
