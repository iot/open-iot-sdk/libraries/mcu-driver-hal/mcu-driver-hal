/* Copyright (c) 2019-2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** \addtogroup hal_GeneralSPI_tests */
/** @{*/

#ifndef MDH_FPGA_SPI_TEST_H
#define MDH_FPGA_SPI_TEST_H

#include "hal/gpio_api.h"
#include "hal/spi_api.h"

#include "fpga_ci_test_shield/SPIControllerTester.h"

typedef enum mdh_test_spi_frequency_e {
    MDH_TEST_SPI_FREQ_200_KHZ = 0,
    MDH_TEST_SPI_FREQ_500_KHZ,
    MDH_TEST_SPI_FREQ_1_MHZ,
    MDH_TEST_SPI_FREQ_2_MHZ,
    MDH_TEST_SPI_FREQ_10_MHZ,
    MDH_TEST_SPI_FREQ_MIN,
    MDH_TEST_SPI_FREQ_MAX
} mdh_test_spi_frequency_t;

typedef enum mdh_test_spi_buffer_sizes_e {
    MDH_TEST_SPI_BUF_SIZES_EQUAL = 0,
    MDH_TEST_SPI_BUF_SIZES_WRITE_GT_READ,
    MDH_TEST_SPI_BUF_SIZES_WRITE_LT_READ,
    MDH_TEST_SPI_BUF_SIZES_WRITE_ONE_WORD, // One word in both directions
} mdh_test_spi_buffer_sizes_t;

typedef enum mdh_test_spi_transfer_type_e {
    MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE = 0,
    MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_BLOCK_WRITE,
    MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_ASYNC
} mdh_test_spi_transfer_type_t;

typedef struct mdh_test_spi_configuration_e {
    mdh_spi_mode_t mode;
    uint32_t word_size;
    mdh_test_spi_transfer_type_t transfer_type;
    mdh_test_spi_frequency_t frequency;
    mdh_test_spi_buffer_sizes_t buffer_sizes;
    bool use_hardware_chip_select;
} mdh_test_spi_configuration_t;

/** Test that the SPI-Controller transfer can be performed in various configurations
 *
 * Given board provides SPI-Controller support.
 * When SPI transmission is performed using different settings.
 * Then data is successfully transferred.
 *
 */
void test_spi_transfer(SPIControllerTester &tester,
                       mdh_gpio_t *chip_select_gpio,
                       mdh_spi_t *spi_device,
                       mdh_test_spi_configuration_t &test_configuration);

/**@}*/

#endif // MDH_FPGA_SPI_TEST_H

/**@}*/
