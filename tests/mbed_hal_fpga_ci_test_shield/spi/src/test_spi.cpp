/* Copyright (c) 2019-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "test_spi.h"

#include "unity.h"
#include "utest/utest_print.h"

extern "C" {
#include "hal/gpio_api.h"
#include "hal/spi_api.h"

#include "mbed_trace/mbed_trace.h"
}

#define MAX_TRANSFER_COUNT 300UL
#define FILL_SYM           (0xF5F5F5F5)
#define DUMMY_SYM          (0xD5D5D5D5)

#define SS_ASSERT   (0)
#define SS_DEASSERT (1)

#define TEST_CAPABILITY_BIT(MASK, CAP) ((1U << CAP) & (MASK))
#define MAKE_SYMBOL(MASK, VAL)         ((0U - (VAL)) & (MASK))

static uint8_t write_buffer[MAX_TRANSFER_COUNT] = {0U};
static uint8_t read_buffer[MAX_TRANSFER_COUNT] = {0U};

static void configure_fpga_ci_shield_tester(SPIControllerTester &tester, mdh_spi_mode_t mode, uint32_t word_size);
static bool check_capabilities(const mdh_spi_capabilities_t *capabilities,
                               const mdh_test_spi_configuration_t &test_configuration);
static void test_synchronous_controller_write(SPIControllerTester &tester,
                                              mdh_gpio_t *chip_select_gpio,
                                              mdh_spi_t *spi_device,
                                              bool use_hardware_chip_select,
                                              uint32_t sym_mask);
static void test_synchronous_controller_block_write(SPIControllerTester &tester,
                                                    mdh_gpio_t *chip_select_gpio,
                                                    mdh_spi_t *spi_device,
                                                    const mdh_test_spi_configuration_t &test_configuration,
                                                    uint32_t sym_mask);
#if DEVICE_SPI_ASYNCH
static void test_asynchronous_controller(SPIControllerTester &tester,
                                         mdh_gpio_t *chip_select_gpio,
                                         mdh_spi_t *spi_device,
                                         bool use_hardware_chip_select,
                                         uint32_t sym_mask);
static void test_irq_handler(mdh_spi_t *self);
#endif // DEVICE_SPI_ASYNCH

static volatile bool async_transfer_done = false;

void test_spi_transfer(SPIControllerTester &tester,
                       mdh_gpio_t *chip_select_gpio,
                       mdh_spi_t *spi_device,
                       mdh_test_spi_configuration_t &test_configuration)
{
    mdh_spi_capabilities_t capabilities;

    mdh_spi_get_capabilities(spi_device, false, &capabilities);

    uint32_t hz;
    switch (test_configuration.frequency) {
        case MDH_TEST_SPI_FREQ_200_KHZ:
            hz = 200000ULL;
            break;
        case MDH_TEST_SPI_FREQ_500_KHZ:
            hz = 500000U;
            break;
        case MDH_TEST_SPI_FREQ_1_MHZ:
            hz = 1000000U;
            break;
        case MDH_TEST_SPI_FREQ_2_MHZ:
            hz = 2000000U;
            break;
        case MDH_TEST_SPI_FREQ_10_MHZ:
            hz = 10000000ULL;
            break;
        case MDH_TEST_SPI_FREQ_MAX:
            hz = capabilities.maximum_frequency;
            break;
        case MDH_TEST_SPI_FREQ_MIN:
        default:
            hz = capabilities.minimum_frequency;
            break;
    }

    TEST_ASSERT_TRUE(check_capabilities(&capabilities, test_configuration));

    uint32_t sym_mask = ((1 << test_configuration.word_size) - 1);

    configure_fpga_ci_shield_tester(tester, test_configuration.mode, test_configuration.word_size);

    mdh_spi_set_format(spi_device, test_configuration.word_size, test_configuration.mode, false);
    mdh_spi_set_frequency(spi_device, hz);

    switch (test_configuration.transfer_type) {
        case MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_WRITE:
            test_synchronous_controller_write(
                tester, chip_select_gpio, spi_device, test_configuration.use_hardware_chip_select, sym_mask);
            break;

        case MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_SYNC_BLOCK_WRITE:
            test_synchronous_controller_block_write(tester, chip_select_gpio, spi_device, test_configuration, sym_mask);
            break;

#if DEVICE_SPI_ASYNCH
        case MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_ASYNC:
            test_asynchronous_controller(
                tester, chip_select_gpio, spi_device, test_configuration.use_hardware_chip_select, sym_mask);
            break;
#endif // DEVICE_SPI_ASYNCH
        default:
            TEST_ASSERT_MESSAGE(0U, "Unsupported transfer type.");
            break;
    }

    // Ensures the chip is deselected at the end of test
    if (!test_configuration.use_hardware_chip_select) {
        mdh_gpio_write(chip_select_gpio, SS_DEASSERT);
    }
}

/* Auxiliary function to check platform capabilities against test case. */
static bool check_capabilities(const mdh_spi_capabilities_t *capabilities,
                               const mdh_test_spi_configuration_t &test_configuration)
{
    if (!TEST_CAPABILITY_BIT(capabilities->word_lengths, (test_configuration.word_size - 1))) {
        utest_printf("\n<Specified symbol size is not supported on this platform> skipped. ");
        return false;
    }

    if (!TEST_CAPABILITY_BIT(capabilities->clk_modes, test_configuration.mode)) {
        utest_printf("\n<Specified spi clock mode is not supported on this platform> skipped. ");
        return false;
    }

    if ((test_configuration.frequency < capabilities->minimum_frequency)
        && (test_configuration.frequency > capabilities->maximum_frequency)) {
        utest_printf("\n<Specified frequency is not supported on this platform> skipped. ");
        return false;
    }

    if (test_configuration.transfer_type == MDH_TEST_SPI_TRANSFER_SPI_CONTROLLER_ASYNC
        && capabilities->async_mode == false) {
        utest_printf("\n<Async mode is not supported on this platform> skipped. ");
        return false;
    }

    if (((test_configuration.buffer_sizes == MDH_TEST_SPI_BUF_SIZES_WRITE_GT_READ)
         || (test_configuration.buffer_sizes == MDH_TEST_SPI_BUF_SIZES_WRITE_LT_READ))
        && capabilities->buffers_equal_length == true) {
        utest_printf("\n<Read size != Write size is not supported on this platform> skipped. ");
        return false;
    }

    if (test_configuration.use_hardware_chip_select && capabilities->hw_cs_handle == false) {
        utest_printf("\n<HW Chip Select handling is not supported on this platform> skipped. ");
        return false;
    }

    return true;
}

static void test_synchronous_controller_write(SPIControllerTester &tester,
                                              mdh_gpio_t *chip_select_gpio,
                                              mdh_spi_t *spi_device,
                                              bool use_hardware_chip_select,
                                              uint32_t sym_mask)
{
    uint32_t checksum = 0UL;
    uint32_t transfer_size = MAX_TRANSFER_COUNT;

    if (!use_hardware_chip_select) {
        mdh_gpio_write(chip_select_gpio, SS_ASSERT);
    }

    for (uint32_t transfer_count = 0UL; transfer_count < transfer_size; transfer_count++) {
        uint32_t tx_data = MAKE_SYMBOL(sym_mask, transfer_count);
        uint32_t data = mdh_spi_controller_write(spi_device, tx_data);
        TEST_ASSERT_EQUAL(transfer_count & sym_mask, data);
        checksum += tx_data;
    }

    if (!use_hardware_chip_select) {
        mdh_gpio_write(chip_select_gpio, SS_DEASSERT);
    }

    TEST_ASSERT_EQUAL(transfer_size, tester.get_transfer_count());
    TEST_ASSERT_EQUAL(checksum, tester.get_receive_checksum());
}

static void test_synchronous_controller_block_write(SPIControllerTester &tester,
                                                    mdh_gpio_t *chip_select_gpio,
                                                    mdh_spi_t *spi_device,
                                                    const mdh_test_spi_configuration_t &test_configuration,
                                                    uint32_t sym_mask)
{
    uint32_t checksum = 0UL;
    uint32_t transfer_size = MAX_TRANSFER_COUNT;

    uint32_t write_size;
    uint32_t read_size;
    switch (test_configuration.buffer_sizes) {
        case MDH_TEST_SPI_BUF_SIZES_WRITE_GT_READ:
            write_size = MAX_TRANSFER_COUNT;
            read_size = MAX_TRANSFER_COUNT / 2U;
            break;
        case MDH_TEST_SPI_BUF_SIZES_WRITE_LT_READ:
            write_size = MAX_TRANSFER_COUNT / 2U;
            read_size = MAX_TRANSFER_COUNT;
            break;
        case MDH_TEST_SPI_BUF_SIZES_WRITE_ONE_WORD:
            write_size = read_size = 1U;
            break;
        case MDH_TEST_SPI_BUF_SIZES_EQUAL:
        default:
            write_size = read_size = MAX_TRANSFER_COUNT;
            break;
    }

    for (uint32_t transfer_count = 0U; transfer_count < MAX_TRANSFER_COUNT; transfer_count++) {
        write_buffer[transfer_count] = MAKE_SYMBOL(sym_mask, transfer_count);
        read_buffer[transfer_count] = 0xFF;

        switch (test_configuration.buffer_sizes) {
            case MDH_TEST_SPI_BUF_SIZES_EQUAL:
            case MDH_TEST_SPI_BUF_SIZES_WRITE_GT_READ:
                checksum += MAKE_SYMBOL(sym_mask, transfer_count);
                break;

            case MDH_TEST_SPI_BUF_SIZES_WRITE_LT_READ:
                if (transfer_count < write_size) {
                    checksum += MAKE_SYMBOL(sym_mask, transfer_count);
                } else {
                    checksum += ((uint8_t)FILL_SYM & sym_mask);
                }
                break;

            case MDH_TEST_SPI_BUF_SIZES_WRITE_ONE_WORD:
                write_buffer[0U] = checksum = 0xAAU;
                transfer_size = 1U;
                break;

            default:
                break;
        }
    }

    if (!test_configuration.use_hardware_chip_select) {
        mdh_gpio_write(chip_select_gpio, SS_ASSERT);
    }

    uint32_t number_bytes_written =
        mdh_spi_controller_block_read_write(spi_device, write_buffer, write_size, read_buffer, read_size, 0xF5);

    if (!test_configuration.use_hardware_chip_select) {
        mdh_gpio_write(chip_select_gpio, SS_DEASSERT);
    }

    TEST_ASSERT_EQUAL(transfer_size, number_bytes_written);

    for (uint32_t i = 0; i < read_size; i++) {
        TEST_ASSERT_EQUAL(i & sym_mask, read_buffer[i]);
    }

    for (uint32_t i = read_size; i < MAX_TRANSFER_COUNT; i++) {
        TEST_ASSERT_EQUAL(0xFF, read_buffer[i]);
    }

    TEST_ASSERT_EQUAL(transfer_size, tester.get_transfer_count());
    TEST_ASSERT_EQUAL(checksum, tester.get_receive_checksum());
}

#if DEVICE_SPI_ASYNCH
static void test_asynchronous_controller(SPIControllerTester &tester,
                                         mdh_gpio_t *chip_select_gpio,
                                         mdh_spi_t *spi_device,
                                         bool use_hardware_chip_select,
                                         uint32_t sym_mask)
{
    uint32_t checksum = 0UL;
    uint32_t transfer_size = MAX_TRANSFER_COUNT;

    for (uint32_t transfer_count = 0; transfer_count < transfer_size; transfer_count++) {
        write_buffer[transfer_count] = MAKE_SYMBOL(sym_mask, transfer_count);
        checksum += MAKE_SYMBOL(sym_mask, transfer_count);
        read_buffer[transfer_count] = 0xAAU;
    }

    async_transfer_done = false;

    if (!use_hardware_chip_select) {
        mdh_gpio_write(chip_select_gpio, SS_ASSERT);
    }

    mdh_spi_asynch_controller_transfer(spi_device,
                                       write_buffer,
                                       MAX_TRANSFER_COUNT,
                                       read_buffer,
                                       MAX_TRANSFER_COUNT,
                                       8U,
                                       (uint32_t)test_irq_handler,
                                       MDH_SPI_EVENT_COMPLETE);

    while (!async_transfer_done) {
    }

    if (!use_hardware_chip_select) {
        mdh_gpio_write(chip_select_gpio, SS_DEASSERT);
    }

    for (uint32_t i = 0; i < MAX_TRANSFER_COUNT; i++) {
        TEST_ASSERT_EQUAL(i & sym_mask, read_buffer[i]);
    }

    TEST_ASSERT_EQUAL(transfer_size, tester.get_transfer_count());
    TEST_ASSERT_EQUAL(checksum, tester.get_receive_checksum());
}

static void test_irq_handler(mdh_spi_t *self)
{
    uint32_t event = mdh_spi_asynch_irq_handler(self);

    if (event & MDH_SPI_EVENT_COMPLETE) {
        async_transfer_done = true;
    }
}
#endif // DEVICE_SPI_ASYNCH

static void configure_fpga_ci_shield_tester(SPIControllerTester &tester, mdh_spi_mode_t mode, uint32_t word_size)
{
    // Configure spi_slave module
    SPITester::SpiMode tester_spi_mode;
    switch (mode) {
        case MDH_SPI_MODE_1:
            tester_spi_mode = SPITester::SpiMode::Mode1;
            break;
        case MDH_SPI_MODE_2:
            tester_spi_mode = SPITester::SpiMode::Mode2;
            break;
        case MDH_SPI_MODE_3:
            tester_spi_mode = SPITester::SpiMode::Mode3;
            break;
        case MDH_SPI_MODE_0:
        default:
            tester_spi_mode = SPITester::SpiMode::Mode0;
            break;
    }

    tester.set_mode(tester_spi_mode);
    tester.set_bit_order(SPITester::MSBFirst);
    tester.set_sym_size(word_size);

    // Reset tester stats and select SPI
    tester.peripherals_reset();
    tester.select_peripheral(SPITester::PeripheralSPI);
}
