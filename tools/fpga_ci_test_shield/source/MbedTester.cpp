/* Copyright (c) 2019-2023 Arm Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "fpga_ci_test_shield/fpga_config.h"
#include "fpga_ci_test_shield/MbedTester.h"

#include <cassert>
#include <cstring>

namespace MbedTesterConst {
const uint8_t physical_pins = 128;
const uint8_t logical_pins = 8;
const uint8_t analog_count = 4;

const size_t key_len = 8;
const uint8_t key[key_len] = {0x92, 0x9d, 0x9a, 0x9b, 0x29, 0x35, 0xa2, 0x65};
} // namespace MbedTesterConst

uint8_t MbedTester::_bit_bang_spi(uint8_t byte_out)
{
    uint8_t byte_in = 0;
    for (int i = 0; i < 8; i++) {
        *_sclk = 0;
        *_sdo = (byte_out >> (7 - i)) & 1;
        _platform_handlers.wait_ns(100);
        *_sclk = 1;
        byte_in |= *_sdi ? 1 << (7 - i) : 0;
        _platform_handlers.wait_ns(100);
    }
    return byte_in;
}

void MbedTester::_initiate_control_transfer(uint32_t addr, bool write_n_read, uint8_t size)
{
    // 8 - Start Key
    for (uint32_t i = 0; i < MbedTesterConst::key_len; i++) {
        _bit_bang_spi(MbedTesterConst::key[i]);
    }

    // 1 - Physical pin index for SDI
    _bit_bang_spi(_sdi_index);

    // 1 - Number of SPI transfers which follow (= N + 5)
    _bit_bang_spi(size + 5);

    // 4 - Little endian address for transfer
    _bit_bang_spi((addr >> (8 * 0)) & 0xFF);
    _bit_bang_spi((addr >> (8 * 1)) & 0xFF);
    _bit_bang_spi((addr >> (8 * 2)) & 0xFF);
    _bit_bang_spi((addr >> (8 * 3)) & 0xFF);

    // 1 - direction
    _bit_bang_spi(write_n_read ? 1 : 0);

    // N - Data to read or write
}

MbedTester::MbedTester(ControlChannel &control_channel, PlatformHandlers &platform_handlers)
    : _sclk_index(control_channel.clk.tester_pin_index), _sdo_index(control_channel.sdo.tester_pin_index),
      _sdi_index(control_channel.sdi.tester_pin_index), _aux_index(control_channel.aux.tester_pin_index),
      _platform_handlers(platform_handlers)
{
    assert(platform_handlers.error != NULL && platform_handlers.wait_ns != NULL && platform_handlers.wait_us != NULL);

    _reset();

    set_control_pins_manual(control_channel.clk.dut_gpio,
                            control_channel.sdo.dut_gpio,
                            control_channel.sdi.dut_gpio,
                            control_channel.aux.dut_gpio);

    _init_io_exp_rst_flag = 0;
}

MbedTester::~MbedTester()
{
    _free_control_pins();
}

void MbedTester::set_control_pins_manual(mdh_gpio_t *clk, mdh_gpio_t *sdo, mdh_gpio_t *sdi, mdh_gpio_t *aux)
{
    if (_sclk_index + 1 != _sdo_index) {
        _platform_handlers.error("SDO pin index does not follow SCLK as required");
    }

    if ((_sdi_index == _sclk_index) || (_sdi_index == _sdo_index)) {
        _platform_handlers.error("SDI conflicts with a control channel");
    }

    if ((_aux_index == _sclk_index) || (_aux_index == _sdo_index) || (_aux_index == _sdi_index)) {
        _platform_handlers.error("AUX conflicts with a control channel");
    }

    // All criteria have been met so set the pins
    _setup_control_pins(clk, sdo, sdi, aux);
}

void MbedTester::pin_map_set(int index, LogicalPin logical)
{
    if (index < 0) {
        _platform_handlers.error("Pin at index %i not in form factor", index);
        return;
    }

    if (logical >= LogicalPinTotal) {
        _platform_handlers.error("Invalid logical pin %i", logical);
        return;
    }

    pin_map_index(index, logical);
}

void MbedTester::pin_map_reset()
{
    for (uint32_t i = 0; i < sizeof(_mapping) / sizeof(_mapping[0]); i++) {
        _mapping[i] = MbedTester::physical_nc;
    }

    uint8_t pin_buf[MbedTesterConst::physical_pins + MbedTesterConst::logical_pins];
    memset(pin_buf, 0xFF, sizeof(pin_buf));
    write(TESTER_REMAP, pin_buf, sizeof(pin_buf));
}

void MbedTester::peripherals_reset()
{
    uint8_t buf = TESTER_CONTROL_RESET_PERIPHERALS;
    write(TESTER_CONTROL_RESET, &buf, sizeof(buf));
}

void MbedTester::reset()
{
    // Reset pullup settings
    pin_pull_reset_all();

    // Reset the FPGA
    uint8_t buf = TESTER_CONTROL_RESET_ALL;
    write(TESTER_CONTROL_RESET, &buf, sizeof(buf));

    // Reset the pinmap
    // NOTE - this is only needed for compatibility with
    //        older firmware which resets the mapping
    //        of all pins to 0x00 rather than 0xFF.
    pin_map_reset();

    // Reset internal state variables
    _reset();
}

void MbedTester::reprogram()
{
    // Trigger reprogramming
    uint8_t buf = TESTER_CONTROL_REPROGRAM;
    write(TESTER_CONTROL_RESET, &buf, sizeof(buf));

    // Reset internal state variables
    _reset();
}

uint32_t MbedTester::version()
{
    uint32_t software_version;

    read(TESTER_CONTROL_VERSION, (uint8_t *)&software_version, sizeof(software_version));

    return software_version;
}

void MbedTester::select_peripheral(Peripheral peripheral)
{
    uint8_t data = peripheral;
    write(TESTER_PERIPHERAL_SELECT, &data, sizeof(data));
}

void MbedTester::pin_pull_reset_all()
{
    _init_io_exp_rst_flag = 1;
    sys_pin_write(I2CReset, 0, true);
    _platform_handlers.wait_us(1);
    sys_pin_write(I2CReset, 0, false);
}

int MbedTester::pin_set_pull(int index, PullMode mode)
{
    if ((index < 0) || (index > 127)) {
        _platform_handlers.error("Pin at index %i not in form factor", index);
        return -1;
    }

    return pin_set_pull_index(index, mode);
}

int MbedTester::pin_set_pull_index(int index, PullMode mode)
{
    // Reset IO expanders once after Mbed reset if user attempts
    // to read/write them without explicitly reseting them
    if (_init_io_exp_rst_flag == 0) {
        pin_pull_reset_all();
    }
    uint8_t chip_num;        // can be 0-5
    uint16_t dev_addr;       // can be 0x44 or 0x46
    uint8_t port_num;        // can be 0-23
    uint8_t output_port_reg; // can be 4, 5, or 6
    uint8_t config_reg;      // can be 12, 13, or 14
    uint8_t reg_bit;         // can be 0-7
    uint8_t cmd0[2];         // for writing configuration register
    uint8_t cmd1[2];         // for writing output port register
    uint8_t i2c_index;       // can be 0, 1, or 2 for TESTER_SYS_IO_MODE_I2C_IO_EXPANDER0/1/2

    chip_num = index / 24;
    if ((chip_num == 0) || (chip_num == 1)) {
        i2c_index = 0;
    } else if ((chip_num == 2) || (chip_num == 3)) {
        i2c_index = 1;
    } else if ((chip_num == 4) || (chip_num == 5)) {
        i2c_index = 2;
    } else {
        _platform_handlers.error("Corrupt index %i, should be 0-127\r\n", index);
        return -1;
    }
    dev_addr = (chip_num % 2) ? 0x44 : 0x46;
    port_num = index % 24;
    output_port_reg = 4 + (port_num / 8);
    config_reg = 12 + (port_num / 8);
    reg_bit = port_num % 8;

    uint8_t read_config_byte[1];
    uint8_t read_output_byte[1];
    if (io_expander_i2c_read(i2c_index, dev_addr, config_reg, read_config_byte, 1) != 0) {
        return -1;
    }
    if (io_expander_i2c_read(i2c_index, dev_addr, output_port_reg, read_output_byte, 1) != 0) {
        return -1;
    }
    cmd0[0] = config_reg;
    if ((mode == PullDown) || (mode == PullUp)) {
        cmd0[1] = read_config_byte[0] & ~(1 << reg_bit);
        cmd1[0] = output_port_reg;
        if (mode == PullDown) {
            cmd1[1] = read_output_byte[0] & ~(1 << reg_bit);
        } else if (mode == PullUp) {
            cmd1[1] = read_output_byte[0] | (1 << reg_bit);
        }
    } else if (mode == PullNone) {
        cmd0[1] = read_config_byte[0] | (1 << reg_bit);
    }

    // write configuration register for all 3 modes
    if (io_expander_i2c_write(i2c_index, dev_addr, cmd0, 2) != 0) {
        return -1;
    }
    // only write output register for pulldown and pullup
    if ((mode == PullDown) || (mode == PullUp)) {
        if (io_expander_i2c_write(i2c_index, dev_addr, cmd1, 2) != 0) {
            return -1;
        }
    }
    return 0;
}

uint8_t MbedTester::io_expander_read(int index, IOExpanderReg reg_type)
{
    return io_expander_read_index(index, reg_type);
}

uint8_t MbedTester::io_expander_read_index(int index, IOExpanderReg reg_type)
{
    // Reset IO expanders once after Mbed reset if user attempts
    // to read/write them without explicitly reseting them
    if (_init_io_exp_rst_flag == 0) {
        pin_pull_reset_all();
    }
    uint8_t read_byte[1] = {0};
    uint8_t chip_num;        // can be 0-5
    uint16_t dev_addr;       // can be 0x44 or 0x46
    uint8_t port_num;        // can be 0-23
    uint8_t input_port_reg;  // can be 0, 1, or 2
    uint8_t output_port_reg; // can be 4, 5, or 6
    uint8_t config_reg;      // can be 12, 13, or 14
    uint8_t reg_bit;         // can be 0-7
    uint8_t i2c_index;

    chip_num = index / 24;
    if ((chip_num == 0) || (chip_num == 1)) {
        i2c_index = 0;
    } else if ((chip_num == 2) || (chip_num == 3)) {
        i2c_index = 1;
    } else if ((chip_num == 4) || (chip_num == 5)) {
        i2c_index = 2;
    } else {
        i2c_index = 0xFF;
        _platform_handlers.error("Invalid pin index, index should be in the range of 0-127");
    }
    dev_addr = (chip_num % 2) ? 0x44 : 0x46;
    port_num = index % 24;
    input_port_reg = (port_num / 8);
    output_port_reg = 4 + (port_num / 8);
    config_reg = 12 + (port_num / 8);
    reg_bit = port_num % 8;
    uint8_t reg;
    if (reg_type == RegInput) {
        reg = input_port_reg;
    } else if (reg_type == RegOutput) {
        reg = output_port_reg;
    } else if (reg_type == RegConfig) {
        reg = config_reg;
    } else {
        reg = 0xFF;
        _platform_handlers.error("Invalid register type, should be: INPUT, OUTPUT, or RegConfig");
    }

    int read_success = io_expander_i2c_read(i2c_index, dev_addr, reg, read_byte, 1);
    assert(read_success == 0);
    uint8_t bit = (read_byte[0] & (1 << reg_bit)) >> reg_bit;
    return bit;
}

int MbedTester::io_expander_i2c_read(uint8_t i2c_index, uint8_t dev_addr, uint8_t start_reg, uint8_t *data, int length)
{
    mbed::DigitalInOut *sda_in = _sdi;
    mbed::DigitalInOut *sda_val = _aux;
    mbed::DigitalInOut *scl_val = _sclk;
    sda_in->input();
    sda_val->output();
    *sda_val = 1;
    scl_val->output();
    sys_pin_mode_i2c_io_expander(i2c_index, _sdi_index, _aux_index, MbedTester::physical_nc, _sclk_index);

    // start condition
    *scl_val = 1;
    _platform_handlers.wait_ns(2500);
    *sda_val = 0;
    _platform_handlers.wait_ns(2500);

    // begin writing data, dev_addr first
    uint8_t send_bit;
    for (int j = 0; j < 2; j += 1) {
        *scl_val = 0;
        *sda_val = 0;
        _platform_handlers.wait_ns(2500);
        for (int i = 7; i > -1; i -= 1) {
            if (j == 0) {
                send_bit = (dev_addr & (1 << i)) >> i;
            } else {
                send_bit = (start_reg & (1 << i)) >> i;
            }
            *sda_val = send_bit;
            _platform_handlers.wait_ns(500);

            *scl_val = 1;
            _platform_handlers.wait_ns(2500);
            *scl_val = 0;
            _platform_handlers.wait_ns(1000);
            *sda_val = 0;
            _platform_handlers.wait_ns(1000);
        }
        // receive ACK from IO extender
        *sda_val = 1; // make sda high z to receive ACK
        // clk the ACK
        *scl_val = 1;
        // read sda to check for ACK or NACK
        if (*sda_in) {
            return -1; // NACK - write failed
        }
        _platform_handlers.wait_ns(2500);
        *scl_val = 0;
        _platform_handlers.wait_ns(2500);
    }

    // start condition
    *sda_val = 1;
    *scl_val = 1;
    _platform_handlers.wait_ns(2500);
    *sda_val = 0;
    _platform_handlers.wait_ns(2500);

    // begin reading data, write (dev_addr | 1) first
    dev_addr |= 1;
    for (int j = -1; j < length; j += 1) {
        uint8_t read_byte = 0;
        for (int i = 7; i > -1; i -= 1) {
            if (j == -1) {
                *scl_val = 0;
                *sda_val = 0;
                send_bit = (dev_addr & (1 << i)) >> i;
                *sda_val = send_bit;
                _platform_handlers.wait_ns(500);

                *scl_val = 1;
                _platform_handlers.wait_ns(2500);
                *scl_val = 0;
                _platform_handlers.wait_ns(1000);
                *sda_val = 0;
                _platform_handlers.wait_ns(1000);
            } else {
                *scl_val = 1;
                read_byte |= (*sda_in << i);
                _platform_handlers.wait_ns(2500);
                *scl_val = 0;
                _platform_handlers.wait_ns(2500);
            }
        }
        if (j > -1) {
            data[j] = read_byte;
        }
        if (j == -1) {
            // receive ACK from IO extender
            *sda_val = 1; // make sda high z to receive ACK
            // clk the ACK
            *scl_val = 1;
            // read sda to check for ACK or NACK
            if (*sda_in) {
                return -1; // NACK - write failed
            }
            _platform_handlers.wait_ns(2500);
            *scl_val = 0;
            _platform_handlers.wait_ns(2500);
        } else {
            if (j == (length - 1)) { // NACK to signal end of read
                *sda_val = 1;
                _platform_handlers.wait_ns(1000);
                *scl_val = 1;
                _platform_handlers.wait_ns(2500);
                *scl_val = 0;
                _platform_handlers.wait_ns(1500);
            } else { // ACK to signal read will continue
                *sda_val = 0;
                _platform_handlers.wait_ns(1000);
                *scl_val = 1;
                _platform_handlers.wait_ns(2500);
                *scl_val = 0;
                _platform_handlers.wait_ns(500);
                *sda_val = 1;
                _platform_handlers.wait_ns(1000);
            }
        }
    }

    // stop condition
    *sda_val = 0;
    _platform_handlers.wait_ns(2500);
    *scl_val = 1;
    _platform_handlers.wait_ns(2500);
    *sda_val = 1;
    _platform_handlers.wait_ns(2500);

    sys_pin_mode_disabled();

    return 0;
}

int MbedTester::io_expander_i2c_write(uint8_t i2c_index, uint8_t dev_addr, uint8_t *data, int length)
{
    mbed::DigitalInOut *sda_in = _sdi;
    mbed::DigitalInOut *sda_val = _aux;
    mbed::DigitalInOut *scl_val = _sclk;
    sda_in->input();
    sda_val->output();
    *sda_val = 1;
    scl_val->output();
    sys_pin_mode_i2c_io_expander(i2c_index, _sdi_index, _aux_index, MbedTester::physical_nc, _sclk_index);

    // start condition
    *scl_val = 1;
    _platform_handlers.wait_ns(2500);
    *sda_val = 0;
    _platform_handlers.wait_ns(2500);

    // begin writing data, dev_addr first
    uint8_t send_bit;
    for (int j = -1; j < length; j += 1) {
        *scl_val = 0;
        *sda_val = 0;
        for (int i = 7; i > -1; i -= 1) {
            if (j == -1) {
                send_bit = (dev_addr & (1 << i)) >> i;
            } else {
                send_bit = (data[j] & (1 << i)) >> i;
            }

            *sda_val = send_bit;
            _platform_handlers.wait_ns(500);

            *scl_val = 1;
            _platform_handlers.wait_ns(2500);
            *scl_val = 0;
            _platform_handlers.wait_ns(1000);
            *sda_val = 0;
            _platform_handlers.wait_ns(1000);
        }
        // receive ACK from IO extender
        *sda_val = 1; // make sda high z to receive ACK
        // clk the ACK
        *scl_val = 1;
        // read sda to check for ACK or NACK
        if (*sda_in) {
            return -1; // NACK - write failed
        }
        _platform_handlers.wait_ns(2500);
        *scl_val = 0;
        _platform_handlers.wait_ns(2500);
    }

    // stop condition
    *sda_val = 0;
    _platform_handlers.wait_ns(2500);
    *scl_val = 1;
    _platform_handlers.wait_ns(2500);
    *sda_val = 1;
    _platform_handlers.wait_ns(2500);

    sys_pin_mode_disabled();

    return 0;
}

int MbedTester::pin_set_pull_bb(int index, PullMode mode)
{
    if ((index < 0) || (index > 127)) {
        _platform_handlers.error("Pin at index %i not in form factor", index);
        return -1;
    }
    uint8_t chip_num;        // can be 0-5
    SystemPin sda;           // can be I2CSda0, I2CSda1, or I2CSda2
    SystemPin scl;           // can be I2CScl0, I2CScl1, or I2CScl2
    uint16_t dev_addr;       // can be 0x44 or 0x46
    uint8_t port_num;        // can be 0-23
    uint8_t output_port_reg; // can be 4, 5, or 6
    uint8_t config_reg;      // can be 12, 13, or 14
    uint8_t reg_bit;         // can be 0-7
    uint8_t cmd0[2];         // for writing configuration register
    uint8_t cmd1[2];         // for writing output port register

    chip_num = index / 24;
    if ((chip_num == 0) || (chip_num == 1)) {
        sda = I2CSda0;
        scl = I2CScl0;
    } else if ((chip_num == 2) || (chip_num == 3)) {
        sda = I2CSda1;
        scl = I2CScl1;
    } else if ((chip_num == 4) || (chip_num == 5)) {
        sda = I2CSda2;
        scl = I2CScl2;
    } else {
        _platform_handlers.error("Pin at index %i not in form factor", index);
        return -1;
    }
    dev_addr = (chip_num % 2) ? 0x44 : 0x46;
    port_num = index % 24;
    output_port_reg = 4 + (port_num / 8);
    config_reg = 12 + (port_num / 8);
    reg_bit = port_num % 8;

    uint8_t read_config_byte[1];
    uint8_t read_output_byte[1];
    if (io_expander_i2c_read_bb(sda, scl, dev_addr, config_reg, read_config_byte, 1) != 0) {
        return -1;
    }
    if (io_expander_i2c_read_bb(sda, scl, dev_addr, output_port_reg, read_output_byte, 1) != 0) {
        return -1;
    }
    cmd0[0] = config_reg;
    if ((mode == PullDown) || (mode == PullUp)) {
        cmd0[1] = read_config_byte[0] & ~(1 << reg_bit);
        cmd1[0] = output_port_reg;
        if (mode == PullDown) {
            cmd1[1] = read_output_byte[0] & ~(1 << reg_bit);
        } else if (mode == PullUp) {
            cmd1[1] = read_output_byte[0] | (1 << reg_bit);
        }
    } else if (mode == PullNone) {
        cmd0[1] = read_config_byte[0] | (1 << reg_bit);
    }

    // write configuration register for all 3 modes
    if (io_expander_i2c_write_bb(sda, scl, dev_addr, cmd0, 2) != 0) {
        return -1;
    }
    // only write output register for pulldown and pullup
    if ((mode == PullDown) || (mode == PullUp)) {
        if (io_expander_i2c_write_bb(sda, scl, dev_addr, cmd1, 2) != 0) {
            return -1;
        }
    }
    return 0;
}

uint8_t MbedTester::io_expander_read_bb(int index, IOExpanderReg reg_type)
{
    uint8_t read_byte[1] = {0};
    uint8_t chip_num;        // can be 0-5
    SystemPin sda;           // can be I2CSda0, I2CSda1, or I2CSda2
    SystemPin scl;           // can be I2CScl0, I2CScl1, or I2CScl2
    uint16_t dev_addr;       // can be 0x44 or 0x46
    uint8_t port_num;        // can be 0-23
    uint8_t input_port_reg;  // can be 0, 1, or 2
    uint8_t output_port_reg; // can be 4, 5, or 6
    uint8_t config_reg;      // can be 12, 13, or 14
    uint8_t reg_bit;         // can be 0-7

    chip_num = index / 24;
    if ((chip_num == 0) || (chip_num == 1)) {
        sda = I2CSda0;
        scl = I2CScl0;
    } else if ((chip_num == 2) || (chip_num == 3)) {
        sda = I2CSda1;
        scl = I2CScl1;
    } else if ((chip_num == 4) || (chip_num == 5)) {
        sda = I2CSda2;
        scl = I2CScl2;
    } else {
        sda = (SystemPin)-1;
        scl = (SystemPin)-1;
        _platform_handlers.error("Invalid pin index, index should be in the range of 0-127");
    }

    dev_addr = (chip_num % 2) ? 0x44 : 0x46;
    port_num = index % 24;
    input_port_reg = (port_num / 8);
    output_port_reg = 4 + (port_num / 8);
    config_reg = 12 + (port_num / 8);
    reg_bit = port_num % 8;
    uint8_t reg;
    if (reg_type == RegInput) {
        reg = input_port_reg;
    } else if (reg_type == RegOutput) {
        reg = output_port_reg;
    } else if (reg_type == RegConfig) {
        reg = config_reg;
    } else {
        reg = 0xFF;
        _platform_handlers.error("Invalid register type, should be: INPUT, OUTPUT, or CONFIG");
    }

    int read_success = io_expander_i2c_read_bb(sda, scl, dev_addr, reg, read_byte, 1);
    assert(read_success == 0);
    uint8_t bit = (read_byte[0] & (1 << reg_bit)) >> reg_bit;
    return bit;
}

int MbedTester::io_expander_i2c_read_bb(
    SystemPin sda, SystemPin scl, uint8_t dev_addr, uint8_t start_reg, uint8_t *data, int length)
{
    // start condition
    sys_pin_write(sda, 0, false);
    sys_pin_write(scl, 0, false);
    sys_pin_write(sda, 0, true);

    // begin writing data, dev_addr first
    uint8_t send_bit;
    for (int j = 0; j < 2; j += 1) {
        sys_pin_write(scl, 0, true);
        sys_pin_write(sda, 0, true);
        for (int i = 7; i > -1; i -= 1) {
            if (j == 0) {
                send_bit = (dev_addr & (1 << i)) >> i;
            } else {
                send_bit = (start_reg & (1 << i)) >> i;
            }
            if (send_bit == 1) {
                sys_pin_write(sda, 0, false);
            } else if (send_bit == 0) {
                sys_pin_write(sda, 0, true);
            }
            sys_pin_write(scl, 0, false);
            sys_pin_write(scl, 0, true);
            sys_pin_write(sda, 0, true);
        }
        // receive ACK from IO extender
        sys_pin_write(sda, 0, false); // make sda high z to receive ACK
        // clk the ACK
        sys_pin_write(scl, 0, false);
        // read sda to check for ACK or NACK
        if (sys_pin_read(sda)) {
            return -1; // NACK - write failed
        }
        sys_pin_write(scl, 0, true);
    }

    // start condition
    sys_pin_write(sda, 0, false);
    sys_pin_write(scl, 0, false);
    sys_pin_write(sda, 0, true);

    // begin reading data, write (dev_addr | 1) first
    dev_addr |= 1;
    for (int j = -1; j < length; j += 1) {
        uint8_t read_byte = 0;
        for (int i = 7; i > -1; i -= 1) {
            if (j == -1) {
                sys_pin_write(scl, 0, true);
                sys_pin_write(sda, 0, true);
                send_bit = (dev_addr & (1 << i)) >> i;
                if (send_bit == 1) {
                    sys_pin_write(sda, 0, false);
                } else if (send_bit == 0) {
                    sys_pin_write(sda, 0, true);
                }
                sys_pin_write(scl, 0, false);
                sys_pin_write(scl, 0, true);
                sys_pin_write(sda, 0, true);
            } else {
                sys_pin_write(scl, 0, false);
                read_byte |= (sys_pin_read(sda) << i);
                sys_pin_write(scl, 0, true);
            }
        }
        if (j > -1) {
            data[j] = read_byte;
        }
        if (j == -1) {
            // receive ACK from IO extender
            sys_pin_write(sda, 0, false); // make sda high z to receive ACK
            // clk the ACK
            sys_pin_write(scl, 0, false);
            // read sda to check for ACK or NACK
            if (sys_pin_read(sda)) {
                return -1; // NACK - write failed
            }
            sys_pin_write(scl, 0, true);
        } else {
            if (j == (length - 1)) { // NACK to signal end of read
                sys_pin_write(sda, 0, false);
                sys_pin_write(scl, 0, false);
                sys_pin_write(scl, 0, true);
            } else { // ACK to signal read will continue
                sys_pin_write(sda, 0, true);
                sys_pin_write(scl, 0, false);
                sys_pin_write(scl, 0, true);
                sys_pin_write(sda, 0, false);
            }
        }
    }

    // stop condition
    sys_pin_write(sda, 0, true);
    sys_pin_write(scl, 0, false);
    sys_pin_write(sda, 0, false);
    return 0;
}

int MbedTester::io_expander_i2c_write_bb(SystemPin sda, SystemPin scl, uint8_t dev_addr, uint8_t *data, int length)
{
    // start condition
    sys_pin_write(sda, 0, false);
    sys_pin_write(scl, 0, false);
    sys_pin_write(sda, 0, true);

    // begin writing data, dev_addr first
    uint8_t send_bit;
    for (int j = -1; j < length; j += 1) {
        sys_pin_write(scl, 0, true);
        sys_pin_write(sda, 0, true);
        for (int i = 7; i > -1; i -= 1) {
            if (j == -1) {
                send_bit = (dev_addr & (1 << i)) >> i;
            } else {
                send_bit = (data[j] & (1 << i)) >> i;
            }
            if (send_bit == 1) {
                sys_pin_write(sda, 0, false);
            } else if (send_bit == 0) {
                sys_pin_write(sda, 0, true);
            }
            sys_pin_write(scl, 0, false);
            sys_pin_write(scl, 0, true);
            sys_pin_write(sda, 0, true);
        }
        // receive ACK from IO extender
        sys_pin_write(sda, 0, false); // make sda high z to receive ACK
        // clk the ACK
        sys_pin_write(scl, 0, false);
        // read sda to check for ACK or NACK
        if (sys_pin_read(sda)) {
            return -1; // NACK - write failed
        }
        sys_pin_write(scl, 0, true);
    }

    // stop condition
    sys_pin_write(sda, 0, true);
    sys_pin_write(scl, 0, false);
    sys_pin_write(sda, 0, false);
    return 0;
}

void MbedTester::set_analog_out(bool enable, float voltage)
{
    uint32_t cycles_high = (int)(100 * voltage);
    uint32_t period = 100;
    set_pwm_period_and_cycles_high(period, cycles_high);
    set_pwm_enable(enable);
}

int MbedTester::set_mux_addr(int index)
{
    if ((index < 0) || (index > 127)) {
        _platform_handlers.error("Pin index %i not in form factor", index);
        return -1;
    }

    return set_mux_addr_index(index);
}

int MbedTester::set_mux_addr_index(int index)
{
    sys_pin_write(AnalogMuxAddr0, index & 0x01, true);
    sys_pin_write(AnalogMuxAddr1, (index & 0x02) >> 1, true);
    sys_pin_write(AnalogMuxAddr2, (index & 0x04) >> 2, true);
    sys_pin_write(AnalogMuxAddr3, (index & 0x08) >> 3, true);
    sys_pin_write(AnalogMuxAddr4, (index & 0x10) >> 4, true);
    sys_pin_write(AnalogMuxAddr5, (index & 0x20) >> 5, true);
    sys_pin_write(AnalogMuxAddr6, (index & 0x40) >> 6, true);
    sys_pin_write(AnalogMuxAddr7, (index & 0x80) >> 7, true);

    return 0;
}

void MbedTester::set_mux_enable(bool val)
{
    if (val == true) {
        sys_pin_write(AnalogMuxEnable, 0, true); // enable analog MUXes
    } else if (val == false) {
        sys_pin_write(AnalogMuxEnable, 1, true); // disable analog MUXes
    }
    _platform_handlers.wait_us(10);
}

void MbedTester::set_pwm_enable(bool val)
{
    uint8_t data;
    if (val == true) {
        data = 1;
    } else if (val == false) {
        data = 0;
    }
    write(TESTER_SYS_IO_PWM_ENABLE, &data, sizeof(data));
}

bool MbedTester::get_pwm_enable()
{
    uint8_t val = 0;
    read(TESTER_SYS_IO_PWM_ENABLE, &val, sizeof(val));
    if (val == 1) {
        return true;
    } else if (val == 0) {
        return false;
    } else {
        _platform_handlers.error("Corrupt pwm enable value");
        return false;
    }
}

void MbedTester::set_pwm_period_and_cycles_high(uint32_t period, uint32_t cycles_high)
{
    set_pwm_enable(false);
    uint32_t p = period - 1;  // period in cycles
    uint32_t d = cycles_high; // number of cycles pwm out is high
    write(TESTER_SYS_IO_PWM_PERIOD, (uint8_t *)&p, sizeof(p));
    write(TESTER_SYS_IO_PWM_CYCLES_HIGH, (uint8_t *)&d, sizeof(d));
    set_pwm_enable(true);
}

uint32_t MbedTester::get_pwm_period()
{
    uint32_t period = 0;
    read(TESTER_SYS_IO_PWM_PERIOD, (uint8_t *)&period, sizeof(period));
    return period + 1; // clk cycles
}

uint8_t MbedTester::get_pwm_cycles_high()
{
    uint8_t cycles_high = 0;
    read(TESTER_SYS_IO_PWM_CYCLES_HIGH, &cycles_high, sizeof(cycles_high));
    return cycles_high;
}

uint16_t MbedTester::get_analogmuxin_measurement()
{
    _platform_handlers.wait_us(1000000); // wait for value to stabalize
    // take snapshot of conversion value to make safe for reading
    set_snapshot();
    uint16_t an_mux_analogin_measurement = 0;
    read(TESTER_SYS_IO_AN_MUX_ANALOGIN_MEASUREMENT,
         (uint8_t *)&an_mux_analogin_measurement,
         sizeof(an_mux_analogin_measurement));
    return an_mux_analogin_measurement;
}

uint16_t MbedTester::get_anin_measurement(int index)
{
    // check index is in bounds
    if ((index < 0) || (index >= MbedTesterConst::analog_count)) {
        _platform_handlers.error("AnalogIn index is out of bounds");
    }
    // take snapshot of conversion value to make safe for reading
    set_snapshot();
    uint16_t anin_measurement = 0;
    read((TESTER_SYS_IO_ANIN0_MEASUREMENT + (index * 10)),
         (uint8_t *)&anin_measurement,
         sizeof(anin_measurement)); // 10 because sizeof measurement + sizeof measurements_sum = 10
    return anin_measurement;
}

void MbedTester::get_anin_sum_samples_cycles(int index, uint64_t *sum, uint32_t *samples, uint64_t *cycles)
{
    // check index is in bounds
    if ((index < 0) || (index >= MbedTesterConst::analog_count)) {
        _platform_handlers.error("AnalogIn index is out of bounds");
    }
    // take snapshot of the sum/samples/cycles so that all 3 values are correct in relation to each other
    set_snapshot();
    read((TESTER_SYS_IO_ANIN0_MEASUREMENTS_SUM + (index * 10)),
         reinterpret_cast<uint8_t *>(sum),
         sizeof(*sum)); // 10 because sizeof measurement + sizeof measurements_sum = 10
    read(TESTER_SYS_IO_NUM_POWER_SAMPLES, reinterpret_cast<uint8_t *>(samples), sizeof(*samples));
    read(TESTER_SYS_IO_NUM_POWER_CYCLES, reinterpret_cast<uint8_t *>(cycles), sizeof(*cycles));
}

void MbedTester::set_snapshot()
{
    uint8_t data = 1;
    write(TESTER_SYS_IO_ADC_SNAPSHOT, &data, sizeof(data));
    _platform_handlers.wait_us(1);
}

void MbedTester::set_sample_adc(bool val)
{
    uint8_t data;
    if (val == true) {
        data = 1;
    } else if (val == false) {
        data = 0;
    }
    write(TESTER_SYS_IO_SAMPLE_ADC, &data, sizeof(data));
}

float MbedTester::get_analog_in()
{
    uint16_t data = get_analogmuxin_measurement();
    float data_f = (float)data / 4095.0f;
    return data_f;
}

float MbedTester::get_anin_voltage(int index)
{
    uint16_t data = get_anin_measurement(index);
    float data_f = (float)data / 4095.0f;
    return data_f;
}

int MbedTester::gpio_read(LogicalPin gpio)
{
    if (gpio >= LogicalPinCount) {
        _platform_handlers.error("Invalid pin for gpio_read");
        return 0;
    }
    uint8_t data = 0;
    read(TESTER_GPIO + gpio, &data, sizeof(data));
    return data;
}

void MbedTester::gpio_write(LogicalPin gpio, int value, bool drive)
{
    if (gpio >= LogicalPinCount) {
        _platform_handlers.error("Invalid pin for gpio_write");
        return;
    }
    uint8_t data = 0;
    data |= value ? (1 << 0) : 0;
    data |= drive ? (1 << 1) : 0;
    write(TESTER_GPIO + gpio, &data, sizeof(data));
}

void MbedTester::io_metrics_start()
{
    uint8_t data = TESTER_IO_METRICS_CTRL_RESET_BIT;
    write(TESTER_IO_METRICS_CTRL, &data, sizeof(data));

    data = TESTER_IO_METRICS_CTRL_ACTIVE_BIT;
    write(TESTER_IO_METRICS_CTRL, &data, sizeof(data));
}

void MbedTester::io_metrics_stop()
{
    uint8_t data = 0;
    write(TESTER_IO_METRICS_CTRL, &data, sizeof(data));
}

void MbedTester::io_metrics_continue()
{
    uint8_t data = TESTER_IO_METRICS_CTRL_ACTIVE_BIT;
    write(TESTER_IO_METRICS_CTRL, &data, sizeof(data));
}

uint32_t MbedTester::io_metrics_min_pulse_low(LogicalPin pin)
{
    if (pin >= LogicalPinCount) {
        _platform_handlers.error("Invalid pin for io_metrics");
        return 0;
    }

    uint32_t data = 0;
    read(TESTER_IO_METRICS_MIN_PULSE_LOW(pin), (uint8_t *)&data, sizeof(data));
    return data;
}

uint32_t MbedTester::io_metrics_min_pulse_high(LogicalPin pin)
{
    if (pin >= LogicalPinCount) {
        _platform_handlers.error("Invalid pin for io_metrics");
        return 0;
    }

    uint32_t data = 0;
    read(TESTER_IO_METRICS_MIN_PULSE_HIGH(pin), (uint8_t *)&data, sizeof(data));
    return data;
}

uint32_t MbedTester::io_metrics_max_pulse_low(LogicalPin pin)
{
    if (pin >= LogicalPinCount) {
        _platform_handlers.error("Invalid pin for io_metrics");
        return 0;
    }

    uint32_t data = 0;
    read(TESTER_IO_METRICS_MAX_PULSE_LOW(pin), (uint8_t *)&data, sizeof(data));
    return data;
}

uint32_t MbedTester::io_metrics_max_pulse_high(LogicalPin pin)
{
    if (pin >= LogicalPinCount) {
        _platform_handlers.error("Invalid pin for io_metrics");
        return 0;
    }

    uint32_t data = 0;
    read(TESTER_IO_METRICS_MAX_PULSE_HIGH(pin), (uint8_t *)&data, sizeof(data));
    return data;
}

uint32_t MbedTester::io_metrics_rising_edges(LogicalPin pin)
{
    if (pin >= LogicalPinCount) {
        _platform_handlers.error("Invalid pin for io_metrics");
        return 0;
    }

    uint32_t data = 0;
    read(TESTER_IO_METRICS_RISING_EDGES(pin), (uint8_t *)&data, sizeof(data));
    return data;
}

uint32_t MbedTester::io_metrics_falling_edges(LogicalPin pin)
{
    if (pin >= LogicalPinCount) {
        _platform_handlers.error("Invalid pin for io_metrics");
        return 0;
    }

    uint32_t data = 0;
    read(TESTER_IO_METRICS_FALLING_EDGES(pin), (uint8_t *)&data, sizeof(data));
    return data;
}

bool MbedTester::sys_pin_read(SystemPin pin)
{

    if (pin >= SystemPinCount) {
        _platform_handlers.error("Invalid pin for gpio_read");
        return 0;
    }
    uint8_t data = 0;
    read(TESTER_SYS_IO + pin, &data, sizeof(data));
    return data;
}

void MbedTester::sys_pin_write(SystemPin pin, int value, bool drive)
{
    if (pin >= SystemPinCount) {
        _platform_handlers.error("Invalid pin for gpio_write");
        return;
    }
    uint8_t data = 0;
    data |= value ? (1 << 0) : 0;
    data |= drive ? (1 << 1) : 0;
    write(TESTER_SYS_IO + pin, &data, sizeof(data));
}

void MbedTester::sys_pin_mode_disabled()
{
    const uint32_t base = LogicalPinTotal;

    pin_map_index(MbedTester::physical_nc, (LogicalPin)(base + 0));
    pin_map_index(MbedTester::physical_nc, (LogicalPin)(base + 1));
    pin_map_index(MbedTester::physical_nc, (LogicalPin)(base + 2));
    pin_map_index(MbedTester::physical_nc, (LogicalPin)(base + 3));

    uint8_t mode = TESTER_SYS_IO_MODE_DISABLED;
    write(TESTER_SYS_IO_MODE, &mode, sizeof(mode));
}

void MbedTester::sys_pin_mode_spi_serial_flash(PhysicalIndex sdo,
                                               PhysicalIndex sdi,
                                               PhysicalIndex clk,
                                               PhysicalIndex csel)
{
    const uint32_t base = LogicalPinTotal;

    pin_map_index(sdo, (LogicalPin)(base + 0));
    pin_map_index(sdi, (LogicalPin)(base + 1));
    pin_map_index(clk, (LogicalPin)(base + 2));
    pin_map_index(csel, (LogicalPin)(base + 3));

    uint8_t mode = TESTER_SYS_IO_MODE_SPI_SERIAL_FLASH;
    write(TESTER_SYS_IO_MODE, &mode, sizeof(mode));
}

void MbedTester::sys_pin_mode_i2c_io_expander(
    int index, PhysicalIndex sda_in, PhysicalIndex sda_val, PhysicalIndex scl_in, PhysicalIndex scl_val)
{
    const uint32_t base = LogicalPinTotal;

    pin_map_index(sda_in, (LogicalPin)(base + 0));
    pin_map_index(sda_val, (LogicalPin)(base + 1));
    pin_map_index(scl_in, (LogicalPin)(base + 2));
    pin_map_index(scl_val, (LogicalPin)(base + 3));

    uint8_t mode = 0;
    if (index == 0) {
        mode = TESTER_SYS_IO_MODE_I2C_IO_EXPANDER0;
    } else if (index == 1) {
        mode = TESTER_SYS_IO_MODE_I2C_IO_EXPANDER1;
    } else if (index == 2) {
        mode = TESTER_SYS_IO_MODE_I2C_IO_EXPANDER2;
    } else {
        _platform_handlers.error("Invalid index for sys_pin_mode_i2c_io_expander");
    }

    write(TESTER_SYS_IO_MODE, &mode, sizeof(mode));
}

void MbedTester::pin_map_index(PhysicalIndex physical_index, LogicalPin logical)
{
    uint8_t remap;
    if ((physical_index >= MbedTesterConst::physical_pins) && (physical_index != MbedTester::physical_nc)) {
        _platform_handlers.error("Invalid physical pin index %i", physical_index);
        return;
    }
    if (logical >= sizeof(_mapping) / sizeof(_mapping[0])) {
        _platform_handlers.error("Invalid logical pin %i", logical);
        return;
    }

    // Unmap the previous pin if it had been mapped
    if (_mapping[logical] < MbedTesterConst::physical_pins) {
        remap = MbedTester::physical_nc;
        write(TESTER_REMAP + _mapping[logical], &remap, sizeof(remap));
    }
    _mapping[logical] = physical_index;

    // Remap physical pin if it is not physical_nc
    if (physical_index < MbedTesterConst::physical_pins) {
        remap = logical;
        write(TESTER_REMAP + physical_index, &remap, sizeof(remap));
    }
    // Remap logical pin
    remap = physical_index;
    write(TESTER_REMAP + MbedTesterConst::physical_pins + logical, &remap, sizeof(remap));
}

void MbedTester::write(uint32_t addr, const uint8_t *data, uint32_t size)
{
    _initiate_control_transfer(addr, true, size);

    for (uint32_t i = 0; i < size; i++) {
        _bit_bang_spi(data[i]);
    }

    *_sclk = 0;
}

void MbedTester::read(uint32_t addr, uint8_t *data, uint32_t size)
{
    _initiate_control_transfer(addr, false, size);

    for (uint32_t i = 0; i < size; i++) {
        data[i] = _bit_bang_spi(0);
    }

    *_sclk = 0;
}

bool MbedTester::verify_control_channel()
{
    uint8_t buf[4];
    read(TESTER_CONTROL, buf, sizeof(buf));
    return memcmp(buf, "mbed", sizeof(buf)) == 0;
}

void MbedTester::_setup_control_pins(mdh_gpio_t *clk, mdh_gpio_t *sdo, mdh_gpio_t *sdi, mdh_gpio_t *aux)
{
    mdh_gpio_set_direction(sdo, MDH_GPIO_DIRECTION_OUT);
    mdh_gpio_set_mode(sdo, MDH_GPIO_MODE_PULL_NONE);
    mdh_gpio_write(sdo, 0UL);

    mdh_gpio_set_direction(clk, MDH_GPIO_DIRECTION_OUT);
    mdh_gpio_set_mode(clk, MDH_GPIO_MODE_PULL_NONE);
    mdh_gpio_write(clk, 0UL);

    _sclk = new mbed::DigitalInOut(clk);
    _sdo = new mbed::DigitalInOut(sdo);
    _sdi = new mbed::DigitalInOut(sdi);
    _aux = new mbed::DigitalInOut(aux);
}

void MbedTester::_free_control_pins()
{
    if (_sclk) {
        _sclk->input();
        delete _sclk;
    }
    _sclk = NULL;
    _sclk_index = MbedTester::physical_nc;

    if (_sdo) {
        _sdo->input();
        delete _sdo;
    }
    _sdo = NULL;
    _sdo_index = MbedTester::physical_nc;

    if (_sdi) {
        _sdi->input();
        delete _sdi;
    }
    _sdi = NULL;
    _sdi_index = MbedTester::physical_nc;

    if (_aux) {
        _aux->input();
        delete _aux;
    }
    _aux = NULL;
    _aux_index = MbedTester::physical_nc;
}

void MbedTester::_reset()
{
    for (uint32_t i = 0; i < sizeof(_mapping) / sizeof(_mapping[0]); i++) {
        _mapping[i] = MbedTester::physical_nc;
    }
}
