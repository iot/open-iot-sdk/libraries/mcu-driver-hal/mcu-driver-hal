/*
 * Copyright (c) 2021-2022 Arm Limited. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef GREENTEA_CUSTOM_IO_H_
#define GREENTEA_CUSTOM_IO_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hal/serial_api.h"

void greentea_init_custom_io(mdh_serial_t *stdio);

#ifdef __cplusplus
}
#endif

#endif // GREENTEA_CUSTOM_IO_H_
