/*
 * Copyright (c) 2021-2023 Arm Limited. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "greentea-client/test_io.h"
#include "greentea-custom_io/custom_io.h"

static mdh_serial_t *stdio_uart = NULL;

void greentea_init_custom_io(mdh_serial_t *stdio)
{
    stdio_uart = stdio;
    mdh_serial_set_baud(stdio_uart, 115200);
}

int greentea_getc(void)
{
    return (int)mdh_serial_get_data(stdio_uart);
}

void greentea_putc(int c)
{
    mdh_serial_put_data(stdio_uart, c);
}

void greentea_write_string(const char *str)
{
    while (*str != '\0') {
        mdh_serial_put_data(stdio_uart, *str++);
    }
}
