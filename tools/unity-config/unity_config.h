/* Copyright (c) 2015-2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef UNITY_CONFIG_H
#define UNITY_CONFIG_H

// What you see here is only an intermediate step in the process of replacing a
// local copy of Unity test framework with the current upstream version.
// Initially, we only want to switch to upstream code base without any other
// changes. The following patches will remove utest test harness and make use of
// Unity's test running feature.

// Do not use setjmp/longjmp to handle failures. Rely on utest test harness
// instead.
#define UNITY_EXCLUDE_SETJMP_H

// Do not define a default test runner. Rely on utest test harness instead.
#define UNITY_SKIP_DEFAULT_RUNNER

// Do not use stdio. Rely on utest test harness instead.
#include "utest/unity_handler.h"
#define UNITY_OUTPUT_CHAR(a) utest_safe_putc(a)

// Match line endings used by the utest test harness.
#define UNITY_PRINT_EOL()        \
    do {                         \
        UNITY_OUTPUT_CHAR('\r'); \
        UNITY_OUTPUT_CHAR('\n'); \
    } while (0)

// Until now, when the local copy of Unity was used, the failure handling macros
// (UNITY_FAIL_AND_BAIL and UNITY_IGNORE_AND_BAIL) were overriden to call utest
// test harness inherited from Mbed OS. This is no longer possible. However,
// looking into Unity's code, one can abuse a different macro that's allowed to
// be user-defined.
//
// Define UNITY_OUTPUT_FLUSH to inspect Unity's internal state and call utest
// failure handlers if needed.
#define UNITY_OUTPUT_FLUSH()                        \
    do {                                            \
        if (Unity.CurrentTestFailed == 1) {         \
            UNITY_PRINT_EOL();                      \
            utest_unity_assert_failure();           \
        } else if (Unity.CurrentTestIgnored == 1) { \
            UNITY_PRINT_EOL();                      \
            utest_unity_ignore_failure();           \
        }                                           \
    } while (0)

#endif // UNITY_CONFIG_H
